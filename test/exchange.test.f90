!
! Test exchange calculation conforms to expectations
!
PROGRAM Exchange_Test
  USE Merrill
  IMPLICIT NONE

  CHARACTER(len=1024) :: program_name
  CHARACTER(len=1024) :: sphere_mesh_filename

  CHARACTER(len=1024) :: argument

  LOGICAL :: test_passed

  REAL(KIND=DP) :: sphere_radius


  CALL GET_COMMAND_ARGUMENT(0, program_name)

  ! Parse command line arguments
  IF(COMMAND_ARGUMENT_COUNT() .NE. 1) THEN
    WRITE(*,*) "USAGE: ", TRIM(program_name), " SPHERE_MESH_FILE"
    STOP 1
  END IF

  ! SPHERE_MESH_FILE
  CALL GET_COMMAND_ARGUMENT(1, argument)
  sphere_mesh_filename = TRIM(argument)


  CALL InitializeMerrill()

  CALL Magnetite(20.0d0)

  CALL ReadPatranMesh(TRIM(sphere_mesh_filename))

  ! Find sphere radius
  sphere_radius = MAXVAL(NORM2(VCL(:,1:3), DIM=2))


  !
  ! Run single-phase sphere anisotropy tests
  !

  Aex = 1.2
  CALL TestExchange()

  !
  ! Run multi-phase sphere anisotropy tests
  !

  CALL TestMultiphaseExchange()
CONTAINS

  SUBROUTINE TestExchange()
    INTEGER :: el
    REAL(KIND=DP), ALLOCATABLE :: expected_h(:,:)
    REAL(KIND=DP) :: expected_e
    REAL(KIND=DP) :: h_i(3), solid_weight, vv
    REAL(KIND=DP), ALLOCATABLE :: d1m(:,:,:)
    REAL(KIND=DP) :: rate
    INTEGER :: sd

    INTEGER :: i,j

    ALLOCATE(expected_h(NNODE,3))
    ALLOCATE(d1m(NNODE,3,3))

    test_passed = .TRUE.

    rate = 1/sphere_radius * pi * 0.1

    d1m = 0
    ! Slowly varying magnetization
    DO i=1,NNODE
      m(i,:) = (/ COS(rate*VCL(i,1)), SIN(rate*VCL(i,1)), 0.0_DP /)
      ! d1m(nnode, mi, d_j)
      d1m(i,:,:) = rate*RESHAPE((/ &
        -SIN(rate*VCL(i,1)), COS(rate*VCL(i,1)), 0.0_DP, & ! dm/dx
        0.0_DP, 0.0_DP, 0.0_DP, & ! dm/dy
        0.0_DP, 0.0_DP, 0.0_DP &  ! dm/dz
      /), (/ 3, 3 /))
      IF(NORM2(m(i,:)) .GT. 0) THEN
        m(i,:) = m(i,:) / NORM2(m(i,:))
      ELSE
        m(i,:) = 1/SQRT(3.0_DP)
      END IF
    END DO

    CALL CalcDemagEx()

    expected_h = 0
    expected_e = 0

    DO el=1,NTRI
      DO i=1,4
        solid_weight = TetSolid(el,i) / solid(TIL(el,i))
        vv = vbox(TIL(el,i))
        sd = TetSubDomains(el)

        expected_h(TIL(el,i),:) = expected_h(TIL(el,i),:) &
          + 2 * Aex(sd) * Ls * (1/(36.0*vol(el))) * (/ &
              ( &
                  SUM(b(el,:)*m(TIL(el,1:4),1))*b(el,i) &
                + SUM(c(el,:)*m(TIL(el,1:4),1))*c(el,i) &
                + SUM(d(el,:)*m(TIL(el,1:4),1))*d(el,i) &
              ), ( &
                  SUM(b(el,:)*m(TIL(el,1:4),2))*b(el,i) &
                + SUM(c(el,:)*m(TIL(el,1:4),2))*c(el,i) &
                + SUM(d(el,:)*m(TIL(el,1:4),2))*d(el,i) &
              ), ( &
                  SUM(b(el,:)*m(TIL(el,1:4),3))*b(el,i) &
                + SUM(c(el,:)*m(TIL(el,1:4),3))*c(el,i) &
                + SUM(d(el,:)*m(TIL(el,1:4),3))*d(el,i) &
              ) &
            /)

        expected_e = expected_e &
          + Aex(sd) * SUM(d1m(TIL(el,i),:,:)**2) * solid_weight * Ls * vv

      END DO
    END DO
    expected_e = expected_e

    DO i=1,NNODE
      h_i = ABS(expected_h(i,:) - hexch(i,:))
      IF(NORM2(h_i) .GT. NORM2(expected_h)*1e-3) THEN
        WRITE(*,*) "TestExchange failed for h: i=", i
        WRITE(*,*) "Expected: ", expected_h(i,:)
        WRITE(*,*) "Found:    ", hexch(i,:)
        WRITE(*,*) "e/h:      ", expected_h(i,:)/hexch(i,:)
        WRITE(*,*) "diff:     ", h_i
        WRITE(*,*) "tol:      ", 1
        test_passed = .FALSE.
      END IF
    END DO

    IF(ABS(ExchangeE - expected_e) .GT. ABS(expected_e)*1e-3) THEN
      WRITE(*,*) "TestExchange failed for e"
      WRITE(*,*) "Expected: ", expected_e
      WRITE(*,*) "Found:    ", ExchangeE
      WRITE(*,*) "e/h:      ", expected_e/ExchangeE
      test_passed = .FALSE.
    END IF

    IF(test_passed) THEN
      WRITE(*,*) "Test Passed"
    ELSE
      WRITE(*,*) "Test Failed!"
      STOP 1
    END IF

  END SUBROUTINE TestExchange

  SUBROUTINE TestMultiphaseExchange()
    INTEGER :: i

    NMaterials = 2
    TetSubDomains = 1
    DO i=1,NTRI
      IF(MOD(i,3) .EQ. 0) TetSubDomains(i) = 2
    END DO

    CALL BuildTetrahedralMeshData()
    CALL BuildFiniteElement()
    CALL BuildMaterialParameters()
    CALL BuildEnergyCalculator()

    Aex = (/ 1.2, 1.2 /)
    CALL TestExchange()

    Aex = (/ 1.2, 2.3 /)
    CALL TestExchange()

    Aex = (/ 3.4, 0.0 /)
    CALL TestExchange()
  END SUBROUTINE TestMultiphaseExchange

END PROGRAM Exchange_Test
