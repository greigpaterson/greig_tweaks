MODULE slatec_dlapqc
CONTAINS
!DECK DLAPQC
      SUBROUTINE DLAPQC (LUN, KPRINT, IPASS)
        USE slatec_dsiccg
        USE slatec_d1mach
        USE slatec_drmgen
        USE slatec_ds2y
        USE slatec_dfill
        USE slatec_dcpplt
        USE slatec_duterr
!***BEGIN PROLOGUE  DLAPQC
!***PURPOSE  Quick check for testing Sparse Linear Algebra Package
!            (SLAP) Version 2.0.2.
!***LIBRARY   SLATEC (SLAP)
!***CATEGORY  D2A4, D2B4
!***TYPE      DOUBLE PRECISION (SLAPQC-S, DLAPQC-D)
!***KEYWORDS  QUICK CHECK, SLAP
!***AUTHOR  Mark K. Seager (LLNL)
!             seager@llnl.gov
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550
!             (510) 423-3141
!***DESCRIPTION
!
! *Arguments:
!     KPRINT = 0  Quick checks - No printing.
!                 Driver       - Short pass or fail message printed.
!              1  Quick checks - No message printed for passed tests,
!                                short message printed for failed tests.
!                 Driver       - Short pass or fail message printed.
!              2  Quick checks - Print short message for passed tests,
!                                fuller information for failed tests.
!                 Driver       - Pass or fail message printed.
!              3  Quick checks - Print complete quick check results.
!                 Driver       - Pass or fail message printed.
!              4  Quick checks - Print complete quick check results.
!                                Prints matrices, etc.  Very verbose!!
!                                                       --------------
!                 Driver       - Pass or fail message printed.
!
! *Description:
!         This is a SLATEC Quick Check program to test the *SLAP*
!         Version 2.0.2 package.  It generates a "random" matrix (See
!         DRMGEN) and then runs all the various methods with all the
!         various preconditioners and all the various stop tests.
!
!         It is assumed that the test is being run interactively and
!         that STDIN (STANDARD INPUT) is Fortran I/O unit I1MACH(1)
!         and STDOUT (STANDARD OUTPUT) is unit I1MACH(2).
!
!         *************************************************************
!         **** WARNING !!! WARNING !!! WARNING !!! WARNING !!! WARNING
!         *************************************************************
!         **** THIS PROGRAM WILL NOT FUNCTION PROPERLY IF THE FORTRAN
!         **** I/O UNITS I1MACH(1) and I1MACH(2) are not connected
!         **** to the program for I/O.
!         *************************************************************
!
!***REFERENCES  (NONE)
!***ROUTINES CALLED  D1MACH, DCPPLT, DFILL, DRMGEN, DS2Y, DSDBCG, DSDCG,
!                    DSDCGN, DSDCGS, DSDGMR, DSDOMN, DSGS, DSICCG,
!                    DSILUR, DSJAC, DSLUBC, DSLUCN, DSLUCS, DSLUGM,
!                    DSLUOM, DUTERR, XERMAX, XSETF, XSETUN
!***COMMON BLOCKS    DSLBLK
!***REVISION HISTORY  (YYMMDD)
!   880601  DATE WRITTEN
!   881213  Revised to meet the new SLATEC prologue standards.
!   890920  Modified to reduce single/double differences and to meet
!           SLATEC standards, as requested at July 1989 CML Meeting.
!   891003  Reduced MAXN to a more reasonable size for quick check.
!   920401  Made routine a SUBROUTINE and made necessary changes to
!           interface with a SLATEC quick check driver.  (WRB)
!   920407  COMMON BLOCK renamed DSLBLK.  (WRB)
!   920511  Added complete declaration section.  (WRB)
!   920602  Eliminated unnecessary variables IOUT and ISTDO and made
!           various cosmetic changes.  (FNF)
!   920602  Reduced problem size for a shorter-running test and
!           corrected lower limit in "DO 80" statement.  (FNF)
!   921021  Changed E's to 1P,D's in output formats.  (FNF)
!***END PROLOGUE  DLAPQC
!
!     The problem size, MAXN, should be large enough that the
!     iterative methods do 10-15 iterations, just to be sure that
!     the truncated methods run to the end of their ropes and enter
!     their error recovery mode.  Thus, for a more thorough test change
!     the following PARAMETER statement to:
!     PARAMETER (MAXN=69, MXNELT=5000, MAXIW=5000, MAXRW=5000)
!
!     .. Parameters ..
      INTEGER MAXN, MXNELT, MAXIW, MAXRW
      PARAMETER (MAXN=25, MXNELT=500, MAXIW=1000, MAXRW=1000)
!     .. Scalar Arguments ..
      INTEGER IPASS, KPRINT, LUN
!     .. Arrays in Common ..
      DOUBLE PRECISION SOLN(MAXN)
!     .. Local Scalars ..
      DOUBLE PRECISION DENS, ERR, FACTOR, TOL
      INTEGER IERR, ISYM, ITER, ITMAX, ITOL, ITOLGM, IUNIT, K, KASE, &
              LENIW, LENW, N, NELT, NELTMX, NFAIL, NMAX, NSAVE
!     .. Local Arrays ..
      DOUBLE PRECISION A(MXNELT), F(MAXN), RWORK(MAXRW), XITER(MAXN)
      INTEGER IA(MXNELT), IWORK(MAXIW), JA(MXNELT)
!     .. Intrinsic Functions ..
      INTRINSIC MAX, REAL
!     .. Common blocks ..
      COMMON /DSLBLK/ SOLN
!
!     The following lines are for the braindamaged Sun FPE handler.
!
!$$$      integer oldmode, fpmode
!***FIRST EXECUTABLE STATEMENT  DLAPQC
!$$$      oldmode = fpmode( 62464 )
!
!     Maximum problem sizes.
!
      NELTMX = MXNELT
      NMAX   = MAXN
      LENIW  = MAXIW
      LENW   = MAXRW
!
!     Set some input data.
!
      N      = NMAX
      ITMAX  = N
      FACTOR = 1.2D0
!
!     Set to print intermediate results if KPRINT.GE.3.
!
      IF( KPRINT.LT.3 ) THEN
         IUNIT = 0
      ELSE
         IUNIT = LUN
      ENDIF
!
!     Set the Error tolerance to depend on the machine epsilon.
!
      TOL = MAX(1.0D3*D1MACH(3),1.0D-6)
      NFAIL = 0
!
!     Test routines using various convergence criteria.
!
      DO 80 KASE = 1, 3
         IF(KASE .EQ. 1 .OR. KASE .EQ. 2) ITOL = KASE
         IF(KASE .EQ. 3) ITOL = 11
!
!         Test routines using nonsymmetric (ISYM=0) and symmetric
!         storage (ISYM=1).  For ISYM=0 a really non-symmetric matrix
!         is generated.  The amount of non-symmetry is controlled by
!         user.
!
         DO 70 ISYM = 0, 1
            IF( KPRINT.GE.2 )  WRITE (LUN, 1050)  N, KASE, ISYM
!
!         Set up a random matrix.
!
            CALL DRMGEN( NELTMX, FACTOR, IERR, N, NELT, &
                 ISYM, IA, JA, A, F, SOLN, RWORK, IWORK, IWORK(N+1) )
            IF( IERR.NE.0 ) THEN
               WRITE(LUN,990) IERR
               NFAIL = NFAIL + 1
               GO TO 70
            ENDIF
            IF( ISYM.EQ.0 ) THEN
               DENS = REAL(NELT)/(N*N)
            ELSE
               DENS = REAL(2*NELT)/(N*N)
            ENDIF
            IF( KPRINT.GE.2 ) THEN
              WRITE(LUN,1020) N, NELT, DENS
              WRITE(LUN,1030) TOL
            ENDIF
!
!         Convert to the SLAP-Column format and
!         write out matrix in SLAP-Column format, if desired.
!
            CALL DS2Y( N, NELT, IA, JA, A, ISYM )
            IF( KPRINT.GE.4 ) THEN
               WRITE(LUN,1040) (K,IA(K),JA(K),A(K),K=1,NELT)
               CALL DCPPLT( N, NELT, IA, JA, A, ISYM, LUN )
            ENDIF
!
!**********************************************************************
!                    BEGINNING OF SLAP QUICK TESTS
!**********************************************************************
!
!         * * * * * *   DSJAC   * * * * * *
!
            !IF( KPRINT.GE.3 ) THEN
            !  WRITE(LUN,1000) 'DSJAC ', ITOL, ISYM
            !ENDIF
            !CALL DFILL( N, XITER, 0.0D0 )
!
            !CALL DSJAC(N, F, XITER, NELT, IA, JA, A, ISYM, &
            !     ITOL, TOL, 2*ITMAX, ITER, ERR, IERR, IUNIT, &
            !     RWORK, LENW, IWORK, LENIW )
!
            !CALL DUTERR( 'DSJAC ',IERR,KPRINT,NFAIL,LUN,ITER,ERR )
!
!         * * * * *  DSGS  * * * * *
!
            !IF( KPRINT.GE.3 ) THEN
            !  WRITE(LUN,1000) 'DSGS  ',ITOL,ISYM
            !ENDIF
            !CALL DFILL( N, XITER, 0.0D0 )
!
            !CALL DSGS(N, F, XITER, NELT, IA, JA, A, ISYM, &
            !     ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
            !     RWORK, LENW, IWORK, LENIW )
!
            !CALL DUTERR( 'DSGS  ',IERR,KPRINT,NFAIL,LUN,ITER,ERR )
!
!         * * * * * *   DSILUR   * * * * * *
!
            !IF( KPRINT.GE.3 ) THEN
            !  WRITE(LUN,1000) 'DSILUR',ITOL,ISYM
            !ENDIF
            !CALL DFILL( N, XITER, 0.0D0 )
!
            !CALL DSILUR(N, F, XITER, NELT, IA, JA, A, ISYM, &
            !     ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
            !     RWORK, LENW, IWORK, LENIW )
!
            !CALL DUTERR( 'DSILUR',IERR,KPRINT,NFAIL,LUN,ITER,ERR )
!
!         * * * * * *   DSDCG    * * * * * *
!
            !IF( ISYM.EQ.1 ) THEN
            !   IF( KPRINT.GE.3 ) THEN
            !      WRITE(LUN,1000) 'DSDCG',ITOL,ISYM
            !   ENDIF
            !   CALL DFILL( N, XITER, 0.0D0 )
!
            !   CALL DSDCG(N, F, XITER, NELT, IA, JA, A, ISYM, &
            !        ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
            !        RWORK, LENW, IWORK, LENIW )
!
            !   CALL DUTERR( 'DSDCG ',IERR,KPRINT,NFAIL,LUN,ITER,ERR )
            !ENDIF
!
!         * * * * * *    DSICCG    * * * * * *
!
            IF( ISYM.EQ.1 ) THEN
               IF( KPRINT.GE.3 ) THEN
                  WRITE(LUN,1000) 'DSICCG',ITOL,ISYM
               ENDIF
               CALL DFILL( N, XITER, 0.0D0 )
!
               CALL DSICCG(N, F, XITER, NELT, IA, JA, A, ISYM, &
                    ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, RWORK, &
                    LENW, IWORK, LENIW )
!
               CALL DUTERR( 'DSICCG',IERR,KPRINT,NFAIL,LUN,ITER,ERR )
            ENDIF
!
!         * * * * * *    DSDCGN   * * * * * *
!
            !IF( KPRINT.GE.3 ) THEN
            !   WRITE(LUN,1000) 'DSDCGN',ITOL,ISYM
            !ENDIF
            !CALL DFILL( N, XITER, 0.0D0 )
!
            !CALL DSDCGN(N, F, XITER, NELT, IA, JA, A, ISYM, ITOL, &
            !     TOL, ITMAX, ITER, ERR, IERR, IUNIT, RWORK, LENW, &
            !     IWORK, LENIW )
!
            !CALL DUTERR( 'DSDCGN',IERR,KPRINT,NFAIL,LUN,ITER,ERR )
!
!         * * * * * *   DSLUCN   * * * * * *
!
            !IF( KPRINT.GE.3 ) THEN
            !   WRITE(LUN,1000) 'DSLUCN',ITOL,ISYM
            !ENDIF
            !CALL DFILL( N, XITER, 0.0D0 )
!
            !CALL DSLUCN(N, F, XITER, NELT, IA, JA, A, ISYM, ITOL, &
            !     TOL, ITMAX, ITER, ERR, IERR, IUNIT, RWORK, LENW, &
            !     IWORK, LENIW )
!
            !CALL DUTERR( 'DSLUCN',IERR,KPRINT,NFAIL,LUN,ITER,ERR )
!
!         * * * * * *    DSDBCG   * * * * * *
!
            !IF( KPRINT.GE.3 ) THEN
            !   WRITE(LUN,1000) 'DSDBCG',ITOL,ISYM
            !ENDIF
            !CALL DFILL( N, XITER, 0.0D0 )
!
            !CALL DSDBCG(N, F, XITER, NELT, IA, JA, A, ISYM, ITOL, &
            !     TOL, ITMAX, ITER, ERR, IERR, IUNIT, RWORK, LENW, &
            !     IWORK, LENIW )
!
            !CALL DUTERR( 'DSDBCG',IERR,KPRINT,NFAIL,LUN,ITER,ERR )
!
!         * * * * * *   DSLUBC   * * * * * *
!
            !IF( KPRINT.GE.3 ) THEN
            !   WRITE(LUN,1000) 'DSLUBC',ITOL,ISYM
            !ENDIF
            !CALL DFILL( N, XITER, 0.0D0 )
!
            !CALL DSLUBC(N, F, XITER, NELT, IA, JA, A, ISYM, &
            !     ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
            !     RWORK, LENW, IWORK, LENIW )
!
            !CALL DUTERR( 'DSLUBC',IERR,KPRINT,NFAIL,LUN,ITER,ERR )
!
!         * * * * * *    DSDCGS   * * * * * *
!
            !IF( KPRINT.GE.3 ) THEN
            !   WRITE(LUN,1000) 'DSDCGS',ITOL,ISYM
            !ENDIF
            !CALL DFILL( N, XITER, 0.0D0 )
!
            !CALL DSDCGS(N, F, XITER, NELT, IA, JA, A, ISYM, ITOL, &
            !     TOL, ITMAX, ITER, ERR, IERR, IUNIT, RWORK, LENW, &
            !     IWORK, LENIW )
!
            !CALL DUTERR( 'DSDCGS',IERR,KPRINT,NFAIL,LUN,ITER,ERR )
!
!         * * * * * *   DSLUCS   * * * * * *
!
            !IF( KPRINT.GE.3 ) THEN
            !   WRITE(LUN,1000) 'DSLUCS',ITOL,ISYM
            !ENDIF
            !CALL DFILL( N, XITER, 0.0D0 )
!
            !CALL DSLUCS(N, F, XITER, NELT, IA, JA, A, ISYM, &
            !     ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
            !     RWORK, LENW, IWORK, LENIW )
!
            !CALL DUTERR( 'DSLUCS',IERR,KPRINT,NFAIL,LUN,ITER,ERR )
!
!         * * * * * *    DSDOMN   * * * * * *
!
!VD$ NOVECTOR
            !DO 30 NSAVE = 0, 3
            !   IF( KPRINT.GE.3 ) THEN
            !      WRITE(LUN,1010) 'DSDOMN',ITOL, ISYM, NSAVE
            !   ENDIF
            !   CALL DFILL( N, XITER, 0.0D0 )
!
            !   CALL DSDOMN(N, F, XITER, NELT, IA, JA, A, &
            !        ISYM, NSAVE, ITOL, TOL, ITMAX, ITER, ERR, IERR, &
            !        IUNIT, RWORK, LENW, IWORK, LENIW )
!
            !   CALL DUTERR( 'DSDOMN',IERR,KPRINT,NFAIL,LUN,ITER,ERR )
 !30         CONTINUE
!
!         * * * * * *   DSLUOM   * * * * * *
!
!VD$ NOVECTOR
            !DO 40 NSAVE=0,3
            !   IF( KPRINT.GE.3 ) THEN
            !      WRITE(LUN,1010) 'DSLUOM',ITOL, ISYM, NSAVE
            !   ENDIF
            !   CALL DFILL( N, XITER, 0.0D0 )
!
            !   CALL DSLUOM(N, F, XITER, NELT, IA, JA, A, &
            !        ISYM, NSAVE, ITOL, TOL, ITMAX, ITER, ERR, IERR, &
            !        IUNIT, RWORK, LENW, IWORK, LENIW )
!
            !   CALL DUTERR( 'DSLUOM',IERR,KPRINT,NFAIL,LUN,ITER,ERR )
 !40         CONTINUE
!
!         * * * * * *   DSDGMR   * * * * * *
!
!VD$ NOVECTOR
            !DO 50 NSAVE = 5, 12
            !   IF( KPRINT.GE.3 ) THEN
            !      WRITE(LUN,1010) 'DSDGMR',ITOL, ISYM, NSAVE
            !   ENDIF
            !   CALL DFILL( N, XITER, 0.0D0 )
            !   ITOLGM = 0
!
            !   CALL DSDGMR(N, F, XITER, NELT, IA, JA, A, &
            !        ISYM, NSAVE, ITOLGM, TOL, ITMAX, ITER, ERR, IERR, &
            !        IUNIT, RWORK, LENW, IWORK, LENIW )
!
            !   CALL DUTERR( 'DSDGMR',IERR,KPRINT,NFAIL,LUN,ITER,ERR )
 !50         CONTINUE
!
!         * * * * * *   DSLUGM   * * * * * *
!
!VD$ NOVECTOR
            !DO 60 NSAVE = 5, 12
            !   IF( KPRINT.GE.3 ) THEN
            !      WRITE(LUN,1010) 'DSLUGM',ITOL, ISYM, NSAVE
            !   ENDIF
            !   CALL DFILL( N, XITER, 0.0D0 )
!
            !   CALL DSLUGM(N, F, XITER, NELT, IA, JA, A, &
            !        ISYM, NSAVE, ITOL, TOL, ITMAX, ITER, ERR, IERR, &
            !        IUNIT, RWORK, LENW, IWORK, LENIW )
!
            !   CALL DUTERR( 'DSLUGM',IERR,KPRINT,NFAIL,LUN,ITER,ERR )
 !60         CONTINUE
 70      CONTINUE
 80   CONTINUE
!
      IF (NFAIL .EQ. 0) THEN
         IPASS = 1
         IF( KPRINT .GE. 2 )  WRITE (LUN, 5001)
      ELSE
         IPASS = 0
         IF( KPRINT .GE. 2 )  WRITE (LUN, 5002) NFAIL
      ENDIF
!
      RETURN
!
  990 FORMAT(/1X, 'DLAPQC -- Fatal error ', I1, ' generating ', &
             '*RANDOM* Matrix.')
 1000 FORMAT(/1X,A6,' : ITOL = ',I2,'   ISYM = ',I1)
 1010 FORMAT(/1X,A6,' : ITOL = ',I2,'   ISYM = ',I1,' NSAVE = ',I2)
 1020 FORMAT(/'                * RANDOM Matrix of size',I5,'*' &
           /'                ', &
           'Number of non-zeros & Density = ', I5,1P,D16.7)
 1030 FORMAT('                Error tolerance = ',1P,D16.7)
 1040 FORMAT(/'  ***** SLAP Column Matrix *****'/ &
              ' Indx   ia   ja     a'/(1X,I4,1X,I4,1X,I4,1X,1P,D16.7))
 1050 FORMAT('1'/' Running tests with  N =',I3,',  KASE =',I2, &
                                ',  ISYM =',I2)
 5001 FORMAT('--------- All double precision SLAP tests passed ', &
             '---------')
 5002 FORMAT('*********',I3,' double precision SLAP tests failed ', &
             '*********')
      END
END MODULE slatec_dlapqc
