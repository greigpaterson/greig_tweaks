MODULE slatec_blachk
CONTAINS
!DECK BLACHK
      SUBROUTINE BLACHK (LUN, KPRINT, IPASS)
        USE slatec_check1
        USE slatec_check2
!***BEGIN PROLOGUE  BLACHK
!***PURPOSE  Quick check for Basic Linear Algebra Subprograms.
!***LIBRARY   SLATEC
!***KEYWORDS  QUICK CHECK
!***AUTHOR  Lawson, C. L., (JPL)
!***DESCRIPTION
!
!     ********************************* TBLA ***************************
!     TEST DRIVER FOR BASIC LINEAR ALGEBRA SUBPROGRAMS.
!     C. L. LAWSON, JPL, 1974 DEC 10, 1975 MAY 28
!
!     UPDATED BY K. HASKELL - JUNE 23,1980
!
!***ROUTINES CALLED  CHECK0, CHECK1, CHECK2, HEADER
!***COMMON BLOCKS    COMBLA
!***REVISION HISTORY  (YYMMDD)
!   751210  DATE WRITTEN
!   890618  REVISION DATE from Version 3.2
!   891214  Prologue converted to Version 4.0 format.  (BAB)
!***END PROLOGUE  BLACHK
      INTEGER IPASS, JTEST(38)
      DOUBLE PRECISION DFAC,DQFAC
      LOGICAL PASS
      COMMON /COMBLA/ NPRINT, ICASE, N, INCX, INCY, MODE, PASS
      DATA SFAC,SDFAC,DFAC,DQFAC / .625E-1, .50, .625D-1, 0.625D-1/
      DATA JTEST /1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, &
                  1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1/
!***FIRST EXECUTABLE STATEMENT  BLACHK
      NPRINT = LUN
      IPASS = 1
!
      IF (KPRINT.GE.2) WRITE (NPRINT,1005)
 1005 FORMAT(1H1,50HQUICK CHECK OF 38 BASIC LINEAR ALGEBRA SUBROUTINES/)
          DO 60 ICASE=1,38
          IF(JTEST(ICASE) .EQ. 0) GO TO 60
          !CALL HEADER (KPRINT)
!
!         INITIALIZE  PASS, INCX, INCY, AND MODE FOR A NEW CASE.
!         THE VALUE 9999 FOR INCX, INCY OR MODE WILL APPEAR IN THE
!         DETAILED  OUTPUT, IF ANY, FOR CASES THAT DO NOT INVOLVE
!         THESE PARAMETERS.
!
          PASS=.TRUE.
          INCX=9999
          INCY=9999
          MODE=9999
              GO TO (12,12,12,12,12,12,12,12,12,12, &
                     12,10,10,12,12,10,10,12,12,12, &
                     12,12,12,12,12,11,11,11,11,11, &
                     11,11,11,11,11,11,11,11),  ICASE
!                                       ICASE = 12-13 OR 16-17
   !10         CALL CHECK0(SFAC,DFAC,KPRINT)
   10         CONTINUE
              GO TO 50
!                                       ICASE = 26-38
   11         CALL CHECK1(SFAC,DFAC,KPRINT)
              GO TO 50
!                                       ICASE =  1-11, 14-15, OR 18-25
   12         CALL CHECK2(SFAC,SDFAC,DFAC,DQFAC,KPRINT)
   50         CONTINUE
!                                                  PRINT
          IF (KPRINT.GE.2 .AND. PASS) WRITE (NPRINT,1001)
      IF (.NOT.PASS) IPASS = 0
   60     CONTINUE
      IF (KPRINT.GE.2 .AND. IPASS.EQ.1) WRITE (NPRINT,1006)
      IF (KPRINT.GE.1 .AND. IPASS.EQ.0) WRITE (NPRINT,1007)
      RETURN
 1001 FORMAT(1H+,39X,4HPASS)
 1006 FORMAT(/54H ****************BLAS PASSED ALL TESTS****************)
 1007 FORMAT(/54H ****************BLAS FAILED SOME TESTS***************)
      END
END MODULE slatec_blachk
