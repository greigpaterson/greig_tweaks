MODULE slatec_ismpl
CONTAINS

!DECK ISMPL
      SUBROUTINE ISMPL (N, M, INDX)
        USE slatec_rand
!***BEGIN PROLOGUE  ISMPL
!***SUBSIDIARY
!***PURPOSE  Generate integer sample.
!            This routine picks M "random" integers in the range 1 to
!            N without any repetitions.
!***LIBRARY   SLATEC (SLAP)
!***TYPE      INTEGER (ISMPL-I)
!***AUTHOR  Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (510) 423-3141
!             seager@llnl.gov
!***ROUTINES CALLED  RAND
!***REVISION HISTORY  (YYMMDD)
!   871119  DATE WRITTEN
!   881213  Previous REVISION DATE
!   890919  Changed to integer name ISMPL.  (MKS)
!   890920  Converted prologue to SLATEC 4.0 format.  (FNF)
!   920511  Added complete declaration section.  (WRB)
!***END PROLOGUE  ISMPL
!     .. Scalar Arguments ..
      INTEGER M, N
!     .. Array Arguments ..
      INTEGER INDX(M)
!     .. Local Scalars ..
      REAL DUMMY
      INTEGER I, ID, J
!     .. Intrinsic Functions ..
      INTRINSIC INT
!***FIRST EXECUTABLE STATEMENT  ISMPL
!
!     Check the input
!
      DUMMY = 0.0
      IF( N*M.LT.0 .OR. M.GT.N ) RETURN
!
!     Set the indices.
      INDX(1) = INT( RAND(DUMMY)*N ) + 1
!VD$ NOCONCUR
      DO 30 I = 2, M
 10      ID = INT( RAND(DUMMY)*N ) + 1
!
!        Check to see if ID has already been chosen.
!VD$ NOVECTOR
!VD$ NOCONCUR
         DO 20 J = 1, I-1
            IF( ID.EQ.INDX(J) ) GOTO 10
 20      CONTINUE
         INDX(I) = ID
 30   CONTINUE
      RETURN
!------------- LAST LINE OF ISMPL FOLLOWS ------------------------------
      END

END MODULE slatec_ismpl
