# README #

This is the source code for the Micromagnetic modelling programme using the
finite element method.

Micromagnetic Earth Related Robust Interpreter Language Laboratory
Finite element solver
 by
 Wyn Williams 2005
 with contributions
 Hubert-Minimization / Paths / Scripting
 by Karl Fabian  2014
 and
 Multiphase / Optimization / Testing, refactoring, packaging
 by Padraig O Conbhui 2017

 Open source routines  by
 G. Benthien  :   string module
 NAG library  :   sparse matrix solver


### What is this repository for? ###

This is an open source code and we welcome contributors.


### How do I get set up? ###

The code is configured using CMake and can be compiled with a standard
compliant fortran compiler.
A script used to compile binary distributions is in
`scripts/merrill-static/unix.sh`.
It has been compiled with the Intel, GNU and PGI fortran compilers.
It can be compiled in Linux and OSX using the standard fortran compilers.
It has been compiled for Windows using the MinGW fortran compiler.


### Contribution guidelines ###

### Who do I talk to? ###

Further information can be obtained from
Wyn Williams at wyn.williams@ed.ac.uk
Karl Fabian at karl.fabian@ngu.no
Padraig O Conbhui at poconbhui@gmail.com
