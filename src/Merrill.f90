!> Convenience module which loads and initializes all the modules used in
!> a typical MERRILL program.
MODULE Merrill

  USE Material_Parameters
  USE Tetrahedral_Mesh_Data
  USE Finite_Element
  USE Magnetization_Path
  USE Hubert_Minimizer
  USE Energy_Calculator
  USE Mesh_IO
  USE Utils

  IMPLICIT NONE

  CONTAINS

  SUBROUTINE InitializeMerrill()
    CALL InitializeUtils()
    CALL InitializeTetrahedralMeshData()
    CALL InitializeFiniteElement()
    CALL InitializeMaterialParameters()
    CALL InitializeMagnetizationPath()
    CALL InitializeHubertMinimizer()
    CALL InitializeEnergyCalculator()
    write(*,*) 'Machine epsilon =', MachEps
  END SUBROUTINE InitializeMerrill

  SUBROUTINE DestroyMerrill()
    CALL DestroyEnergyCalculator()
    CALL DestroyMagnetizationPath()
    CALL DestroyMaterialParameters()
    CALL DestroyFiniteElement()
    CALL DestroyTetrahedralMeshData()
  END SUBROUTINE DestroyMerrill

END MODULE Merrill
