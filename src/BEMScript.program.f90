!     ***********************************************************************
!     *                   MERRILL 1.3.0                                     *
!     * Micromagnetic Earth Related Robust Interpreter Language Laboratory  *
!     *                   Finite element solver                             *
!     *                   by                                                *
!     *                   Wyn Williams 2005                                 *
!     *                   with contributions                                *
!     *                   Hubert-Minimization / Paths /Scripting            *
!     *                   by Karl Fabian  2014                              *
!     *                                                                     *
!     *                  Open source routines  by                           *
!     *                  Press et al. :   sorting                           *
!     *                  G. Benthien  :   string module                     *
!     *                  NAG library  :   sparse matrix solver              *
!     ***********************************************************************

!> The MScript parser program
PROGRAM BEM_Script

  USE Loop_Parser
  USE strings
  USE Merrill
  USE Command_Parser
  USE Variable_Setter

  IMPLICIT NONE

  INTEGER :: i, nargs, linecnt, ransalt
  CHARACTER (LEN=200) :: &
    scriptfile, line, args(20), &
    command, ransaltstr
  LOGICAL :: exist2
  INTEGER :: ierr

  !
  ! Process command line arguments
  !


  !cccccccccccccccccc  Argument is the name of the script file  ccccccccccccccc
  call GET_COMMAND_ARGUMENT(1, scriptfile)
  call RemoveSp(scriptfile)
  WRITE(*,*)  MERRILL_VERSION_STRING
  WRITE(*,*)  'Script file is : <'//scriptfile(:LEN_TRIM(scriptfile))//'>'

  !cc Check that to see if there is a random 'salt' (useful for parallel) ccccc
  if (COMMAND_ARGUMENT_COUNT() > 1) then
    call GET_COMMAND_ARGUMENT(2, ransaltstr)
    call removeSp(ransaltstr)
    read(ransaltstr, '(I10)') ransalt
    write(*,*) 'Using salt value:', ransalt
  else
    ransalt=0
  endif


  !---------------------------------------------------------------
  !            Initialize all modules used.
  !---------------------------------------------------------------

  CALL InitializeMerrill()

  ! Reinitialize Utils (ie sdrand) with ransalt (BEFORE DRAND is called!)
  CALL InitializeUtils(sdrand_salt = ransalt)

  CALL InitializeCommandParser()
  CALL InitializeVariableSetter()
  CALL InitializeLoopParser()


  !  CHECK IF INPUT FILES EXIST
  INQUIRE(FILE=scriptfile, EXIST=exist2)
  DO WHILE(exist2.EQV..FALSE.)
    IF(exist2) THEN
      CONTINUE
    ELSE
      WRITE(*,*) 'Cannot OPEN file :', scriptfile
      WRITE(*,*) 'Input new script file name:'
      READ(*,*) scriptfile
      scriptfile=scriptfile(:LEN_TRIM(scriptfile))
      INQUIRE(FILE=scriptfile, EXIST=exist2)
      IF(exist2) WRITE(*,*) 'Found scriptfile :',scriptfile
    ENDIF
  ENDDO

  !---------------------------------------------------------------
  !            SCRIPT LANGUAGE INTERPRETER
  !---------------------------------------------------------------
  !            Start reading and executing scriptfile.
  !            Relies heavily on the parsing functions of Module strings
  !            by  G. Benthien (http://gbenthien.net/strings/)
  !---------------------------------------------------------------
  !
  call ReadLoopFile(scriptfile)  ! Performs loop parser

  DO linecnt=1,AllLineCnt

    nargs=NArgsOut(linecnt)
    args(1:nargs)=LineOutStack(linecnt,1:nargs)
    args(nargs+1:) = ""

    command=lowercase(args(1))
    line=''
    Do i=1,nargs
      line=line(:LEN_TRIM(line))//' '//args(i)(:LEN_TRIM(args(i)))
    End Do
    Write(*,*) " Parsing : "//line(:LEN_TRIM(line))

    ierr = NO_PARSE

    SELECT CASE(command)
        CASE('stop', 'end')
            WRITE(*,*) "Done"
            ierr = PARSE_SUCCESS
            EXIT
        CASE('define', 'undefine', 'addto', 'multby')
            ierr = PARSE_SUCCESS
            CONTINUE
        CASE DEFAULT
            CALL ParseCommand(TRIM(command), args(1:nargs), ierr)
    END SELECT

    IF(ierr .EQ. NO_PARSE) THEN
        WRITE(*,*) "Unknown command '", TRIM(command), "' on line", linecnt
        WRITE(*,*) "Line:", TRIM(line)
        WRITE(*,*) "Exiting!"
        EXIT
    ELSE IF(ierr .EQ. PARSE_ERROR) THEN
        WRITE(*,*) "Error in '", TRIM(command), "' on line", linecnt
        WRITE(*,*) "Line: ", TRIM(line)
        WRITE(*,*) "Exiting!"
        EXIT
    END IF

  END DO


  CALL DestroyLoopParser()
  CALL DestroyVariableSetter()
  CALL DestroyCommandParser()

  CALL DestroyMerrill()

  STOP 'Scripting finished'

END PROGRAM BEM_Script
