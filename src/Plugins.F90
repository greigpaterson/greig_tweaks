!> The Plugins module contains routines for loading libraries which can extend
!> MERRILL's functionality at run time.
MODULE Plugins
    USE :: ISO_C_BINDING
    IMPLICIT NONE

    INTEGER(C_INT), PARAMETER :: RTLD_LAZY = 1, RTLD_NOW = 2, RTLD_GLOBAL = 256

    ! Interfaces for Linux dynamic library routines
    INTERFACE
        !> Interface for void *dlopen(const char* filename, int mode);
        FUNCTION dlopen(filename, mode) BIND(C, NAME="dlopen")
            !>
            !> Interface for void *dlopen(const char* filename, int mode);
            !>
            !> @param[in] filename The filename to open.
            !> @param[in] mode     The mode to open it in.
            !> @returns A handle to the library.
            !>

            USE ISO_C_BINDING
            IMPLICIT NONE

            TYPE(C_PTR) :: dlopen
            CHARACTER(C_CHAR), INTENT(IN) :: filename(*)
            INTEGER(C_INT), VALUE :: mode
        END FUNCTION dlopen

        !> Interface for void *dlsym(void *handle, const char *name);
        FUNCTION dlsym(handle,name) BIND(C,NAME="dlsym")
            !>
            !> Interface for void *dlsym(void *handle, const char *name);
            !>
            !> @param[in] handle The handle to the library returned by dlopen.
            !> @param[in] name   The name of the function to get.
            !> @returns A pointer to the function.
            !>

            USE ISO_C_BINDING
            IMPLICIT NONE

            TYPE(C_FUNPTR) :: dlsym
            TYPE(C_PTR), VALUE :: handle
            CHARACTER(C_CHAR), INTENT(IN) :: name(*)
        END FUNCTION

        !> Interface for char *dlerror(void);
        FUNCTION c_dlerror() BIND(C, NAME="dlerror")
            !> Interface for char *dlerror(void);
            !>
            !> Defining slightly different name because of char* return type
            !> and dealing with it later.
            USE ISO_C_BINDING
            IMPLICIT NONE

            TYPE(C_PTR) :: c_dlerror
        END FUNCTION c_dlerror
    END INTERFACE

    INTERFACE
        !> Interface for void InitializeMerrillPlugin();
        SUBROUTINE InitializeMerrillPlugin() BIND(C)
            !> Interface for void InitializeMerrillPlugin();
            !>
            !> This is the entry function for a library. When MERRILL loads a
            !> plugin it will call this routine defined in the library.
            !> The plugin is then expected to inject itself into whatever parts
            !> of MERRILL it needs, e.g. by calling
            !> energy_calculator.addenergycalulator() and
            !> command_parser.addcommandparser()
        END SUBROUTINE InitializeMerrillPlugin
    END INTERFACE

    CONTAINS

#ifdef MERRILL_ENABLE_PLUGINS
    FUNCTION dlerror()
      USE ISO_FORTRAN_ENV

      CHARACTER(KIND=C_CHAR, len=1024) :: dlerror

      TYPE(C_PTR) :: c_char_ptr
      CHARACTER(KIND=C_CHAR), POINTER :: c_char_arr(:)
      CHARACTER :: ch

      INTEGER :: i

      c_char_ptr = c_dlerror()

      dlerror = ""
      IF( C_ASSOCIATED(c_char_ptr) ) THEN
        CALL C_F_POINTER(c_char_ptr, c_char_arr, (/ 1024 /))
        DO i=1,1024
          ch = c_char_arr(i)
          IF(ch .NE. C_NULL_CHAR) THEN
            dlerror(i:i) = ch
          ELSE
            EXIT
          END IF
        END DO
      END IF
    END FUNCTION dlerror

    SUBROUTINE LoadPlugin(library_filename)
        CHARACTER(len=*), INTENT(IN) :: library_filename

        TYPE(C_PTR) :: handle
        TYPE(C_FUNPTR) :: c_initialize_plugin_ptr
        PROCEDURE(InitializeMerrillPlugin), BIND(C), POINTER :: &
            InitializeMerrillPlugin_ptr


        handle = dlopen(TRIM(library_filename) // C_NULL_CHAR, RTLD_LAZY)
        IF( .NOT. C_ASSOCIATED(handle) ) THEN
          WRITE(*,*) "dlerror: ", TRIM(dlerror())
          WRITE(*,*) "Error: Unable to open ", TRIM(library_filename)
          STOP
        END IF

        c_initialize_plugin_ptr = dlsym( &
          handle, "InitializeMerrillPlugin" // C_NULL_CHAR &
        )
        IF( .NOT. C_ASSOCIATED(c_initialize_plugin_ptr) ) THEN
            WRITE(*,*) "ERROR: Unable to find procedure InitializeMerrillPlugin"
            STOP
        END IF

        CALL C_F_PROCPOINTER( &
            c_initialize_plugin_ptr, InitializeMerrillPlugin_ptr &
        )

        WRITE(*,*) "Loading Plugin ", TRIM(library_filename)
        CALL InitializeMerrillPlugin_ptr()
    END SUBROUTINE LoadPlugin
#else
    SUBROUTINE LoadPlugin(library_filename)
        CHARACTER(len=*), INTENT(IN) :: library_filename

        WRITE(*,*) "Error: LoadPlugin not enabled"
        STOP
    END SUBROUTINE
#endif
END MODULE Plugins
