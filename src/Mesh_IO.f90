!> A Module containing routines for reading and writing mesh files.
MODULE Mesh_IO
  USE Utils
  IMPLICIT NONE

  CHARACTER(LEN=1024) :: datafile, stem, hystfile, vboxfile, demagfile

CONTAINS

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! Subroutines for Mesh IO related operations
  !    ->  ReadPatranMesh                 : read mesh from a patran file
  !    ->  WriteMag                       : writes out the magnetization
  !                                            (.dat) files
  !    ->  WriteHyst                      : writes out m.h_est against
  !                                            |h_ext|
  !    ->  WriteTecplot                   : writes out the TecPlot format
  !                                            for visualization
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  SUBROUTINE BuildMeshIO()
    USE Tetrahedral_Mesh_Data
    USE Material_Parameters
    USE Finite_Element
    USE Energy_Calculator
    IMPLICIT NONE

    CALL BuildTetrahedralMeshData()
    CALL BuildFiniteElement()
    CALL BuildMaterialParameters()
    CALL BuildEnergyCalculator()
  END SUBROUTINE BuildMeshIO

  !---------------------------------------------------------------------------
  ! ReadPatranMesh: Read meshfile containing a PATRAN Neutral File
  !---------------------------------------------------------------------------

  SUBROUTINE ReadPatranMesh(meshfile)
    USE Tetrahedral_Mesh_Data
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: meshfile
    INTEGER :: meshunit

    INTEGER :: i
    INTEGER, ALLOCATABLE :: idx(:)

    ! Header values
    INTEGER :: IT, ID, IV, KC, N1, N2, N3, N4, N5

    ! Packet 2 data card 1
    INTEGER :: P2C2_nodes, P2C2_config, P2C2_ceid
    REAL(KIND=DP) :: P2C2_th(3)

    CHARACTER(len=12) block_name

    CHARACTER(len=20), PARAMETER :: HEADER_FMT = "(I2, 8I8)"
    INTEGER :: ios

    !-------------------------------------------------------------------------
    !            Start reading MESH file (.pat format)
    !-------------------------------------------------------------------------
    !
    ! File format taken from
    !     Patran 2014.1 Reference Manual Part 1: Basic Functions,
    !     The Neutral File, p 892
    ! found at
    !     https://simcompanion.mscsoftware.com/infocenter/
    !         index?page=content&id=DOC10756
    !         &cat=2014.1_PATRAN_DOCS&actp=LIST
    !
    ! A Patran file is a series of packets. A packet contains several
    ! lines called "cards". The first card is a header with identifying
    ! information, including the number of subsequent cards/lines, and
    ! the rest are the relevant information information for the packet.
    !
    ! The main do loop should be iterating over packets, with the initial
    ! "read" reading a packet's header card and the "read"s within the
    ! SELECT CASE should be reading off the remaining cards in the packet.
    !

    NFIX = 0

    ! First we open the file
    OPEN( &
      NEWUNIT=meshunit, FILE=meshfile, &
      STATUS='OLD', ACTION='READ', IOSTAT=ios &
    )
    IF(ios .NE. 0) THEN
      WRITE(*,*) "Error opening file: ", TRIM(meshfile)
      STOP
    END IF

    DO
        !
        ! We read the header card for the packet.
        ! This read should *always* be reading a header card. If it's not,
        ! something is seriously wrong.
        !
        ! The header format is the same for every card, so we'll read it
        ! here, and use that information to figure out how it should be
        ! processed later.
        !
        ! IT here is the packet type, KC is the number of extra lines
        ! in the packet. N1 - N5 are optional extra data used by the packet.
        !
        READ(meshunit, HEADER_FMT, IOSTAT=ios) &
            IT, ID, IV, KC, N1, N2, N3, N4, N5

        ! Special case for card 99 and eof:
        ! On End card or end of file, exit the do loop.
        IF(IT == 99 .OR. ios < 0) EXIT

        ! Here we process each packet, reading the rest of its cards.
        !
        ! The Patran standard says these packets *should* appear in order,
        ! if at all.
        SELECT CASE(IT)

            !------------------------------------------------------------------
            ! Packet 25: Title Card
            !------------------------------------------------------------------
            ! The title card packet contains the following:
            !         25 ID IV KC
            !         TITLE
            !
            ! where:  ID = 0 (not applicable)
            !         IV = 0 (not applicable
            !         KC = 1
            !         TITLE = Identifying title ...
            !
            ! format: (I2, 8I8)
            !         (80A4)
            !------------------------------------------------------------------
            CASE(25)
                ! KC should be 1.
                ! Ignore this packet.
                DO i=1,KC
                    READ(meshunit,*)
                END DO


            !------------------------------------------------------------------
            ! Packet 26: Summary Data
            !------------------------------------------------------------------
            ! The summary data packet contains the following:
            !         26 ID IV KC N1 N2 N3 N4 N5
            !         DATE TIME VERSION
            !
            ! where:  ID = 0 (not applicable)
            !         IV = 0 (not applicable)
            !         KC = 1
            !         N1 = number of nodes
            !         N2 = number of elements
            !         N3 = number of materials
            !         N4 = number of element properties
            !         N5 = number of coordinate frames
            !         DATE = dd-mm-yy
            !         TIME = hh:mm:ss
            !         VERSION = ??
            !
            ! format: (I2, 8I8)
            !         (80A4)
            !------------------------------------------------------------------
            CASE(26)
                NNODE  = N1
                NTRI   = N2

                ! Ignore the rest of this packet.
                DO i=1,KC
                    READ(meshunit,*)
                END DO

                ! Allocate all arrays that depend on NNODE and NTRI
                CALL MeshAllocate(NNODE, NTRI)


            !------------------------------------------------------------------
            ! Packet 01: Node Data
            !------------------------------------------------------------------
            ! The node data packet contains the following:
            !         1 ID IV KC
            !         X Y Z
            !         ICF GTYPE NDF CONFIG CID PSP
            !
            ! where:  ID = node ID
            !         IV = 0 (not applicable)
            !         KC = number of lines in data card = 2
            !         X, Y, Z = X, Y, Z cartesian coordinate of the node
            !         ICF = 1 (referenced)
            !         GTYPE = G
            !         NDF = 2 or 3 for 2D or 3D model respectively
            !         CONFIG = 0 (not applicable)
            !         CID = 0 i.e. global cartesian coordinate system
            !         PSPC = 000000 (not used)
            !
            ! format: (I2, 8I8)
            !         (3E16.9) (Maybe actually (E15.8, 1X, E15.8, 1X, E15.8)
            !         (I1, 1A1, I8, I8, I8, 2X, 6I1)
            !------------------------------------------------------------------
            CASE(1)
                ! ID = Node ID

                ! Card 1: X Y Z data
                READ(meshunit, "(3E16.9)") VCL(ID,1),VCL(ID,2),VCL(ID,3)

                ! Ignore the rest of this packet
                DO i=2,KC
                    READ(meshunit,*)
                END DO


            !------------------------------------------------------------------
            ! Packet 02: Element Data
            !------------------------------------------------------------------
            ! The element data packet contains the following:
            !         2 ID IV KC N1 N2
            !         NODES CONFIG PID CEID q1 q2 q3
            !         LNODES
            !         ADATA
            !
            ! where:  ID = element ID
            !         IV = shape (2=bar, 3=tri, 4=quad,
            !                     5=tet, 7=wedge, 8=hex, 9=pyra)
            !         KC = number of lines in data card
            !         N1 = 0 (not used)
            !         N2 = 0 (not used)
            !         NODES = number of nodes in the element
            !         CONFIG = 0 (not used)
            !         PID = element property ID
            !         CEID = 0 (not used)
            !         q1,q2,q3 = 0 (not used)
            !         LNODES = element corner nodes followed by
            !                  additional nodes
            !         ADATA : not used
            !
            ! format: (I2, 8I8)
            !         (I8, I8, I8, I8, 3E16.9)
            !         (10I8)
            !------------------------------------------------------------------
            CASE(2)
                ! ID = Element ID

                ! Card 1 contains number of nodes, element configuration,
                ! property id etc.
                ! We'll put the property ID into TetSubDomains(ID)
                READ(meshunit, "(4I8,3E16.9)") &
                    P2C2_nodes, P2C2_config, TetSubDomains(ID), P2C2_ceid, &
                    P2C2_th

                ! Card 2 contains node ids. We assume a linear tetrahedron,
                ! so we should only have 4 nodes.
                READ(meshunit, "(4I8)") &
                    TIL(ID,1), TIL(ID,2), TIL(ID,3), TIL(ID,4)
                TIL(ID,5) = 0

                ! Ignore the rest of this packet
                DO i=3,KC
                    READ(meshunit,*)
                END DO


            ! Named components, used for block ids

            !------------------------------------------------------------------
            ! Packet 21: Named Components
            !------------------------------------------------------------------
            ! The element data packet contains the following:
            !         21 ID IV KC
            !         NAME
            !         NTYPE(1) ID(1) NTYPE(2) ID(2) ... NTYPE(5) ID(5)
            !         NTYPE(1) ID(1) NTYPE(2) ID(2) ... NTYPE(5) ID(5)
            !         ...
            !
            ! where:  ID = component number (i.e a number if for the nodeset)
            !         IV = 2* number of data pairs.
            !               i.e. IV/2 is the number of elements referenced.
            !         KC = number of lines in data card
            !               KC = 1 + (IV + 9)/10 for text files
            !         NAME = component name (often a nodeset name or number)
            !         NTYPE(i) = Element type.
            !               Node = 5
            !               Tetrahedron = 9
            !         ID(i) = Element ID.
            !
            ! format: (I2, 8I8)
            !         (A12)
            !         (10I8)
            !         (10I8)
            !         ...
            !------------------------------------------------------------------
            CASE(21)
                ! IV = 2* the number of data pairs
                ! ID = Component number

                ! Card 1 is component name.
                READ(meshunit, "(A12)") block_name

                ! Warn about block ids and fixed nodes
                WRITE(*,*)
                WRITE(*,*) "Patran file contains block with name '", &
                    TRIM(block_name), "' and id: ", ID
                WRITE(*,*) "Note that all blocks apart from block id 0 &
                    &are assumed to contain fixed magnetizations!"

                ! Card 2 is a set of (NTYPE, ID) pairs, 5 pairs to a line.
                IF(ALLOCATED(idx)) DEALLOCATE(idx)
                ALLOCATE(idx(IV))

                ! Read all lines/pairs in at once
                READ(meshunit,*) (idx(i),i=1,IV)

                DO i=1,IV/2
                  IF(idx(2*(i-1)+2) .EQ. 5) THEN
                    ! NTYPE = 5 for Nodes.
                    ! sets the block number for each node of the mesh
                    ! Block IDs seem to start at 0, we want them to start at 1.
                    NodeBlockNumber(idx(2*(i-1)+1))=ID+1
                  ELSE IF(idx(2*(i-1)+2) .EQ. 9) THEN
                    ! NTYPE = 9 for Tetrahedra
                    TetSubDomains(idx(2*(i-1)+1))=ID+1
                  END IF
                END DO

                NFIX=NFIX+IV/2
                DEALLOCATE(idx)

            ! Unknown card, ignore
            CASE DEFAULT
                WRITE(*,"(A,I2,A)") "ReadPatranMesh: Ignoring Packet Type ", IT, &
                    & " in file '" // TRIM(meshfile) // "'"
                DO i=1,KC
                    READ(meshunit,*)
                END DO

        END SELECT

    END DO

    CLOSE(meshunit)

    CALL BuildMeshIO()
  END SUBROUTINE ReadPatranMesh


  !----------------------------------------------------------------------------
  ! ReadTecplotMesh: Read meshfile containing a Tecplot ASCII File
  !----------------------------------------------------------------------------
  SUBROUTINE ReadTecplotMesh(meshfile)
    USE Tetrahedral_Mesh_Data
    USE strings
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: meshfile
    INTEGER :: meshunit

    INTEGER, PARAMETER :: MAX_TECPLOT_CHARS_PER_LINE = 32000
    CHARACTER(len=MAX_TECPLOT_CHARS_PER_LINE) :: line
    CHARACTER(len=1024) :: token
    CHARACTER(len=*), PARAMETER :: separators = " ,"

    INTEGER, PARAMETER :: PARSE_SUCCESS = 1, PARSE_ERROR = -1

    INTEGER :: NNODE_read, NTRI_read

    INTEGER :: ierr

    INTEGER :: ParserState
    INTEGER, PARAMETER :: &
      PARSING_HEADERS = 1, PARSING_ZONE     = 2, &
      ALLOCATING_MESH = 3, &
      PARSING_NODES   = 4, PARSING_ELEMENTS = 5, &
      PARSING_DONE    = 6

    ! First we open the file
    OPEN( &
      NEWUNIT=meshunit, FILE=meshfile, &
      STATUS='OLD', ACTION='READ', IOSTAT=ierr &
    )
    IF(ierr .NE. 0) THEN
      WRITE(*,*) "Error opening file: ", TRIM(meshfile)
      STOP
    END IF

    ParserState = PARSING_HEADERS
    NNODE_read = -1
    NTRI_read  = -1
    ! Loop over lines
    DO WHILE(ParserState .NE. PARSING_DONE)
      line = ""
      READ(meshunit,'(A)', iostat=ierr, iomsg=token) line
      IF(ierr .NE. 0) THEN
        IF(IS_IOSTAT_END(ierr)) THEN
          EXIT
        ELSE
          WRITE(*,*) token
          ERROR STOP
        END IF
      END IF


      !
      ! Skip comments
      !
      CALL DropInitialSeparators(line)
      IF(line(1:1) .EQ. "#") THEN
        CYCLE
      END IF


      !
      ! PARSING_HEADERS
      !
      ! Parsing Headers
      ! Expecting something like
      !   TITLE = "my fancy title"
      !   VARIABLES = "X", "Y", "Z", "Mx", "My", "Mz"
      ! terminated by a line beginning with ZONE
      !
      ! Maybe a little more complex than necessary, but it should
      ! allow for header items to be added by users in any order.
      !
      IF(ParserState .EQ. PARSING_HEADERS) THEN

        ! Loop over the tokens
        DO
          ! Parse one token, expecting a keyword.
          CALL ParseToken(line, token)

          IF(LEN_TRIM(token) .GT. 0) THEN

            ! Match token for further parsing
            SELECT CASE(lowercase(TRIM(token)))
              ! Parse TITLE
              CASE("title")
                CALL ParseEqOrDie(line, token)

                ! Parse title value. Drop it, because we don't use it.
                CALL ParseToken(line, token)


              ! Parse VARIABLES
              CASE("variables")
                CALL ParseEqOrDie(line, token)

                ! The rest of the line is variable tokens.
                ! Expect X, Y, Z, optional Mx, My, Mz, then EOF
                CALL ParseToken(line, token)
                IF(TRIM(token) .NE. "X") THEN
                  WRITE(*,*) 'ERROR: Expected first variable to be "X"'
                  ERROR STOP
                END IF

                CALL ParseToken(line, token)
                IF(TRIM(token) .NE. "Y") THEN
                  WRITE(*,*) 'ERROR: Expected second variable to be "Y"'
                  ERROR STOP
                END IF

                CALL ParseToken(line, token)
                IF(TRIM(token) .NE. "Z") THEN
                  WRITE(*,*) 'ERROR: Expected third variable to be "Z"'
                  ERROR STOP
                END IF

                ! Try parse Mx. If we get it, parse My and Mz.
                CALL ParseToken(line, token)
                IF(LEN_TRIM(token) .GT. 0) THEN
                  IF(TRIM(token) .NE. "Mx") THEN
                    WRITE(*,*) 'ERROR: Expected fourth variable to be "Mx"'
                    ERROR STOP
                  END IF

                  CALL ParseToken(line, token)
                  IF(TRIM(token) .NE. "My") THEN
                    WRITE(*,*) 'ERROR: Expected fourth variable to be "My"'
                    ERROR STOP
                  END IF

                  CALL ParseToken(line, token)
                  IF(TRIM(token) .NE. "Mz") THEN
                    WRITE(*,*) 'ERROR: Expected fourth variable to be "Mz"'
                    ERROR STOP
                  END IF
                END IF


              ! Parse ZONE.
              ! Move on to the next phase.
              CASE("zone")
                ! Done parsing header! Move on to parsing ZONE header.
                ParserState = PARSING_ZONE

                ! Break out of the token parsing loop
                EXIT
            END SELECT
          ELSE
            ! No token parsed, end of line, break out of token parsing loop
            EXIT
          END IF

        END DO ! Parsing tokens
      END IF ! Parsing Headers


      !
      ! PARSING_ZONE
      !
      ! Parsing ZONE Header
      ! Expecting something like
      !   ZONE T="...", N=1234
      !     E=567 F=FEPOINT ET=TETRAHEDRON
      ! possibly spanning multiple lines,
      ! terminated by the first numerical value of the actual data.
      !
      ! Again, maybe a little more complex than necessary, but it should
      ! allow for header items to be added by users in any order.
      !
      IF(ParserState .EQ. PARSING_ZONE) THEN
        ! Parse tokens
        DO
          CALL ParseToken(line, token)

          IF(LEN_TRIM(token) .GT. 0) THEN

            ! Match token for further parsing
            SELECT CASE(lowercase(TRIM(token)))
              ! Parse zone title. Ignore.
              CASE("t")
                CALL ParseEqOrDie(line, token)

                CALL ParseToken(line, token)

              ! Parse format. Expect FEPOINT or die.
              CASE("f")
                CALL ParseEqOrDie(line, token)

                CALL ParseToken(line, token)
                IF(lowercase(TRIM(token)) .NE. "fepoint") THEN
                  WRITE(*,*) "ERROR: Unsupported value for F."
                  WRITE(*,*) "ERROR: Expected F=FEPOINT"
                  ERROR STOP
                END IF

              CASE("et")
                CALL ParseEqOrDie(line, token)

                CALL ParseToken(line, token)
                IF(lowercase(TRIM(token)) .NE. "tetrahedron") THEN
                  WRITE(*,*) "ERROR: Unsupported value for ET."
                  WRITE(*,*) "ERROR: Expected ET=TETRAHEDRON"
                  ERROR STOP
                END IF

              CASE("n")
                CALL ParseEqOrDie(line, token)
                CALL ParseToken(line, token)
                IF(LEN_TRIM(token) .EQ. 0) THEN
                  WRITE(*,*) "ERROR: Expected value for N."
                  ERROR STOP
                END IF
                READ(token, *) NNODE

              CASE("e")
                CALL ParseEqOrDie(line, token)
                CALL ParseToken(line, token)
                IF(LEN_TRIM(token) .EQ. 0) THEN
                  WRITE(*,*) "ERROR: Expected value for E."
                  ERROR STOP
                END IF
                READ(token, *) NTRI

              CASE DEFAULT
                ! Check if it's a number ([digit], ., or +/-)
                IF(SCAN(token(1:1), "0123456789.+-") .GT. 0) THEN
                  line = TRIM(token) // " " // TRIM(line)

                  ! Move parser along to node parsing
                  ParserState = ALLOCATING_MESH
                  EXIT
                END IF

            END SELECT

          ELSE
            ! No token parsed, end of line
            EXIT
          END IF
        END DO
      END IF


      !
      ! ALLOCATING_MESH
      !
      ! Allocating mesh based on N and E
      !
      IF(ParserState .EQ. ALLOCATING_MESH) THEN
        CALL MeshAllocate(NNODE, NTRI) 
        NNODE_read = 1
        NTRI_read  = 1

        ParserState = PARSING_NODES
      END IF


      !
      ! PARSING_NODES
      !
      ! Parse node coordinate positions
      !
      IF(ParserState .EQ. PARSING_NODES) THEN
        IF(NNODE_read .LE. NNODE) THEN
          READ(line, *) &
            VCL(NNODE_read, 1), VCL(NNODE_read, 2), VCL(NNODE_read, 3)

          NNODE_read = NNODE_read + 1
        ELSE
          ParserState = PARSING_ELEMENTS
        END IF
      END IF


      !
      ! PARSING_ELEMENTS
      !
      ! Parse tetrahedron connectivities
      !
      IF(ParserState .EQ. PARSING_ELEMENTS) THEN
        IF(NTRI_read .LE. NTRI) THEN
          READ(line, *) &
            TIL(NTRI_read, 1), TIL(NTRI_read, 2), &
            TIL(NTRI_read, 3), TIL(NTRI_read, 4)
          TIL(NTRI_read, 5) = 0

          NTRI_read = NTRI_read + 1
        ELSE
          ParserState = PARSING_DONE
        END IF
      END IF

    END DO ! Loop over lines


    CLOSE(meshunit)

    CALL BuildMeshIO()

  CONTAINS
    SUBROUTINE DropInitialSeparators(line)
      CHARACTER(len=*), INTENT(INOUT) :: line

      INTEGER :: i

      ! Drop separators from start of line
      DO i=1,LEN(line)
        IF(SCAN(separators, line(i:i)) .EQ. 0) THEN
          line = line(i:)
          EXIT
        END IF
      END DO
    END SUBROUTINE DropInitialSeparators

    SUBROUTINE ParseToken(line, token)
      CHARACTER(len=*), INTENT(INOUT) :: line
      CHARACTER(len=*), INTENT(OUT)   :: token

      INTEGER :: i
      LOGICAL :: quoting

      CALL DropInitialSeparators(line)

      ! add characters up until first separator
      token = ""
      quoting = .FALSE.

      ! Try parse quote
      IF(line(1:1) .EQ. '"') THEN
        quoting = .TRUE.
        line = line(2:)
      END IF

      DO i=1,LEN(line)
        ! Scan up to delimiter
        IF( &
          (SCAN(separators//"=", line(i:i)) .NE. 0) &
          .OR. &
          (quoting .AND. (line(i:i) .EQ. '"')) &
        ) THEN
          token = line(:i-1)
          line  = line(i:)
          IF(quoting) line = line(2:)
          EXIT
        END IF
      END DO
    END SUBROUTINE ParseToken

    SUBROUTINE ParseEq(line, ierr)
      CHARACTER(len=*), INTENT(INOUT) :: line
      INTEGER, INTENT(OUT) :: ierr

      INTEGER :: i

      CALL DropInitialSeparators(line)

      ierr = PARSE_ERROR
      DO i=1,LEN(line)
        IF(line(i:i) .EQ. "=") THEN
          line = line(i+1:)
          ierr = PARSE_SUCCESS
          EXIT
        END IF
      END DO

    END SUBROUTINE ParseEq

    SUBROUTINE ParseEqOrDie(line, token)
      CHARACTER(len=*), INTENT(INOUT) :: line
      CHARACTER(len=*), INTENT(IN) :: token

      INTEGER :: ierr

      CALL ParseEq(line, ierr)
      IF(ierr .NE. PARSE_SUCCESS) THEN
        WRITE(*,*) "ERROR: Expected = after ", TRIM(token)
        ERROR STOP
      END IF
    END SUBROUTINE ParseEqOrDie

  END SUBROUTINE ReadTecplotMesh


  !---------------------------------------------------------------
  ! WriteMag
  !---------------------------------------------------------------

  SUBROUTINE WriteMag()
    USE Tetrahedral_Mesh_Data
    USE Material_Parameters
    IMPLICIT NONE

    INTEGER i
    INTEGER :: dataunit

    OPEN(NEWUNIT=dataunit, FILE=datafile, STATUS='unknown')
    DO i=1,NNODE
      WRITE(dataunit,3000) &
        VCL(i,1), VCL(i,2), VCL(i,3), &
        m(i,1), m(i,2), m(i,3)
    END DO
    CLOSE(dataunit)

    3000 FORMAT(7(f10.6, ' '))
  END SUBROUTINE WriteMag


  !---------------------------------------------------------------
  ! WriteHyst
  !---------------------------------------------------------------

  SUBROUTINE WriteHyst( )
    USE Tetrahedral_Mesh_Data
    USE Material_Parameters
    USE Finite_Element
    IMPLICIT NONE

    INTEGER :: i, l
    REAL(KIND=DP) :: mhdot, mag_av(3), mag_max, mag_maxv, mag_v(3)
    LOGICAL :: exist2
    INTEGER :: hystunit

    INQUIRE(FILE=hystfile, EXIST = exist2)

    OPEN(NEWUNIT=hystunit, FILE=hystfile, STATUS='unknown', POSITION='append')

    IF(exist2.EQV..FALSE.) THEN
!      WRITE(hystunit, '(5A16)') &
!        "m.h", "|H| (Tesla)", &
!        "<Mx>/<Ms>","<My>/<Ms>","<Mz>/<Ms>"
      WRITE(hystunit, '(6(A16,:,","))') &
        "mu0.H (Tesla)", "m.h (Am^2)", "m.h/Ms",  &
        "<Mx>/<Ms>","<My>/<Ms>","<Mz>/<Ms>"
    END IF

    mhdot   = 0

    mag_maxv =0
    DO i=1,NNODE
!      mhdot = mhdot + (m(i,1)*Hz(1) + m(i,2)*Hz(2) + m(i,3)*Hz(3))*vbox(i)

! calculate the average magnetisation per node over neighbouring elements which cam be of 
! different material  
     mag_av(:)  = 0
     mag_max = 0
      DO l=CNR_IM(i),CNR_IM(i+1)-1
        mag_av(:) = mag_av(:) + Ms(SDNR_IM(l))*m(RNR_IM(l),:)*InterpolationMatrix(l)
        mag_max   = mag_max   + Ms(SDNR_IM(l))*InterpolationMatrix(l)
      END DO
      mag_maxv=mag_maxv + mag_max*vbox(i)
      mag_v(:)=mag_v(:)+ mag_av(:)*vbox(i)
      mhdot = mhdot + (mag_av(1)*Hz(1) + mag_av(2)*Hz(2) + mag_av(3)*Hz(3))*vbox(i)     
    END DO

!    WRITE(hystunit, '(5ES16.8)') mhdot/(total_volume*mag_max), extapp, mag_av(:)/mag_max
!    WRITE(hystunit, '(5ES16.8)') mhdot/(mag_maxv), extapp, mag_v(:)/mag_maxv
    WRITE(hystunit, '(6(ES16.8,:,","))') extapp, mhdot, mhdot/(mag_maxv), mag_v(:)/mag_maxv
    CLOSE(hystunit)
  END SUBROUTINE WriteHyst
  
  
  !---------------------------------------------------------------
  ! WriteDemag
  !---------------------------------------------------------------

  
    SUBROUTINE WriteDemag(extapp2, stem, loopcount)
    USE Tetrahedral_Mesh_Data
    USE Material_Parameters
    USE strings
    USE Finite_Element
    IMPLICIT NONE

    REAL(KIND=DP),    INTENT(IN) :: extapp2
    INTEGER,          INTENT(IN) :: loopcount
    CHARACTER(LEN=*), INTENT(IN) :: stem

    INTEGER :: i,l
    CHARACTER (LEN=80) ::  Nstr,Estr,Zstr
    CHARACTER (LEN=200) :: head
    INTEGER :: multunit
    REAL(KIND=DP):: hd_x,hd_y,hd_z



    
    if(abs(extapp2 - (-1.0)) < MachEps) then
      call writenum(zone, Zstr,'(F10.3)')
      zone = zone + zoneinc
    else
      call writenum(extapp2, Zstr,'(F10.3)')
    endif

    call writenum(NNODE,  Nstr,'(I8)')
    call writenum(NTRI,   Estr,'(I8)')
    head='ZONE T="'//TRIM(Zstr)//'"  N='//TRIM(Nstr)//',  E='//TRIM(Estr)

    IF(loopcount==1) THEN

      OPEN( &
        NEWUNIT=multunit, FILE=stem(:LEN_TRIM(stem))//"_demag.tec", &
        STATUS='unknown' &
      )
      WRITE(multunit,*) 'TITLE = ','"'//stem(:LEN_TRIM(stem))//'"'
      WRITE(multunit,*) 'VARIABLES = "X","Y","Z","Hd_x","Hd_y","Hd_z"'
      WRITE(multunit,*) TRIM(head)//', F=FEPOINT, ET=TETRAHEDRON'
      DO i=1,NNODE
        hd_x=0.
        hd_y=0.
        hd_z=0.

! The demagnetising field = demag gradient/(mu_0*()(Ms *vbox)))
! but the interpolation matrix already contains the vbox box volume        
          DO l=CNR_IM(i),CNR_IM(i+1)-1
          hd_x=hd_x + &
          hdemag(i,1)/(mu*Ms(SDNR_IM(l))*InterpolationMatrix(l))
          hd_y=hd_y + &
          hdemag(i,2)/(mu*Ms(SDNR_IM(l))*InterpolationMatrix(l))
          hd_z=hd_z + &
          hdemag(i,3)/(mu*Ms(SDNR_IM(l))*InterpolationMatrix(l))
          enddo  
         
        WRITE(multunit,3001) &
          VCL(i,1), VCL(i,2), VCL(i,3), &
          -hd_x,-hd_y,-hd_z

      ENDDO
      DO i=1,NTRI
        WRITE(multunit,3002) TIL(i,1), TIL(i,2), TIL(i,3), TIL(i,4)
      ENDDO

    ENDIF



    IF(loopcount>1) THEN

      OPEN( &
        UNIT=multunit, FILE=stem(:LEN_TRIM(stem))//"_demag.tec", &
        STATUS='unknown', POSITION='APPEND' &
      )
      WRITE(multunit,*) TRIM(head),  ', F=FEPOINT, ET=TETRAHEDRON',&
        &'VARSHARELIST =([1-3]=1), CONNECTIVITYSHAREZONE = 1'

      DO i=1,NNODE
              hd_x=0.
        hd_y=0.
        hd_z=0.

! The demagnetising field = demag gradient/(mu_0*()(Ms *vbox)))
! but the interpolation matrix already contains the vbox box volume        
          DO l=CNR_IM(i),CNR_IM(i+1)-1
          hd_x=hd_x + &
          hdemag(i,1)/(mu*Ms(SDNR_IM(l))*InterpolationMatrix(l))
          hd_y=hd_y + &
          hdemag(i,2)/(mu*Ms(SDNR_IM(l))*InterpolationMatrix(l))
          hd_z=hd_z + &
          hdemag(i,3)/(mu*Ms(SDNR_IM(l))*InterpolationMatrix(l))
          enddo  
      
        WRITE(multunit,3003) -hd_x, -hd_y, -hd_z
      ENDDO
    ENDIF

    CLOSE(multunit)


    3001 FORMAT(6(ES16.7))
    3002 FORMAT(4(i7))
    3003 FORMAT(3(ES16.7))
  END SUBROUTINE WriteDemag
  
  
  

  !---------------------------------------------------------------
  ! WriteVBox
  !---------------------------------------------------------------

  SUBROUTINE WriteVBox( )
    USE Tetrahedral_Mesh_Data
    USE Material_Parameters
    USE Finite_Element
    IMPLICIT NONE

    INTEGER :: i, l
    REAL(KIND=DP) :: mhdot, mag_av(3), mag_max
    LOGICAL :: exist2
    INTEGER :: vboxunit

    INQUIRE(FILE=vboxfile, EXIST = exist2)

    OPEN(NEWUNIT=vboxunit, FILE=vboxfile, STATUS='unknown')

    IF(exist2.EQV..FALSE.) THEN
      WRITE(vboxunit, '(7A16)') &
        "x", "y", "z","mx", "my", "mz", "vbox"
    END IF

    mhdot   = 0
    mag_av  = 0
    mag_max = 0
    DO i=1,NNODE
      !mhdot = mhdot + (m(i,1)*Hz(1) + m(i,2)*Hz(2) + m(i,3)*Hz(3))*vbox(i)

      !DO l=CNR_IM(i),CNR_IM(i+1)-1
      !  mag_av(:) = mag_av(:) + Ms(SDNR_IM(l))*m(RNR_IM(l),:)*InterpolationMatrix(l)
      !  mag_max   = mag_max   + Ms(SDNR_IM(l))*InterpolationMatrix(l)
      !END DO

      write(vboxunit, 3004) &
          VCL(i,1), VCL(i,2), VCL(i,3), &
          m(i,1), m(i,2), m(i,3), vbox(i)

    END DO

    CLOSE(vboxunit)

    3004 FORMAT(7(ES16.7))
  END SUBROUTINE WriteVBox


  !---------------------------------------------------------------
  ! ReadMag
  !---------------------------------------------------------------

  SUBROUTINE ReadMag(dfile)
    USE Material_Parameters
    IMPLICIT NONE

    INTEGER i,NVAL
    CHARACTER(1) :: CH, pCH
    CHARACTER(1) :: DELIM=" "

    REAL(KIND=DP) :: x, y, z, mx, my, mz
    CHARACTER (LEN=200) ::  dfile
    INTEGER :: dunit

    !we first need to know if the data file is of the format
    ! x y x mx my mz    or just mx my mz
    ! count how many numbers there are in the first line
    OPEN(NEWUNIT=dunit, FILE=dfile, STATUS='unknown')

    ! Read first record one character at a time and count delimeters
    NVAL=0
    pch=DELIM

    DO
      ! eor=10 jumps to label 10 on end of row
      READ(dunit, '(A)', ADVANCE='no', EOR=10) ch

      ! increment counter only if the current in not a space and
      ! previous character was a space
      IF((ch/=DELIM).and.(pch==DELIM)) THEN
        NVAL=NVAL+1
      END IF
      !  set previous character to current character
      pch=ch
    END DO
    ! read command skips here on EOR
    10 CONTINUE

    REWIND(dunit)
    CLOSE(dunit)
    ! NVAL now contains the number of columns in the first line of a file


    WRITE(*,*) 'The magnetization file contains',NVAL,'columns'
    OPEN(NEWUNIT=dunit, FILE=dfile, STATUS='unknown')

    SELECT CASE(NVAL)
      CASE(3)
        DO i=1,NNODE
          READ(dunit,*)mx,my,mz
          m(i,1)=mx
          m(i,2)=my
          m(i,3)=mz
        END DO
        CLOSE(dunit)

      CASE(6)
        DO i=1,NNODE
          READ(dunit,*) x,y,z,mx,my,mz
          m(i,1)=mx
          m(i,2)=my
          m(i,3)=mz
        END DO
        CLOSE(dunit)

      CASE DEFAULT
        WRITE(*,*) 'Error counting colums in file ', TRIM(dfile)
        CLOSE(dunit)
        STOP

    END SELECT

  END SUBROUTINE ReadMag


  !---------------------------------------------------------------
  ! WriteTecplot
  !---------------------------------------------------------------

  SUBROUTINE WriteTecplot(extapp, stem, loopcount)
    USE Tetrahedral_Mesh_Data
    USE strings
    IMPLICIT NONE

    REAL(KIND=DP),    INTENT(IN) :: extapp
    INTEGER,          INTENT(IN) :: loopcount
    CHARACTER(LEN=*), INTENT(IN) :: stem

    INTEGER :: i
    CHARACTER (LEN=80) ::  Nstr,Estr,Zstr
    CHARACTER (LEN=200) :: head
    INTEGER :: multunit

    if(abs(extapp - (-1.0)) < MachEps) then
      call writenum(zone, Zstr,'(F10.3)')
      zone = zone + zoneinc
    else
      call writenum(extapp, Zstr,'(F10.3)')
    endif

    call writenum(NNODE,  Nstr,'(I8)')
    call writenum(NTRI,   Estr,'(I8)')
    head='ZONE T="'//TRIM(Zstr)//'"  N='//TRIM(Nstr)//',  E='//TRIM(Estr)

    IF(loopcount==1) THEN

      OPEN( &
        NEWUNIT=multunit, FILE=stem(:LEN_TRIM(stem))//"_mult.tec", &
        STATUS='unknown' &
      )
      WRITE(multunit,*) 'TITLE = ','"'//stem(:LEN_TRIM(stem))//'"'
      WRITE(multunit,*) 'VARIABLES = "X","Y","Z","Mx","My","Mz"'
      WRITE(multunit,*) TRIM(head)//', F=FEPOINT, ET=TETRAHEDRON'
      DO i=1,NNODE
        WRITE(multunit,3001) &
          VCL(i,1), VCL(i,2), VCL(i,3), &
          m(i,1), m(i,2), m(i,3)
      ENDDO
      DO i=1,NTRI
        WRITE(multunit,3002) TIL(i,1), TIL(i,2), TIL(i,3), TIL(i,4)
      ENDDO

    ENDIF


    IF(loopcount>1) THEN

      OPEN( &
        UNIT=multunit, FILE=stem(:LEN_TRIM(stem))//"_mult.tec", &
        STATUS='unknown', POSITION='APPEND' &
      )
      WRITE(multunit,*) TRIM(head),  ', F=FEPOINT, ET=TETRAHEDRON',&
        &'VARSHARELIST =([1-3]=1), CONNECTIVITYSHAREZONE = 1'

      DO i=1,NNODE
        WRITE(multunit,3003) m(i,1), m(i,2), m(i,3)
      ENDDO
    ENDIF

    CLOSE(multunit)


    3001 FORMAT(6(ES16.7))
    3002 FORMAT(4(i7))
    3003 FORMAT(3(ES16.7))
  END SUBROUTINE WriteTecplot


  SUBROUTINE GenerateCubeMesh(width, edge_length)
    USE Tetrahedral_Mesh_Data
    USE Finite_Element
    USE Material_Parameters
    IMPLICIT NONE

    REAL(KIND=DP), INTENT(IN) :: width, edge_length
    INTEGER :: nnodes_x
    REAL(KIND=DP) :: actual_edge_length

    ! number of nodes in one dim
    ! Target nodes is width/edge_length with some geometric factor
    ! for the shape of tets in the cubic cells.
    nnodes_x = CEILING(width / (edge_length*0.91*SQRT(2.0D0))) + 1
    CALL BuildUnitCube(nnodes_x)
    VCL(:,1:3) = VCL(:,1:3)*width

    CALL BuildMeshIO()
  END SUBROUTINE GenerateCubeMesh


  SUBROUTINE GenerateSphereMesh(width, edge_length)
    USE Tetrahedral_Mesh_Data
    USE Finite_Element
    USE Material_Parameters
    IMPLICIT NONE

    REAL(KIND=DP), INTENT(IN) :: width, edge_length
    INTEGER :: nnodes_x
    INTEGER :: i
    REAL(KIND=DP) :: x, y, z, xp, yp, zp, box_width, tt

    ! Start by building a 2*unit cube mesh

    ! Using a very nody cube for more "spherical" symmetry.
    ! A coarser tetrahedralization causes issues where corners
    ! get flattened against the side of the sphere.

    ! number of nodes in one dim
    ! nnodes_x is the number of corner nodes in one dim
    ! Using edge_length/SQRT(3) because our most abundant edge
    ! is along the cube diagonal.
    ! Using +1 to ensure at least 2x2x2 corner nodes needed to build a single
    ! cube.
    nnodes_x = CEILING(width / (edge_length*SQRT(3.0D0))) + 1

    CALL BuildUnitCube(nnodes_x)
    VCL(:,1:3) = 2*VCL(:,1:3)


    ! Transform vertices from the surface of a 2*unit cube to a unit sphere,
    ! then rescale to the actual cube/sphere diameter at that point
    DO i=1,NNODE
      x = VCL(i,1)
      y = VCL(i,2)
      z = VCL(i,3)

      box_width = MAX(ABS(x), ABS(y), ABS(z))

      tt = SQRT(x**2 + y**2 + z**2)
      IF(tt .GT. 0.0D0) THEN
        ! tt here is a value such that (x,y,z) + tt*(x,y,z) is on a sphere
        ! of radius box_width.
        tt = (box_width**2)/tt - 1

        ! Scale tt to keep "boxiness" in the sphere,
        ! 0 <= box_width <= 1
        ! I did this by accident, I have no idea why it works, but it
        ! works pretty nicely! (sphere mesh is complete crap without this)
        tt = tt*box_width
      ELSE
        tt = 0
      END IF

      xp = x + tt*x
      yp = y + tt*y
      zp = z + tt*z

      VCL(i,1:3) = (/ xp, yp, zp /) * width/2
    END DO

    CALL BuildMeshIO()

  END SUBROUTINE GenerateSphereMesh

  SUBROUTINE BuildUnitCube(nnodes_x)
    USE Tetrahedral_Mesh_Data

    INTEGER, INTENT(IN) :: nnodes_x
    INTEGER, ALLOCATABLE :: vertex_index(:,:,:,:)
    REAL(KIND=DP) :: actual_edge_length
    INTEGER :: ncubes

    INTEGER :: i, j, k
    INTEGER :: nTIL, nVCL

    actual_edge_length = 1.0 / (nnodes_x - 1)
    ncubes = (nnodes_x - 1)**3

    ! Nodes are on each corner, on each face and at the center of each cube.
    NNODE = (nnodes_x) ** 3 + 3*(nnodes_x)*(nnodes_x-1)**2 + ncubes
    NTRI = 24*ncubes
    CALL MeshAllocate(NNODE, NTRI)

    ! Storing vertex indices instead of computing them, because
    ! it was easier during development. I wasn't sure how best to
    ! keep track of the face indices. It's probably something simple.
    !
    ! vertex_index(:,:,:, 1) = corner index
    ! vertex_index(:,:,:, 2) = center index
    ! vertex_index(:,:,:, 3) = xy face index
    ! vertex_index(:,:,:, 4) = yz face index
    ! vertex_index(:,:,:, 5) = xz face index
    ALLOCATE(vertex_index(nnodes_x, nnodes_x, nnodes_x, 5))
    vertex_index = -1

    nVCL = 1
    nTIL = 1
    DO k=1,nnodes_x
    DO j=1,nnodes_x
    DO i=1,nnodes_x
      ! Build node positions
      ! Always build the *next* node, a center node and the *current* cube
      ! Start loop on ghost cube, to ensure the previous nodes are always built
      vertex_index(i,j,k, 1) = nVCL
      nVCL = nVCL+1
      VCL(vertex_index(i,j,k, 1), 1:3) = &
        (/ (i-1), (j-1), (k-1) /) * actual_edge_length &
        - 1.0/2


      ! k face
      IF(i.NE.1 .AND. j.NE.1) THEN
        vertex_index(i,j,k, 3) = nVCL
        nVCL = nVCL+1
        VCL(vertex_index(i,j,k, 3), 1:3) = &
          VCL(vertex_index(i,j,k, 1), 1:3) &
          - (/ 0.5, 0.5, 0.0 /) * actual_edge_length
      END IF

      ! i face
      IF(j.NE.1 .AND. k.NE.1) THEN
        vertex_index(i,j,k, 4) = nVCL
        nVCL = nVCL+1
        VCL(vertex_index(i,j,k, 4), 1:3) = &
          VCL(vertex_index(i,j,k, 1), 1:3) &
          - (/ 0.0, 0.5, 0.5 /) * actual_edge_length
      END IF

      ! j face
      IF(i.NE.1 .AND. k.NE.1) THEN
        vertex_index(i,j,k, 5) = nVCL
        nVCL = nVCL+1
        VCL(vertex_index(i,j,k, 5), 1:3) = &
          VCL(vertex_index(i,j,k, 1), 1:3) &
          - (/ 0.5, 0.0, 0.5 /) * actual_edge_length
      END IF

      ! Not building ghost cube, build actual cube
      IF(i.NE.1 .AND. j.NE.1 .AND. k.NE.1) THEN
        
        ! Add central vertex
        vertex_index(i,j,k, 2) = nVCL
        nVCL = nVCL+1
        VCL(vertex_index(i,j,k, 2), 1:3) = ( &
          VCL(vertex_index(i,j,k, 1), 1:3) &
          + VCL(vertex_index(i-1,j-1,k-1, 1), 1:3) &
        ) / 2

        ! Build tets

        ! Back Face k = k-1
        TIL(nTIL ,1:4) = (/ &
          vertex_index(i  , j  , k-1, 3), &
          vertex_index(i-1, j-1, k-1, 1), &
          vertex_index(i,   j-1, k-1, 1), &
          vertex_index(i,   j,   k  , 2) &
        /)
        nTIL = nTIL+1

        TIL(nTIL ,1:4) = (/ &
          vertex_index(i  , j  , k-1, 3), &
          vertex_index(i,   j-1, k-1, 1), &
          vertex_index(i,   j,   k-1, 1), &
          vertex_index(i,   j,   k  , 2) &
        /)
        nTIL = nTIL+1

        TIL(nTIL, 1:4) = (/ &
          vertex_index(i  , j  , k-1, 3), &
          vertex_index(i,   j,   k-1, 1), &
          vertex_index(i-1, j,   k-1, 1), &
          vertex_index(i,   j,   k  , 2) &
        /)
        nTIL = nTIL+1

        TIL(nTIL, 1:4) = (/ &
          vertex_index(i  , j  , k-1, 3), &
          vertex_index(i-1, j,   k-1, 1), &
          vertex_index(i-1, j-1, k-1, 1), &
          vertex_index(i,   j,   k  , 2) &
        /)
        nTIL = nTIL+1


        ! Front Face k = k
        TIL(nTIL, 1:4) = (/ &
          vertex_index(i  , j  , k  , 3), &
          vertex_index(i  , j-1, k  , 1), &
          vertex_index(i-1, j-1, k  , 1), &
          vertex_index(i  , j  , k  , 2) &
        /)
        nTIL = nTIL+1

        TIL(nTIL, 1:4) = (/ &
          vertex_index(i  , j  , k  , 3), &
          vertex_index(i-1, j-1, k  , 1), &
          vertex_index(i-1, j  , k  , 1), &
          vertex_index(i  , j  , k  , 2) &
        /)
        nTIL = nTIL+1

        TIL(nTIL, 1:4) = (/ &
          vertex_index(i  , j  , k  , 3), &
          vertex_index(i-1, j  , k  , 1), &
          vertex_index(i  , j  , k  , 1), &
          vertex_index(i  , j  , k  , 2) &
        /)
        nTIL = nTIL+1

        TIL(nTIL, 1:4) = (/ &
          vertex_index(i  , j  , k  , 3), &
          vertex_index(i  , j  , k  , 1), &
          vertex_index(i  , j-1, k  , 1), &
          vertex_index(i  , j  , k  , 2) &
        /)
        nTIL = nTIL+1

        ! Left Face i = i-1
        TIL(nTIL, 1:4) = (/ &
          vertex_index(i-1, j  , k  , 4), &
          vertex_index(i-1, j-1, k  , 1), &
          vertex_index(i-1, j-1, k-1, 1), &
          vertex_index(i  , j  , k  , 2) &
        /)
        nTIL = nTIL+1

        TIL(nTIL, 1:4) = (/ &
          vertex_index(i-1, j  , k  , 4), &
          vertex_index(i-1, j-1, k-1, 1), &
          vertex_index(i-1, j  , k-1, 1), &
          vertex_index(i  , j  , k  , 2) &
        /)
        nTIL = nTIL+1

        TIL(nTIL, 1:4) = (/ &
          vertex_index(i-1, j  , k  , 4), &
          vertex_index(i-1, j  , k-1, 1), &
          vertex_index(i-1, j  , k  , 1), &
          vertex_index(i  , j  , k  , 2) &
        /)
        nTIL = nTIL+1

        TIL(nTIL, 1:4) = (/ &
          vertex_index(i-1, j  , k  , 4), &
          vertex_index(i-1, j  , k  , 1), &
          vertex_index(i-1, j-1, k  , 1), &
          vertex_index(i  , j  , k  , 2) &
        /)
        nTIL = nTIL+1


        ! Right Face i = i
        TIL(nTIL, 1:4) = (/ &
          vertex_index(i  , j  , k  , 4), &
          vertex_index(i  , j-1, k-1, 1), &
          vertex_index(i  , j-1, k  , 1), &
          vertex_index(i  , j  , k  , 2) &
        /)
        nTIL = nTIL+1

        TIL(nTIL, 1:4) = (/ &
          vertex_index(i  , j  , k  , 4), &
          vertex_index(i  , j-1, k  , 1), &
          vertex_index(i  , j  , k  , 1), &
          vertex_index(i  , j  , k  , 2) &
        /)
        nTIL = nTIL+1

        TIL(nTIL, 1:4) = (/ &
          vertex_index(i  , j  , k  , 4), &
          vertex_index(i  , j  , k  , 1), &
          vertex_index(i  , j  , k-1, 1), &
          vertex_index(i  , j  , k  , 2) &
        /)
        nTIL = nTIL+1

        TIL(nTIL, 1:4) = (/ &
          vertex_index(i  , j  , k  , 4), &
          vertex_index(i  , j  , k-1, 1), &
          vertex_index(i  , j-1, k-1, 1), &
          vertex_index(i  , j  , k  , 2) &
        /)
        nTIL = nTIL+1


        ! Top Face j = j-1
        TIL(nTIL, 1:4) = (/ &
          vertex_index(i  , j-1, k  , 5), &
          vertex_index(i-1, j-1, k-1, 1), &
          vertex_index(i-1, j-1, k  , 1), &
          vertex_index(i  , j  , k  , 2) &
        /)
        nTIL = nTIL+1

        TIL(nTIL, 1:4) = (/ &
          vertex_index(i  , j-1, k  , 5), &
          vertex_index(i-1, j-1, k  , 1), &
          vertex_index(i  , j-1, k  , 1), &
          vertex_index(i  , j  , k  , 2) &
        /)
        nTIL = nTIL+1

        TIL(nTIL, 1:4) = (/ &
          vertex_index(i  , j-1, k  , 5), &
          vertex_index(i  , j-1, k  , 1), &
          vertex_index(i  , j-1, k-1, 1), &
          vertex_index(i  , j  , k  , 2) &
        /)
        nTIL = nTIL+1

        TIL(nTIL, 1:4) = (/ &
          vertex_index(i  , j-1, k  , 5), &
          vertex_index(i  , j-1, k-1, 1), &
          vertex_index(i-1, j-1, k-1, 1), &
          vertex_index(i  , j  , k  , 2) &
        /)
        nTIL = nTIL+1

        ! Bottom Face j = j
        TIL(nTIL, 1:4) = (/ &
          vertex_index(i  , j  , k  , 5), &
          vertex_index(i-1, j  , k-1, 1), &
          vertex_index(i  , j  , k-1, 1), &
          vertex_index(i  , j  , k  , 2) &
        /)
        nTIL = nTIL+1

        TIL(nTIL, 1:4) = (/ &
          vertex_index(i  , j  , k  , 5), &
          vertex_index(i  , j  , k-1, 1), &
          vertex_index(i  , j  , k  , 1), &
          vertex_index(i  , j  , k  , 2) &
        /)
        nTIL = nTIL+1

        TIL(nTIL, 1:4) = (/ &
          vertex_index(i  , j  , k  , 5), &
          vertex_index(i  , j  , k  , 1), &
          vertex_index(i-1, j  , k  , 1), &
          vertex_index(i  , j  , k  , 2) &
        /)
        nTIL = nTIL+1

        TIL(nTIL, 1:4) = (/ &
          vertex_index(i  , j  , k  , 5), &
          vertex_index(i-1, j  , k  , 1), &
          vertex_index(i-1, j  , k-1, 1), &
          vertex_index(i  , j  , k  , 2) &
        /)
        nTIL = nTIL+1
      END IF
    END DO
    END DO
    END DO
  END SUBROUTINE BuildUnitCube

END MODULE Mesh_IO
