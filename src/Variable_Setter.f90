!> This module provides interfaces for exposing to and setting variables from
!> the MScript language.
MODULE Variable_Setter
  USE Command_Parser
  IMPLICIT NONE

  !> A type representing a Variable in MScript and functions for setting
  !> and updating the equivalent object in Fortran.
  TYPE :: VariableSetter
    !> The name of the variable
    CHARACTER(len=200) :: name

    !> The type of the variable
    INTEGER :: variable_type 


    !> A pointer to an Integer variable
    INTEGER,             POINTER :: integer_ptr

    !> A pointer to a Real variable
    REAL(KIND=DP),       POINTER :: real_ptr

    !> A pointer to a function that sets a Real value
    PROCEDURE(RealFunction), POINTER, NOPASS :: real_function => NULL()

    !> A pointer to a Character variable
    CHARACTER(len=1024), POINTER :: character_ptr

    !> A pointer to a Logical variable
    LOGICAL,             POINTER :: logical_ptr


    !> The callback to call when the variable is set.
    PROCEDURE(SetVariableCallback), POINTER, PASS :: callback
  END TYPE VariableSetter


  ! A list of values representing the type of variable held by the
  ! VariableSetter

  !> A variablesetter.variable_type value: No variable type set
  INTEGER, PARAMETER :: UNSET_VARIABLE     = 0

  !> A variablesetter.variable_type value: An Integer variable
  INTEGER, PARAMETER :: INTEGER_VARIABLE   = 1

  !> A variablesetter.variable_type value: A Real variable
  INTEGER, PARAMETER :: REAL_VARIABLE      = 2

  !> A variablesetter.variable_type value: A Real Function variable
  INTEGER, PARAMETER :: REAL_FUNCTION      = 3

  !> A variablesetter.variable_type value: A Character variable
  INTEGER, PARAMETER :: CHARACTER_VARIABLE = 4

  !> A variablesetter.variable_type value: A Logical variable
  INTEGER, PARAMETER :: LOGICAL_VARIABLE   = 5


  ABSTRACT INTERFACE
    !> An interface for a callback routine to run after a variable has been
    !> set with the VariableSetter.
    SUBROUTINE SetVariableCallback(set_variable_parser, ierr)
      !> An interface for a callback routine to run after a variable has been
      !> set with the VariableSetter.
      !>
      !> @param[inout] set_variable_parser The VariablSetter used to set the
      !>                                   variable.
      !> @param[out]   ierr                The result of the parsing and
      !>                                   setting.
      IMPORT VariableSetter
      IMPLICIT NONE

      CLASS(VariableSetter), INTENT(INOUT) :: set_variable_parser
      INTEGER, INTENT(OUT) :: ierr
    END SUBROUTINE SetVariableCallback

    !> An interface for getting and setting values via a Real function.
    FUNCTION realfunction(index, value, ierr)
    !> @brief An interface for getting and setting values via a Real function.
    !>
    !> @returns The value of the function at the given point.
    !> @param[in]  index The index to update the array at.
    !> @param[in]  value The value to update at.
    !> @param[out] ierr  The result of the parse and set.
    !>
    !> This is intended for getters and setters on Real arrays which may
    !> be allocated after the command parser has been initialized, meaning
    !> Pointers to the array can't be used.
    !>
      USE Utils
      REAL(KIND=DP) :: RealFunction
      INTEGER, OPTIONAL, INTENT(IN) :: index
      REAL(KIND=DP), OPTIONAL, INTENT(IN) :: value
      INTEGER, INTENT(OUT) :: ierr
    END FUNCTION realfunction

  END INTERFACE


  !> The list of variable setters.
  TYPE(VariableSetter), ALLOCATABLE :: variable_setters(:)


  !> Interface for adding variables and variable setters to the MScript
  !> language.
  INTERFACE AddVariable
    MODULE PROCEDURE :: &
        AddIntegerVariableSetter, &
        AddRealVariableSetter, AddRealFunctionVariableSetter, &
        AddCharacterVariableSetter, &
        AddLogicalVariableSetter
  END INTERFACE

  CONTAINS


  !>
  !> Initialize variable_setters array with the default variable setters
  !> Requires InitializeCommandParser already initialized
  !>
  SUBROUTINE InitializeVariableSetter()
    IMPLICIT NONE

    CALL DestroyVariableSetter()

    ! Add command 'set' which sets the variables given here
    CALL AddCommandParser("set", ParseSet)
    ! Add command 'SetSubDomain' which sets the variables given here
    ! for a given subdomain
    CALL AddCommandParser("setsubdomain", ParseSetSubDomain)

    ALLOCATE(variable_setters(0))

    CALL AddVariable("pathn",              PathN, SetPathNCallback)
    CALL AddVariable("numberofpathstates", PathN, SetPathNCallback)
    CALL AddVariable( &
      "exchangecalculator", WhichExchange, SetExchangeCalculatorCallback &
    )
    CALL AddVariable("zone",      zone, SetZoneCallback)
    CALL AddVariable("zonevalue", zone, SetZoneCallback)
    CALL AddVariable("zoneinc",       zoneinc)
    CALL AddVariable("zoneincrement", zoneinc)
    CALL AddVariable("mu",           mu)
    CALL AddVariable("permeability", mu)
    CALL AddVariable("lengthscale", Ls, SetLengthScaleCallback)
    CALL AddVariable("ls",          Ls, SetLengthScaleCallback)
    CALL AddVariable("meshscale", Ls, SetMeshScaleCallback)

    ! Usual magnetic parameters
    CALL AddVariable("ms",               SetMs)
    CALL AddVariable("satmagnetization", SetMs)
    CALL AddVariable("exchange",         SetAex)
    CALL AddVariable("aex",              SetAex)
    CALL AddVariable("anisotropy",       SetK1)
    CALL AddVariable("k1",               SetK1)
    CALL AddVariable("k2",               SetK2)

    ! HubertMinimizer parameters
    CALL AddVariable("ftolerance",    FTolerance)
    CALL AddVariable("gtolerance",    GTolerance)
    CALL AddVariable("alphascale",    AlphaScale)
    CALL AddVariable("minalpha",      MinAlpha)
    CALL AddVariable("dalpha",        DAlpha)
    CALL AddVariable("typicalenergy", typicalenergy)

    CALL AddVariable("maxenergyevaluations", MaxEnergyEval)
    CALL AddVariable("maxenergyeval",        MaxEnergyEval)

    ! Magnetization Path parameters
    CALL AddVariable("nebspring",      PHooke)
    CALL AddVariable("springconstant", PHooke)
    CALL AddVariable("curvatureweight", CurveWeight)
    CALL AddVariable("maxrestarts", MaxRestarts)
    CALL AddVariable("maxpathevaluations", MaxPathEval)
    CALL AddVariable("maxpatheval",        MaxPathEval)

    CALL AddVariable("meshnumber",    MaxMeshNumber)
    CALL AddVariable("maxmeshnumber", MaxMeshNumber)
    CALL AddVariable("stem",         stem, SetStemCallback)
    CALL AddVariable("filetemplate", stem, SetStemCallback)
    CALL AddVariable("filestem",     stem, SetStemCallback)

    CALL AddVariable("allexchange", CalcAllExchQ)
  END SUBROUTINE InitializeVariableSetter


  !> Destroy and clean up the variable_setters array.
  SUBROUTINE DestroyVariableSetter()
    IF(ALLOCATED(variable_setters)) DEALLOCATE(variable_setters)
  END SUBROUTINE DestroyVariableSetter


  !> Initialize an empty instance of a VariableSetter
  !>
  !> @param[inout] variable_setter The VariableSetter to initialize
  SUBROUTINE InitializeDefaultVariableSetter(variable_setter)
    IMPLICIT NONE

    TYPE(VariableSetter), INTENT(OUT) :: variable_setter

    variable_setter%name = "NULL"
    variable_setter%integer_ptr   => NULL()
    variable_setter%real_ptr      => NULL()
    variable_setter%real_function => NULL()
    variable_setter%character_ptr => NULL()
    variable_setter%logical_ptr   => NULL()
    variable_setter%variable_type =  UNSET_VARIABLE
    variable_setter%callback      => DefaultSetCallback
  END SUBROUTINE InitializeDefaultVariableSetter


  !> Add an INTEGER variable
  !>
  !> @param[in] variable_name The name of the variable.
  !> @param[in] integer_ptr   The variable to point to.
  !> @param[in] callback      A callback to be called when the variable
  !>                          has been set (optional).
  SUBROUTINE AddIntegerVariableSetter(variable_name, integer_ptr, callback)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: variable_name
    INTEGER, TARGET, INTENT(INOUT) :: integer_ptr
    PROCEDURE(SetVariableCallback), OPTIONAL :: callback

    TYPE(VariableSetter) :: variable_setter

    CALL InitializeDefaultVariableSetter(variable_setter)

    variable_setter%name = variable_name 
    variable_setter%integer_ptr => integer_ptr
    variable_setter%variable_type = INTEGER_VARIABLE
    IF(PRESENT(callback)) variable_setter%callback => callback

    CALL AddVariableSetterInstance(variable_setter)
  END SUBROUTINE AddIntegerVariableSetter


  !> Add a REAL variable
  !>
  !> @param[in] variable_name The name of the variable.
  !> @param[in] real_ptr      The variable to point to.
  !> @param[in] callback      A callback to be called when the variable
  !>                          has been set (optional).
  SUBROUTINE AddRealVariableSetter(variable_name, real_ptr, callback)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: variable_name
    REAL(KIND=DP), TARGET, INTENT(INOUT) :: real_ptr
    PROCEDURE(SetVariableCallback), OPTIONAL :: callback

    TYPE(VariableSetter) :: variable_setter

    CALL InitializeDefaultVariableSetter(variable_setter)

    variable_setter%name = variable_name 
    variable_setter%real_ptr => real_ptr
    variable_setter%variable_type = REAL_VARIABLE
    IF(PRESENT(callback)) variable_setter%callback => callback

    CALL AddVariableSetterInstance(variable_setter)
  END SUBROUTINE AddRealVariableSetter


  !> Add a REAL FUNCTION variable
  !>
  !> @param[in] variable_name    The name of the variable.
  !> @param[in] real_function_cb The function to call to set the variable.
  !> @param[in] callback         A callback to be called when the variable
  !>                             has been set (optional).
  SUBROUTINE AddRealFunctionVariableSetter( &
    variable_name, real_function_cb, callback &
  )
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: variable_name
    PROCEDURE(RealFunction) :: real_function_cb
    PROCEDURE(SetVariableCallback), OPTIONAL :: callback

    TYPE(VariableSetter) :: variable_setter

    CALL InitializeDefaultVariableSetter(variable_setter)

    variable_setter%name = variable_name 
    variable_setter%real_function => real_function_cb
    variable_setter%variable_type = REAL_FUNCTION
    IF(PRESENT(callback)) variable_setter%callback => callback

    CALL AddVariableSetterInstance(variable_setter)
  END SUBROUTINE AddRealFunctionVariableSetter


  !> Add a CHARACTER variable
  !>
  !> @param[in] variable_name The name of the variable.
  !> @param[in] character_ptr The variable to point to.
  !> @param[in] callback      A callback to be called when the variable
  !>                          has been set (optional).
  SUBROUTINE AddCharacterVariableSetter(variable_name, character_ptr, callback)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: variable_name
    CHARACTER(len=1024), TARGET, INTENT(INOUT) :: character_ptr
    PROCEDURE(SetVariableCallback), OPTIONAL :: callback

    TYPE(VariableSetter) :: variable_setter

    CALL InitializeDefaultVariableSetter(variable_setter)

    variable_setter%name = variable_name 
    variable_setter%character_ptr => character_ptr
    variable_setter%variable_type = CHARACTER_VARIABLE
    IF(PRESENT(callback)) variable_setter%callback => callback

    CALL AddVariableSetterInstance(variable_setter)
  END SUBROUTINE AddCharacterVariableSetter


  !> Add a LOGICAL variable
  !>
  !> @param[in] variable_name The name of the variable.
  !> @param[in] logical_ptr   The variable to point to.
  !> @param[in] callback      A callback to be called when the variable
  !>                          has been set (optional).
  SUBROUTINE AddLogicalVariableSetter(variable_name, logical_ptr, callback)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: variable_name
    LOGICAL, TARGET, INTENT(INOUT) :: logical_ptr
    PROCEDURE(SetVariableCallback), OPTIONAL :: callback

    TYPE(VariableSetter) :: variable_setter

    CALL InitializeDefaultVariableSetter(variable_setter)

    variable_setter%name = variable_name 
    variable_setter%logical_ptr => logical_ptr
    variable_setter%variable_type = LOGICAL_VARIABLE
    IF(PRESENT(callback)) variable_setter%callback => callback

    CALL AddVariableSetterInstance(variable_setter)
  END SUBROUTINE AddLogicalVariableSetter


  !> Add an instance of VariableSetterType to the variable_setters array.
  !>
  !> @param[in] variable_setter The instance to add to the variable_setters
  !>                            array.
  SUBROUTINE AddVariableSetterInstance(variable_setter)
    IMPLICIT NONE

    TYPE(VariableSetter), INTENT(IN) :: variable_setter

    TYPE(VariableSetter), ALLOCATABLE :: tmp(:)
    INTEGER :: n_setters

    ALLOCATE(tmp(SIZE(variable_setters)+1))

    n_setters = SIZE(variable_setters)
    IF(n_setters .GT. 0) tmp(1:n_setters) = variable_setters
    CALL MOVE_ALLOC(tmp, variable_setters)
    n_setters = n_setters+1

    variable_setters(n_setters) = variable_setter
  END SUBROUTINE AddVariableSetterInstance


  !> Run the setter in variable_setters with the name variable_name
  !>
  !> @param[in]  variable_name  The name of the variable to set.
  !> @param[in]  variable_index The index on the variable to set.
  !> @param[in]  value_string   The value to parse and set.
  !> @param[out] ierr           The result of the parse and set.
  SUBROUTINE SetVariable(variable_name, variable_index, value_string, ierr)
    USE strings
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: variable_name
    CHARACTER(len=*), INTENT(IN) :: variable_index
    CHARACTER(len=*), INTENT(IN) :: value_string
    INTEGER, INTENT(OUT) :: ierr

    TYPE(VariableSetter) :: variable_setter
    INTEGER :: i

    INTEGER       :: iv
    REAL(KIND=DP) :: dv

    INTEGER :: ios


    ierr = NO_PARSE

    DO i=1,SIZE(variable_setters)
      IF(TRIM(variable_setters(i)%name) .EQ. TRIM(lowercase(variable_name))) THEN
        variable_setter = variable_setters(i)

        SELECT CASE(variable_setter%variable_type)

          CASE(UNSET_VARIABLE)
            WRITE(*,*) "ERROR: Variable ", TRIM(variable_setter%name), "is of unknown type."
            ierr = PARSE_ERROR

          CASE(INTEGER_VARIABLE)
            CALL value(value_string, iv, ios)
            IF( ios == 0 ) THEN
              variable_setter%integer_ptr = iv
              ierr = PARSE_SUCCESS
            ELSE
              WRITE(*,*) "Error: Unable to parse integer from ", value_string
              ierr = PARSE_ERROR
            END IF

          CASE(REAL_VARIABLE)
            CALL value(value_string, dv, ios)
            IF( ios == 0 ) THEN
              variable_setter%real_ptr = dv
              ierr = PARSE_SUCCESS
            ELSE
              WRITE(*,*) "Error: Unable to parse real from ", value_string
              ierr = PARSE_ERROR
            END IF

          CASE(REAL_FUNCTION)
            CALL value(value_string, dv, ios)

            IF( ios == 0 ) THEN
              IF(TRIM(variable_index) .EQ. ':') THEN
                dv = variable_setter%real_function(value=dv, ierr=ierr)
              ELSE
                CALL value(variable_index, iv, ios)
                IF( ios == 0 ) THEN
                  dv = variable_setter%real_function(index=iv, value=dv, ierr=ierr)
                END IF
              END IF
            ELSE
              WRITE(*,*) "Error: Unable to parse real from ", value_string
              ierr = PARSE_ERROR
            END IF

          CASE(CHARACTER_VARIABLE)
            variable_setter%character_ptr = value_string
            ierr = PARSE_SUCCESS

          CASE(LOGICAL_VARIABLE)
            CALL value(value_string, iv, ios)
            IF( ios == 0 ) THEN
              IF( iv > 0 ) THEN
                variable_setter%logical_ptr = .TRUE.
              ELSE
                variable_setter%logical_ptr = .FALSE.
              END IF
              ierr = PARSE_SUCCESS
            ELSE
              WRITE(*,*) "Error: Unable to parse integer/logical from ", value_string
              ierr = PARSE_ERROR
            END IF
        END SELECT

        IF(ASSOCIATED(variable_setter%callback)) THEN
          CALL variable_setter%callback(ierr)
        END IF

        EXIT
      END IF
    END DO

    IF( ierr .EQ. NO_PARSE ) THEN
      WRITE(*,*) "Unable to find variable ", TRIM(variable_name), " to set"
      ierr = PARSE_ERROR
    ELSE IF( ierr .EQ. PARSE_ERROR ) THEN
      WRITE(*,*) "Error setting variable ", TRIM(variable_name)
      ierr = PARSE_ERROR
    END IF
  END SUBROUTINE SetVariable


  !> Parser to hook into Command_Parser to add a `Set` command to MScript.
  !>
  !> MScript example to set a variable called `MyVariable`.
  !> @code
  !>    Set MyVariable %value
  !>    Set MyVariable %value sd = n
  !> @endcode
  !>
  !> @param[in]  args The arguments to parse
  !> @param[out] ierr The result of the parse and set.
  SUBROUTINE ParseSet(args, ierr)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    CHARACTER(len=LEN(args)) :: idx_str
    INTEGER :: sd
    LOGICAL :: l

    IF(SIZE(args) .GE. 3) THEN
      IF(SIZE(args) .EQ. 6) THEN
        sd = ParseSubdomain(args(4:6), ierr)

        IF(ierr .NE. PARSE_ERROR) THEN
          idx_str = ""
          WRITE(idx_str, *) sd
          CALL SetVariable(args(2), idx_str, args(3), ierr)
        ELSE
          RETURN
        END IF
      ELSE IF(AssertNArgs(args, 3)) THEN
        CALL SetVariable(args(2), ':', args(3), ierr)
      ELSE
        ierr = PARSE_ERROR
      END IF
    ELSE
      l = AssertNArgs(args, 3)
      ierr = PARSE_ERROR
    END IF
  END SUBROUTINE ParseSet


  !> A parser for the `SetSubdomain` command
  !>
  !> MScript example to set the variable `VarName` to the value `1.5e6`
  !> on the subdomain `2`
  !> @code
  !>    SetSubDomain 2 VarName 1.5e6
  !> @endcode
  !>
  !> @param[in]  args The arguments to parse.
  !> @param[out] ierr The result of the parse and set.
  SUBROUTINE ParseSetSubDomain(args, ierr)
    USE strings
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    CHARACTER(len=(LEN(args))) :: idx_str
    INTEGER :: sd_id
    INTEGER :: i

    IF(AssertNArgs(args, 4)) THEN
      ! Find Subdomain for given SubDomainId
      ! Replace sd_id with the subdomain index
      CALL value(args(2), sd_id, ierr)
      DO i=1,SIZE(SubDomainIds)
        IF(sd_id .EQ. SubDomainIds(i)) THEN
          sd_id = i
          EXIT
        END IF
      END DO

      ! Convert sd_id to string
      idx_str = ""
      WRITE(idx_str, *) sd_id

      CALL SetVariable(args(3), idx_str, args(4), ierr)
    ELSE
      ierr = PARSE_ERROR
    END IF
  END SUBROUTINE ParseSetSubDomain


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! VariableSetter callbacks                                                   !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  !> A default callback for a VariableSetter, which does nothing.
  !>
  !> @param[in]  variable_setter The variable setter which set the variable.
  !> @param[out] ierr            The result of the callback.
  SUBROUTINE DefaultSetCallback(variable_setter, ierr)
    IMPLICIT NONE

    CLASS(VariableSetter), INTENT(INOUT) :: variable_setter
    INTEGER, INTENT(OUT) :: ierr

    ierr = PARSE_SUCCESS
  END SUBROUTINE DefaultSetCallback


  !> The callback for setting PathN which calls PathAllocate().
  !>
  !> @param[in]  variable_setter The variable setter which set the variable.
  !> @param[out] ierr            The result of the callback.
  SUBROUTINE SetPathNCallback(variable_setter, ierr)
    IMPLICIT NONE

    CLASS(VariableSetter), INTENT(INOUT) :: variable_setter
    INTEGER, INTENT(OUT) :: ierr

    CALL PathAllocate()

    ierr = PARSE_SUCCESS
  END SUBROUTINE


  !> The callback for setting the exchange calculator.
  !> Tests whether a valid exchange calculator was chosen.
  !> Currently broken and does nothing.
  !>
  !> @param[in]  variable_setter The variable setter which set the variable.
  !> @param[out] ierr            The result of the callback.
  SUBROUTINE SetExchangeCalculatorCallback(variable_setter, ierr)
    IMPLICIT NONE

    CLASS(VariableSetter), INTENT(INOUT) :: variable_setter
    INTEGER, INTENT(OUT) :: ierr

    INTEGER :: i

    !> @todo FIXME
    !i = variable_setters%integer_ptr
    i = 1

    IF((i > 0) .AND. (i < 5)) THEN
      ierr = PARSE_SUCCESS
    ELSE
      ierr = PARSE_ERROR
    END IF
  END SUBROUTINE SetExchangeCalculatorCallback


  !> The callback for setting the Zone variable.
  !> Sets `zoneflag` to -1.
  !>
  !> @param[in]  variable_setter The variable setter which set the variable.
  !> @param[out] ierr            The result of the callback.
  SUBROUTINE SetZoneCallback(variable_setter, ierr)
    IMPLICIT NONE

    CLASS(VariableSetter), INTENT(INOUT) :: variable_setter
    INTEGER, INTENT(OUT) :: ierr

    zoneflag=-1.0 ! Flag for using zone in WriteTecPlot()

    ierr = PARSE_SUCCESS
  END SUBROUTINE


  !> Callback for setting the length scale.
  !> Takes the value that was set and changes it to the format actually
  !> used by MERRILL. Also updates the `EnergyUnit` variable.
  !>
  !> @param[in]  variable_setter The variable setter which set the variable.
  !> @param[out] ierr            The result of the callback.
  SUBROUTINE SetLengthScaleCallback(variable_setter, ierr)
    IMPLICIT NONE

    CLASS(VariableSetter), INTENT(INOUT) :: variable_setter
    INTEGER, INTENT(OUT) :: ierr

    REAL(KIND=DP) :: Ls

    Ls = variable_setter%real_ptr
    EnergyUnit= Kd*total_volume/(Ls**(3.0/2.0))

    ierr = PARSE_SUCCESS
  END SUBROUTINE SetLengthScaleCallback


  !> Callback for setting the length scale after setting the `MeshScale`
  !> variable.
  !> Updates `Ls` and `EnergyUnit` to the appropriate values.
  !>
  !> @param[in]  variable_setter The variable setter which set the variable.
  !> @param[out] ierr            The result of the callback.
  SUBROUTINE SetMeshScaleCallback(variable_setter, ierr)
    IMPLICIT NONE

    CLASS(VariableSetter), INTENT(INOUT) :: variable_setter
    INTEGER, INTENT(OUT) :: ierr

    REAL(KIND=DP) :: LInv
    REAL(KIND=DP) :: Ls

    LInv = variable_setter%real_ptr 
    variable_setter%real_ptr = 1./(LInv*LInv)  !!  Square !
    Ls = variable_setter%real_ptr
    EnergyUnit= Kd*total_volume/(Ls**(3.0/2.0))

    ierr = PARSE_SUCCESS
  END SUBROUTINE SetMeshScaleCallback


  !> Asserts the given index is within the given array indices.
  !> Prints an error and returns a `PARSE_ERROR` if it isn't.
  !>
  !> @param[in]  idx   The index to test.
  !> @param[in]  array The array whose bounds we want to check
  !> @param[out] ierr  The result of the check
  !> @returns Whether `idx` is a valid index for `array`.
  LOGICAL FUNCTION AssertIndexInRange(idx, array, ierr)
    INTEGER, INTENT(IN) :: idx
    REAL(KIND=DP), INTENT(IN) :: array(:)
    INTEGER, INTENT(OUT) :: ierr

    IF(idx .GE. LBOUND(array,1) .AND. idx .LE. UBOUND(array,1)) THEN
      AssertIndexInRange = .TRUE.
      ierr = PARSE_SUCCESS
    ELSE
      WRITE(*,*) "index out of bound:"
      WRITE(*,*) "  requested: ", idx
      WRITE(*,*) "  bounds: ", LBOUND(array), UBOUND(array)
      AssertIndexInRange = .FALSE.
      ierr = PARSE_ERROR
    END IF
  END FUNCTION AssertIndexInRange


  !> Get/Set an array at the given index with the given value.
  !> If no index is given, set all the entries of the array to the given value.
  !> If no value is given, return the value at the given index.
  !>
  !> @param[in]  array The array to set.
  !> @param[in]  index The index for the array where we want to set. (optional)
  !> @param[in]  value The value we want to set. (optional)
  !> @param[out] ierr  The result of the indexing and setting.
  !> @returns The value of `array` at index `index`. If `index` has been
  !>          omitted, return the first value in `array`.
  REAL(KIND=DP) FUNCTION SetArray(array, index, value, ierr)
    REAL(KIND=DP), INTENT(INOUT) :: array(:)
    INTEGER, OPTIONAL, INTENT(IN) :: index
    REAL(KIND=DP), OPTIONAL, INTENT(IN) :: value
    INTEGER, INTENT(OUT) :: ierr

    ierr = PARSE_SUCCESS

    ! Handle setting the array
    IF(PRESENT(value)) THEN
      IF(PRESENT(index)) THEN
        IF(AssertIndexInRange(index, Array, ierr)) THEN
          Array(index) = value
        ELSE
          ierr = PARSE_ERROR
          RETURN
        END IF
      ELSE
        Array = value
      END IF
    END IF

    ! Handle the return value
    IF(PRESENT(index)) THEN
      IF(AssertIndexInRange(index, Array, ierr)) THEN
        SetArray = Array(index)
      ELSE
        ierr = PARSE_ERROR
        RETURN
      END IF
    ELSE
      SetArray = Array(LBOUND(Array,1))
    END IF
  END FUNCTION SetArray


  !> A RealFunction for setting `Ms`.
  !>
  !> @param[in]  index The subdomain to set `Ms` for.
  !> @param[in]  value The value to set `Ms` to.
  !> @param[out] ierr The result of setting.
  !> @returns The value for `Ms` at `index`.
  REAL(KIND=DP) FUNCTION SetMs(index, value, ierr)
    INTEGER, OPTIONAL, INTENT(IN) :: index
    REAL(KIND=DP), OPTIONAL, INTENT(IN) :: value
    INTEGER, INTENT(OUT) :: ierr

    SetMs = SetArray(Ms, index, value, ierr)

    IF(PRESENT(value)) THEN
      Kd = mu*MAXVAL(Ms)**2*0.5
      EnergyUnit = Kd*total_volume/(Ls**(3.0/2.0))
      IF(NONZERO(Aex(MAXLOC(Ms,1)))) LambdaEx = SQRT(Aex(MAXLOC(Ms,1))/Kd)
    END IF
  END FUNCTION SetMs


  !> A RealFunction for setting `Aex`.
  !>
  !> @param[in]  index The subdomain to set `Aex` for.
  !> @param[in]  value The value to set `Aex` to.
  !> @param[out] ierr The result of setting.
  !> @returns The value for `Aex` at `index`.
  REAL(KIND=DP) FUNCTION SetAex(index, value, ierr)
    INTEGER, OPTIONAL, INTENT(IN) :: index
    REAL(KIND=DP), OPTIONAL, INTENT(IN) :: value
    INTEGER, INTENT(OUT) :: ierr

    ierr = PARSE_SUCCESS
    SetAex = SetArray(Aex, index, value, ierr)

    IF(PRESENT(value)) THEN
      IF(NONZERO(Kd)) LambdaEx= SQRT(Aex(MAXLOC(Ms,1))/Kd)
    END IF
  END FUNCTION SetAex


  !> A RealFunction for setting `K1`.
  !>
  !> @param[in]  index The subdomain to set `K1` for.
  !> @param[in]  value The value to set `K1` to.
  !> @param[out] ierr The result of setting.
  !> @returns The value for `K1` at `index`.
  REAL(KIND=DP) FUNCTION SetK1(index, value, ierr)
    INTEGER, OPTIONAL, INTENT(IN) :: index
    REAL(KIND=DP), OPTIONAL, INTENT(IN) :: value
    INTEGER, INTENT(OUT) :: ierr

    ierr = PARSE_SUCCESS
    SetK1 = SetArray(K1, index, value, ierr)

    IF(PRESENT(value)) THEN
      IF(NONZERO(Kd)) QHardness = K1(MAXLOC(Ms,1))/Kd
    END IF
  END FUNCTION SetK1


  !> A RealFunction for setting `K2`.
  !>
  !> @param[in]  index The subdomain to set `K2` for.
  !> @param[in]  value The value to set `K2` to.
  !> @param[out] ierr The result of setting.
  !> @returns The value for `K2` at `index`.
  REAL(KIND=DP) FUNCTION SetK2(index, value, ierr)
    INTEGER, OPTIONAL, INTENT(IN) :: index
    REAL(KIND=DP), OPTIONAL, INTENT(IN) :: value
    INTEGER, INTENT(OUT) :: ierr

    ierr = PARSE_SUCCESS
    SetK2 = SetArray(K2, index, value, ierr)
  END FUNCTION SetK2


  !> The callback for the `stem` variable.
  !> This sets the `datafile` variable to the appropriate `stem` value.
  !>
  !> @param[in]  variable_setter The variable setter which set the variable.
  !> @param[out] ierr            The result of the callback.
  SUBROUTINE SetStemCallback(variable_setter, ierr)
    IMPLICIT NONE

    CLASS(VariableSetter), INTENT(INOUT) :: variable_setter
    INTEGER, INTENT(OUT) :: ierr

    datafile=stem(:LEN_TRIM(variable_setter%character_ptr))//'.dat'

    ierr = PARSE_SUCCESS
  END SUBROUTINE SetStemCallback

END MODULE Variable_Setter
