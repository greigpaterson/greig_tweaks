!> The Material_Parameters module contains variables and functions defining the
!> various physical constants for magnetic materials, along with the
!> energies and H-fields.
MODULE Material_Parameters

  USE Utils
  USE Tetrahedral_Mesh_Data

  IMPLICIT NONE
  SAVE

  !        Input from   .consts file variables

  ! Temperature T and Curie Temperature TC
  REAL(KIND=DP)  T, TC

  REAL(KIND=DP)  extappst,extappfin,extappstep
  REAL(KIND=DP)  hz(3)
  CHARACTER (LEN=16) ::  solver
  INTEGER MF,JACFLG,JPRE,MAXORD,IZONE


  INTEGER :: rest, MaxRestarts, MaxEnergyEval, MaxPathEval, WhichExchange

  !        Material parameters

  INTEGER :: NMaterials
  REAL(KIND=DP), TARGET, ALLOCATABLE :: Ms(:), K1(:), K2(:), Aex(:)
  REAL(KIND=DP) :: Ls, mu, Kd, LambdaEx, QHardness, EnergyUnit


  ! Anisotropy forms

  INTEGER, ALLOCATABLE :: anisform(:)

  INTEGER, PARAMETER :: &
    ANISFORM_CUBIC = 1, &
    ANISFORM_UNIAXIAL = 2

  ! Easy Axes for anisotropy
  ! EasyAxis(3,NMaterials)  -> Uniaxial Anisotropy
  !   EasyAxis((x,y,z), sd)
  ! CubicAxes(3,3,NMaterials) -> Cubic Anisotropy
  !   CubicAxes((x,y,z), n, sd) where n enumerates axis n

  REAL(KIND=DP), ALLOCATABLE :: EasyAxis(:,:), CubicAxes(:,:,:)

  !        Time bookkeeping

  REAL(KIND=DP) cputime_now,cputime_start , cpuold
  INTEGER  elapsed_start, elapsed_now, elapsedold


  !        Problem size (usually = NNODES or 2*NNODES )

  ! External field strength
  REAL(KIND=DP) extapp


  !        Effective field contributions

  REAL(KIND=DP)  magx,magy,magz,hznorm,eanorm


  REAL(KIND=DP)                              &
    DemagEnerg, BEnerg,                      &
    AnisEnerg, AnisEnerg1, AnisEnerg2,       &
    ExchangeE, ExEnerg2, ExEnerg3, ExEnerg4

  REAL(KIND=DP), ALLOCATABLE ::                         &
    htot(:,:),                                          &
    hanis(:,:), hdemag(:,:), hext(:,:),                 &
    hexch(:,:), hexch2(:,:), hexch3(:,:) , hexch4(:,:)

  REAL(KIND=DP), ALLOCATABLE :: DExPhi(:), DExTheta(:)


CONTAINS


  !---------------------------------------------------------------
  !            Evaluate program arguments
  !---------------------------------------------------------------

  ! Initialize all variables in this module
  SUBROUTINE InitializeMaterialParameters()

    CALL DestroyMaterialParameters()

    ! Unused
    ! mcon(6), T, ag, extappst ,extappfin, extappstep
    ! solver
    ! MF,JACFLG,JPRE,MAXORD
    ! magx,magy,magz,
    ! cputime_now,cputime_start , cpuold
    ! elapsed_start, elapsed_now, elapsedold

    ! External field
    hz = 0.0


    ! Minimizer parameters
    rest = 0
    MaxRestarts=4
    MaxEnergyEval=100000
    MaxPathEval=10000
    WhichExchange = 1 ! Laplace


    !
    ! Initializing multiphase dependent variables
    !

    ! We want Magnetite(t) to work as expected before BuildMaterialParameters
    ! is called.

    ! Material parameters
    NMaterials = 1

    ! Saturation Magnetization, Anisotropy, Exchange constants.
    ALLOCATE(Ms(1), K1(1), K2(1), Aex(1))

    Ms  = 0.0
    K1  = 0.0
    K2  = 0.0
    Aex = 0.0


    ! Anisotropy Axes
    ALLOCATE(anisform(1), EasyAxis(3,1), CubicAxes(3,3,1))

    ! Anisotropy forms
    anisform(:) = ANISFORM_CUBIC

    ! Uniaxial axis
    EasyAxis(:,1) = (/ 1.0, 0.0, 0.0 /)
    
    ! Cubic Axes
    CubicAxes(:,:,:) = 0.0
    CubicAxes(1,1,:) = 1.0
    CubicAxes(2,2,:) = 1.0
    CubicAxes(3,3,:) = 1.0


    ! Default length scale in microns. Ls = length scale squared.
    Ls = 1e12
    ! Permeability of free space 4 pi 10^-7
    mu = 4*(4*atan(REAL(1,KIND=DP)))*1e-7

    Kd = 0.0
    LambdaEx   = 0.0
    QHardness  = 0.0
    EnergyUnit = 0.0


    ! !        Problem size (usually = NNODES or 2*NNODES )

    ! External field strength
    extapp=0.0

    ! !        Effective field contributions

    ! REAL(KIND=DP)  hznorm,eanorm


    AnisEnerg  = 0.0
    DemagEnerg = 0.0
    BEnerg     = 0.0
    ExchangeE  = 0.0
    ExEnerg2   = 0.0
    ExEnerg3   = 0.0
    ExEnerg4   = 0.0
  END SUBROUTINE InitializeMaterialParameters

  SUBROUTINE DestroyMaterialParameters()
    IF(ALLOCATED(Ms))  DEALLOCATE(Ms)
    IF(ALLOCATED(K1))  DEALLOCATE(K1)
    IF(ALLOCATED(K2))  DEALLOCATE(K2)
    IF(ALLOCATED(Aex)) DEALLOCATE(Aex)

    IF(ALLOCATED(anisform))  DEALLOCATE(anisform)
    IF(ALLOCATED(EasyAxis))  DEALLOCATE(EasyAxis)
    IF(ALLOCATED(CubicAxes)) DEALLOCATE(CubicAxes)

    IF(ALLOCATED(htot))     DEALLOCATE(htot)
    IF(ALLOCATED(hanis))    DEALLOCATE(hanis)
    IF(ALLOCATED(hdemag))   DEALLOCATE(hdemag)
    IF(ALLOCATED(hext))     DEALLOCATE(hext)
    IF(ALLOCATED(hexch))    DEALLOCATE(hexch)
    IF(ALLOCATED(hexch2))   DEALLOCATE(hexch2)
    IF(ALLOCATED(hexch3))   DEALLOCATE(hexch3)
    IF(ALLOCATED(hexch4))   DEALLOCATE(hexch4)
    IF(ALLOCATED(DExPhi))   DEALLOCATE(DExPhi)
    IF(ALLOCATED(DExTheta)) DEALLOCATE(DExTheta)
  END SUBROUTINE DestroyMaterialParameters

  SUBROUTINE BuildMaterialParameters()
    ! Set NMaterials from number of SubDomainIds
    NMaterials = SIZE(SubDomainIds)

    ! Saturation Magnetization, Anisotropy, Exchange constants.
    CALL ReAlloc1(Ms,  NMaterials)
    CALL ReAlloc1(K1,  NMaterials)
    CALL ReAlloc1(K2,  NMaterials)
    CALL ReAlloc1(Aex, NMaterials)

    CALL ReAlloci1(anisform,  NMaterials)
    CALL ReAlloc2(EasyAxis,  NMaterials)
    CALL ReAlloc3(CubicAxes, NMaterials)

    CALL FieldAllocate()
  CONTAINS
    SUBROUTINE ReAlloc1(target, new_size)
      REAL(KIND=DP), ALLOCATABLE, INTENT(INOUT) :: target(:)
      INTEGER, INTENT(IN) :: new_size

      REAL(KIND=DP), ALLOCATABLE :: tmp(:)

      IF(ALLOCATED(target)) THEN
        CALL MOVE_ALLOC(target, tmp)
        ALLOCATE(target(new_size))
        target = tmp(1)
        target(1:MIN(new_size, SIZE(tmp))) = tmp(1:MIN(new_size, SIZE(tmp)))
      ELSE
        WRITE(*,*) "ERROR: ReAlloc called on unallocated variable!"
        WRITE(*,*) "Called in BuildMaterialParameters"
        ERROR STOP
      END IF
    END SUBROUTINE ReAlloc1

    SUBROUTINE ReAlloci1(target, new_size)
      INTEGER, ALLOCATABLE, INTENT(INOUT) :: target(:)
      INTEGER, INTENT(IN) :: new_size

      INTEGER, ALLOCATABLE :: tmp(:)

      IF(ALLOCATED(target)) THEN
        CALL MOVE_ALLOC(target, tmp)
        ALLOCATE(target(new_size))
        target = tmp(1)
        target(1:MIN(new_size, SIZE(tmp))) = tmp(1:MIN(new_size, SIZE(tmp)))
      ELSE
        WRITE(*,*) "ERROR: ReAlloc called on unallocated variable!"
        WRITE(*,*) "Called in BuildMaterialParameters"
        ERROR STOP
      END IF
    END SUBROUTINE ReAlloci1

    SUBROUTINE ReAlloc2(target, new_size)
      REAL(KIND=DP), ALLOCATABLE, INTENT(INOUT) :: target(:,:)
      INTEGER, INTENT(IN) :: new_size

      REAL(KIND=DP), ALLOCATABLE :: tmp(:,:)
      INTEGER :: i

      IF(ALLOCATED(target)) THEN
        CALL MOVE_ALLOC(target, tmp)
        ALLOCATE(target(SIZE(tmp, DIM=1), new_size))
        FORALL(i=1:SIZE(target, DIM=1)) target(i,:) = tmp(i,1)
        target(:, 1:MIN(new_size, SIZE(tmp, DIM=2))) &
          = tmp(:, 1:MIN(new_size, SIZE(tmp, DIM=2)))
      ELSE
        WRITE(*,*) "ERROR: ReAlloc called on unallocated variable!"
        WRITE(*,*) "Called in BuildMaterialParameters"
        ERROR STOP
      END IF
    END SUBROUTINE ReAlloc2

    SUBROUTINE ReAlloc3(target, new_size)
      REAL(KIND=DP), ALLOCATABLE, INTENT(INOUT) :: target(:,:,:)
      INTEGER, INTENT(IN) :: new_size

      REAL(KIND=DP), ALLOCATABLE :: tmp(:,:,:)
      INTEGER :: i, j

      IF(ALLOCATED(target)) THEN
        CALL MOVE_ALLOC(target, tmp)
        ALLOCATE(target(SIZE(tmp, DIM=1), SIZE(tmp, DIM=2), new_size))
        FORALL(i=1:SIZE(target, DIM=1), j=1:SIZE(target, DIM=2))
          target(i, j, :) = tmp(i, j, 1)
        END FORALL
        target(:, :, 1:MIN(new_size, SIZE(tmp, DIM=3))) &
          = tmp(:, :, 1:MIN(new_size, SIZE(tmp, DIM=3)))
      ELSE
        WRITE(*,*) "ERROR: ReAlloc called on unallocated variable!"
        WRITE(*,*) "Called in BuildMaterialParameters"
        ERROR STOP
      END IF
    END SUBROUTINE ReAlloc3
  END SUBROUTINE BuildMaterialParameters


  !---------------------------------------------------------------
  !            SetRotationMatrix
  !---------------------------------------------------------------

  SUBROUTINE SetRotationMatrix(phirot, thetarot, alpharot, sd)
    IMPLICIT NONE

    REAL(KIND=DP), INTENT(IN) :: phirot, thetarot, alpharot
    INTEGER, OPTIONAL, INTENT(IN) :: sd

    INTEGER :: sd_min, sd_max
    INTEGER :: i

    IF(PRESENT(sd)) THEN
      sd_min = sd
      sd_max = sd
    ELSE
      sd_min = 1
      sd_max = NMaterials
    END IF

    DO i=sd_min, sd_max
      CubicAxes(1,1,i) =  dcos(phirot)*dcos(thetarot)
      CubicAxes(2,1,i) = -dsin(phirot)*dcos(alpharot) &
                          + dcos(phirot)*dsin(thetarot)*dsin(alpharot)
      CubicAxes(3,1,i) = -dsin(phirot)*dsin(alpharot) &
                          - dsin(thetarot)*dcos(phirot)*dcos(alpharot)

      CubicAxes(1,2,i) = dsin(phirot)*dcos(thetarot)
      CubicAxes(2,2,i) = dcos(phirot)*dcos(alpharot) &
                          + dsin(phirot)*dsin(thetarot)*dsin(alpharot)
      CubicAxes(3,2,i) = dcos(phirot)*dsin(alpharot) &
                          - dsin(phirot)*dsin(thetarot)*dcos(alpharot)

      CubicAxes(1,3,i) =  dsin(thetarot)
      CubicAxes(2,3,i) = -dcos(thetarot)*dsin(alpharot)
      CubicAxes(3,3,i) =  dcos(thetarot)*dcos(alpharot)
    END DO

    WRITE(*,*) "CubicAxes:"
    DO i=1,NMaterials
      WRITE(*,*) "SD: ", i
      WRITE(*,*) "  ", CubicAxes(:,1,i)
      WRITE(*,*) "  ", CubicAxes(:,2,i)
      WRITE(*,*) "  ", CubicAxes(:,3,i)
    END DO
  END SUBROUTINE SetRotationMatrix


  !---------------------------------------------------------------
  !          FieldAllocate
  !---------------------------------------------------------------

  SUBROUTINE FieldAllocate()
    IMPLICIT NONE
    REAL(KIND=DP) :: hznorm, eanorm
    INTEGER :: i

    IF(NNODE<1) THEN
      write(*,*) ' ERROR ALLOCATING FIELDS: NNODE=',NNODE
      STOP
    ENDIF

    IF(ALLOCATED(hdemag)) DEALLOCATE(hdemag)
    IF(ALLOCATED(htot))   DEALLOCATE(htot)
    IF(ALLOCATED(hdemag)) DEALLOCATE(hdemag)
    IF(ALLOCATED(hanis))  DEALLOCATE(hanis)
    IF(ALLOCATED(hext))   DEALLOCATE(hext)
    IF(ALLOCATED(hexch))  DEALLOCATE(hexch)
    IF(ALLOCATED(hexch2)) DEALLOCATE(hexch2)
    IF(ALLOCATED(hexch3)) DEALLOCATE(hexch3)
    IF(ALLOCATED(hexch4)) DEALLOCATE(hexch4)
    
    IF(ALLOCATED(DExPhi))   DEALLOCATE(DExPhi)
    IF(ALLOCATED(DExTheta)) DEALLOCATE(DExTheta)

    ALLOCATE(                                                            &
      htot(NNODE,3),                                                     &
      hdemag(NNODE,3), hanis(NNODE,3),  hext(NNODE,3),                   &
      hexch(NNODE,3),  hexch2(NNODE,3), hexch3(NNODE,3), hexch4(NNODE,3) &
    )
    ALLOCATE(DExPhi(NNODE), DExTheta(NNODE))

    htot   = 0.0
    hdemag = 0.0
    hanis  = 0.0
    hext   = 0.0
    hexch  = 0.0
    hexch2 = 0.0
    hexch3 = 0.0
    hexch4 = 0.0

    DExPhi   = 0.0
    DExTheta = 0.0


    !---------------------------------------------------------------
    ! Normalize external field and anisotropy direction vectors.
    !---------------------------------------------------------------

    hznorm = SQRT(hz(1)**2 + hz(2)**2 + hz(3)**2)
    IF(hznorm > 0.0) THEN
      hz(1) = hz(1)/hznorm
      hz(2) = hz(2)/hznorm
      hz(3) = hz(3)/hznorm
    ENDIF

    DO i=1,NMaterials
      eanorm = SQRT(EasyAxis(1,i)**2 + EasyAxis(2,i)**2 + EasyAxis(3,i)**2)
      IF(eanorm > 0.0) THEN
        EasyAxis(1,i) = EasyAxis(1,i)/eanorm
        EasyAxis(2,i) = EasyAxis(2,i)/eanorm
        EasyAxis(3,i) = EasyAxis(3,i)/eanorm
      ENDIF
    END DO

  END SUBROUTINE FieldAllocate


  !---------------------------------------------------------------
  ! SUBROUTINE Magnetite( TCelsius)
  !        defines temperature dependent material constants
  !        for magnetite above room temperature
  !---------------------------------------------------------------

  SUBROUTINE Magnetite(TCelsius)
    IMPLICIT NONE
    REAL(KIND=DP) :: TCelsius


    T=TCelsius
    TC=580 ! Curie temperature of magnetite in Celsius

    if(T<0) write(*,*) 'Warning :: Magnetite material constants for &
        &T<0 deg Celsius are not reliable !! T=',TCelsius
    if(T.LE.TC) then
      anisform = ANISFORM_CUBIC
      Aex = (SQRT( 21622.526+816.476*(TC-T))-147.046 )/408.238e11
      Ms = 737.384*51.876*(TC-T)**0.4
      K1 = -2.13074e-5*(TC-T)**3.2
      Kd = mu*MAXVAL(Ms)*MAXVAL(Ms)*0.5
      LambdaEx = SQRT(Aex(MAXLOC(Ms,1))/Kd)
      QHardness = K1(MAXLOC(Ms,1))/Kd
    else
      write(*,*) 'Warning :: Magnetite material constants for &
        &T>580 deg Celsius are ZERO T=',TCelsius
      anisform = ANISFORM_CUBIC
      Aex = 0
      Ms  = 0
      K1  = 0
    endif
  END SUBROUTINE Magnetite

  !---------------------------------------------------------------
  ! SUBROUTINE Magnetite( TCelsius)
  !        defines temperature dependent material constants
  !        for magnetite above room temperature
  !---------------------------------------------------------------

  SUBROUTINE TM54(TCelsius)
    IMPLICIT NONE
    REAL(KIND=DP) :: TCelsius
! Data for TM54 provided by Evgeniya Khakhalova, Institute of Rock Mag, Minnesota, 2018

    T=TCelsius
    TC=216 ! Curie temperature of TM54 in Celsius

    if(T<0) write(*,*) 'Warning :: TM54 material constants for &
        &T<0 deg Celsius are not reliable !! T=',TCelsius
    if(T.LE.TC) then
      anisform = ANISFORM_CUBIC
      Aex =1.004E-22*T**5 - 5.150E-20*T**4 + 1.013E-17*T**3 - 1.009E-15*T**2 &
      + 8.858E-15*T + 8.007E-12


      Ms = -1.0256E-05*T**4 - 1.4336E-02*T**3 + 2.3745E+00*T**2 &
      - 4.7881E+02*T + 1.6958E+05



      K1 = 8.656E-07*T**4 - 4.561E-04*T**3 + 4.585E-02*T**2 + 8.620E+00*T - 1.293E+03


      Kd = mu*MAXVAL(Ms)*MAXVAL(Ms)*0.5
      
      LambdaEx = SQRT(Aex(MAXLOC(Ms,1))/Kd)
      QHardness = K1(MAXLOC(Ms,1))/Kd
    else
      write(*,*) 'Warning :: TM54 material constants for &
        &T>212 deg Celsius are ZERO T=',TCelsius
      anisform = ANISFORM_CUBIC
      Aex = 0
      Ms  = 0
      K1  = 0
    endif
  END SUBROUTINE TM54
  
  

  !---------------------------------------------------------------
  ! SUBROUTINE Iron( TCelsius)
  !        defines temperature dependent material constants
  !        for iron above room temperature
  !---------------------------------------------------------------

  SUBROUTINE Iron(TCelsius)
    IMPLICIT NONE
    REAL(KIND=DP) :: TCelsius


    ! Temperatures T and TC in Kelvin here

    T=TCelsius + 273 ! polynomial expressions for iron use Temperature in Kelvin
    TC=1044 ! Curie temperature of iron in Kelvin

    if(T<273) write(*,*) 'Warning :: Iron material constants for T<0 deg Celsius are not reliable !! T=',TCelsius
    if(T.LE.TC) then
      anisform = ANISFORM_CUBIC

      Aex=-1.8952e-12+(3.0657e-13)*T - (1.599e-15)*(T**2) + (4.0151e-18)*(T**3) &
        - (5.3728e-21)*(T**4) + (3.6501e-24)*(T**5) - (9.9515e-28)*(T**6)

      Ms= 1.75221e6 - (1.21716e3)*T+(33.3368)*(T**2) -(0.363228)*(T**3) + (1.96713e-3)*(T**4) &
        - (5.98015e-6)*(T**5) + (1.06587e-8)*(T**6) - (1.1048e-11)*(T**7) + (6.16143e-15)*(T**8) &
        - (1.42904e-18)*(T**9)


      ! 4 Sept 2016: k1 is derived from fig 7.24 in Culity And Graham 'Intro to magnetic materials 2011.
      ! however the data is form Honda et al 1928. More recent measurements give room temp vales a bit higher (Graham 1958)
      ! so we scale the temperature dependent values below to give yield the more recent value 4.8e4 at room temp.


      k1=54967.1 + 44.2946*T - 0.426485*(T**2) + 0.000811152*(T**3) - (1.07579e-6)*(T**4) &
        + (8.83207e-10)*(T**5) - (2.90947e-13)*(T**6)
      k1=k1*(480.0/456.0)
      ! older slightly less accurate value below
      !               k1=56645.8 + 10.655*T - 0.215214*(T**2) + 0.000212721*(T**3) - (2.24971e-7)*(T**4) &
      !                  + (2.90075e-10)*(T**5) - (1.29527e-13)*(T**6)
      !                k1=k1*(485.0/456.0)

      ! k2 for iron is included here agin form observations from fig7.24 in Culity and Graham. However more recent
      ! measurements by Graham 1958 show the value at least 100 times smaller than Honda et al 1928. Presently we
      ! recommend that K2=0 for iron at all temperatures
      K2 = 0.0
      if(T>282.AND.T<=379) K2=194200. + 306.864*(-282.663+T) - 0.00630088*(-282.663+T)**3
      if(T>379.AND.T<=480) K2=218100. + 132.229*(-378.781+T) - 1.81688*(-378.781+T)**2 + 0.0041246*(-378.781+T)**3
      if(T>480.AND.T<=576) K2=217100. - 109.287*(-480.43+T) - 0.559099*(-480.43+T)**2 - 0.0137673*(-480.43+T)**3
      if(T>576.AND.T<=680) K2=189400. - 595.383*(-576.221+T) - 4.51544*(-576.221+T)**2 + 0.0227715*(-576.221+T)**3
      if(T>680.AND.T<=777) k2=104700. - 798.569*(-679.664+T) + 2.55121*(-679.664+T)**2 - 0.00495542*(-679.664+T)**3
      if(T>777.AND.T<=877) K2=46400. - 441.912*(-777.388+T) + 1.09842*(-777.388+T)**2 - 0.00368236*(-777.388 + T)**3


      Kd= mu*MAXVAL(Ms)*MAXVAL(Ms)*0.5
      LambdaEx= SQRT(Aex(MAXLOC(Ms,1))/Kd)
      QHardness= K1(MAXLOC(Ms,1))/Kd
    else
      write(*,*) 'Warning :: Iron material constants for T>1044 deg Celsius are ZERO T=',TCelsius
      anisform = ANISFORM_CUBIC
      Aex= 0.
      Ms=0.
      K1=0.
      K2=0.
    endif
  END SUBROUTINE Iron

END MODULE Material_Parameters
