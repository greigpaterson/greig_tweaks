!> The Energy_Calculator module contains the calculators for the various
!> magnetic energies, including the demagnetizing, anisotropy and exchange
!> energies.
MODULE Energy_Calculator
  USE Utils
  IMPLICIT NONE

  !> The energy log file name
  CHARACTER (LEN=1024) :: logfile

  !> The unit for the energy log file
  ! ww set logunit to zero removed the = 0
  INTEGER :: logunit

  !> If .TRUE., then the energy should be logged to the energy log file.
  LOGICAL :: EnergyLogging

  !> If .TRUE., then run all the exchange calculators.
  LOGICAL :: CalcAllExchQ

  !> The number of bad tetrahedra in the mesh
  INTEGER :: NBadTets


  !> A generic type for an energy calculator.
  !> This is intended for adding extra energy calculators to MERRILL.
  !> An energy calculator is minimally defined by a \c name, an \c energy and a
  !> magnetic field, \c h.
  !> A number of routines are provided for setting up, cleaning up, and running
  !> the calculator.
  TYPE :: EnergyCalculator
    !> The name of the EnergyCalculator, used for identifying the instance.
    CHARACTER(len=1024) :: Name

    !> The H-field.
    !> This is added to the total h-field when the h-fields are
    !> calculated. This can be updated by the Run procedure.
    REAL(KIND=DP), ALLOCATABLE :: h(:,:)

    !> The energy.
    !> This is added to the total energy when the energies are
    !> calculated. This can be updated by the Run procedure.
    REAL(KIND=DP) :: Energy

    !> The initialization function, called when the EnergyCalculator is first
    !> added to the ExtraEnergyCalculators list using AddEnergyCalculator.
    PROCEDURE(EnergyCalculatorProcedure), POINTER, PASS :: Initialize => NULL()

    !> This is run when BuildEnergyCalculator is run.
    PROCEDURE(EnergyCalculatorProcedure), POINTER, PASS :: Build => NULL()

    !> This is run when DestroyEnergyCalculator or RemoveEnergyCalculator is
    !> run.
    PROCEDURE(EnergyCalculatorProcedure), POINTER, PASS :: Destroy => NULL()

    !> This is run when ET_GRAD is run, i.e. when all the energies and fields
    !> are being updated.
    PROCEDURE(EnergyCalculatorProcedure), POINTER, PASS :: Run => NULL()
  END TYPE EnergyCalculator

  INTERFACE
    !> Interface for the procedures in EnergyCalculator.
    SUBROUTINE EnergyCalculatorProcedure(self)
      !> Interface for the procedures in EnergyCalculator.
      !> @param[in] self An instance of the EnergyCalculator.
      IMPORT EnergyCalculator
      CLASS(EnergyCalculator), INTENT(INOUT) :: self
    END SUBROUTINE EnergyCalculatorProcedure
  END INTERFACE


  !> A list of the extra EnergyCalculators, added by plugins and users using
  !> the AddEnergyCalculator function.
  TYPE(EnergyCalculator), ALLOCATABLE :: ExtraEnergyCalculators(:)


  CONTAINS


  !> Initialize the variables in this module
  SUBROUTINE InitializeEnergyCalculator()
    USE Magnetization_Path
    IMPLICIT NONE

    CALL DestroyEnergyCalculator()

!    logunit = 0

    ! (no logfile is written)
    EnergyLogging = .FALSE.

    ! To speed up calculation : .false. suppresses calculation of
    !   unused exchange energies
    CalcAllExchQ = .FALSE.

    NBadTets = 0

    ALLOCATE(ExtraEnergyCalculators(0))

    CALL InitializeMagnetizationPathPointers( &
      BuildEnergyCalculator, EnergyMin, ET_GRAD &
    )
  END SUBROUTINE InitializeEnergyCalculator

  !> Destroy and clean up the variables in this module.
  SUBROUTINE DestroyEnergyCalculator()
    INTEGER :: i
    CLOSE(logunit)

    IF(ALLOCATED(ExtraEnergyCalculators)) THEN
      DO i=1,SIZE(ExtraEnergyCalculators)
        CALL ExtraEnergyCalculators(i)%Destroy()
      END DO
      DEALLOCATE(ExtraEnergyCalculators)
    END IF
  CONTAINS
    SUBROUTINE CloseIfOpen(unit)
      INTEGER, INTENT(INOUT) :: unit
      LOGICAL :: is_open

      is_open = .FALSE.
      IF(unit .NE. 0) INQUIRE(UNIT=unit, OPENED=is_open)
      IF(is_open) CLOSE(unit)

      unit = 0
    END SUBROUTINE CloseIfOpen
  END SUBROUTINE DestroyEnergyCalculator


  !> Build all the objects necessary to use the Energy_Calculator module.
  SUBROUTINE BuildEnergyCalculator()
    IMPLICIT NONE

    INTEGER :: i

    DO i=1,SIZE(ExtraEnergyCalculators)
      IF(ASSOCIATED(ExtraEnergyCalculators(i)%Build)) THEN
        CALL ExtraEnergyCalculators(i)%Build()
      END IF
    END DO
  END SUBROUTINE BuildEnergyCalculator


  !> Add a calculator to the ExtraEnergyCalculators array.
  !> eg
  !> @code
  !>     TYPE(EnergyCalculator) :: my_calculator
  !>
  !>     ALLOCATE(my_calculator%h(NNODE,3))
  !>     my_calculator%Initialize => my_calculator_initialize
  !>     my_calculator%Destroy    => my_calculator_destroy
  !>     my_calculator%Run        => my_calculator_run
  !>
  !>     CALL AddEnergyCalculator("amazing calculator", my_calculator)
  !> @endcode
  SUBROUTINE AddEnergyCalculator(name, calculator)
    USE ISO_C_BINDING
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: name
    TYPE(EnergyCalculator), INTENT(INOUT) :: calculator

    TYPE(EnergyCalculator), ALLOCATABLE :: tmp_eec(:)

    INTEGER :: i


    ! Warn if name is already taken
    DO i=1,SIZE(ExtraEnergyCalculators)
      IF(TRIM(ExtraEnergyCalculators(i)%Name) .EQ. TRIM(name)) THEN
        WRITE(*,*) "WARNING: ", TRIM(name), " is already a calculator!"
      END IF
    END DO

    ! Allocate larger array and copy to it
    ALLOCATE(tmp_eec(SIZE(ExtraEnergyCalculators)+1))
    DO i=1,SIZE(ExtraEnergyCalculators)
      tmp_eec(i) = ExtraEnergyCalculators(i)
    END DO
    CALL MOVE_ALLOC(tmp_eec, ExtraEnergyCalculators)

    ! Initialize and add the new calculator
    i = SIZE(ExtraEnergyCalculators)
    ExtraEnergyCalculators(i) = calculator
    ExtraEnergyCalculators(i)%Name = name
    IF(ASSOCIATED(ExtraEnergyCalculators(i)%Initialize)) THEN
      CALL ExtraEnergyCalculators(i)%Initialize()
    END IF

  END SUBROUTINE AddEnergyCalculator


  !> Remove a calculator of a given name
  !> eg
  !> @code
  !>  CALL RemoveEnergyCalculator("my_derived_calculator")
  !> @endcode
  SUBROUTINE RemoveEnergyCalculator(name)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: name

    TYPE(EnergyCalculator), ALLOCATABLE :: tmp_eec(:)

    INTEGER :: i


    ! Pop off all calculators named name

    i=0
    DO
      i = i+1

      ! If we've gotten to the end, break the loop
      IF( i .GT. SIZE(ExtraEnergyCalculators) ) THEN
        EXIT
      END IF

      IF(TRIM(ExtraEnergyCalculators(i)%Name) .EQ. TRIM(name)) THEN
        WRITE(*,*) "Removing calculator: ", TRIM(name)

        CALL ExtraEnergyCalculators(i)%Destroy()

        ! Pop off calculator i
        ALLOCATE(tmp_eec(SIZE(ExtraEnergyCalculators)-1))
        tmp_eec(1:i-1) = ExtraEnergyCalculators(1:i-1)
        tmp_eec(i:) = ExtraEnergyCalculators(i:SIZE(tmp_eec))
        CALL MOVE_ALLOC(tmp_eec, ExtraEnergyCalculators)

        ! Do loop again with the same value of i
        i = i-1
      END IF

    END DO

  END SUBROUTINE RemoveEnergyCalculator


  !---------------------------------------------------------------
  !  TOP LEVEL ENERGY MINIMIZATION ROUTINE
  !---------------------------------------------------------------

  !> Find the value of `M` which is a Local Energy Minimum for `E(M)`.
  SUBROUTINE EnergyMin()

    USE Material_Parameters
    USE Tetrahedral_Mesh_Data
    USE Finite_Element
    USE Hubert_Minimizer


    IMPLICIT NONE

    INTEGER :: i,neval
    REAL(KIND=DP) :: etot, Eold, Ediff
    REAL(KIND=DP) :: XGUESS(2*NNODE) ,XX(2*NNODE) ,GRAD(2*NNODE)
    INTEGER :: maxvar, restartcount

    maxvar=2*NNODE
    neval=0
    EnergyUnit= Kd*total_volume/(Ls**(3.0/2.0))

    Eold=999.
    Ediff=999.
    restartcount=0
    do while (abs(ediff)>1.d-8 .AND. restartcount.lt.MaxRestarts)
      print*,'RESTART = ', restartcount
      restartcount=restartcount+1
      if (restartcount.eq.MaxRestarts) print*,'FINAL ENERGY ITERATION'

      do i = 1, NNODE
        XGUESS(2*i-1)=acos(m(i,3))
        XGUESS(2*i)=atan2(m(i,2),m(i,1))
      enddo

      XX(:)=0.
      GRAD(:)=0.

      call HubertMinimize(ET_GRAD, MAXVAR, XGUESS, XX, GRAD, ETOT, neval)

      ediff=Etot-Eold

      Eold=Etot
      ! results of minimisation returned in X
      do i=1,NNODE
        m(i,1)=sin(XX(2*i-1))*cos(XX(2*i))
        m(i,2)=sin(XX(2*i-1))*sin(XX(2*i))
        m(i,3)=cos(XX(2*i-1))
      enddo

    enddo !  do while restart loop


    print*,'*** FINISHED MINIMIZATION RESTARTS'

    RETURN
  END SUBROUTINE EnergyMin


  !---------------------------------------------------------------
  ! BEM  : ET_GRAD(ETOT,G,X,neval)
  !        Only remaining energy and gradient function
  !---------------------------------------------------------------

  !> Update all the magnetic fields and energies and return the energy
  !> gradient in spherical polar form (with the radial component dropped).
  !>
  !> @param[out]   ETOT  The total magnetic energy
  !> @param[out]   grad  The magnetic energy gradient (in spherical polars)
  !> @param[in]    X     The initial guess for the LEM
  !> @param[inout] neval The number of times this function has been evaluated
  SUBROUTINE ET_GRAD(ETOT, grad, X, neval)

    USE Material_Parameters
    USE Tetrahedral_Mesh_Data
    USE Finite_Element
    USE Magnetization_Path, ONLY: &
        AddInitPathEnergyQ, PMag, InitAlpha, InitDelta, InitRefPos, Distance, &
        firstrow

    IMPLICIT NONE

    REAL(KIND=DP), INTENT(OUT) :: etot
    REAL(KIND=DP), INTENT(OUT) :: grad(:)
    REAL(KIND=DP), INTENT(IN)  :: X(:)
    INTEGER, INTENT(INOUT) :: neval

    INTEGER :: i,N,l
    INTEGER(KIND=8), SAVE :: global_neval = 0
    REAL(KIND=DP) :: phi,theta,edist,dist,UsedExchangeEn
    REAL(KIND=DP) :: EnScale
    REAL(KIND=DP) :: mag_av(3),mag_maxv, mag_max


    INTEGER :: WhichExchangeHere

    global_neval = global_neval+1

    EnScale= Kd*total_volume  ! Energy scale to transform into units of Kd V
    ! Note that a factor Ls**(-3/2) is intrinsic by using Aex*Ls
    IF(.NOT.NONZERO(EnScale)) EnScale = 1

    ! increment the counter for number of energy evalulations
    neval=neval+1

    MeanMag(:)=0.
    mag_maxv=0.
    WhichExchangeHere = WhichExchange

    ! when calling with negative neval the cartesian magnetization
    !   is not changed!!
    if( neval >0) then
      ! this is used for Magnetization paths --> PathEnergyAt(pos) !!
      do n=1,NNODE
        m(n,1)=sin(X(2*n-1))*cos(X(2*n))
        m(n,2)=sin(X(2*n-1))*sin(X(2*n))
        m(n,3)=cos(X(2*n-1))
        mag_av(:)  = 0
        mag_max = 0 
! calculate the average magnetisation per node over neighbouring elements which cam be of 
! different material     
    	DO l=CNR_IM(n),CNR_IM(n+1)-1
        mag_av(:) = mag_av(:) + Ms(SDNR_IM(l))*m(RNR_IM(l),:)*InterpolationMatrix(l)
        mag_max   = mag_max   + Ms(SDNR_IM(l))*InterpolationMatrix(l)
        END DO
      mag_maxv=mag_maxv + mag_max*vbox(n)
      MeanMag(:)=MeanMag(:)+ mag_av(:)*vbox(n)
      enddo
        MeanMag(:)=MeanMag(:)/mag_maxv
    endif




    ! Calculates demag and Laplace exchange (gradient+energy)
    call CalcDemagEx()
    ! Calculates anisotropy and external field (gradient+energy)
    call CalcAnisExt()


    if(CalcAllExchQ .eqv. .true.) then  ! calculate  all exchange energies
      call ExEnergyGrad3(X)  !  Test alternative exchange energy -> ExEnerg2
      if (WhichExchange==3 .and. NBadTets>0) then
        WhichExchangeHere=2
        write(*,*) 'Ex changed 3 -> 2, NBad =',NBadTets
      endif
      call ExGrad()
      call ExGrad4()
    else ! calculate  only the requested exchange energy (for faster execution)
      select case(WhichExchangeHere)
      case(2)
        call ExGrad()
      case(3)
        call ExEnergyGrad3(X)  !  Use alternative exchange energy -> ExEnerg2
        if (WhichExchange==3 .and. NBadTets>0) then
          WhichExchangeHere=2
          call ExGrad()
          write(*,*) 'Ex changed 3 -> 2, NBad =',NBadTets
        endif
      case(4)
        call ExGrad4()
      end select
    endif

    do i=1,SIZE(ExtraEnergyCalculators)
        CALL ExtraEnergyCalculators(i)%Run()
    end do


    !
    !      1)   Collect total energy
    !
    UsedExchangeEn = 0
    select case(WhichExchangeHere)
      case(1)
        UsedExchangeEn=ExchangeE
      case(2)
        UsedExchangeEn= ExEnerg2
      case(3)
        UsedExchangeEn= ExEnerg3
      case(4)
        UsedExchangeEn= ExEnerg4
    end select

    etot=AnisEnerg+ UsedExchangeEn+ DemagEnerg + BEnerg


    do i=1,SIZE(ExtraEnergyCalculators)
        etot = etot + ExtraEnergyCalculators(i)%Energy
    end do


    if(AddInitPathEnergyQ) then
      edist= InitDelta-Distance(PMag(InitRefPos,:,:),m(:,:))
      edist=InitAlpha*edist*edist
      etot=etot+edist
    endif

    if(EnergyLogging) then
      if(AddInitPathEnergyQ) then
        if (firstrow==0) then
        write(logunit,*) "FIRST ROW"
        WRITE(logunit, '(2A12,1A20,9A28)') &
        "Global-N-Eval", "N-Eval","E-Anis", "E-Exch",&
        "E-ext", "E-Demag", "E-distance" ,&
        "E-Tot","Mx","My","Mz"
        firstrow=1
        endif
        write(logunit, '(2I12,TR2,9E28.20)') &
          global_neval, neval,AnisEnerg/EnScale, UsedExchangeEn/EnScale, &
          BEnerg/EnScale, DemagEnerg/EnScale, edist/EnScale, &
          etot/EnScale, &
          MeanMag(1),MeanMag(2),MeanMag(3)
      else
        write(logunit, '(2I12,TR2,11E28.20)')  &
          global_neval, neval,AnisEnerg/EnScale, ExchangeE/EnScale, &
          ExEnerg2/EnScale, ExEnerg3/EnScale,ExEnerg4/EnScale, &
          BEnerg/EnScale, DemagEnerg/EnScale, etot/EnScale, &
          MeanMag(1),MeanMag(2),MeanMag(3)
      endif
    endif

    !
    !      2)   Collect total gradient
    !
    DO i =1, NNODE
      gradc(i,:) = hanis(i,:) + hdemag(i,:) + hext(i,:)
      select case(WhichExchangeHere)
        case(1)
          gradc(i,:)= gradc(i,:)+hexch(i,:)
        case(2)
          gradc(i,:)= gradc(i,:)+hexch2(i,:)
        case(3)
          !                   gradc(i,:)= gradc(i,:)+hexch3(i,:)
          continue
        case(4)
          gradc(i,:)= gradc(i,:)+hexch4(i,:)
      end select
    END DO

    do i=1,SIZE(ExtraEnergyCalculators)
        gradc = gradc + ExtraEnergyCalculators(i)%h
    end do

    if(AddInitPathEnergyQ) then !! InitPathEnergy gradient should be added!!
      dist=Distance(PMag(InitRefPos,:,:),m(:,:))
      DO i =1, NNODE
        gradc(i,:) = gradc(i,:) &
          + 2*InitAlpha*(InitDelta/dist-1) &
            *PMag(InitRefPos,i,:) &
            *vbox(i)/total_volume  !! InitPathEnergy gradient!!
      END DO
    endif


    !
    !      2)   Transform gradient to polar magnetizations
    !
    DO i =1, NNODE
      phi= X(2*i)
      theta=X(2*i-1)
      grad(2*i-1)= gradc(i,1)*cos(theta )*cos(phi)   &
        &   + gradc(i,2)*cos(theta )*sin(phi)  &
        &   - gradc(i,3) * sin(theta )
      grad(2*i)= -gradc(i,1)*sin(theta )*sin(phi ) &
        &        + gradc(i,2)*sin(theta )*cos(phi)
      if (WhichExchangeHere==3) then
        grad(2*i-1)=grad(2*i-1) +Aex(1)*ls*DExTheta(i)
        grad(2*i)= grad(2*i) +Aex(1)*ls*DExPhi(i)
      endif

      ! projection on m
      ! sp=gradc(i,1)*m(i,1)+gradc(i,2)*m(i,2)+gradc(i,3)*m(i,3)
      ! make gradc perpendicular to m
      ! gradc(i,:)=gradc(i,:)-sp*m(i,:)
    END DO
    !
    !      3)   Set gradient at fixed magnetizations to zero
    !

    if (NFIX >0) THEN   !  In case of fixed nodes
      Do i=1,NNODE
        if (NodeBlockNumber(i)>1) then  ! All free nodes are in block number 1 !!
          grad(2*i)=0      ! set gradient =0 for fixed nodes
          grad(2*i+1)=0
        endif
      enddo
    endif

    DO i=1,NNODE
    END DO

    RETURN
  END SUBROUTINE ET_GRAD


  !---------------------------------------------------------------
  ! BEM  :  GradientTest()
  !---------------------------------------------------------------

  !> A function for testing the gradients
  !> @todo DOCUMENT ME
  SUBROUTINE  GradientTest()

    USE Material_Parameters
    USE Tetrahedral_Mesh_Data
    USE Finite_Element

    IMPLICIT NONE

    INTEGER :: i,neval,ind,l
    REAL(KIND=DP) :: G(NNODE*2), G2(NNODE*2), X(NNODE*2), etot
    REAL(KIND=DP) :: eref, delta
    REAL(KIND=DP) :: mag_av(3), mag_max, mag_maxv

    ! Controlling tolerance of linear CG solver
    FEMTolerance=1.d-10   ! via global FEMTolerance

    neval=1
    MeanMag(:)=0.
    ! Theta (co-lat) indicies= 2n-1, phi (azimuth)  indicies= 2n
    
    do i=1,NNODE
      X(2*i-1)=acos(m(i,3))
      X(2*i)= atan2(m(i,2),m(i,1))
      mag_av(:)  = 0
      mag_max = 0 
! calculate the average magnetisation per node over neighbouring elements which cam be of 
! different material     
    	DO l=CNR_IM(i),CNR_IM(i+1)-1
        mag_av(:) = mag_av(:) + Ms(SDNR_IM(l))*m(RNR_IM(l),:)*InterpolationMatrix(l)
        mag_max   = mag_max   + Ms(SDNR_IM(l))*InterpolationMatrix(l)
        END DO
      mag_maxv=mag_maxv + mag_max*vbox(i)
      MeanMag(:)=MeanMag(:)+m(i,:)*vbox(i)
    enddo

    MeanMag(:)=MeanMag(:)/mag_maxv



    call ET_GRAD(eref,G,X,neval)


    delta=0.001
    do ind=50,Min( NNode,100)
      X(2*ind)=X(2*ind)+delta
      m(ind,1)=sin(X(2*ind-1))*cos(X(2*ind))
      m(ind,2)=sin(X(2*ind-1))*sin(X(2*ind))
      m(ind,3)=cos(X(2*ind-1))


      call ET_GRAD(ETOT,G2,X,neval)

      write(*,*) "phi:   dE, DE/d:",ind,G(2*ind),(etot-eref)/delta
      X(2*ind)=X(2*ind)-delta
      X(2*ind-1)=X(2*ind-1)+delta
      m(ind,1)=sin(X(2*ind-1))*cos(X(2*ind))
      m(ind,2)=sin(X(2*ind-1))*sin(X(2*ind))
      m(ind,3)=cos(X(2*ind-1))

      call ET_GRAD(ETOT,G2,X,neval)

      write(*,*) "theta: dE, DE/d:",ind,G(2*ind-1),(etot-eref)/delta
      X(2*ind-1)=X(2*ind-1)-delta
      m(ind,1)=sin(X(2*ind-1))*cos(X(2*ind))
      m(ind,2)=sin(X(2*ind-1))*sin(X(2*ind))
      m(ind,3)=cos(X(2*ind-1))

    enddo


    RETURN
  END SUBROUTINE GradientTest


  !---------------------------------------------------------------
  ! BEM  :  ReportEnergy()
  !---------------------------------------------------------------

  !> Print out info about the current energies
  SUBROUTINE  ReportEnergy()

    USE Material_Parameters
    USE Tetrahedral_Mesh_Data
    USE Finite_Element

    IMPLICIT NONE

    INTEGER :: i, neval
    REAL(KIND=DP) :: G(NNODE*2), X(NNODE*2), etot
    REAL(KIND=DP) :: EnScale, TypEn, KdV
    REAL(KIND=DP) :: MeanEdgeLength
    LOGICAL :: TmpQ


    EnScale = Kd*total_volume  ! Energy scale to transform into units of Kd V
    ! Note that a factor Ls**(-3/2) is intrinsic by using Aex*Ls
    TmpQ = CalcAllExchQ
    CalcAllExchQ=.TRUE.

    ! Controlling tolerance of linear CG solver
    FEMTolerance = 1.d-10   ! via global FEMTolerance

    neval = 1
    MeanMag = 0
    ! Theta (co-lat) indicies= 2n-1, phi (azimuth)  indicies= 2n
    DO i=1,NNODE
      X(2*i-1) = ACOS(m(i,3))
      X(2*i)   = ATAN2(m(i,2),m(i,1))
      MeanMag(:) = MeanMag(:) + m(i,:)*vbox(i)
    END DO
    MeanMag(:)=MeanMag(:)/total_volume

    CALL ET_GRAD(etot,G,X,neval)

    KdV= Kd*total_volume/(SQRT(Ls)**3)
    IF(.NOT. NONZERO(KdV)) KdV = 1
    TypEn = SQRT(Kd*SQRT(Aex(1)*ABS(K1(1)))) &
        *(total_volume/(SQRT(Ls)**3))**(5./6.)
    IF(.NOT. (NONZERO(TypEn))) TypEn = Kd*total_volume/(SQRT(Ls)**3)
    IF(.NOT. (NONZERO(TypEn))) TypEn = Aex(1)*total_volume**(1./3.)/SQRT(Ls)
    IF(.NOT. (NONZERO(TypEn))) TypEn = ABS(K1(1))*total_volume/(SQRT(Ls)**3)

    ! Get average edge length
    MeanEdgeLength = 0
    DO i=1,NTRI
      MeanEdgeLength = MeanEdgeLength &
        + NORM2(VCL(TIL(i,1),1:3) - VCL(TIL(i,2),1:3)) &
        + NORM2(VCL(TIL(i,1),1:3) - VCL(TIL(i,3),1:3)) &
        + NORM2(VCL(TIL(i,1),1:3) - VCL(TIL(i,4),1:3)) &
        + NORM2(VCL(TIL(i,2),1:3) - VCL(TIL(i,3),1:3)) &
        + NORM2(VCL(TIL(i,2),1:3) - VCL(TIL(i,4),1:3)) &
        + NORM2(VCL(TIL(i,3),1:3) - VCL(TIL(i,4),1:3))
    END DO
    MeanEdgeLength = MeanEdgeLength / (6*NTRI)

    WRITE(*,*)

    WRITE(*, '(A)') MERRILL_VERSION_STRING // " (Williams, Fabian, O Conbhui)"
    WRITE(*,*)

    WRITE(*, '(A)') "Mesh data:"
    WRITE(*, '(A16,I16)') "Nodes", NNODE
    WRITE(*, '(A16,I16)') "Tetrahedra", NTRI
    WRITE(*, '(A16,I16)') "Boundary Nodes", BNODE
    WRITE(*, '(A16,I16)') "Boundary Faces", BFCE
    WRITE(*, '(A16,ES16.8)') "Volume", total_volume
    WRITE(*,*)

    WRITE(*, '(A)') "Material data:"
    DO i=1,NMaterials
      WRITE(*,'(A16,I16)') "SubDomain ID", SubDomainIds(i)
      WRITE(*,'(A16,ES16.8)') "Ms",  Ms(i)
      WRITE(*,'(A16,ES16.8)') "K1",  K1(i)
      WRITE(*,'(A16,ES16.8)') "K2",  K2(i)
      WRITE(*,'(A16,ES16.8)') "Aex", Aex(i)
      WRITE(*,'(A16,ES16.8)') "Vol", (SUM(vol(:), TetSubDomains .EQ. i))/(SQRT(Ls)**3)
      SELECT CASE(anisform(i))
        CASE(ANISFORM_CUBIC)
          WRITE(*,'(A16,A16)') "Anisotropy", "CUBIC"
          WRITE(*,'(A16,3ES16.8)') "Axes", CubicAxes(:,1,i)
          WRITE(*,'(A16,3ES16.8)') "", CubicAxes(:,2,i)
          WRITE(*,'(A16,3ES16.8)') "", CubicAxes(:,3,i)
        CASE(ANISFORM_UNIAXIAL)
          WRITE(*,'(A16,A16)') "Anisotropy", "UNIAXIAL"
          WRITE(*,'(A16,3ES16.8)') "Axis", EasyAxis(:,i)
      END SELECT
      WRITE(*,*)
    END DO

    WRITE(*, '(A16,ES16.7)') "Vol^(1/3) (m)", (total_volume**(1./3.))/SQRT(Ls)
    WRITE(*, '(A16,ES16.7)') "QHardness", QHardness
    WRITE(*, '(A16,ES16.7)') "Exch Len (nm)", LambdaEx*1.d9
    WRITE(*, '(A16,ES16.7)') "Avg Edge", MeanEdgeLength
    WRITE(*, '(A16,ES16.7)') "Avg Edge (nm)", MeanEdgeLength/SQRT(Ls)*1.d9
    WRITE(*, '(A16,ES16.7)') "Kd", Kd
    WRITE(*, '(A16,ES16.7)') "Kd V (J)", Kd*total_volume/(SQRT(Ls)**3)
    WRITE(*,*)

    WRITE(*, '(A)') "External field direction (x,y,z) and strength B (T):"
    WRITE(*, '(4A14)') "hx","hy", "hz", "B (T)"
    WRITE(*, '(4ES14.6)')  hz(1), hz(2), hz(3), extapp
    WRITE(*,*)

    !EnScale = Kd V
    WRITE(*, '(A)') "Energies in units of Kd V:"
    WRITE(*, '(A16,ES16.8)') "E-Anis",  AnisEnerg/EnScale
    WRITE(*, '(A16,ES16.8)') "E-Ext",   BEnerg/EnScale
    WRITE(*, '(A16,ES16.8)') "E-Demag", DemagEnerg/EnScale
    WRITE(*, '(A16,ES16.8)') "E-Exch",  ExchangeE/EnScale
    WRITE(*,*)

    WRITE(*, '(A16,ES16.8)') "E-Exch2",  ExEnerg2/EnScale
    WRITE(*, '(A16,ES16.8)') "E-Exch3",  ExEnerg3/EnScale
    WRITE(*, '(A16,ES16.8)') "E-Exch4",  ExEnerg4/EnScale
    WRITE(*,*)
    WRITE(*, '(A16,ES16.8)') "E-Tot",  etot/EnScale
    WRITE(*,*)

    WRITE(*, '(A)') "Energies in units of J:"
    WRITE(*, '(A16,ES16.8)') "E-Anis",  AnisEnerg/(SQRT(Ls)**3)
    WRITE(*, '(A16,ES16.8)') "E-Ext",   BEnerg/(SQRT(Ls)**3)
    WRITE(*, '(A16,ES16.8)') "E-Demag", DemagEnerg/(SQRT(Ls)**3)
    WRITE(*, '(A16,ES16.8)') "E-Exch",  ExchangeE/(SQRT(Ls)**3)
    WRITE(*,*)

    WRITE(*, '(A16,ES16.8)') "E-Exch2",  ExEnerg2/(SQRT(Ls)**3)
    WRITE(*, '(A16,ES16.8)') "E-Exch3",  ExEnerg3/(SQRT(Ls)**3)
    WRITE(*, '(A16,ES16.8)') "E-Exch4",  ExEnerg4/(SQRT(Ls)**3)
    WRITE(*,*)
    WRITE(*, '(A16,ES16.8)') "E-Tot",  etot/(SQRT(Ls)**3)
    DO i=1,SIZE(ExtraEnergyCalculators)
        WRITE(*,*) &
            TRIM(ExtraEnergyCalculators(i)%Name), ":", &
            ExtraEnergyCalculators(i)%Energy/EnScale
    END DO
    WRITE(*,*)



    WRITE(*,'(A)') "Average magnetization:"
    WRITE(*, '(A16,E16.8)') "<Mx>", MeanMag(1)
    WRITE(*, '(A16,E16.8)') "<My>", MeanMag(2)
    WRITE(*, '(A16,E16.8)') "<Mz>", MeanMag(3)
    WRITE(*, '(A16,E16.8)') "<M>",  NORM2(MeanMag)
    WRITE(*,*)

    WRITE(*, '(A)')  "Typical energy scale Kd^(1/2) (A K1)^(1/4) V^(5/6):"
    WRITE(*, '(A25,ES16.8)') "Typical Energy (J)   ", TypEn
    WRITE(*, '(A25,ES16.8)') "Typical Energy (Kd V)", TypEn/KdV
    WRITE(*,*)

    CalcAllExchQ = TmpQ
  END SUBROUTINE ReportEnergy


  !---------------------------------------------------------------
  ! BEM  : ENERGY CALCULATIONS
  !---------------------------------------------------------------

  !> Calculate the Demagnetizing and Exchange energies and gradients.
  SUBROUTINE CalcDemagEx( )
    USE Material_Parameters
    USE Tetrahedral_Mesh_Data
    USE Finite_Element
    USE slatec_dcg
    USE slatec_dsmv
    USE slatec_dsmtv
    USE slatec_dsllti

    IMPLICIT NONE

    REAL(KIND=DP) :: TOL, ERR
    INTEGER i,j,l
    INTEGER ITER, ISYM, ITOL, IERR, IUNIT, ITMAX

    INTEGER, PARAMETER :: LOCRB=1, LOCIB=11
    INTEGER :: PNM_LOCEL, PNM_LOCDIN, PNM_LOCR, PNM_LOCZ, PNM_LOCP, PNM_LOCDZ
    INTEGER :: PDM_LOCEL, PDM_LOCDIN, PDM_LOCR, PDM_LOCZ, PDM_LOCP, PDM_LOCDZ

    REAL(KIND=DP) :: Ms_max, rMs_max


    !
    ! Taken from DSIGGC
    !
    ! Set up IWORK and RWORK values for DCG
    PNM_LOCEL  = LOCRB
    PDM_LOCEL  = LOCRB
    PNM_LOCDIN = PNM_LOCEL  + nze_pnm
    PDM_LOCDIN = PDM_LOCEL  + nze_pdm
    PNM_LOCR   = PNM_LOCDIN + NNODE
    PDM_LOCR   = PDM_LOCDIN + NNODE
    PNM_LOCZ   = PNM_LOCR   + NNODE
    PDM_LOCZ   = PDM_LOCR   + NNODE
    PNM_LOCP   = PNM_LOCZ   + NNODE
    PDM_LOCP   = PDM_LOCZ   + NNODE
    PNM_LOCDZ  = PNM_LOCP   + NNODE
    PDM_LOCDZ  = PDM_LOCP   + NNODE

    ! calculate phi1, i.e. f
    !   grad^2 phi1 = div m
    !   (div m) . n = 0 on the boundary
    ! Add div m to force vector
    ! Add m.n Neumann condition to force vector
    ! totfx isn't used until later, so we'll use it to store f

    ! Find reciprocal max Ms
    Ms_max = MAXVAL(Ms)
    IF(NONZERO(Ms_max)) THEN
      rMs_max = 1/Ms_max
    ELSE
      Ms_max  = 1
      rMs_max = 1
    END IF

    ! Calculate totfx = div . m
    ! We do
    !   div . m / Ms_max
    ! to account for relative multiphase values of Ms, but without having
    ! to multiply by the full value of Ms, for numerical stability
    totfx = 0
    DO i=1,NNODE
      DO j=CNR4(i),CNR4(i+1)-1
        totfx(i) = totfx(i) &
          + rMs_max * Ms(SDNR4(j)) * ( &
                m(RNR4(j),1)*FAX(j) &
              + m(RNR4(j),2)*FAY(j) &
              + m(RNR4(j),3)*FAZ(j) &
            )
      END DO
    END DO

    ! Set PNM fixed boundary condition RHS value
    totfx(NNODE) = 0



    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !     solve linear equations DSICCG values
    ISYM=0
    ITOL=1
    TOL=FEMTolerance
    ITMAX=4000
    IUNIT=0

    ! Solve grad^2 fx = totfx for fx, with boundary condition fx(NNODE) = 0

    ! Same DCG call as in DSICCG with precomputed RWORK and IWORK for
    ! PoissonNeumannMatrix
    CALL DCG( &
      NNODE, totfx, fx, nze_pnm, RNR_PNM, CNR_PNM, PoissonNeumannMatrix, &
      ISYM, DSMTV, DSLLTI, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
      PNM_RWORK(PNM_LOCR), PNM_RWORK(PNM_LOCZ), PNM_RWORK(PNM_LOCP), &
      PNM_RWORK(PNM_LOCDZ), PNM_RWORK(1), PNM_IWORK(1) &
    )

    IF (IERR/=0)  WRITE(*,*) 'GMRES error in phi 1:',IERR


    !!!!!!!!!!!!!!!!!!!
    !     calculate phi2, i.e. f2
    !     again, totfx isn't being used, so we'll use that.

    ! On the boundary:
    ! phi2 = phi1*(solid_angle/4pi -1)
    !           + integral phi1/(4pi |r|^3) (r.n) dtheta dphi
    totfx = 0
    DO i=1,BNODE
      totfx(BoundaryNodeIndex(i)) = SUM(fx(BoundaryNodeIndex(:))*BA(:,i))
    END DO

    !
    ! For integrate_{tet} grad(u_i) . grad(v_j) dV with v = 0 at the boundary,
    ! move known values of u_i to the RHS.
    !
    ! PoissonDirichletMatrix is built with rows and columns containing boundary
    ! values set to zero, with a 1 on the diagonal.  When multiplied with a
    ! force vector, the resulting value is missing contributions from boundary
    ! nodes. These need to be added back in. Since the value of u_i is known
    ! there, they can be added to the RHS and PoissonDirichletMatrix remains
    ! symmetric.
    !
    ! We want to locate row/column positions containing known boundary values,
    ! but for rows that aren't for a v_i on the boundary since v_i is zero
    ! at boundary nodes.
    ! Where NodeOnBoundary(j) == .TRUE.  column j contains u_i on the boundary.
    ! Where NodeOnBoundary(RNR_PNM(l)) == .FALSE., row RNR_PNM(l) isn't using
    ! v_i on the boundary.
    !
    ! Where we find one of these, add the integral for the (RNR(l), j)
    ! position, integral_{tet} grad(u(j)) . grad(v(RNR(l))) dV, to the RHS.
    ! Minus sign since it's + integral ... on the LHS.
    !

    DO i=1,NNODE
      DO l=CNR_PNM(i),CNR_PNM(i+1)-1
        IF(NodeOnBoundary(i) .AND. .NOT. NodeOnBoundary(RNR_PNM(l))) THEN
          ! We don't need to worry about totfx(i) referencing a previously
          ! updated totfx, since totfx(i) are boundary values and
          ! totfx(RNR_PNM(l)) aren't.
          totfx(RNR_PNM(l))=totfx(RNR_PNM(l))-PoissonNeumannMatrix(l)*totfx(i)
        ENDIF
      END DO
    END DO

    ! Solve
    !   grad^2 phi2 = 0
    !   phi2 set on boundary above
    ! Same DCG call as in DSICCG with precomputed RWORK and IWORK for
    ! PoissonDirichletMatrix
    CALL DCG( &
      NNODE, totfx, fx2, nze_pdm, RNR_PDM, CNR_PDM, PoissonDirichletMatrix, &
      ISYM, DSMTV, DSLLTI, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
      PDM_RWORK(PDM_LOCR), PDM_RWORK(PDM_LOCZ), PDM_RWORK(PDM_LOCP), &
      PDM_RWORK(PDM_LOCDZ), PDM_RWORK(1), PDM_IWORK(1) &
    )

    IF (IERR/=0)  WRITE(*,*) 'GMRES error in phi 2:',IERR

    ! Now set totfx from fx and fx2
    totfx = fx + fx2

    ! directly calculate energy and gradient
    hdemag     = 0
    DemagEnerg = 0
    hexch      = 0
    ExchangeE  = 0
    DO i=1,NNODE
      ! Gradient of the demag energy
      DO j=CNR4(i),CNR4(i+1)-1
        hdemag(i,:) = hdemag(i,:) &
          + totfx(RNR4(j)) &
            * Ms(SDNR4(j)) &
            * YA4(j,:)
      END DO
      
      hdemag(i,:) = Ms_max * mu * hdemag(i,:)

      DemagEnerg = DemagEnerg &
        & +(hdemag(i,1)*m(i,1)+hdemag(i,2)*m(i,2)+hdemag(i,3)*m(i,3))/2.

      ! Gradient of the exchange energy
      DO j=CNR_EM(i),CNR_EM(i+1)-1
        hexch(i,:) = hexch(i,:) &
          + m(RNR_EM(j),:) &
            * ExchangeMatrix(j) &
            * Aex(SDNR_EM(j))
      END DO
      hexch(i,:)=2*hexch(i,:)*ls

      ExchangeE = ExchangeE &
        & +(hexch(i,1)*m(i,1)+hexch(i,2)*m(i,2)+hexch(i,3)*m(i,3))/2.
    END DO
  END SUBROUTINE CalcDemagEx


  !> New Exchange energy  calculation using
  !> @code
  !>     Eex = (grad theta)^2 +sin^2 theta (grad phi)^2
  !> @endcode
  SUBROUTINE ExEnergy3( )

    USE Material_Parameters
    USE Tetrahedral_Mesh_Data
    USE Finite_Element

    IMPLICIT NONE

    INTEGER i,j,s
    REAL(KIND=DP)  vv, exen, th0
    REAL(KIND=DP)  dph(3),dth(3),phi,theta,mavg(3),pavg


    !     calculate exchange energy based on gradient phi and theta
    !                    Eex = (grad theta)^2 +sin^2 theta (grad phi)^2

    exen=0.

    DO i=1,NTRI
      vv= vol(i)      ! represented volume per tetrahedron
      mavg(:)=m(Til(i,1),:)+m(Til(i,2),:)+m(Til(i,3),:)+m(Til(i,4),:)
      ! average angle phi for backrotation around z- axis
      pavg=atan2(mavg(2),mavg(1))
      th0=0.
      dph(:)=0
      dth(:)=0
      DO j=1,4
        s=TIL(i,j)
        ! back rotation of m by - pavg
        ! avoids discontinuity for angles phi near -pi
        phi=atan2(m(s,2)*cos(pavg)-m(s,1)*sin(pavg),&
          &   m(s,1)*cos(pavg)+m(s,2)*sin(pavg))
        ! Note that back rotation around z doesn't change any gradient !!
        theta=acos(m(s,3))
        dph(1)=dph(1)+phi*b(i,j)
        dph(2)=dph(2)+phi*c(i,j)
        dph(3)=dph(3)+phi*d(i,j)
        dth(1)=dth(1)+theta*b(i,j)
        dth(2)=dth(2)+theta*c(i,j)
        dth(3)=dth(3)+theta*d(i,j)
        th0=th0+theta
      END DO
      exen=exen+  (dth(1)**2+dth(2)**2+dth(3)**2)/(36*vv)
      exen=exen+ (dph(1)**2+dph(2)**2+dph(3)**2)*(sin(th0*0.25))**2/(36*vv)
    END DO
    ExEnerg3 =   exen !  set global variable in module Material_Parameters
    RETURN
  END SUBROUTINE ExEnergy3


  !> New Exchange energy  gradient calculation using
  !> @code
  !>    Eex = (grad theta)^2 +sin^2 theta (grad phi)^2
  !> @endcode
  !>
  !> Calculate exchange energy based on gradient phi and theta
  !> @code
  !>    Eex = (grad theta)^2 +sin^2 theta (grad phi)^2
  !> @endcode
  !> this works only when the magnetizations at all tetrahedra
  !> vertices lie in one common half space
  !> tetrahedra where this does not apply are counted in NBadTets
  !> if bad tetrahedra occur this hinders nucleation because the
  !> exchange energy is then substantially overestimated
  SUBROUTINE ExEnergyGrad3(X)

    USE Material_Parameters
    USE Tetrahedral_Mesh_Data
    USE Finite_Element

    IMPLICIT NONE

    INTEGER i, s, tt
    REAL(KIND=DP) :: vv, exen, th0, ddp, ddt, sqdenom, rtdenom
    REAL(KIND=DP) :: tmp, mtt(3), rm(3,3), cp, sp, ct, st
    REAL(KIND=DP) :: dph(3), dth(3), phi, theta, mavg(3), pavg, tavg
    REAL(KIND=DP) :: X(:)


    exen=0.
    DExPhi(:)=0.
    DExTheta(:)=0.
    NBadTets=0

    DO tt=1,NTRI
      vv= vol(tt)     ! represented volume per tetrahedron
      mavg(:)=0.
      DO i=1,4
        mavg(:)=mavg(:)+m(TIL(tt,i),:) ! calculate average magnetization
      END DO
      tmp=sqrt(mavg(1)*mavg(1)+mavg(2)*mavg(2)+mavg(3)*mavg(3))
      IF(NONZERO(tmp)) THEN
        mavg(:)=mavg(:)/tmp
      ELSE
        mavg = 1/sqrt(3.0)
      END IF
      DO i=1,4
        sp = &
          mavg(1)*m(TIL(tt,i),1) &
          + mavg(2)*m(TIL(tt,i),2) &
          + mavg(3)*m(TIL(tt,i),3)
        If (sp <0) NBadTets=NBadTets+1
      END DO
      pavg=atan2(mavg(2),mavg(1))  ! optimal direction to rotate x-axis to
      tavg=acos(mavg(3))
      ct=cos(tavg); st=sin(tavg) ! direction cosines
      cp=cos(pavg); sp=sin(pavg)
      ! rotation matrix for optimal rotation
      rm(1,1)=cp*ct; rm(1,2)=sp*ct;rm(1,3)=st;
      ! first  - pavg around z then -tavg around y
      rm(2,1)=-sp; rm(2,2)=cp;rm(2,3)=0;
      rm(3,1)=-cp*st; rm(3,2)=-sp*st;rm(3,3)=ct;
      th0=0.
      dph(:)=0.
      dth(:)=0.
      DO i=1,4
        s=TIL(tt,i)
        ! rotate m to optimal coord. system
        mtt(:)=rm(:,1)*m(s,1)+rm(:,2)*m(s,2)+rm(:,3)*m(s,3)
        phi=atan2(mtt(2),mtt(1))
        theta=acos(mtt(3))
        dph(1)=dph(1)+phi *b(tt,i)  ! phi gradient in original coord. system
        dph(2)=dph(2)+phi *c(tt,i)  ! is phi grad divided by st
        dph(3)=dph(3)+phi *d(tt,i)
        dth(1)=dth(1)+theta*b(tt,i)
        dth(2)=dth(2)+theta*c(tt,i)
        dth(3)=dth(3)+theta*d(tt,i)
        th0=th0+theta
      END DO
      DO i=1,4
        s=TIL(tt,i)
        phi=atan2(m(s,2),m(s,1))
        theta=acos(m(s,3))
        ddp=2*( &
            b(tt,i)*dph(1)+c(tt,i)*dph(2)+d(tt,i)*dph(3) &
          )*(sin(th0*0.25))**2/(36*vv)
        ddt=2*(b(tt,i)*dth(1)+c(tt,i)*dth(2)+d(tt,i)*dth(3))/(36*vv)
        ddt=ddt+  (dph(1)**2+dph(2)**2+dph(3)**2)*sin(th0*0.5)*0.25/(36*vv)
        rtdenom=cos(pavg-phi)*st*sin(theta)-ct*cos(theta)
        rtdenom=sqrt(1-rtdenom*rtdenom)
        sqdenom=st*st*cos(theta)**2+st*ct*cos(pavg-phi)*sin(2*theta)
        sqdenom=sqdenom+(1-st*st**cos(pavg-phi)**2)*sin(theta)**2
        tmp=cp*st*cos(theta)*cos(phi)+cos(theta)*sin(phi)*sp*st+ct*sin(theta)
        IF(NONZERO(sqdenom)) THEN
          tmp=tmp*sin(theta)/sqdenom
        ELSE
          tmp = 0
        END IF
        !if(tmp/=tmp) tmp=0.
        !if(ISNAN(tmp)) tmp=0.
        DExPhi(s)=DExPhi(s) +ddp*tmp
        IF(NONZERO(rtdenom)) THEN
          tmp= st*sin(pavg-phi)*sin(theta)/rtdenom
        ELSE
          tmp = 0
        END IF
        !if(tmp/=tmp) tmp=0.
        !if(ISNAN(tmp)) tmp=0.
        DExPhi(s)=DExPhi(s) +ddt*tmp
        IF(NONZERO(sqdenom)) THEN
          tmp= -st*sin(pavg-phi)/sqdenom
        ELSE
          tmp = 0
        END IF
        !if(tmp/=tmp) tmp=0.
        !if(ISNAN(tmp)) tmp=0.
        DExTheta(s)=DExTheta(s) +ddp*tmp
        tmp=cos(pavg-phi)*cos(theta)*st+ct*sin(theta)
        IF(NONZERO(rtdenom)) THEN
          tmp=tmp/rtdenom
        ELSE
          tmp = 0
        END IF
        !if(tmp/=tmp) tmp=0.
        !if(ISNAN(tmp)) tmp=0.
        DExTheta(s)=DExTheta(s) +ddt*tmp
      END DO
      exen=exen+  (dth(1)**2+dth(2)**2+dth(3)**2)/(36*vv)
      exen=exen+ (dph(1)**2+dph(2)**2+dph(3)**2)*(sin(th0*0.25))**2/(36*vv)
    END DO
    !  set global variable in module Material_Parameters
    ExEnerg3 =   Aex(1)*ls*exen
    ! Do i=1,NNODE  ! transform from polar gradients to cartesian gradients
    !   phi=X(2*i)
    !   theta=X(2*i-1)
    !   dmp= -sin(theta) *sin(phi)
    !   dmt= cos(theta) *cos(phi)
    !   if(abs(dmp)> MachEps)  hexch3(i,1)=Aex*ls*DExPhi(i)/dmp
    !   if(abs(dmt)> MachEps)  hexch3(i,1)=hexch3(i,1)+Aex*ls*DExTheta(i)/dmt
    !   dmp= sin(theta) *cos(phi)
    !   dmt= cos(theta) *sin(phi)
    !   if(abs(dmp)> MachEps)  hexch3(i,2)=Aex*ls*DExPhi(i)/dmp
    !   if(abs(dmt)> MachEps)  hexch3(i,2)=hexch3(i,2)+Aex*ls*DExTheta(i)/dmt
    !   dmt= -sin(theta)
    !   if(abs(dmt)> MachEps)  hexch3(i,3)=Aex*ls*DExTheta(i)/dmt
    ! End do
    RETURN
  END SUBROUTINE ExEnergyGrad3


  !> New Exchange energy calculation
  !>
  !> calculate exchange energy based on linear angular rotation along edges
  !> each edge in tetrahedron TIL(i,:) has associated volume  vv = vol(i)/6
  !> each edge contributes multiple times for each tetrahedron it belongs to
  !> the exchange between nodes s and t is estimated as
  !> @code
  !>   A * vv/len^2 * arccos( m(s,:).m(t,:))^2
  !> @endcode
  !> len is the distance between the nodes
  !> @code
  !>   len^2 = (VCL(s,:)-VCL(t,:)).(VCL(s,:)-VCL(t,:))
  !> @endcode
  SUBROUTINE ExEnerg( )

    USE Material_Parameters
    USE Tetrahedral_Mesh_Data
    USE Finite_Element

    IMPLICIT NONE

    INTEGER i,j,k,s,tt
    REAL(KIND=DP)  vv,   sp, len2, exen,ac,tmp


    exen=0.

    DO i=1,NTRI
      vv= vol(i)/2. ! represented volume per tetrahedral edge = 3 *vol /6
      ! The factor 3 occurs because for each of the 4 nodes
      ! the gradient has three terms d/dx,d/dy,d/dz represented by
      ! the three edges starting at the node
      tmp=0.
      if ( vv > MachEps) then
        Do j=1,3
          s=TIL(i,j)
          Do k=j+1,4
            tt=TIL(i,k)
            len2= ( VCL(s,1)-VCL(tt,1))*( VCL(s,1)-VCL(tt,1))   +  &
              &( VCL(s,2)-VCL(tt,2))*( VCL(s,2)-VCL(tt,2))   +  &
              &( VCL(s,3)-VCL(tt,3))*( VCL(s,3)-VCL(tt,3))
            !            scalar product of node magnetizations
            sp= m(s,1)*m(tt,1)+m(s,2)*m(tt,2)+m(s,3)*m(tt,3)
            IF(-1 .LE. sp .AND. sp .LE. 1) THEN
              ac=acos(sp)
              IF(NONZERO(len2)) THEN
                ac=ac*ac/len2
              ELSE
                ac = 0
              END IF
              !if(ac/=ac)   ac=0. !  Catch NaN
              !if(ISNAN(ac))   ac=0. !  Catch NaN
              tmp=tmp+ ac     !  correct phi^2
              !if(tmp/=tmp) then
            ELSE
              !if(ISNAN(tmp)) then
              tmp=1
              vv=0d0
              !  !            write(*,*) 'ExchangeEnergy2 NaN in Tet:',i,j,k,vv
              !endif
            END IF
          End Do
        End Do
        exen=exen+ vv*tmp
      endif
    END DO
    ! set global variable in module Material_Parameters
    ExEnerg2 =  Aex(1)*ls* exen
    RETURN
  END SUBROUTINE ExEnerg


  !> New Exchange gradient calculation.
  !>
  !> calculate exchange energy based on linear angular rotation along edges
  !> each edge in tetrahedron `TIL(i,:)` has associated volume `vv = vol(i)/6`
  !> each edge contributes multiple times for each tetrahedron it belongs to
  !> the exchange between nodes `s` and `t` is estimated as
  !> @code
  !>    A * vv/len^2 * arccos( m(s,:).m(t,:))^2
  !> @endcode
  !> len is the distance between the nodes
  !> @code
  !>    len^2 = (VCL(s,:)-VCL(t,:)).(VCL(s,:)-VCL(t,:))
  !> @endcode
  SUBROUTINE ExGrad( )

    USE Material_Parameters
    USE Tetrahedral_Mesh_Data
    USE Finite_Element

    IMPLICIT NONE

    INTEGER i,j,k,s,tt
    REAL(KIND=DP)  vv,   sp, len2, exen,ac,tmp,rt


    exen=0.
    hexch2(:,:)=0.0  !  contains afterwards the GRADIENT not the exchange field
    DO i=1,NTRI
      vv= vol(i)/6.  ! volume per edge of the tetrahedron
      tmp=0.
      if(vv > MachEps) then
        Do j=1,3
          s=TIL(i,j)
          Do k=j+1,4
            tt=TIL(i,k)
            len2= ( VCL(s,1)-VCL(tt,1))*( VCL(s,1)-VCL(tt,1))   +  &
              &( VCL(s,2)-VCL(tt,2))*( VCL(s,2)-VCL(tt,2))   +  &
              &( VCL(s,3)-VCL(tt,3))*( VCL(s,3)-VCL(tt,3))
            !            scalar product of node magnetizations
            sp= m(s,1)*m(tt,1)+m(s,2)*m(tt,2)+m(s,3)*m(tt,3)
            IF(-1 .LE. sp .AND. sp .LE. 1) THEN
              ac=acos(sp)
              IF(NONZERO(len2)) THEN
                ac=ac*ac/len2
              ELSE
                ac = 0
              END IF
              !if(ac/=ac)   ac=0. !  Catch NaN
              !if(ISNAN(ac))   ac=0. !  Catch NaN
              tmp=tmp+ ac     !  correct phi^2
              !if(tmp/=tmp) then
            ELSE
              !if(ISNAN(tmp)) then
              tmp=1
              vv=0d0
              !  !            write(*,*) 'ExchangeEnergy2 NaN in Tet:',i,j,k,vv
              !endif
            END IF
            rt=sqrt(MAX(1-sp*sp, REAL(0,KIND=DP))) ! in that case acos(sp)=0 anyway
            IF( &
              NONZERO(len2) .AND. NONZERO(rt) &
              .AND. &
              -1 .LT. sp .AND. sp .LT. 1 &
            ) THEN
              ac=-6*vv/len2*acos(sp)/rt
            ELSE
              ac = 0
            END IF
            !if(ac/=ac) ac=0.  ! NaN here implies vv=0
            !if(ISNAN(ac)) ac=0.  ! NaN here implies vv=0
            hexch2(s,:) =hexch2(s,:)+ ac*m(tt,:)
            hexch2(tt,:) =hexch2(tt,:)+ ac*m(s,:)
          End Do
        End Do
        exen=exen+ 3*vv*tmp
      endif
    END DO
    ! use only gradient components perpendicular to m
    DO i=1,NNODE
      hexch2(i,:)=Aex(1)*ls*hexch2(i,:)
    END DO
    ! set global variable in module Material_Parameters
    ExEnerg2 =  Aex(1)*ls* exen
    RETURN
  END SUBROUTINE ExGrad


  !> New Exchange gradient calculation.
  !>
  !> calculate exchange energy based on linear angular rotation
  !> from tetrahedron centroid to its 4 vertices.
  !>
  !> Each of the four lines in tetrahedron `TIL(i,:)`
  !> has associated volume `vv = vol(i)/4`
  !> each line contributes three times (as x,y,z coordinate direction)
  !> the exchange between nodes `t` and centroid `s`
  !> is estimated as
  !> @code
  !>    A * vv/len^2 * arccos( mq.m(t,:))^2
  !> @endcode
  !> `len` is the distance between `s` and `t`.
  SUBROUTINE ExGrad4( )

    USE Material_Parameters
    USE Tetrahedral_Mesh_Data
    USE Finite_Element

    IMPLICIT NONE

    INTEGER i,j,k,ind(4)
    REAL(KIND=DP)  vv,   sp,  exen,ac,tmp,rt,sprt
    REAL(KIND=DP)  mq(3), mqs(3), rq(3) ,dst(4), rs(4,3)


    exen=0.
    hexch4(:,:)=0.0  !  contains afterwards the GRADIENT not the exchange field
    DO i=1,NTRI
      vv= 0.75*vol(i)  ! 3/4 volume per line inside the tetrahedron
      if(vv > MachEps) then
        tmp=0.
        ind(:)=TIL(i,1:4)
        rs(:,:)=VCL(ind(:),1:3)
        rq(:)= (rs(1,:) +rs(2,:) +rs(3,:) +rs(4,:))*0.25
        dst(:)= sqrt((rq(1)-rs(:,1))**2+(rq(2)-rs(:,2))**2+(rq(3)-rs(:,3))**2)
        mqs(:)= m(ind(1),:)+m(ind(2),:)+m(ind(3),:)+m(ind(4),:)
        rt=sqrt(mqs(1)*mqs(1)+mqs(2)*mqs(2)+mqs(3)*mqs(3))
        if(rt<MachEps) then
          rt=1.
          mqs(1)=1.
        endif
        IF(NONZERO(rt)) THEN
          mq(:)=mqs(:)/rt
        ELSE
          mq(:) = mqs(:)
        END IF
        Do j=1,4
          sp= m(ind(j),1)*mq(1)+m(ind(j),2)*mq(2)+m(ind(j),3)*mq(3)
          IF(-1 .LT. sp .AND. sp .LT. 1) THEN
            ac=acos(sp )
          ELSE
            ac = 0
          END IF
          !tmp=ac*ac/(dst(j)*dst(j))*vv
          tmp=(dst(j)*dst(j))*vv
          IF(NONZERO(tmp)) THEN
            tmp = ac*ac / tmp
          ELSE
            tmp = 0
          END IF
          !if(tmp/=tmp)   tmp=0. !  Catch NaN
          !if(ISNAN(tmp))   tmp=0. !  Catch NaN
          exen=exen+tmp
          sprt=sqrt(MAX(1-sp*sp, REAL(0, KIND=DP)))
          IF(NONZERO(sprt)) THEN
            tmp=-2*vv *ac/sprt
          ELSE
            tmp = 0
          END IF
          Do k=1,4
            if(j==k) then
              IF(NONZERO(dst(k))) THEN
                hexch4(ind(k), :)=hexch4(ind(k), :)+mq(:)*tmp/(dst(k)*dst(k))
              END IF
            endif

            IF(NONZERO(dst(j))) THEN
              hexch4(ind(k), :) = hexch4(ind(k), :) &
                + (m(ind(j),:)-sp*mq(:))/rt*tmp/(dst(j)*dst(j))
            END IF
          End Do
        End Do
      endif
    END DO
    DO i=1,NNODE
      hexch4(i,:)=Aex(1)*ls*hexch4(i,:)
    END DO
    ! set global variable in module Material_Parameters
    ExEnerg4 =  Aex(1)*ls* exen
    RETURN
  END SUBROUTINE ExGrad4



  !> Calculate the Anisotropy end External/Zeeman energy and gradient
  SUBROUTINE CalcAnisExt()

    USE Material_Parameters
    USE Tetrahedral_Mesh_Data
    USE Finite_Element

    IMPLICIT NONE

    INTEGER :: i, l
    REAL(KIND=DP) :: a(3), m2(3), ga(3)
    INTEGER :: sd


    ! Calculate anisotropy energy and gradient (wrt m) in correct units (J)

    ! Initialize energies to 0
    AnisEnerg  = 0
    AnisEnerg1 = 0
    AnisEnerg2 = 0

    ! Initialize gradient field to 0
    ! hanis contains the GRADIENT, not the effective field of
    ! the anisotropy energy
    hanis = 0

    DO i=1,NNODE
      DO l=CNR_IM(i),CNR_IM(i+1)-1
        sd = SDNR_IM(l)

        SELECT CASE(anisform(sd))
          CASE(ANISFORM_CUBIC)

            ! Rotated magnetizations
            a(:) = &
                m(RNR_IM(l),1)*CubicAxes(1,:,sd) &
              + m(RNR_IM(l),2)*CubicAxes(2,:,sd) &
              + m(RNR_IM(l),3)*CubicAxes(3,:,sd)
            m2(:)=a(:)**2

            ! Rotated gradient
            ga(1) = ( &
                2*K1(sd)*a(1)*(m2(2)+m2(3)) &
              + 2*K2(sd)*a(1)*m2(2)*m2(3)   &
            )
            ga(2) = ( &
                2*K1(sd)*a(2)*(m2(1)+m2(3)) &
              + 2*K2(sd)*a(2)*m2(1)*m2(3)   &
            )
            ga(3) = ( &
                2*K1(sd)*a(3)*(m2(2)+m2(1)) &
              + 2*K2(sd)*a(3)*m2(1)*m2(2)   &
            )

            ! Backrotated gradient
            hanis(i,:) = hanis(i,:) &
              + ( &
                  ga(1)*CubicAxes(:,1,sd) &
                + ga(2)*CubicAxes(:,2,sd) &
                + ga(3)*CubicAxes(:,3,sd) &
              ) * InterpolationMatrix(l)

            ! K1 Energy in rotated coordinates
            AnisEnerg1 = AnisEnerg1 &
              + K1(sd)*(m2(1)*m2(2)+m2(1)*m2(3)+m2(3)*m2(2)) &
                * InterpolationMatrix(l)
            AnisEnerg2 = AnisEnerg2 &
              + K2(sd)*(m2(1)*m2(2)*m2(3)) &
                * InterpolationMatrix(l)

          ! END CASE(ANISFORM_CUBIC)

          CASE(ANISFORM_UNIAXIAL)

            ! Uniaxial gradient
            hanis(i,:) = hanis(i,:) &
              -2*K1(sd)*( &
                  EasyAxis(1,sd)*m(RNR_IM(l),1) &
                + EasyAxis(2,sd)*m(RNR_IM(l),2) &
                + EasyAxis(3,sd)*m(RNR_IM(l),3) &
              ) * EasyAxis(:,sd) * InterpolationMatrix(l)

            ! Energy
            AnisEnerg1 = AnisEnerg1 &
              + K1(sd)*( &
                1 - ( &
                    EasyAxis(1,sd)*m(RNR_IM(l),1) &
                  + EasyAxis(2,sd)*m(RNR_IM(l),2) &
                  + EasyAxis(3,sd)*m(RNR_IM(l),3) &
                )**2 &
              ) * InterpolationMatrix(l)
          ! END CASE(ANISFORM_UNIAXIAL)

        END SELECT
      END DO
    END DO

    AnisEnerg = AnisEnerg1 + AnisEnerg2


    ! Calculate external field / Zeeman energy and gradient (wrt m) in correct
    ! units (J)

    BEnerg=0.
    hext=0.
    DO i=1,NNODE
      DO l=CNR_IM(i),CNR_IM(i+1)-1
        ! Uniform field energy GRADIENT
        hext(i,:) = hext(i,:) - Ms(SDNR_IM(l))*extapp*hz(:)*InterpolationMatrix(l)

        ! Energy
        BEnerg = BEnerg &
          - Ms(SDNR_IM(l))*extapp*( &
              hz(1)*m(RNR_IM(l),1) &
            + hz(2)*m(RNR_IM(l),2) &
            + hz(3)*m(RNR_IM(l),3) &
          )*InterpolationMatrix(l)
      END DO
    END DO
  END SUBROUTINE CalcAnisExt


END MODULE Energy_Calculator
