!> A module containing routines for building a tetrahedral mesh and related
!> data structures.
MODULE Tetrahedral_Mesh_Data
  USE Utils
  USE qshep3d
  IMPLICIT NONE
  SAVE

  ! NNODE : Number of nodes in the mesh
  ! NTRI  : Number of tetrahedra in the mesh
  ! BFCE  : Number of boundary faces in the mesh
  ! BNODE : Number of boundary nodes in the mesh
  ! NFIX  : Number of nodes with fixed magnetization in the mesh

  INTEGER  MaxMeshNumber

  INTEGER  NNODE, NTRI, BFCE, BNODE ,NFIX
  INTEGER, ALLOCATABLE  ::  SaveNNODE(:), SaveNTRI(:), SaveBFCE(:), SaveBNODE(:)


  ! VCL(:,3) : 1-3 : List of node coordinates
  ! vol(:) :  List of tetrahedra volumes
  ! vbox(:) :  List of  volumes associated to nodes
  ! solid(:) :  List of  solid angles associated to nodes (4 pi for interior)
  ! NodeBlockNumber(:) : List of block number to which each node is assigned.
  !     Nodes which are free during minimization have number 1
  ! NodeBodyIndex(:) : Index of body for the given node
  !     (1,2.. for first, second,... separate polyhedra ...)
  ! NodeOnBoundary(:) : Value .TRUE. if node is on the boundary of a polyhedron
  !     .FALSE. otherwise
  ! TetNodeBlockNumber : Used to characterize fixed or free nodes in the mesh.
  !     Free=1


  REAL(KIND=DP), ALLOCATABLE :: VCL(:,:)
  REAL(KIND=DP), ALLOCATABLE :: vol(:), vbox(:), solid(:)
  INTEGER, ALLOCATABLE :: NodeBodyIndex(:)
  LOGICAL, ALLOCATABLE :: NodeOnBoundary(:)
  INTEGER, ALLOCATABLE :: NodeBlockNumber(:)

  TYPE(SaveDPArray), ALLOCATABLE :: SaveVCL(:)
  TYPE(SaveDPRow), ALLOCATABLE :: Savevbox(:), Savevol(:)
  TYPE(SaveIntRow), ALLOCATABLE :: SaveNodeBodyIndex(:)
  TYPE(SaveLogicalRow), ALLOCATABLE :: SaveNodeOnBoundary(:)


  ! TIL(:,5) : 1-4 : List of indices of tetrahedra vertices 5: index of body
  ! BDFACE(:,2) : 1-4 : List of (index of tetrahedron, face index) for boundary
  !     faces
  ! b,c,d(:4) :  Lists of  shape coefficients associated to tetrahedra
  ! TetSolid(:,4) : The solid angle inside the tetrahedron for each node of the
  !     tetrahedron.
  ! TetSubDomains(:) : The subdomain of each tetrahedron.
  ! SubDomainIds(:) : The ID of each subdomain as given by the input mesh file.

  INTEGER, ALLOCATABLE :: TIL(:,:), BDFACE(:,:), NEIGH(:,:)
  REAL(KIND=DP), ALLOCATABLE :: b(:,:), c(:,:), d(:,:)
  REAL(KIND=DP), ALLOCATABLE :: TetSolid(:,:)
  INTEGER, ALLOCATABLE :: TetSubDomains(:), SubDomainIds(:)

  TYPE(SaveIntArray), ALLOCATABLE :: SaveTIL(:), SaveBDFACE(:)
  TYPE(SaveDPArray), ALLOCATABLE  :: Saveb(:), Savec(:), Saved(:)


  ! m(:,3) : Magnetization vectors associated to nodes
  ! mold(:,3) : Previous magnetization vectors associated to nodes
  ! mdotn(:,3) : normal components ???

  REAL(KIND=DP), ALLOCATABLE  :: gradc(:,:), &
    & mdotn(:,:), m(:,:), totfx(:), fx(:), fx2(:)

  REAL(KIND=DP) :: total_volume, MeanMag(3)
  REAL(KIND=DP) :: zone, zoneinc, zoneflag  ! Zone variables for WriteTecplot


  ! Variables for MeshInterpolate: Values depend on m

  LOGICAL :: InterpolatableQ=.FALSE.

  INTEGER :: IntNQ, IntNW, IntNR
  INTEGER, ALLOCATABLE :: IntLCELL(:,:,:), IntLNEXT(:)
  REAL(KIND=DP) :: IntXYZMin(3), IntXYZDel(3), IntRMax(3)
  REAL(KIND=DP), ALLOCATABLE :: IntRSQ(:,:), IntA(:,:,:)



  CONTAINS

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! Subroutines for Mesh related operations
  !    ->  InitializeTetrahedralMeshData  : ensure variables and arrays are
  !                                            ready to be used with any
  !                                            routines in this module and
  !                                            set reasonable default values
  !    ->  BuildTetrahedralMeshData       : run any routines that calculate
  !                                            useful mesh relations after
  !                                            VCL and TIL have been set
  !    ->  MeshAllocate and FaceAllocate  : allocate right amount of memory
  !    ->  GETBOUNDARY                    : analyses the mesh for
  !                                            boundaries
  !    ->  SOLIDANGLE                     : calculate solid angles at
  !                                            boundary nodes
  !    ->  GETANGLE                       : calculate solid angle for
  !                                            tetrahedron
  !    ->  SHAPECOEFFICIENTS              : calculate volumes and shape
  !                                            coefficients for tetrahedra
  !    ->  WRITEmag                       : writes out the magnetization
  !                                            (.dat) and restart
  !                                            (.restart) files
  !    ->  WRITEhyst                      : writes out m.h_est against
  !                                            |h_ext|
  !    ->  WRITETecplot                   : writes out the  TecPlot format
  !                                            for visualization
  !    ->  MeshInterpolate                : Interpolate mesh functions at
  !                                            arbitrary points
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  ! Set some reasonable default values
  SUBROUTINE InitializeTetrahedralMeshData()

    IMPLICIT NONE

    NFIX=0  ! No fixed mesh nodes as default

    ! Redefine if more than 5 meshes needed
    ! not memory critical because only pointers are allocated
    ! Large memory blocks are allocated only when meshes are loaded
    MaxMeshNumber=5

    ! zone/zoneinc not used
    zone=0.0
    zoneinc=1.0
    zoneflag=0.0

    MeanMag = 0

  END SUBROUTINE InitializeTetrahedralMeshData

  SUBROUTINE DestroyTetrahedralMeshData()
    INTERFACE TryDeallocate
      PROCEDURE :: &
        TryDeallocateDP, TryDeallocateDPArr, &
        TryDeallocateInt, TryDeallocateIntArr, &
        TryDeallocateLogical
    END INTERFACE

    IF(ALLOCATED(SaveNNODE)) DEALLOCATE(SaveNNODE)
    IF(ALLOCATED(SaveNTRI))  DEALLOCATE(SaveNTRI)
    IF(ALLOCATED(SaveBFCE))  DEALLOCATE(SaveBFCE)
    IF(ALLOCATED(SaveBNODE)) DEALLOCATE(SaveBNODE)

    IF(ALLOCATED(VCL))   DEALLOCATE(VCL)
    IF(ALLOCATED(vol))   DEALLOCATE(vol)
    IF(ALLOCATED(vbox))  DEALLOCATE(vbox)
    IF(ALLOCATED(solid)) DEALLOCATE(solid)
    IF(ALLOCATED(NodeBodyIndex))   DEALLOCATE(NodeBodyIndex)
    IF(ALLOCATED(NodeOnBoundary))  DEALLOCATE(NodeOnBoundary)
    IF(ALLOCATED(NodeBlockNumber)) DEALLOCATE(NodeBlockNumber)

    CALL TryDeallocate(SaveVCL)
    CALL TryDeallocate(Savevbox)
    CALL TryDeallocate(Savevol)
    CALL TryDeallocate(SaveNodeBodyIndex)
    CALL TryDeallocate(SaveNodeOnBoundary)
    IF(ALLOCATED(TIL))    DEALLOCATE(TIL)
    IF(ALLOCATED(BDFACE)) DEALLOCATE(BDFACE)
    IF(ALLOCATED(NEIGH))  DEALLOCATE(NEIGH)
    IF(ALLOCATED(b)) DEALLOCATE(b)
    IF(ALLOCATED(c)) DEALLOCATE(c)
    IF(ALLOCATED(d)) DEALLOCATE(d)
    IF(ALLOCATED(TetSolid))      DEALLOCATE(TetSolid)
    IF(ALLOCATED(TetSubDomains)) DEALLOCATE(TetSubDomains)
    IF(ALLOCATED(SubDomainIds))  DEALLOCATE(SubDomainIds)

    CALL TryDeallocate(SaveTIL)
    CALL TryDeallocate(SaveBDFACE)
    CALL TryDeallocate(Saveb)
    CALL TryDeallocate(Savec)
    CALL TryDeallocate(Saved)
    IF(ALLOCATED(gradc))   DEALLOCATE(gradc)
    IF(ALLOCATED(mdotn))   DEALLOCATE(mdotn)
    IF(ALLOCATED(m))       DEALLOCATE(m)
    IF(ALLOCATED(totfx))   DEALLOCATE(totfx)
    IF(ALLOCATED(fx))      DEALLOCATE(fx)
    IF(ALLOCATED(fx2))     DEALLOCATE(fx2)
    IF(ALLOCATED(IntLCELL)) DEALLOCATE(IntLCELL)
    IF(ALLOCATED(IntLNEXT)) DEALLOCATE(IntLNEXT)
    IF(ALLOCATED(IntRSQ))   DEALLOCATE(IntRSQ)
    IF(ALLOCATED(IntA))     DEALLOCATE(IntA)
  CONTAINS
    SUBROUTINE TryDeallocateDP(s)
      TYPE(SaveDPRow), ALLOCATABLE, INTENT(INOUT) :: s(:)
      INTEGER :: i
      IF(ALLOCATED(s)) THEN
        DO i=1,SIZE(s)
          IF(ALLOCATED(s(i)%DPSave)) DEALLOCATE(s(i)%DPSave)
        END DO
        DEALLOCATE(s)
      END IF
    END SUBROUTINE TryDeallocateDP

    SUBROUTINE TryDeallocateInt(s)
      TYPE(SaveIntRow), ALLOCATABLE, INTENT(INOUT) :: s(:)
      INTEGER :: i
      IF(ALLOCATED(s)) THEN
        DO i=1,SIZE(s)
          IF(ALLOCATED(s(i)%IntSave)) DEALLOCATE(s(i)%IntSave)
        END DO
        DEALLOCATE(s)
      END IF
    END SUBROUTINE TryDeallocateInt

    SUBROUTINE TryDeallocateDPArr(s)
      TYPE(SaveDPArray), ALLOCATABLE, INTENT(INOUT) :: s(:)
      INTEGER :: i
      IF(ALLOCATED(s)) THEN
        DO i=1,SIZE(s)
          IF(ALLOCATED(s(i)%DPArrSave)) DEALLOCATE(s(i)%DPArrSave)
        END DO
        DEALLOCATE(s)
      END IF
    END SUBROUTINE TryDeallocateDPArr

    SUBROUTINE TryDeallocateIntArr(s)
      TYPE(SaveIntArray), ALLOCATABLE, INTENT(INOUT) :: s(:)
      INTEGER :: i
      IF(ALLOCATED(s)) THEN
        DO i=1,SIZE(s)
          IF(ALLOCATED(s(i)%IntArrSave)) DEALLOCATE(s(i)%IntArrSave)
        END DO
        DEALLOCATE(s)
      END IF
    END SUBROUTINE TryDeallocateIntArr

    SUBROUTINE TryDeallocateLogical(s)
      TYPE(SaveLogicalRow), ALLOCATABLE, INTENT(INOUT) :: s(:)
      INTEGER :: i
      IF(ALLOCATED(s)) THEN
        DO i=1,SIZE(s)
          IF(ALLOCATED(s(i)%LogicalSave)) DEALLOCATE(s(i)%LogicalSave)
        END DO
        DEALLOCATE(s)
      END IF
    END SUBROUTINE TryDeallocateLogical
  END SUBROUTINE DestroyTetrahedralMeshData


  !---------------------------------------------------------------
  !        BuildTetrahedralMeshData
  !        Call the routines necessary to build any useful
  !        mesh relations defined here after VCL and TIL have
  !        been set.
  !---------------------------------------------------------------
  SUBROUTINE BuildTetrahedralMeshData()
    INTEGER :: i, j

    ! Compact arbitrarily numbered TetSubDomains into enumerated domains
    ! and store the original numbering in SubDomainIds
    IF(ALLOCATED(SubDomainIds)) DEALLOCATE(SubDomainIds)
    ALLOCATE(SubDomainIds(1))
    SubDomainIds(1) = TetSubDomains(1)
    DO i=1,SIZE(TetSubDomains)
      ! If TetSubDomains(i) not in SubDomainIds, add it.
      IF(.NOT. ANY(SubDomainIds(:) .EQ. TetSubDomains(i))) THEN
        BLOCK
          INTEGER, ALLOCATABLE :: tmp(:)
          ALLOCATE(tmp(SIZE(SubDomainIds)+1))
          tmp(1:SIZE(SubDomainIds)) = SubDomainIds(:)
          CALL MOVE_ALLOC(tmp, SubDomainIds)
        END BLOCK
        SubDomainIds(SIZE(SubDomainIds)) = TetSubDomains(i)
      END IF
    END DO

    ! Sort SubDomainIds
    BLOCK
      INTEGER, ALLOCATABLE :: sdid2(:,:)
      ALLOCATE(sdid2(SIZE(SubDomainIds),1))
      sdid2(:,1) = SubDomainIds
      CALL qsort(sdid2, default_smaller)
      SubDomainIds = sdid2(:,1)
    END BLOCK

    ! Renumber TetSubDomains
    DO i=1,SIZE(TetSubDomains)
      DO j=1,SIZE(SubDomainIds)
        IF(TetSubDomains(i) .EQ. SubDomainIds(j)) THEN
          TetSubDomains(i) = j
          EXIT
        END IF
      END DO
    END DO


    ! NEIGHfromFACELST
    ! sets neighborship relations for tetrahedra
    ! and determines boundary faces

    CALL NEIGHfromFACELST()


    WRITE(*,*) ' Mesh Data'
    WRITE(*,*) '-----------'
    WRITE(*,*) NNODE,'nodes'
    WRITE(*,*) NTRI, 'elements'
    WRITE(*,*) BFCE, 'boundary faces'


    CALL FaceAllocate()
    ! Mesh preparation for calculations  MODULE Tetrahedral_Mesh_Data
    CALL getboundary()
    CALL shapecoeffs()
    CALL solidangle()
  END SUBROUTINE BuildTetrahedralMeshData



  !---------------------------------------------------------------
  !          MeshAllocate and FaceAllocate
  !---------------------------------------------------------------

  SUBROUTINE MeshAllocate(nodes, tetrahedra)

    IMPLICIT NONE
    INTEGER nodes, tetrahedra

    NNODE = nodes
    NTRI  = tetrahedra


    IF(ALLOCATED(VCL))     DEALLOCATE(VCL)
    IF(ALLOCATED(vol))     DEALLOCATE(vol)
    IF(ALLOCATED(vbox))    DEALLOCATE(vbox)
    IF(ALLOCATED(solid))   DEALLOCATE(solid)
    IF(ALLOCATED(NodeBodyIndex))  DEALLOCATE(NodeBodyIndex)
    IF(ALLOCATED(NodeOnBoundary)) DEALLOCATE(NodeOnBoundary)
    IF(ALLOCATED(NodeBlockNumber))  DEALLOCATE(NodeBlockNumber)

    IF(ALLOCATED(TIL))     DEALLOCATE(TIL)
    IF(ALLOCATED(NEIGH))   DEALLOCATE(NEIGH)
    IF(ALLOCATED(b))       DEALLOCATE(b)
    IF(ALLOCATED(c))       DEALLOCATE(c)
    IF(ALLOCATED(d))       DEALLOCATE(d)
    IF(ALLOCATED(TetSolid))         DEALLOCATE(TetSolid)
    IF(ALLOCATED(TetSubDomains))    DEALLOCATE(TetSubDomains)
    IF(ALLOCATED(SubDomainIds))     DEALLOCATE(SubDomainIds)


    ALLOCATE(VCL(NNODE,3))
    ALLOCATE(vol(NTRI))
    ALLOCATE(vbox(NNODE))
    ALLOCATE(solid(NNODE))
    ALLOCATE(NodeBodyIndex(NNODE))
    ALLOCATE(NodeOnBoundary(NNODE))
    ALLOCATE(NodeBlockNumber(NNODE))

    ALLOCATE(TIL(NTRI,5))
    ALLOCATE(NEIGH(NTRI,4))
    ALLOCATE(b(NTRI,4))
    ALLOCATE(c(NTRI,4))
    ALLOCATE(d(NTRI,4))
    ALLOCATE(TetSolid(NTRI,4))
    ALLOCATE(TetSubDomains(NTRI))
    ALLOCATE(SubDomainIds(1))


    NodeBodyIndex  = 0
    NodeOnBoundary = .FALSE.
    NodeBlockNumber = 1  ! Default : All nodes are free

    TetSubDomains   = 1
    SubDomainIds    = 1


    IF(ALLOCATED(gradc))  DEALLOCATE(gradc)
    IF(ALLOCATED(mdotn))  DEALLOCATE(mdotn)
    IF(ALLOCATED(m))      DEALLOCATE(m)
    IF(ALLOCATED(totfx))  DEALLOCATE(totfx)
    IF(ALLOCATED(fx))     DEALLOCATE(fx)
    IF(ALLOCATED(fx2))    DEALLOCATE(fx2)

    ALLOCATE(gradc(NNODE,3))
    ALLOCATE(mdotn(NNODE,3))
    ALLOCATE(m(NNODE,3))
    ALLOCATE(totfx(NNODE))
    ALLOCATE(fx(NNODE))
    ALLOCATE(fx2(NNODE))

    ! Initialize m to [111]
    ! Demag solver complains for m = 0.
    m = 1/SQRT(3.0d0)

    ! Initialize values so Valgrind stops complaining
    gradc = 0
    fx    = 0
    fx2   = 0
    mdotn = 0
    totfx = 0

  END SUBROUTINE MeshAllocate


  SUBROUTINE FaceAllocate()
    IMPLICIT NONE
    If(ALLOCATED(BDFACE)) DEALLOCATE(BDFACE)
    ALLOCATE(BDFACE(BFCE,2))
  END SUBROUTINE FaceAllocate


  !---------------------------------------------------------------
  !        NEIGHfromFACELST
  !        sets neighborship relations for tetrahedra
  !        and determines boundary faces
  !---------------------------------------------------------------

  SUBROUTINE NEIGHfromFACELST( )

    IMPLICIT NONE

    INTEGER, ALLOCATABLE :: FACELST(:,:)

    INTEGER i, swap
    LOGICAL :: eq3


    ! FACELST contains all faces
    ! FACELST(i,:): i-th face
    !   1-3 : node indices
    !   4 : position of missing vertex from TIL(j,:)  (= tetrahedron face index)
    !   5 : Index j of tetrahedron in  TIL

    ALLOCATE(FACELST(4*NTRI,5))

    DO i=1,NTRI
      FACELST(4*i-3,1)=TIL(i,2)
      FACELST(4*i-3,2)=TIL(i,4)
      FACELST(4*i-3,3)=TIL(i,3)

      FACELST(4*i-2,1)=TIL(i,1)
      FACELST(4*i-2,2)=TIL(i,3)
      FACELST(4*i-2,3)=TIL(i,4)

      FACELST(4*i-1,1)=TIL(i,1)
      FACELST(4*i-1,2)=TIL(i,4)
      FACELST(4*i-1,3)=TIL(i,2)

      FACELST(4*i  ,1)=TIL(i,1)
      FACELST(4*i  ,2)=TIL(i,2)
      FACELST(4*i  ,3)=TIL(i,3)

      FACELST(4*i-3,4)=1
      FACELST(4*i-3,5)=i
      FACELST(4*i-2,4)=2
      FACELST(4*i-2,5)=i

      FACELST(4*i-1,4)=3
      FACELST(4*i-1,5)=i
      FACELST(4*i  ,4)=4
      FACELST(4*i  ,5)=i
    END DO

    ! SORT vertices of each face in FACELST by index number,
    ! keeping record of orientation.
    ! This makes sure that a face shared by two elements has the same
    ! triple of vertices in the same order.
    DO i=1,4*NTRI
      IF(FACELST(i,1)>FACELST(i,2)) THEN
        swap=FACELST(i,1); FACELST(i,1)=FACELST(i,2); FACELST(i,2)=swap
        FACELST(i,4)= -FACELST(i,4)
      END IF
      IF(FACELST(i,2)>FACELST(i,3)) THEN
        swap=FACELST(i,2); FACELST(i,2)=FACELST(i,3); FACELST(i,3)=swap
        FACELST(i,4)= -FACELST(i,4)
      END IF
      IF(FACELST(i,1)>FACELST(i,2)) THEN
        swap=FACELST(i,1); FACELST(i,1)=FACELST(i,2);FACELST(i,2)=swap
        FACELST(i,4)= -FACELST(i,4)
      END IF
    END DO

    ! Sort the FACELST.
    ! Equivalent faces in neighbouring faces should be next to each other
    ! after sorting.
    CALL qsort(FACELST, default_smaller)

    ! Get the number of boundary faces
    Neigh(:,:)=0
    BFCE=4*NTRI
    DO i=1,4*NTRI-1
      eq3=(FACELST(i,1)==FACELST(i+1,1).AND. &
           FACELST(i,2)==FACELST(i+1,2).AND. &
           FACELST(i,3)==FACELST(i+1,3))
      IF (eq3) THEN
        ! FACELST(i,5) and FACELST(i+1,5) are neighbors
        NEIGH(FACELST(i,5),Abs(FACELST(i,4)))=FACELST(i+1,5)
        NEIGH(FACELST(i+1,5),Abs(FACELST(i+1,4)))=FACELST(i,5)
        BFCE=BFCE-2
      END IF
    END DO


    RETURN
  END SUBROUTINE NEIGHfromFACELST


  !---------------------------------------------------------------
  ! BEM  : GETBOUNDARY
  !---------------------------------------------------------------

  SUBROUTINE getboundary( )
    IMPLICIT NONE

    INTEGER i,j, BF
    INTEGER e2,n1,n2,n3,nn1,nn2,chk,cubdy,l


    ! create element neighbour list in NEIGH

    ! create list of boundary elements in BDFACE
    BF=0
    DO i=1,NTRI
      DO j=1,4
        IF (NEIGH(i,j)==0) THEN
          BF=BF+1

          BDFACE(BF,1)=i
          BDFACE(BF,2)=j

          n1=MOD(j+1,4)+1
          n2=MOD(j+2,4)+1
          n3=MOD(j,4)+1

          NodeOnBoundary(TIL(i,n1)) = .TRUE.
          NodeOnBoundary(TIL(i,n2)) = .TRUE.
          NodeOnBoundary(TIL(i,n3)) = .TRUE.
        ENDIF
      END DO
    END DO

    ! Cound number of boundary nodes
    BNODE=0
    DO j=1,NNODE
        IF (NodeOnBoundary(j)) BNODE=BNODE+1
    END DO
    WRITE(*,*) BNODE, 'boundary nodes'

    ! assign body number to TIL(:,5)
    TIL(:,5)=0
    ! set starting element e2 for body 1
    e2=1
    chk=0
    cubdy=0
    DO WHILE (chk==0)
      cubdy=cubdy+1
      TIL(e2,5)=cubdy

      IF (NEIGH(e2,1)/=0) TIL(NEIGH(e2,1),5)=cubdy
      IF (NEIGH(e2,2)/=0) TIL(NEIGH(e2,2),5)=cubdy
      IF (NEIGH(e2,3)/=0) TIL(NEIGH(e2,3),5)=cubdy
      IF (NEIGH(e2,4)/=0) TIL(NEIGH(e2,4),5)=cubdy

      chk=0
      DO WHILE (chk==0)
        chk=1
        DO i=1,NTRI
          DO j=1,4
            IF (NEIGH(i,j)/=0) THEN
              IF ( TIL(NEIGH(i,j),5)==cubdy ) THEN
                IF (TIL(i,5)==0) THEN
                  TIL(i,5)=cubdy
                  chk=0
                ENDIF
                DO l=1,4
                  IF (NEIGH(i,l)/=0) THEN
                    IF (TIL(NEIGH(i,l),5)==0) THEN
                      TIL(NEIGH(i,l),5)=cubdy
                      chk=0
                    ENDIF
                  ENDIF
                END DO
              ENDIF
            ENDIF
          END DO
        END DO
      END DO

      ! find starting element for next body
      chk=1
      DO i=1,NTRI
        IF (TIL(i,5)==0) THEN
          e2=i
          chk=0
        ENDIF
      END DO
    END DO

    ! assign body number to NodeBodyIndex
    DO i=1,NTRI
      DO j=1,4
        NodeBodyIndex(TIL(i,j)) = TIL(i,5)
      END DO
    END DO

    ! count number of nodes and elements in each body
    WRITE(*,*) cubdy,'separate bodies'
    DO i=1,cubdy
      nn1=0
      nn2=0
      DO j=1,NNODE
        IF(NodeBodyIndex(j) .EQ. i) nn1=nn1+1
      END DO
      WRITE(*,*) nn1,'nodes in body ',i
      DO j=1,NTRI
        IF (ABS( TIL(j,5) - i ) < MachEps) nn2=nn2+1
      END DO
      WRITE(*,*) nn2,'elements in body',i
    END DO

    RETURN
  END SUBROUTINE getboundary


  !---------------------------------------------------------------
  ! BEM  : SOLIDANGLE
  !---------------------------------------------------------------

  SUBROUTINE solidangle()

    ! --- All we have to do in order to find the solid angle that
    ! --- each node subtends at the surface is to add up all the solid angles
    ! --- that each surface node makes with the opposite surafce of each
    ! --- tetahedra to which it belongs.

    IMPLICIT NONE

    INTEGER i,cn,p1,p2,p3
    ! REAL(KIND=DP), INTRINSIC :: GET_ANGLE

    solid(:)=0.

    DO i=1, NTRI
      cn = TIL(i,1)
      p1 = TIL(i,3)
      p2 = TIL(i,2)
      p3 = TIL(i,4)
      ! using the GET_ANGLE function
      TetSolid(i, 1) = GET_ANGLE(cn,p1,p2,p3)
      solid(cn) = solid(cn) + TetSolid(i, 1)

      cn = TIL(i,2)
      p1 = TIL(i,1)
      p2 = TIL(i,3)
      p3 = TIL(i,4)
      ! using the GET_ANGLE function
      TetSolid(i, 2) = GET_ANGLE(cn,p1,p2,p3)
      solid(cn) = solid(cn) + TetSolid(i, 2)

      cn = TIL(i,3)
      p1 = TIL(i,4)
      p2 = TIL(i,2)
      p3 = TIL(i,1)
      ! using the GET_ANGLE function
      TetSolid(i, 3) = GET_ANGLE(cn,p1,p2,p3)
      solid(cn) = solid(cn) + TetSolid(i, 3)

      cn = TIL(i,4)
      p1 = TIL(i,1)
      p2 = TIL(i,2)
      p3 = TIL(i,3)
      ! using the GET_ANGLE function
      TetSolid(i, 4) = GET_ANGLE(cn,p1,p2,p3)
      solid(cn) = solid(cn) + TetSolid(i, 4)
    END DO ! the i loop

    DO i=1, NNODE
      ! Ensure that interior nodes are exactly 4*pi.
      IF ( .NOT.(NodeOnBoundary(i)) ) solid(i) = 4*pi
    ENDDO
  END SUBROUTINE solidangle


  !---------------------------------------------------------------
  ! BEM  FUNCTION: GETANGLE
  !---------------------------------------------------------------


  REAL(KIND=DP) FUNCTION GET_ANGLE(cn,p1,p2,p3 )

    IMPLICIT NONE
    INTEGER  p1,p2,p3,cn
    REAL(KIND=DP) pi,pp1(3),pp2(3),pp3(3),top,bottom

    pi=4.0d0*atan(1.0d0)

    ! -- p1= r0-r1
    pp1(1:3)=VCL(cn,1:3)-VCL(p1,1:3)

    ! -- p2= r0-r2
    pp2(1:3)=VCL(cn,1:3)-VCL(p2,1:3)

    ! -- p3= r0-r3
    pp3(1:3)=VCL(cn,1:3)-VCL(p3,1:3)

    top=SQRT(DOT_PRODUCT(pp1,pp1)*DOT_PRODUCT(pp2,pp2)*DOT_PRODUCT(pp3,pp3)) &
      & +SQRT(DOT_PRODUCT(pp1,pp1))*DOT_PRODUCT(pp2,pp3) &
      & +SQRT(DOT_PRODUCT(pp2,pp2))*DOT_PRODUCT(pp3,pp1) &
      & +SQRT(DOT_PRODUCT(pp3,pp3))*DOT_PRODUCT(pp1,pp2)

    bottom=SQRT( &
      &     2*( &
      &         SQRT(DOT_PRODUCT(pp2,pp2)*DOT_PRODUCT(pp3,pp3)) &
      &         + DOT_PRODUCT(pp2,pp3) &
      &     ) &
      &     *( &
      &         SQRT(DOT_PRODUCT(pp3,pp3)*DOT_PRODUCT(pp1,pp1)) &
      &         + DOT_PRODUCT(pp3,pp1) &
      &     ) &
      &     *( &
      &         SQRT(DOT_PRODUCT(pp1,pp1)*DOT_PRODUCT(pp2,pp2)) &
      &         + DOT_PRODUCT(pp1,pp2) &
      &     ) &
      & )

    ! -- the result

    IF ( (.NOT.(NONZERO(bottom))).OR.(abs(top)>abs(bottom))) THEN
      GET_ANGLE= 0
    ELSE
      GET_ANGLE = (2.0d0*(dacos(top/bottom)))
    ENDIF

  END FUNCTION GET_ANGLE


  !---------------------------------------------------------------
  ! BEM  : SHAPECOEFFICIENTS
  !---------------------------------------------------------------

  SUBROUTINE shapecoeffs()
    IMPLICIT NONE

    INTEGER i,j
    REAL(KIND=DP) volume
    INTEGER n1,n2,n3,n4



    DO i=1,NTRI

      DO j=0,3

        n1=TIL(i,mod((j+1),4)+1)
        n2=TIL(i,mod((j+2),4)+1)
        n3=TIL(i,mod((j+3),4)+1)
        n4=TIL(i,j+1)

        volume=  &
          &  VCL(n1,1)*VCL(n2,2)*VCL(n3,3)-VCL(n1,1)*VCL(n2,3)*VCL(n3,2) &
          & -VCL(n2,1)*VCL(n1,2)*VCL(n3,3)+VCL(n2,1)*VCL(n1,3)*VCL(n3,2) &
          & +VCL(n3,1)*VCL(n1,2)*VCL(n2,3)-VCL(n3,1)*VCL(n1,3)*VCL(n2,2) &
          & -VCL(n4,1)*VCL(n2,2)*VCL(n3,3)+VCL(n4,1)*VCL(n2,3)*VCL(n3,2) &
          & +VCL(n2,1)*VCL(n4,2)*VCL(n3,3)-VCL(n2,1)*VCL(n4,3)*VCL(n3,2) &
          & -VCL(n3,1)*VCL(n4,2)*VCL(n2,3)+VCL(n3,1)*VCL(n4,3)*VCL(n2,2) &
          & +VCL(n4,1)*VCL(n1,2)*VCL(n3,3)-VCL(n4,1)*VCL(n1,3)*VCL(n3,2) &
          & -VCL(n1,1)*VCL(n4,2)*VCL(n3,3)+VCL(n1,1)*VCL(n4,3)*VCL(n3,2) &
          & +VCL(n3,1)*VCL(n4,2)*VCL(n1,3)-VCL(n3,1)*VCL(n4,3)*VCL(n1,2) &
          & -VCL(n4,1)*VCL(n1,2)*VCL(n2,3)+VCL(n4,1)*VCL(n1,3)*VCL(n2,2) &
          & +VCL(n1,1)*VCL(n4,2)*VCL(n2,3)-VCL(n1,1)*VCL(n4,3)*VCL(n2,2) &
          & -VCL(n2,1)*VCL(n4,2)*VCL(n1,3)+VCL(n2,1)*VCL(n4,3)*VCL(n1,2)

        vol(i)=abs(volume)/6.d0

        b(i,j+1) = ( &
          &     VCL(n2,2)*VCL(n1,3)-VCL(n3,2)*VCL(n1,3) &
          &     -VCL(n1,2)*VCL(n2,3)+VCL(n3,2)*VCL(n2,3) &
          &     +VCL(n1,2)*VCL(n3,3)-VCL(n2,2)*VCL(n3,3) &
          & )/sign(1.0d0,volume)

        c(i,j+1) = ( &
          &     -VCL(n2,1)*VCL(n1,3)+VCL(n3,1)*VCL(n1,3) &
          &     +VCL(n1,1)*VCL(n2,3)-VCL(n3,1)*VCL(n2,3) &
          &     -VCL(n1,1)*VCL(n3,3)+VCL(n2,1)*VCL(n3,3) &
          & )/sign(1.0d0,volume)

        d(i,j+1) = ( &
          &     VCL(n2,1)*VCL(n1,2)-VCL(n3,1)*VCL(n1,2) &
          &     -VCL(n1,1)*VCL(n2,2)+VCL(n3,1)*VCL(n2,2) &
          &     +VCL(n1,1)*VCL(n3,2)-VCL(n2,1)*VCL(n3,2) &
          & )/sign(1.0d0,volume)

      END DO
    END DO


    ! calculate volume vbox associated with node i

    vbox(:)=0.
    DO i=1,NTRI
      DO j=1,4
        vbox(TIL(i,j))=vbox(TIL(i,j))+(0.25*vol(i))
      END DO
    END DO

    total_volume=0.0d0
    do i= 1, NTRI
      total_volume=total_volume+ vol(i)
    end do

    write(*,*) 'Total volume = ',total_volume
    RETURN
  END SUBROUTINE shapecoeffs




  !---------------------------------------------------------------
  ! MeshInterpolate
  !---------------------------------------------------------------

  SUBROUTINE MeshInterpolate(vec,IntM )
    IMPLICIT NONE

    INTEGER IntERROR
    REAL(KIND=DP) :: vec(3), IntM(3),IntNorm
    ! 0, if no errors were encountered.
    ! 1, if n, nq, nw, or nr is out of range.
    ! 2, if duplicate nodes were encountered.
    ! 3, if all nodes are coplanar.

    IntNQ=10
    IntNW=15
    IntNR=int( real(NNODE/2)**(1.0/3.0) )

    IF(InterpolatableQ.EQV..FALSE.) THEN
      IF(ALLOCATED(IntLCELL)) DEALLOCATE(IntLCELL,IntLNEXT,IntRSQ,IntA)
      allocate(IntLCELL(IntNR,IntNR,IntNR),IntLNEXT(NNODE))
      allocate(IntRSQ(NNODE,3),IntA(9,NNODE,3))

      call qshep3 (NNODE, VCL(:,1),VCL(:,2),VCL(:,3),m(:,1),&
        & IntNQ,IntNW,IntNR,IntLCELL,IntLNEXT, &
        & IntXYZMin, IntXYZDel, IntRMax(1), IntRSQ(:,1),IntA(:,:,1), IntERROR)
      IF(IntERROR/=0) Write(*,*) 'IntERROR mx. ERR=',IntERROR

      call qshep3 (NNODE, VCL(:,1),VCL(:,2),VCL(:,3),m(:,2),&
        & IntNQ,IntNW,IntNR,IntLCELL,IntLNEXT, &
        & IntXYZMin, IntXYZDel, IntRMax(2), IntRSQ(:,2),IntA(:,:,2), IntERROR)
      IF(IntERROR/=0) Write(*,*) 'IntERROR my. ERR=',IntERROR

      call qshep3 (NNODE, VCL(:,1),VCL(:,2),VCL(:,3),m(:,3),&
        & IntNQ,IntNW,IntNR,IntLCELL,IntLNEXT, &
        & IntXYZMin, IntXYZDel, IntRMax(3), IntRSQ(:,3),IntA(:,:,3), IntERROR)
      IF(IntERROR/=0) Write(*,*) 'IntERROR mz. ERR=',IntERROR

      InterpolatableQ=.TRUE.
    ENDIF

    IntM(1) = qs3val(vec(1),vec(2),vec(3), &
      &  NNODE,VCL(:,1),VCL(:,2),VCL(:,3), m(:,1), &
      & IntNR, IntLCELL, IntLNEXT, &
      & IntXYZMin, IntXYZDel, IntRMax(1), IntRSQ(:,1), IntA(:,:,1))

    IntM(2) = qs3val(vec(1),vec(2),vec(3), &
      &  NNODE,VCL(:,1),VCL(:,2),VCL(:,3), m(:,2), &
      & IntNR, IntLCELL, IntLNEXT, &
      & IntXYZMin, IntXYZDel, IntRMax(2), IntRSQ(:,2), IntA(:,:,2))

    IntM(3) = qs3val(vec(1),vec(2),vec(3), &
      &  NNODE,VCL(:,1),VCL(:,2),VCL(:,3), m(:,3), &
      & IntNR, IntLCELL, IntLNEXT, &
      & IntXYZMin, IntXYZDel, IntRMax(3), IntRSQ(:,3), IntA(:,:,3))

    IntNorm= sqrt(IntM(1)*IntM(1)+IntM(2)*IntM(2)+IntM(3)*IntM(3))
    IntM(1)=IntM(1)/IntNorm;IntM(2)=IntM(2)/IntNorm;IntM(3)=IntM(3)/IntNorm;
  END SUBROUTINE MeshInterpolate


  !---------------------------------------------------------------
  !         Working with multiple meshes
  !---------------------------------------------------------------


  !---------------------------------------------------------------
  !           SaveMesh(MeshNo) saves all important global variables
  !                            of the mesh into arrays
  !                            The index of the mesh is MeshNo
  !---------------------------------------------------------------

  SUBROUTINE SaveMesh(MeshNo)
    IMPLICIT NONE

    INTEGER MeshNo

    If(MeshNo> MaxMeshNumber) THEN
      Write(*,*) ' Meshnumber ',MeshNo,' too large'
      RETURN
    ENDIF


    If(ALLOCATED(SaveNNODE).EQV..FALSE.)  THEN
      ALLOCATE(SaveNNODE(MaxMeshNumber))
      ALLOCATE(SaveNTRI(MaxMeshNumber))
      ALLOCATE(SaveBFCE(MaxMeshNumber))
      ALLOCATE(SaveBNODE(MaxMeshNumber))

      ALLOCATE(SaveTIL(MaxMeshNumber))
      ALLOCATE(SaveBDFACE(MaxMeshNumber))

      ALLOCATE(SaveVCL(MaxMeshNumber))
      ALLOCATE(Savevol(MaxMeshNumber))
      ALLOCATE(Savevbox(MaxMeshNumber))
      ALLOCATE(Saveb(MaxMeshNumber))
      ALLOCATE(Savec(MaxMeshNumber))
      ALLOCATE(Saved(MaxMeshNumber))
      ALLOCATE(SaveNodeBodyIndex(MaxMeshNumber))
      ALLOCATE(SaveNodeOnBoundary(MaxMeshNumber))
    ENDIF

    SaveNNODE(MeshNo)=NNODE
    SaveNTRI(MeshNo)=NTRI
    SaveBFCE(MeshNo)=BFCE
    SaveBNODE(MeshNo)=BNODE


    CALL AllocSet(TIL, SaveTIL(MeshNo)%IntArrSave)
    CALL AllocSet(BDFACE, SaveBDFACE(MeshNo)%IntArrSave)

    CALL AllocSet(vbox, Savevbox(MeshNo)%DPSave)
    CALL AllocSet(vol, Savevol(MeshNo)%DPSave)
    CALL AllocSet(VCL, SaveVCL(MeshNo)%DPArrSave)
    CALL AllocSet(b, Saveb(MeshNo)%DPArrSave)
    CALL AllocSet(c, Savec(MeshNo)%DPArrSave)
    CALL AllocSet(d, Saved(MeshNo)%DPArrSave)
    CALL AllocSet(NodeBodyIndex, SaveNodeBodyIndex(MeshNo)%IntSave)
    CALL AllocSet(NodeOnBoundary, SaveNodeOnBoundary(MeshNo)%LogicalSave)

  END SUBROUTINE SaveMesh


  !---------------------------------------------------------------
  !           LoadMesh(MeshNo) loads all important global variables
  !                            of the mesh
  !                            The index of the mesh is MeshNo
  !---------------------------------------------------------------

  SUBROUTINE LoadMesh(MeshNo)
    IMPLICIT NONE

    INTEGER MeshNo

    If(MeshNo> MaxMeshNumber) THEN
      Write(*,*) ' Meshnumber ',MeshNo,' too large'
      RETURN
    ENDIF

    If(ALLOCATED(SaveNNODE).EQV..FALSE.)  THEN
      Write(*,*) ' No meshes saved ... '
      RETURN
    ENDIF

    NNODE=SaveNNODE(MeshNo)
    NTRI=SaveNTRI(MeshNo)
    BFCE=SaveBFCE(MeshNo)
    BNODE=SaveBNODE(MeshNo)

    CALL AllocSet(SaveTIL(MeshNo)%IntArrSave, TIL)
    CALL AllocSet(SaveBDFACE(MeshNo)%IntArrSave, BDFACE)

    CALL AllocSet(SaveVCL(MeshNo)%DPArrSave, VCL)
    CALL AllocSet(Savevbox(MeshNo)%DPSave, vbox)
    CALL AllocSet(Savevol(MeshNo)%DPSave, vol)
    CALL AllocSet(Saveb(MeshNo)%DPArrSave, b)
    CALL AllocSet(Savec(MeshNo)%DPArrSave, c)
    CALL AllocSet(Saved(MeshNo)%DPArrSave, d)
    CALL AllocSet(SaveNodeBodyIndex(MeshNo)%IntSave, NodeBodyIndex)
    CALL AllocSet(SaveNodeOnBoundary(MeshNo)%LogicalSave, NodeOnBoundary)


    IF(ALLOCATED(gradc))  DEALLOCATE(gradc)
    IF(ALLOCATED(mdotn))  DEALLOCATE(mdotn)
    IF(ALLOCATED(m))      DEALLOCATE(m)
    IF(ALLOCATED(totfx))  DEALLOCATE(totfx)
    IF(ALLOCATED(fx))     DEALLOCATE(fx)
    IF(ALLOCATED(fx2))    DEALLOCATE(fx2)

    ALLOCATE(gradc(NNODE,3))
    ALLOCATE(mdotn(NNODE,3))
    ALLOCATE(m(NNODE,3))
    ALLOCATE(totfx(NNODE))
    ALLOCATE(fx(NNODE))
    ALLOCATE(fx2(NNODE))

    gradc = 0
    mdotn = 0
    m = 1/SQRT(REAL(3,KIND=DP))
    totfx = 0
    fx = 0
    fx2 = 0

  END SUBROUTINE LoadMesh


  !---------------------------------------------------------------
  ! RemeshTo(MeshNo) takes the current magnetization m
  !                  of the mesh and remeshes it to the saved
  !                  mesh with index MeshNo
  !                  The resulting magnetization is in the array mremesh
  !                  This is accessible and of right size after LoadMesh(MeshNo)
  !---------------------------------------------------------------

  SUBROUTINE RemeshTo(MeshNo, mremesh)
    IMPLICIT NONE

    REAL(KIND=DP), ALLOCATABLE, INTENT(INOUT) :: mremesh(:,:)
    INTEGER, INTENT(IN) :: MeshNo
    INTEGER :: SNNODE , i
    REAL(KIND=DP) :: srvec(3)
    REAL(KIND=DP) :: MLen2

    If(MeshNo> MaxMeshNumber) THEN
      Write(*,*) ' Meshnumber ',MeshNo,' too large'
      RETURN
    ENDIF

    If(ALLOCATED(SaveNNODE).EQV..FALSE.)  THEN
      Write(*,*) ' No meshes saved ... '
      RETURN
    ENDIF

    ! If(ALLOCATED(SaveVCL(MeshNo)%DPArrSave).EQV..FALSE.)  THEN
    !   Write(*,*) ' No XYZ  saved  for Meshnumber ',MeshNo
    !   RETURN
    ! ENDIF

    SNNODE = SaveNNODE(MeshNo)
    If(ALLOCATED(mremesh) ) DEALLOCATE(mremesh)
    ALLOCATE( mremesh(SNNODE,3))


    InterpolatableQ = .FALSE.
    Do i=1,SNNODE
      srvec(:)=SaveVCL(MeshNo)%DPArrSave(i,:)
      call MeshInterpolate(srvec, mremesh(i,:))
    END DO

    DO i=1,SNNODE
        MLen2 = SUM(mremesh(i,:)**2)
        mremesh(i,:) = mremesh(i,:) / SQRT(MLen2)
    END DO

  END SUBROUTINE RemeshTo


  !---------------------------------------------------------------
  !     subroutine  InvertMag( strx,stry,strz  )
  !---------------------------------------------------------------

  subroutine InvertMag( strx,stry,strz  )
    USE strings
    IMPLICIT NONE

    CHARACTER(LEN=*) :: strx,stry,strz
    integer ::  ios,i
    REAL(KIND=DP) :: inv(3)

    call value(strx,inv(1),ios)
    if(ios.eq.0) THEN
      call value(stry,inv(2),ios)
      if(ios.eq.0)  call value(strz,inv(3),ios)
    endif
    if (ios/=0) return


    inv(:)=inv(:)/abs(inv(:))
    Do i=1,NNODE
      m(i,:)=m(i,:)*inv(:)
    EndDo

    return
  end subroutine InvertMag


  !---------------------------------------------------------------
  !     subroutine  ModifyMag( str,angle )
  !---------------------------------------------------------------

  subroutine ModifyMag( str,angle )
    USE strings
    IMPLICIT NONE

    CHARACTER(LEN=*), INTENT(IN) :: str
    CHARACTER(LEN=LEN(str)) :: lstr
    integer ::  i,j
    REAL(KIND=DP) :: angle, dm(3),pm(3),dmnorm,rangle=0.

    lstr=lowercase(str)
    rangle=tan(angle/180.*3.1415926535897)
    do j=1,3  ! calculate a random but uniform disturbation vector
      dm(j)=DRAND()-0.5
    enddo

    do i=1,NNODE
      ! in this case every spin is disturbed independently
      if (lstr.eq.'random') then
        do j=1,3
          dm(j)=DRAND()-0.5
        enddo
      endif

      pm(1)=dm(2)*m(i,3)-dm(3)*m(i,2)
      pm(2)=dm(3)*m(i,1)-dm(1)*m(i,3)
      pm(3)=dm(1)*m(i,2)-dm(2)*m(i,1)
      dmnorm=rangle/sqrt(pm(1)**2 + pm(2)**2 + pm(3)**2)

      do j=1,3
        dm(j)=m(i,j)+pm(j)*dmnorm
      enddo

      dmnorm=sqrt(dm(1)**2 + dm(2)**2 + dm(3)**2)
      IF(dmnorm > 0.0) THEN
        dm(1)=dm(1)/dmnorm
        dm(2)=dm(2)/dmnorm
        dm(3)=dm(3)/dmnorm
      ENDIF

      m(i,:)=dm(:)
    enddo

    return
  end subroutine ModifyMag


!---------------------------------------------------------------
!     subroutine randomchange(args(2),args(3),sd )
!---------------------------------------------------------------

  subroutine randomchange( str,sran, sd )
    USE strings
    IMPLICIT NONE

    CHARACTER(LEN=*), INTENT(IN) :: str, sran
    CHARACTER(LEN=LEN(str)) :: lstr
    INTEGER, OPTIONAL, INTENT(IN) :: sd
    integer ::  ios,i,j,k, sd_min, sd_max
    REAL(KIND=DP) :: dm(3),pm(3),dmnorm,rangle=0.
    
    IF(PRESENT(sd)) THEN


    lstr=lowercase(str)
    if (lstr.eq.'all') then
!      do i=1,NNODE
      do i=1,NTRI
!        if(NodeBlockNumber(i)<2) then  ! only free nodes are randomized !!
         if(TetSubDomains(i).eq.sd) then !only randomise nodes of this subdomain
         do k=1,4 !nodes per tet
          do j=1,3
            dm(j)=DRAND()-0.5
          enddo
          dmnorm=sqrt(dm(1)**2 + dm(2)**2 + dm(3)**2)
          IF(dmnorm > 0.0) THEN
            dm(1)=dm(1)/dmnorm
            dm(2)=dm(2)/dmnorm
            dm(3)=dm(3)/dmnorm
          ENDIF
          m(Til(i,k),:)=dm(:)
         enddo 
        endif
      enddo
    else
      call value(sran,rangle,ios)
      if(ios/=0) write (*,*)  'ERROR setting random angle: ',sran
      rangle=tan(rangle/180.*3.1415926535897)
      do i=1,NTRI
        if(TetSubDomains(i).eq. sd) then  ! only randomise nodes in this domain
         do k=1,4
          do j=1,3
            dm(j)=DRAND()-0.5
          enddo
          pm(1)=dm(2)*m(i,3)-dm(3)*m(i,2)
          pm(2)=dm(3)*m(i,1)-dm(1)*m(i,3)
          pm(3)=dm(1)*m(i,2)-dm(2)*m(i,1)
          dmnorm=rangle/sqrt(pm(1)**2 + pm(2)**2 + pm(3)**2)
          do j=1,3
            dm(j)=m(i,j)+pm(j)*dmnorm
          enddo
          dmnorm=sqrt(dm(1)**2 + dm(2)**2 + dm(3)**2)
          IF(dmnorm > 0.0) THEN
            dm(1)=dm(1)/dmnorm
            dm(2)=dm(2)/dmnorm
            dm(3)=dm(3)/dmnorm
          ENDIF
          m(Til(i,k),:)=dm(:)
         enddo 
        endif
      enddo
    endif
    
    ELSE
    

    lstr=lowercase(str)
    if (lstr.eq.'all') then
      do i=1,NNODE
        if(NodeBlockNumber(i)<2) then  ! only free nodes are randomized !!
          do j=1,3
            dm(j)=DRAND()-0.5
          enddo
          dmnorm=sqrt(dm(1)**2 + dm(2)**2 + dm(3)**2)
          IF(dmnorm > 0.0) THEN
            dm(1)=dm(1)/dmnorm
            dm(2)=dm(2)/dmnorm
            dm(3)=dm(3)/dmnorm
          ENDIF
          m(i,:)=dm(:)
        endif
      enddo
    else
      call value(sran,rangle,ios)
      if(ios/=0) write (*,*)  'ERROR setting random angle: ',sran
      rangle=tan(rangle/180.*3.1415926535897)
      do i=1,NNODE
        if(NodeBlockNumber(i)<2) then  ! only free nodes are randomized !!
          do j=1,3
            dm(j)=DRAND()-0.5
          enddo
          pm(1)=dm(2)*m(i,3)-dm(3)*m(i,2)
          pm(2)=dm(3)*m(i,1)-dm(1)*m(i,3)
          pm(3)=dm(1)*m(i,2)-dm(2)*m(i,1)
          dmnorm=rangle/sqrt(pm(1)**2 + pm(2)**2 + pm(3)**2)
          do j=1,3
            dm(j)=m(i,j)+pm(j)*dmnorm
          enddo
          dmnorm=sqrt(dm(1)**2 + dm(2)**2 + dm(3)**2)
          IF(dmnorm > 0.0) THEN
            dm(1)=dm(1)/dmnorm
            dm(2)=dm(2)/dmnorm
            dm(3)=dm(3)/dmnorm
          ENDIF
          m(i,:)=dm(:)
        endif
      enddo
    endif

    ENDIF
    
    
    
   return
  end subroutine randomchange


  !
  ! VortexState(p, [p0], [v], [orientation])
  !   Seed a rough vortex state with core oriented along the line
  !   from point p0 to p, with component v in the direction of the core.
  !   Orientation can be specified using
  !     orientation = 'RH'
  !   or
  !     orientation = 'LH'
  !   Default orientation is RH.
  !
  SUBROUTINE VortexMag(p, p0, v, orientation)
    REAL(KIND=DP), INTENT(IN) :: p(3)
    REAL(KIND=DP), OPTIONAL, INTENT(IN) :: p0(3), v
    CHARACTER(len=*), OPTIONAL, INTENT(IN) :: orientation

    INTEGER :: orientation_v
    INTEGER, PARAMETER :: RH = 1, LH = -1

    REAL(KIND=DP) :: p0_v(3), v_v

    REAL(KIND=DP) :: dirn(3)
    INTEGER :: i

    ! Set p0
    IF(PRESENT(p0)) THEN
      p0_v = p0
    ELSE
      p0_v = 0
    END IF

    ! Set v
    IF(PRESENT(v)) THEN
      v_v = v
    ELSE
      v_v = 0
    END IF

    ! Set orientation
    IF(PRESENT(orientation)) THEN
      SELECT CASE(orientation)
        CASE('RH')
          orientation_v = RH
        CASE('LH')
          orientation_v = LH
        CASE DEFAULT
          WRITE(*,*) "ERROR: VortexState: Unknown orientation ", TRIM(orientation)
          ERROR STOP
      END SELECT
    ELSE
      orientation_v = RH
    END IF

    dirn = p - p0

    DO i=1,NNODE
      m(i,:) = orientation_v*CROSS(dirn, VCL(i,1:3) - p0_v) + v_v*dirn

    BLOCK
      REAL(KIND=DP) :: normm

      normm = NORM2(m(i,:))
      IF(NONZERO(normm)) THEN
        m(i,:) = m(i,:) / normm
      END IF
    END BLOCK
    END DO

  CONTAINS
    FUNCTION CROSS(a,b)
      REAL(KIND=DP) :: CROSS(3)
      REAL(KIND=DP), INTENT(IN) :: a(3), b(3)

      CROSS(1) = a(2)*b(3) - a(3)*b(2)
      CROSS(2) = a(3)*b(1) - a(1)*b(3)
      CROSS(3) = a(1)*b(2) - a(2)*b(1)
    END FUNCTION CROSS
  END SUBROUTINE VortexMag

END MODULE Tetrahedral_Mesh_Data
