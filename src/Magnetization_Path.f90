!> A module containing the NEB path calculation procedures.
MODULE Magnetization_Path
  USE Tetrahedral_Mesh_Data
  USE Material_Parameters
  USE Finite_Element

  IMPLICIT NONE
  SAVE

  !> Number of structures along the path 
  INTEGER PathN


  !
  ! Path arrays and values
  !

  !> Path magnetization structures 
  REAL(KIND=DP), ALLOCATABLE :: PMag(:,:,:)

  !> Path tangent vectors
  REAL(KIND=DP), ALLOCATABLE :: PTan(:,:,:)

  !> Path energy gradient vectors
  REAL(KIND=DP), ALLOCATABLE :: PEGrad(:,:,:) 

  !> Path nudged-elastic-band gradient vectors
  REAL(KIND=DP), ALLOCATABLE :: PNEBGrad(:,:,:) 


  !> Path energies 
  REAL(KIND=DP), ALLOCATABLE :: PEn(:)

  !> Path distances
  REAL(KIND=DP), ALLOCATABLE :: PDist(:)

  !> @todo DOCUMENT ME
  REAL(KIND=DP), ALLOCATABLE :: PEGradNorm(:)

  !> @todo DOCUMENT ME
  REAL(KIND=DP), ALLOCATABLE :: PAlign(:)

  !> Cumulative path distances
  REAL(KIND=DP), ALLOCATABLE :: CumulPDist(:)

  !> @todo DOCUMENT ME
  REAL(KIND=DP), ALLOCATABLE :: PSpringForce(:)   

  !> Flags for recording changes in Path
  !> (only changed nodes are recalculated)
  LOGICAL, ALLOCATABLE :: PathChangedQ(:)


  !
  ! Path parameters
  !

  !> Path length 
  REAL(KIND=DP) :: PLength

  !> Geometric action along path 
  REAL(KIND=DP) :: PAction

  !> @todo DOCUMENT ME
  REAL(KIND=DP) :: PDeltaGeodesic 

  !> @todo DOCUMENT ME
  REAL(KIND=DP) :: PHooke

  !> @todo DOCUMENT ME
  REAL(KIND=DP) :: CurveWeight

  !> @todo DOCUMENT ME
  REAL(KIND=DP) :: PTarget

  !> Alpha for initial path determination: typical energy variation
  REAL(KIND=DP) :: InitAlpha

  !> Intended distance from initial state during creation of initial path
  REAL(KIND=DP) :: InitDelta

  !> Distance between initial and final state of the path 
  REAL(KIND=DP) :: PathEndDist

  !> @todo DOCUMENT ME
  REAL(KIND=DP) :: InitEVar(10)

  !> Reference position for initial path calculation
  INTEGER :: InitRefPos

  !> @todo DOCUMENT ME
  LOGICAL :: AddInitPathEnergyQ=.FALSE.

  !> @todo DOCUMENT ME
  LOGICAL :: PathLoggingQ


  !
  ! Log file names
  !

  !> @todo DOCUMENT ME
  CHARACTER(LEN=200) :: PathInFile

  !> @todo DOCUMENT ME
  CHARACTER(LEN=200) :: PathOutFile
  
   !> @todo DOCUMENT ME
  CHARACTER(LEN=200) :: ZoneInFile

  !> @todo DOCUMENT ME
  CHARACTER(LEN=200) :: PLogEnFile

  !> @todo DOCUMENT ME
  CHARACTER(LEN=200) :: PLogGradFile

  !> @todo DOCUMENT ME
  CHARACTER(LEN=200) :: PLogDistFile

  !> @todo DOCUMENT ME
  CHARACTER(LEN=200) :: PLogFile


  !
  ! Log file units
  !

  !> @todo DOCUMENT ME
  INTEGER :: PLogEnUnit = 0

  !> @todo DOCUMENT ME
  INTEGER :: PLogGradUnit = 0

  !> @todo DOCUMENT ME
  INTEGER :: PLogDistUnit = 0
  
  ! Log file header flag
  INTEGER :: firstrow = 0


  !> Breaking circular dependencies on Energy_Calculator
  INTERFACE
    !> An interface for the energy_calculator.buildenergycalculator()
    !> subroutine.
    SUBROUTINE BuildEnergyCalculatorProcedure
      !> An interface for the energy_calculator.buildenergycalculator()
      !> subroutine.
    END SUBROUTINE BuildEnergyCalculatorProcedure


    !> An interface for the energy_calculator.energymin() subroutine.
    SUBROUTINE EnergyMinProcedure
      !> An interface for the energy_calculator.energymin() subroutine.
    END SUBROUTINE EnergyMinProcedure


    !> An interface for the energy_calculator.et_grad() subroutine.
    SUBROUTINE ET_GRAD_PROCEDURE(ETOT, grad, X, neval)
      !>
      !> @param[out]   ETOT  The total magnetic energy.
      !> @param[out]   grad  The manetic energy gradient (in spherical polars)
      !> @param[in]    X     The initial guess for the LEM (in spherical polars)
      !> @param[inout] neval The number of times the function has been
      !>                     evaluated.
      !>

      USE Utils
      IMPLICIT NONE
    
      REAL(KIND=DP), INTENT(OUT) :: ETOT
      REAL(KIND=DP), INTENT(OUT) :: grad(:)
      REAL(KIND=DP), INTENT(IN)  :: X(:)
      INTEGER, INTENT(INOUT)     :: neval
    END SUBROUTINE ET_GRAD_PROCEDURE
  END INTERFACE

  ! These procedure pointers should be set up by Energy_Calculator

  !> A pointer to the energy_calculator.buildenergycalculator() subroutine.
  PROCEDURE(BuildEnergyCalculatorProcedure), POINTER :: &
    BuildEnergyCalculator => NULL()

  !> A pointer to the energy_calculator.energymin() subroutine.
  PROCEDURE(EnergyMinProcedure), POINTER :: EnergyMin => NULL()

  !> A pointer to the energy_calculator.et_grad() subroutine.
  PROCEDURE(ET_GRAD_PROCEDURE), POINTER :: ET_GRAD => NULL()

  ! Don't export the pointers outside this module
  PRIVATE :: BuildEnergyCalculator, EnergyMin, ET_GRAD

CONTAINS

  !---------------------------------------------------------------
  !          InitializeMagnetizationPath
  !             Set reasonable defaults
  !---------------------------------------------------------------

  !> Initialize the Magnetization_Path module
  SUBROUTINE InitializeMagnetizationPath()
    IMPLICIT NONE

    CALL DestroyMagnetizationPath()

    PLogEnUnit = 0
    PLogGradUnit = 0
    PLogDistUnit = 0

    PHooke=0.
    CurveWeight=0.

    BuildEnergyCalculator => NULL()
    EnergyMin => NULL()
    ET_GRAD => NULL()
  END SUBROUTINE InitializeMagnetizationPath

  !> Destroy and clean up the variables in the Magnetization_Path module
  SUBROUTINE DestroyMagnetizationPath()
    IF(ALLOCATED(PMag))         DEALLOCATE(PMag)
    IF(ALLOCATED(PTan))         DEALLOCATE(PTan)
    IF(ALLOCATED(PEGrad))       DEALLOCATE(PEGrad)
    IF(ALLOCATED(PNEBGrad))     DEALLOCATE(PNEBGrad)
    IF(ALLOCATED(PEn))          DEALLOCATE(PEn)
    IF(ALLOCATED(PDist))        DEALLOCATE(PDist)
    IF(ALLOCATED(PEGradNorm))   DEALLOCATE(PEGradNorm)
    IF(ALLOCATED(PAlign))       DEALLOCATE(PAlign)
    IF(ALLOCATED(CumulPDist))   DEALLOCATE(CumulPDist)
    IF(ALLOCATED(PSpringForce)) DEALLOCATE(PSpringForce)
    IF(ALLOCATED(PathChangedQ)) DEALLOCATE(PathChangedQ)

    CALL CloseIfOpen(PLogEnUnit)
    CALL CloseIfOpen(PLogGradUnit)
    CALL CloseIfOpen(PLogDistUnit)
  CONTAINS
    SUBROUTINE CloseIfOpen(unit)
      INTEGER, INTENT(INOUT) :: unit
      LOGICAL :: is_open

      is_open = .FALSE.
      IF(unit .NE. 0) INQUIRE(UNIT=unit, OPENED=is_open)
      IF(is_open) CLOSE(UNIT=unit)

      unit = 0
    END SUBROUTINE CloseIfOpen
  END SUBROUTINE DestroyMagnetizationPath


  !> Set the pointers to the necessary energy_calculator subroutines.
  SUBROUTINE InitializeMagnetizationPathPointers( &
    BuildEnergyCalculator_in, EnergyMin_in, ET_GRAD_in &
  )
    PROCEDURE(BuildEnergyCalculator) :: BuildEnergyCalculator_in
    PROCEDURE(EnergyMin) :: EnergyMin_in
    PROCEDURE(ET_GRAD_PROCEDURE) :: ET_GRAD_in

    BuildEnergyCalculator => BuildEnergyCalculator_in
    EnergyMin => EnergyMin_in
    ET_GRAD => ET_GRAD_in
  END SUBROUTINE InitializeMagnetizationPathPointers


  !---------------------------------------------------------------
  !          PathAllocate
  !---------------------------------------------------------------

  !> Allocate the variables necessary for a the magnetization path in
  !> an NEB calculation.
  SUBROUTINE PathAllocate( )

    IMPLICIT NONE

    If(ALLOCATED(PMag))         DEALLOCATE(PMag)
    If(ALLOCATED(PTan))         DEALLOCATE(PTan)
    If(ALLOCATED(PEGrad))       DEALLOCATE(PEGrad)
    If(ALLOCATED(PNEBGrad))     DEALLOCATE(PNEBGrad)
    If(ALLOCATED(PEGradNorm))   DEALLOCATE(PEGradNorm)
    If(ALLOCATED(PAlign))       DEALLOCATE(PAlign)
    If(ALLOCATED(PEn))          DEALLOCATE(PEn)
    If(ALLOCATED(PDist))        DEALLOCATE(PDist)
    If(ALLOCATED(CumulPDist))   DEALLOCATE(CumulPDist)
    If(ALLOCATED(PSpringForce)) DEALLOCATE(PSpringForce)
    If(ALLOCATED(PathChangedQ)) DEALLOCATE(PathChangedQ)


    If(ALLOCATED(PMag))         write(*,*) 'Alloc 1'
    If(ALLOCATED(PTan))         write(*,*) 'Alloc 2'
    If(ALLOCATED(PEGrad))       write(*,*) 'Alloc 3'
    If(ALLOCATED(PNEBGrad))     write(*,*) 'Alloc 4'
    If(ALLOCATED(PEGradNorm))   write(*,*) 'Alloc 5'
    If(ALLOCATED(PEn))          write(*,*) 'Alloc 6'
    If(ALLOCATED(PDist))        write(*,*) 'Alloc 7'
    If(ALLOCATED(CumulPDist))   write(*,*) 'Alloc 8'
    If(ALLOCATED(PAlign))       write(*,*) 'Alloc 9'
    If(ALLOCATED(PSpringForce)) write(*,*) 'Alloc 10'
    If(ALLOCATED(PathChangedQ)) write(*,*) 'Alloc 11'

    Allocate(PMag(PathN,NNODE,3), PTan(PathN,NNODE,3) )
    Allocate(PEGrad(PathN,NNODE,3), PNEBGrad(PathN,NNODE,3))
    Allocate(PEn(PathN), PDist(PathN), CumulPDist(PathN))
    Allocate(PEGradNorm(PathN), PAlign(PathN), PathChangedQ(PathN))
    Allocate(PSpringForce(PathN))

    PMag(:,:,:)     = 0.
    PTan(:,:,:)     = 0.
    PEGrad(:,:,:)   = 0.
    PNEBGrad(:,:,:) = 0.
    PEn(:)          = 0.
    PDist(:)        = 0.
    CumulPDist(:)   = 0.
    PEGradNorm(:)   = 0.
    PAlign(:)       = 0.
    PSpringForce(:) = 0.

    PathChangedQ(:) = .TRUE.

  END SUBROUTINE PathAllocate


  !> Calculate linear distances and cumulative distances 
  !> along the path
  SUBROUTINE PathRenewDist( )

    IMPLICIT NONE
    INTEGER i 

    CumulPDist(1)=0
    Do i=1,PathN-1
       ! PDist(i) =  Distance between structures i and i+1
       PDist(i)=Distance(PMag(i,:,:),PMag(i+1,:,:))

       ! Cumulative distance to structure i+1
       CumulPDist(i+1)=CumulPDist(i)+PDist(i)
    EndDo

    !  Total path length
    PLength=CumulPDist(PathN)

    !  Distance between start and end 
    PathEndDist=Distance(PMag(1,:,:),PMag(PathN,:,:))

  END SUBROUTINE PathRenewDist


  !>  Calculate normalized tangent vectors, `PTan` as weighted finite
  !>  differences of the magnetization vectors.
  !>  All tangent vectors are projected into the S2^N tangent space
  SUBROUTINE PathTangents( )

    IMPLICIT NONE
    INTEGER pn
    DOUBLE PRECISION x1,x2,x3,Dm1(NNODE,3),Dm2(NNODE,3)

    ! Calculate normalized tangent vectors along the path
    PTan(1,:,:)= (PMag(2,:,:)-PMag(1,:,:))

    ! projects path tangent on magnetization tangent space
    call S2Project(PTan(1,:,:),PMag(1,:,:))
    x2=MagNorm(PTan(1,:,:))
    PTan(1,:,:)=PTan(1,:,:)/x2


    PTan(PathN,:,:)= (PMag(PathN,:,:)-PMag(PathN-1,:,:))
    call S2Project(PTan(PathN,:,:),PMag(PathN,:,:)) 
    x2=MagNorm(PTan(PathN,:,:))
    PTan(PathN,:,:)=PTan(PathN,:,:)/x2


    Do pn=2,PathN-1
       x1=CumulPDist(pn-1)
       x2=CumulPDist(pn)
       x3=CumulPDist(pn+1)
       Dm1(:,:)= PMag(pn,:,:)-PMag(pn-1,:,:)
       Dm2(:,:)= PMag(pn+1,:,:)-PMag(pn,:,:)
       PTan(pn,:,:) = Dm1(:,:)*(x3-x2)/(x3-x1)/(x2-x1) + Dm2(:,:)*(x2-x1)/(x3-x1)/(x3-x2)
       call S2Project(PTan(pn,:,:),PMag(pn,:,:))
       x2=MagNorm(PTan(pn,:,:))
       PTan(pn,:,:)=PTan(pn,:,:)/x2
       PSpringForce(pn)=PHooke*(PDist(pn )-PDist(pn-1))
    EndDo

  END SUBROUTINE PathTangents


  !> Calculate normalized tangent vectors, `PTan` using the method of
  !> G. Henkelman and H. Jonsson, (2000).
  !> All tangent vectors are projected into the S2^N tangent space
  SUBROUTINE HenkelmanTangents( )

    IMPLICIT NONE
    INTEGER pn
    DOUBLE PRECISION DEmax,DEMin,norm,Dm1(NNODE,3),Dm2(NNODE,3)

    PTan(1,:,:)= (PMag(2,:,:)-PMag(1,:,:))    ! Calculate normalized tangent vectors along the path
    call S2Project(PTan(1,:,:),PMag(1,:,:)) ! projects path tangent on magnetization tangent space
    norm=MagNorm(PTan(1,:,:))
    PTan(1,:,:)=PTan(1,:,:)/norm


    PTan(PathN,:,:)= (PMag(PathN,:,:)-PMag(PathN-1,:,:))
    call S2Project(PTan(PathN,:,:),PMag(PathN,:,:)) 
    norm=MagNorm(PTan(PathN,:,:))
    PTan(PathN,:,:)=PTan(PathN,:,:)/norm


    Do pn=2,PathN-1
       Dm1(:,:)= PMag(pn,:,:)-PMag(pn-1,:,:)   ! tau- 
       Dm2(:,:)= PMag(pn+1,:,:)-PMag(pn,:,:)   ! tau+
       if ( (PEn(pn+1).ge.PEn(pn)).and.(PEn(pn).ge.PEn(pn-1))) then
          PTan(pn,:,:) = PMag(pn+1,:,:)-PMag(pn,:,:) 
       else 
          if ( (PEn(pn+1).le.PEn(pn)).and.(PEn(pn).le.PEn(pn-1))) then
             PTan(pn,:,:) = PMag(pn,:,:)-PMag(pn-1,:,:) 
          else
             Dm1(:,:)= PMag(pn,:,:)-PMag(pn-1,:,:)   ! tau- 
             Dm2(:,:)= PMag(pn+1,:,:)-PMag(pn,:,:)   ! tau+
             DEMax=abs(PEn(pn+1)-PEn(pn))
             DEMin=abs(PEn(pn-1)-PEn(pn))
             if(DEMax<DEMin) then
                DEMax=DEMin
                DEMin=abs(PEn(pn+1)-PEn(pn))
             endif
             if (PEn(pn+1).ge.PEn(pn-1)) then 
                PTan(pn,:,:) = Dm2(:,:)*DEMax +Dm1(:,:)*DEMin
             else
                PTan(pn,:,:) = Dm2(:,:)*DEMin +Dm1(:,:)*DEMax
             endif
          endif
       endif
       call S2Project(PTan(pn,:,:),PMag(pn,:,:))
       norm=MagNorm(PTan(pn,:,:))
       PTan(pn,:,:)=PTan(pn,:,:)/norm
       PSpringForce(pn)=PHooke*(PDist(pn )-PDist(pn-1))
    EndDo

  END SUBROUTINE HenkelmanTangents


  !---------------------------------------------------------------
  ! WRITETecplotPath 
  !---------------------------------------------------------------

  !> Write the current magnetization path into a multi-zone tecplot file
  !> with filename `PathOutFile`, where `PathOutFile` is a global variable.
  SUBROUTINE WriteTecplotPath( )
    IMPLICIT NONE
    INTEGER i,pn 
    INTEGER :: PathOutUnit

    OPEN(NEWUNIT=PathOutUnit,file= PathOutFile, status='unknown')
    WRITE(PathOutUnit,*) 'TITLE = ','Magnetization path  "'//PathOutFile(:LEN_TRIM(PathOutFile))//'"'
    WRITE(PathOutUnit,*) 'VARIABLES = "X","Y","Z","Mx","My","Mz"'

    DO pn=1,PathN

       IF(pn==1) THEN
          WRITE(PathOutUnit,*)'ZONE T="',pn,'"N=',NNODE,',E=',NTRI
          WRITE(PathOutUnit,*)'F=FEPOINT, ET=TETRAHEDRON'
          DO i=1,NNODE
             WRITE(PathOutUnit,3001) VCL(i,1),VCL(i,2),VCL(i,3), &
                  & PMag(pn,i,1),PMag(pn,i,2),PMag(pn,i,3)
          ENDDO
          DO i=1,NTRI
             WRITE(PathOutUnit,3002) TIL(i,1),TIL(i,2),TIL(i,3),TIL(i,4)
          ENDDO
       ELSE
          WRITE(PathOutUnit,*) 'ZONE T="',pn,'"N=',NNODE,',E=',NTRI 
          WRITE(PathOutUnit,*) 'F=FEPOINT, ET=TETRAHEDRON, VARSHARELIST =([1-3]=1), CONNECTIVITYSHAREZONE = 1'
          DO i=1,NNODE
             WRITE(PathOutUnit,3003) PMag(pn,i,1),PMag(pn,i,2),PMag(pn,i,3)
          ENDDO
       ENDIF
    ENDDO

    CLOSE(PathOutUnit)

    3001 FORMAT(6(f10.6, ' '))
    3002 FORMAT(4(i7))
    3003 FORMAT(3(f10.6, ' '))

    RETURN
  END SUBROUTINE WriteTecplotPath


  !---------------------------------------------------------------
  !        ReadTecplotPath 
  !---------------------------------------------------------------

  !> Read the multi-zone tecplot file `PathInFile` into the path variables.
  !> @todo DOCUMENT ME
  SUBROUTINE ReadTecplotPath( )
    USE Finite_Element
    USE Material_Parameters     


    IMPLICIT NONE
    INTEGER i,pn, zonecnt,linecnt, ios,nodecnt,tetcnt,was4,dstop, num, spc, RESULT

    CHARACTER (LEN=20) ::  zonetest ! read in first part of the line that should contain the word ZONE
    CHARACTER (LEN=300) ::  line

    DOUBLE PRECISION mnorm

    DOUBLE PRECISION dummy1, dummy2, dummy3
    INTEGER :: dtil1, dtil2, dtil3, dtil4

    INTEGER :: PathInUnit

    OPEN(NEWUNIT=PathInUnit,file= PathInFile, status='unknown')
    zonecnt=0
    linecnt=0

! search for the 
! search for the word 'TETRAHEDRON' in each line of text read in 
! since this always occurs in the last line before data
    i=0
    RESULT=0
    nodecnt=0            
    tetcnt=0
    was4=0
    dstop=0
    ios=0
    
    ! count the number of zones in a file
    DO WHILE(ios.GE.0)
       linecnt=linecnt+1
       READ(PathInUnit,IOSTAT=ios,FMT='(A7)') zonetest
       if (ios /= 0) exit  ! needed to prevent read past EOF
       RESULT=INDEX(zonetest,'TETRAHEDRON')
       IF(RESULT /= 0) zonecnt=zonecnt+1
    ENDDO

    IF(zonecnt<2) THEN
       IF(zonecnt==0) THEN
          write(*,*) ' File ',PathInFile, ' contains no ZONE. '
       ELSE  
          write(*,*) ' File ',PathInFile, ' contains only one ZONE. '
       ENDIF
       RETURN
    ENDIF

    PathN= zonecnt
    write(*,*) ' File ',PathInFile(:LEN_TRIM(PathInFile)), ' contains  ', PathN, ' ZONEs'
    REWIND(PathInUnit)


    
! search for the word 'TETRAHEDRON' in each line of text read in 
! since this always occurs in the last line before data
    i=0
    RESULT=0
    DO WHILE (RESULT.EQ.0)
      i=i+1
   	  READ(PathInUnit,FMT='(A)') line
!      write(*,*) 'reading line',i, line ! for testing only
      RESULT=INDEX(line,'TETRAHEDRON')
    END DO


! this next section counts the numbers of nodes and elements - HOWEVER - it can be sued to check that
! they mesh file you are using matches the same NODE and ELEMENT number as your data

    ios=0
    DO WHILE((ios.GE.0).AND.(dstop==0)) 
       READ(PathInUnit,IOSTAT=ios,FMT='(A)') line   !Read line
       if (ios /= 0) exit  ! needed to prevent read past EOF
       num=0  ! it follows Fortran magic to count columns (num) in the input line
       spc=1  ! last char was space(or other char <32)
       Do i=1,LEN(line)
          select case(iachar(line(i:i)))
          case(0:32)
             spc=1  ! last char was space(or other char <32)
          case(33:)
             if(spc==1) num=num+1  ! if previous char was space : increase column count
             spc=0
          end select
       End DO
       if(num==6) then 
          if(was4==0) then
             nodecnt=nodecnt+1  ! 6 columns with no preceding 4 column line => one more node 
          else
             dstop=1   ! stop when first 6 column line occurs after a 4 column line
          endif
       else
          if(num==4) then       ! 4 columns   => one more tetrahedron 
             was4=1 
             tetcnt=tetcnt+1  
          else
             dstop=1  !stop if unexpected column number occurs (should never happen)
          endif
       endif
    END DO
    
    if (NNODE /= nodecnt .OR. NTRI /= tetcnt) then
    	write (*,*) 'STOPPING - mesh file size does not match your path data'
    	STOP
    	endif 
!       NNODE= nodecnt
!       NTRI = tetcnt



    REWIND(PathInUnit)

! rewind file an read in the magnetization states along path
! fist kip the header lines in file
! search for the word 'TETRAHEDRON' in each line of text read in 
! since this always occurs in the last line before data
    i=0
    RESULT=0
    DO WHILE (RESULT.EQ.0)
      i=i+1
   	  READ(PathInUnit,FMT='(A)') line
!      write(*,*) 'reading line',i, line ! for testing only
      RESULT=INDEX(line,'TETRAHEDRON')
    END DO


!  WE MUST ALWAYS READ IN MESH so mesh separately so never allocate mesh memory here 
    !---------------------------------------------------------------
    !            Allocate mesh arrays that depend on NNODE and NTRI
    !                     and then path arrays that depend on PathN, NNODE and NTRI
    !---------------------------------------------------------------       
    write(*,*) ' NNODE / NTRI :',NNODE, ' / ', NTRI     
    !CALL MeshAllocate(NNODE, NTRI)   
    write(*,*) ' Mesh allocated',NNODE,   NTRI     
    CALL PathAllocate()   
    write(*,*) ' Path allocated',PathN     
    !     Reading in the first structure together with mesh

    pn=1

    DO i=1,NNODE
       READ(PathInUnit,*) dummy1,dummy2,dummy3,PMag(pn,i,1),PMag(pn,i,2),PMag(pn,i,3)
       VCL(i,4)=0.0
       mnorm=sqrt(PMag(pn,i,1)**2 + PMag(pn,i,2)**2 + PMag(pn,i,3)**2)
       !         print*,'norm=',mnorm
       PMag(pn,i,1)=PMag(pn,i,1)/mnorm
       PMag(pn,i,2)=PMag(pn,i,2)/mnorm
       PMag(pn,i,3)=PMag(pn,i,3)/mnorm
       m(i,:)=PMag(pn,i,:)  
    END DO

    DO i=1,NTRI
       READ (PathInUnit,*) dtil1,dtil2,dtil3,dtil4
    END DO

    !CALL BuildTetrahedralMeshData()
    !CALL BuildFiniteElement()
    !CALL BuildMaterialParameters()
    !CALL BuildEnergyCalculator()

    DO pn=2,PathN
       READ(PathInUnit,*)
       READ(PathInUnit,*)
       DO i=1,NNODE
          READ(PathInUnit,*)  PMag(pn,i,1),PMag(pn,i,2),PMag(pn,i,3)
          mnorm=sqrt(PMag(pn,i,1)**2 + PMag(pn,i,2)**2 + PMag(pn,i,3)**2)
          PMag(pn,i,1)=PMag(pn,i,1)/mnorm
          PMag(pn,i,2)=PMag(pn,i,2)/mnorm
          PMag(pn,i,3)=PMag(pn,i,3)/mnorm
       END DO
    ENDDO
    CLOSE(PathInUnit)
    call PathRenewDist()

    WRITE(*,*)  'TecPlot path read. Make sure material parameters are correctly assigned !'

  END SUBROUTINE ReadTecplotPath
  
  
  
  !> Read the zone `izone` from the tecplot file `ZoneInFile`, where `izone`
  !> and `ZoneInFile` are global variables.
  SUBROUTINE ReadTecplotZone()
    USE Finite_Element
    USE Material_Parameters     


    IMPLICIT NONE
    INTEGER i,pn, zonecnt,linecnt, ios,nodecnt,tetcnt,was4,dstop, num, spc,RESULT
 
    INTEGER :: ZoneInUnit

    CHARACTER (LEN=20) ::  zonetest ! read first part of line that should contain the ZONE variable
    CHARACTER (LEN=300) ::  line

    
    DOUBLE PRECISION mnorm,x,y,z,mx,my,mz,e1,e2,e3,e4

    OPEN(NEWUNIT=ZoneInUnit,file=ZoneInFile, status='unknown')
    zonecnt=0
    linecnt=0
    i=0
    RESULT=0
    nodecnt=0            
    tetcnt=0
    was4=0
    dstop=0
    ios=0
    

    DO WHILE(ios.GE.0)
      linecnt=linecnt+1
      READ(ZoneInUnit,'(A)',IOSTAT=ios) zonetest
      if (ios /= 0) exit  ! needed to prevent read past EOF
      RESULT=INDEX(zonetest,"ZONE")
!      print*, RESULT, zonetest
      IF(RESULT.NE.0) zonecnt=zonecnt+1
    ENDDO
    

       IF(zonecnt==0) then
       write(*,*) ' File ',ZoneInFile(:LEN_TRIM(ZoneInFile)), ' STOPPING - input contains no ZONE. '
		STOP
       ENDIF



    write(*,*) ' File ',ZoneInFile(:LEN_TRIM(ZoneInFile)), ' contains  ', zonecnt, ' ZONEs'
    REWIND(ZoneInUnit)
    if(izone.gt.zonecnt) then
      write(*,*) '*** ERROR** you asked for zone',izone, 'but file contains a max zone number', zonecnt
      STOP
    endif
    nodecnt=0            
    tetcnt=0
    was4=0
    dstop=0
    ios=0

! search for the word 'TETRAHEDRON' in each line of text read in 
! since this always occurs in the last line before data
    i=0
    RESULT=0
    DO WHILE (RESULT.EQ.0)
      i=i+1
      READ(ZoneInUnit,FMT='(A)') line
!      write(*,*) 'reading line',i, line ! for testing only
      RESULT=INDEX(line,'TETRAHEDRON')
    END DO
    
    

! this next section counts the numbers of nodes and elements - HOWEVER - it can be sued to check that
! they mesh file you are using matches the same NODE and ELEMENT number as your data    
    ios=0
    DO WHILE((ios.GE.0).AND.(dstop==0)) 
      READ(ZoneInUnit,IOSTAT=ios,FMT='(A)') line   !Read line
      if (ios /= 0) exit  ! needed in case of only one zone - othewise attempts to read past EOF
      num=0  ! it follows Fortran magic to count columns (num) in the input line
      spc=1  ! last char was space(or other char <32)
      Do i=1,LEN(line)
        select case(iachar(line(i:i)))
          case(0:32)
             spc=1  ! last char was space(or other char <32)
          case(33:)
             if(spc==1) num=num+1  ! if previous char was space : increase column count
             spc=0
        end select
      End DO
      if(num==6) then 
        if(was4==0) then
          nodecnt=nodecnt+1  ! 6 columns with no preceding 4 column line => one more node 
        else
          dstop=1   ! stop when first 6 column line occurs after a 4 column line
        endif
      else
        if(num==4) then       ! 4 columns   => one more tetrahedron 
          was4=1 
          tetcnt=tetcnt+1  
        else
          dstop=1  !stop if unexpected column number occurs (should never happen)
        endif
      endif
    END DO
    
    
    
    if (NNODE /= nodecnt .OR. NTRI /= tetcnt) then
    	write (*,*) 'STOPPING - mesh file size does not match your zone data'
    	Write (*,*) 'Mesh Node and Element =',NNODE,NTRI
    	Write (*,*) 'ZONE Node and Element =',nodecnt,tetcnt    	
    	STOP
    	endif 
!    NNODE= nodecnt
!    NTRI = tetcnt

    REWIND(ZoneInUnit)


! search for the word 'TETRAHEDRON' in each line of text read in 
! since this always occurs in the last line before data
    i=0
    RESULT=0
    DO WHILE (RESULT.EQ.0)
      i=i+1
   	  READ(ZoneInUnit,FMT='(A)') line
!      write(*,*) 'reading line',i, line ! for testing only
      RESULT=INDEX(line,'TETRAHEDRON')
    END DO

    pn=1
    If (izone.eq.1) then
      write(*,*) 'Reading Zone',izone
      DO i=1,NNODE
        READ(ZoneInUnit,*) x,y,z,mx,my,mz
        mnorm=sqrt(mx**2 + my**2 + mz**2)
!                print*,'norm=',mnorm
        m(i,1)=mx/mnorm
        m(i,2)=my/mnorm
        m(i,3)=mz/mnorm
      END DO
    else
      DO i=1,NNODE
        READ(ZoneInUnit,*) x,y,z,mx,my,mz
      END DO
    endif
    
        
    DO i=1,NTRI
      READ (ZoneInUnit,*) e1,e2,e3,e4
    END DO

    If (izone.gt.1) then
      DO pn=2,zonecnt
        READ(ZoneInUnit,*)
        READ(ZoneInUnit,*)
        If(pn==izone) then
          write(*,*) 'Reading Zone',izone
          DO i=1,NNODE
            READ(ZoneInUnit,*)  mx,my,mz
            mnorm=sqrt(mx**2 + my**2 + mz**2)
            m(i,1)=mx/mnorm
            m(i,2)=my/mnorm
            m(i,3)=mz/mnorm
          END DO
          EXIT
        ELSE
          DO i=1,NNODE
            READ(ZoneInUnit,*)  mx,my,mz
          END DO
        ENDIF
      ENDDO
    endif
    CLOSE(ZoneInUnit)
  END SUBROUTINE ReadTecplotZone


  !> Projects the vector `vec` on the S2^N tangent space of `mag`.
  !> Assumes `mag(i,:)` are unit vectors  i.e. in S2.
  !> The result is pointwise normal to mag
  !>
  !> @param[inout] vec The vector to project
  !> @param[in]    mag The space to project on to.
  !>                   Assumes `mag` contains only unit vectors.
  subroutine S2Project(vec,mag)
    implicit none

    integer  i
    double precision vec(NNODE,3) , mag(NNODE,3) 
    double precision  sp  

    do i=1,NNODE
       sp=vec(i,1)*mag(i,1)+vec(i,2)*mag(i,2)+vec(i,3)*mag(i,3)
       vec(i,:)= vec(i,:) -  sp*mag(i,:)
    enddo

  end subroutine S2Project


  !> Calculates the vector distance between two magnetization states \f$m_1\f$
  !> \f$m_2\f$.
  !>
  !> The distance, \f$ d \f$, is calculated as
  !> \f[
  !>    d = \sqrt{ \langle m_1-m_2 | m_1-m_2 \rangle }
  !> \f]
  !> with the scalar product
  !> \f[
  !>    \langle m_1 | m_2 \rangle  = \frac{1}{V}  \int m_1 \cdot m_2\ dV
  !> \f]
  !> assuming  \f$ ||m_1|| = ||m_2|| = 1 \f$ pointwise,
  !> yielding \f$ ||m_1-m_2||  =  \sqrt{ 2 - 2 \langle m_1 | m_2 \rangle } \f$
  !>
  !> @param[in] m1 The variable \f$m_1\f$.
  !> @param[in] m2 The variable \f$m_2\f$.
  !> @returns The distance \f$ \sqrt{ \langle m_1-m_2 | m_1-m_2 \rangle } \f$
  double precision function Distance(m1,m2)
    implicit none

    double precision m1(NNODE,3) , m2(NNODE,3) , mm(NNODE,3) 

    mm(:,:)=m1(:,:)-m2(:,:)
    Distance=MagNorm(mm)
  end function Distance


  !> Calculates the scalar product
  !> \f[  \langle m_1 | m_2 \rangle  = \frac{1}{V}  \int m_1 \cdot m_2\ dV \f]
  !>
  !> @param[in] m1 The \f$m_1\f$ variable.
  !> @param[in] m2 The \f$m_2\f$ variable.
  !> @returns The scalar product \f$ \langle m_1 | m_2 \rangle \f$.
  double precision function ScalarProd(m1, m2)
    implicit none

    integer  i
    double precision m1(NNODE,3) , m2(NNODE,3) 
    double precision  sp , spsum 

    spsum = 0.
    do i=1,NNODE
       sp=m1(i,1)*m2(i,1)+m1(i,2)*m2(i,2)+m1(i,3)*m2(i,3)
       spsum= spsum + sp*vbox(i)
    enddo
    ScalarProd= spsum /total_volume 
  end function ScalarProd


  !> Calculates the norm of \c m1.
  !>
  !> Calculates the norm, \f$ d \f$ of the vector \f$ m_1 \f$ as
  !> \f[
  !>    d = \sqrt{ \langle m_1 | m_1 \rangle }
  !> \f]
  !> with the scalar product
  !> \f[
  !>    \langle m_1 | m_2 \rangle  = \frac{1}{V}  \int m_1 \cdot m_2 dV
  !> \f]
  !>
  !> @param[in] m1 The magnetization \f$ m_1 \f$.
  !> @returns The average magnetization over the volume.
  double precision function MagNorm(m1)
    implicit none

    integer  i
    double precision m1(NNODE,3)  
    double precision  sp , spsum

    spsum = 0.

    do i=1,NNODE
       sp=m1(i,1)*m1(i,1)+m1(i,2)*m1(i,2)+m1(i,3)*m1(i,3)
       spsum= spsum + sp*vbox(i)
    enddo
    MagNorm=sqrt( spsum /total_volume)
  end function MagNorm



  !> For t in [0,1] calculates the magnetization state mt 
  !> that interpolates at t between the magnetization states m1,m2   
  !>
  !> @param[in]  m1 The magnetization at t=0.
  !> @param[in]  m2 The magnetization at t=1.
  !> @param[out] mt The interpolated magnetization.
  !> @param[in]  t  The interpolation parameter. Must be in [0,1].
  subroutine PathInterpolate(m1,m2,mt,t)
    implicit none

    integer  i
    double precision mt(:,:), m1(:,:) , m2(:,:) 
    double precision phi, stp, a, b, sp, t
    double precision sp_eps

    sp_eps = 1E-10

    DO i=1,NNODE
      ! scalar product m1.m2
      sp = m1(i,1)*m2(i,1)+m1(i,2)*m2(i,2)+m1(i,3)*m2(i,3)
      
      IF ((sp < -1.0) .or. (sp > 1.0)) THEN
        WRITE(*,*) "WARNING: 'sp' value is out of range: ", sp
        ! Some code here to clamp 'sp' to [-1,1]??
      ENDIF

      IF ( abs(sp-1.0) < sp_eps) THEN  !  parallel : no change
        ! 1-eps < sp < 1+eps => -eps < sp-1 < eps 
        !                    => |eps-1| < eps
        mt(i,1)=m1(i,1)
        mt(i,2)=m1(i,2)
        mt(i,3)=m1(i,3)
      ELSE 
        IF ( abs(sp+1.0) < sp_eps ) THEN ! antiparallel : choose the closer one
          ! -1-eps < sp < -1+eps => -eps < sp+1 < eps
          !                      => |sp+1| < eps
          mt(i,1)=SIGN( m1(i,1), 0.500003-t)
          mt(i,2)=SIGN( m1(i,2), 0.500003-t)
          mt(i,3)=SIGN( m1(i,3), 0.500003-t)
        ELSE
          ! use the analytical linear interpolation in rotation angle
          phi = acos(sp)
          stp= sin(t*phi)
          a= cos(t*phi) - stp/tan(phi)
          b=stp/sin(phi)
          mt(i,1)=a* m1(i,1)+ b*m2(i,1)
          mt(i,2)=a* m1(i,2)+ b*m2(i,2)
          mt(i,3)=a* m1(i,3)+ b*m2(i,3)
        ENDIF
      ENDIF
    ENDDO

    RETURN      
  end  subroutine PathInterpolate
  !----------------!     


  !> Interpolates the current path over `NewPathN` equidistant points.
  !> The initial and final states remain unchanged   
  !>
  !> @param[in] NewPathN The number of path points to refine the path to
  subroutine RefinePathTo(NewPathN)
    implicit none

    integer  i,NewPathN,uind,mind,lind
    double precision,Allocatable ::  NewPMag(:,:,:) 
    double precision  ndist,t 

    call PathRenewDist()

    Allocate(NewPMag(NewPathN,NNODE,3) )
    !         Initial and final states remain unchanged   
    NewPMag(1,:,:) = PMag(1,:,:)
    NewPMag(NewPathN,:,:) = PMag(PathN,:,:)               

    Do i=2,NewPathN-1
       ndist= PLength*(i-1)/(NewPathN-1)
       !               search insert position in CumulPDist such that
       !               CumulPDist(lind)<= ndist <=  CumulPDist(uind) and uind=lind+1     
       !               By halving interval lengths -> O( Log2(Newpath) )  

       lind=1;uind=PathN    
       Do While(uind-lind>1)
          mind=(uind+lind)/2
          If(CumulPDist(mind)<=ndist) then
             lind=mind
          Else
             uind=mind
          Endif
       Enddo
       !               t in (0,1) is the correct position between lind/uind
       t=(ndist-CumulPDist(lind))/(CumulPDist(uind)-CumulPDist(lind)) 
       call PathInterpolate(PMag(lind,:,:),PMag(uind,:,:),NewPMag(i,:,:),t) ! interpolate between old magnetizations

    End Do

    PathN=NewPathN              ! Assign new path length 
    call PathAllocate()         ! Deallocates old path allocates new sizes
    PMag(:,:,:)=NewPMag(:,:,:)  ! Assigns new path 
    call PathRenewDist()            ! New calculation of distances  
    PathChangedQ(:)=.TRUE.
    Deallocate(NewPMag)
  end  subroutine RefinePathTo



  !> Calls the energy evaluation for position `pos` and
  !> calculates the gradient of the energy at `pos`
  !>
  !> @param[in] pos The position to calculate the energy and gradient at.
  subroutine PathEnergyAt(pos)
    implicit none

    integer  pos, neval

    double precision  grad(NNODE*2),X(2*NNODE)

    !external  ET_GRAD

    neval=-100  ! Negative neval for ET_GRAD  -> keeps m(:,:) unchanged!!
    FEMTolerance=1.d-10         ! Controlling tolerance of linear CG solver
    If ( pos< 1 .or. pos>PathN) Return

    m(:,:)=PMag(pos,:,:)

    call ET_GRAD(PEn(pos),grad,X,neval)

    PEGrad(pos,:,:)= gradc(:,:)

  end subroutine PathEnergyAt


  !> Calculates the energy for the structure at index `idx`.
  !>
  !> @param[in] idx The structure index to calculate the energy for
  !> @returns The energy at structure `idx`.
  function structureEnergy(idx) result(structEnergy)
    implicit none

    ! Arguments and return values
    integer,          intent(in)  :: idx
    double precision              :: structEnergy

    ! Local variables
    integer                              :: neval
    double precision                     :: energy
    double precision, dimension(2*NNODE) :: grad
    double precision, dimension(2*NNODE) :: x

    !external  ET_GRAD

    ! Function body
    neval        = -100     ! negative value hack (see above).
    femTolerance =  1.d-10  ! CG solver tolerance (see above).

    m(:,:) = pmag(idx, :, :)
    
    call ET_GRAD(energy, grad, x, neval)

    structEnergy = energy
  end function structureEnergy


  !> Calculates the energy for the current magnetisation, `m`.
  !>
  !> @returns The energy for the current magnetization
  function currentMagEnergy() result(currEnergy)
    implicit none

    ! Return values
    double precision :: currEnergy

    ! Local variables
    integer                              :: neval
    double precision                     :: energy, dgscale
    double precision, dimension(2*NNODE) :: grad
    double precision, dimension(2*NNODE) :: x

    !external ET_GRAD

    ! Function body
    neval = -100          ! negative value hack.
    dgscale = 0.0
    femTolerance = 1.d-10 ! CG solver tolerance.

    ! Assume that global m(:,:) contains magnetisation
    call ET_GRAD(energy, grad, x, neval)

    currEnergy = energy
  end function currentMagEnergy


  !> Calls pathenergyat for each changed position, `pos`, and
  !> calculates energy at that position.
  subroutine CalcPathAction()
    USE slatec_avint
    !  SUBROUTINE AVINT (X, Y, N, XLO, XUP, ANS, IERR)
    !  Integration routine Davis & RabinowitzT
    implicit none

    integer  pos, IERR
    REAL(KIND=DP) sp,PCurv(NNODE,3),EnScale
    logical :: RecalculateQ


    EnScale= Kd*total_volume  ! Energy scale to transform into units of Kd V 
    ! Note that a factor Ls**(-3/2) is intrinsic by using Aex*Ls

    RecalculateQ=.TRUE.

    Do While (RecalculateQ)
       call PathRenewDist() ! Gets distances renewed
       RecalculateQ=.FALSE.
       DO pos =1, PathN
          if(PathChangedQ(pos)) then          
             call PathEnergyAt(pos)  ! if magnetization has changed calculate new energy and gradient
             call S2Project(PEGrad(pos,:,:),PMag(pos,:,:))  ! project gradient in S2^N tangent space
             PEGradNorm(pos)=MagNorm(PEGrad(pos,:,:))
             PathChangedQ(pos)=.FALSE.
          endif
       END DO

       ! Check if end energies are not local minima
       !  in this case take the neighboring state as new end state

       If(PEn(1)>1.03*PEn(2)) then 
          PMag(1,:,:)=PMag(2,:,:)
          call PathInterpolate(PMag(1,:,:),PMag(3,:,:),PMag(2,:,:),0.5d0) ! interpolate new 2. state
          write(*,*) '--> New Minimum at path position 1'
          PathChangedQ(1)=.TRUE.
          PathChangedQ(2)=.TRUE. 
          RecalculateQ=.TRUE.
       End If
       If(PEn(PathN)>1.03*PEn(PathN-1)) then 
          PMag(PathN,:,:)=PMag(PathN-1,:,:)
          call PathInterpolate(PMag(PathN,:,:),PMag(PathN-2,:,:),PMag(PathN-1,:,:),0.5d0) ! interpolate new -2. state
          write(*,*) '--> New Minimum at path position ',PathN
          PathChangedQ(PathN)=.TRUE.
          PathChangedQ(PathN-1)=.TRUE. 
          RecalculateQ=.TRUE.
       End If
    End Do

    call HenkelmanTangents() ! Gets  tangents renewed by Henkelman method using new energies


    DO pos =1, PathN
       if (PEGradNorm(pos)>0.) then
          sp= ScalarProd(PTan(pos,:,:),PEGrad(pos,:,:))/PEGradNorm(pos)
       else 
          sp=1.
       endif
       PAlign(pos)= acos(abs(sp)) ! PAlign is angular deviation from gradient to tangent 

       PNEBGrad(pos,:,:)= PEGrad(pos,:,:)- (sp-PSpringForce(pos))*PTan(pos,:,:)
    EndDo
    ! Integration of energy-gradient norm along the path yields PAction
    !           write (*,*) (CumulPDist(pos),pos=1,PathN)
    !           write (*,*) (PEGradNorm(pos),pos=1,PathN)

    CALL  AVINT(CumulPDist, PEGradNorm, PathN, CumulPDist(1), CumulPDist(PathN), PAction, IERR) 
    Do pos=2,PathN-1
      if(NONZERO(CurveWeight)) then
        !  curvature at pos
        PCurv(:,:)=(PTan(pos+1,:,:)-PTan(pos-1,:,:))/(PDist(pos-1)+PDist(pos))
        ! project curvature in S2^N tangent space
        call S2Project(PCurv(:,:),PMag(pos,:,:))
        PNEBGrad(pos,:,:)=PNEBGrad(pos,:,:)- PEGradNorm(pos)*CurveWeight*PCurv(:,:)
      endif
      if(NONZERO(PHooke)) then
        PTarget=PAction+PSpringForce(pos)**2/PHooke/2
      else
        PTarget=PAction
      endif
    EndDo
    CALL  AVINT(CumulPDist, PAlign, PathN, CumulPDist(1), CumulPDist(PathN), PDeltaGeodesic, IERR) 
    ! average angular gradient deviation from tangent along path
    PDeltaGeodesic=PDeltaGeodesic/PLength
    if(PathLoggingQ) then
      write(PLogEnUnit, * )   ( PEn(pos)/EnScale,pos=1,PathN)
      write(PLogGradUnit, * ) ( PEGradNorm(pos)/EnScale,pos=1,PathN)
      write(PLogDistUnit, * ) ( CumulPDist(pos),pos=1,PathN)
    endif

  end  subroutine CalcPathAction


  !> Estimate the variability of the energy near position `pos` in the path,
  !> and set `InitAlpha` to a reasonable value using it.
  !>
  !> This is used to obtain an estimate for `InitAlpha` which must be chose to
  !> be large enough to constrain the distance but small enough to realistically
  !> reflect the magnetic energy
  !>
  !> @param[in] pos The position in the path to estimate the energy variability.
  subroutine EnergyVariability(pos)
    implicit none

    integer  pos,i ,neval

    REAL(KIND=DP)  angle,EZero,ModEnergy, rvar,uvar
    double precision  grad(NNODE*2),X(2*NNODE)
    CHARACTER (LEN=20) :: str
    !external  ET_GRAD
    AddInitPathEnergyQ=.FALSE.

    If ( pos< 1 .or. pos>PathN) Return
    m(:,:)=PMag(pos,:,:)
    neval=-100  ! Negative neval for ET_GRAD  -> keeps m(:,:) unchanged!!
    FEMTolerance=1.d-10         ! Controlling tolerance of linear CG solver

    call ET_GRAD(EZero,grad,X,neval)

    rvar=0.
    uvar=0.
    Do i=1,3
       angle=1.0*i 
       m(:,:)=PMag(pos,:,:)
       str= 'random'
       call ModifyMag( str, angle )
       call ET_GRAD(ModEnergy,grad,X,neval)
       InitEVar(i)=ModEnergy-EZero
       rvar=rvar+ (InitEVar(i)/angle/angle)**2 

       m(:,:)=PMag(pos,:,:)
       str= 'uniform'
       call ModifyMag( str, angle )
       call ET_GRAD(ModEnergy,grad,X,neval)
       InitEVar(i+3)=ModEnergy-EZero
       uvar=uvar+ (InitEVar(i+3)/angle)**2 
    End Do

    ! approximate energy variation per degree angular distance
    InitAlpha =sqrt(uvar)+sqrt(rvar)
    write(*,*) 'QQ EV sqrt(uvar) =',sqrt(uvar)          
    write(*,*) 'QQ EV sqrt(rvar) =',sqrt(rvar)          

    ! The more states the path has the better each distance should be kept
    InitAlpha= 4*PathN*PathN*InitAlpha
    InitDelta= 1.5

    write(*,*) 'QQ EV InitAlpha =',InitAlpha            


  end subroutine EnergyVariability


  !> Make an initial magnetization path
  !>
  !> assumes that PathN is set and Initial and final states are loaded
  !> 1) Estimates InitAlpha using EnergyVariability(pos)
  !> 2) Minimizes modified energy along the path 
  !>     to force states of prescribed distances
  subroutine MakeInitialPath()
    implicit none

    integer  i ,TMPRestart,TMPEval

    REAL(KIND=DP)  angle

    CHARACTER (LEN=20) :: str

    PathEndDist=Distance(PMag(1,:,:),PMag(PathN,:,:))
    write(*,*) 'QQ MIP PathEndDist=',PathEndDist

    TMPRestart =MaxRestarts ! Stores value of MaxRestarts
    MaxRestarts=2           ! No restarts for intermediate Minima
    TMPEval =MaxEnergyEval  ! Stores value of MaxEnergyEval
    ! MaxEnergyEval=1000      ! No more than so many energy evaluations for intermediate states

    call  EnergyVariability(PathN) 

    InitRefPos=1
    AddInitPathEnergyQ=.TRUE.       
    Do i= PathN-1,2,-1
       InitDelta = PathEndDist/(PathN-1)*(i-1)
       m(:,:)=PMag(i+1,:,:) 
       str= 'random'
       angle=2.0
       call ModifyMag( str, angle )
       call EnergyMin( )
       PMag(i,:,:)=m(:,:)
       PathChangedQ(i)=.TRUE.
    Enddo
    AddInitPathEnergyQ=.FALSE.

    call PathRenewDist( )

    MaxRestarts=TMPRestart  ! Restores value of MaxRestarts
    MaxEnergyEval=TMPEval   ! Restores value of MaxEnergyEval

  end subroutine MakeInitialPath


  !> Minimize the path
  !>
  !> Uses an adaptive minimization routine to change the path
  !> towards minimal geometric action along the pseudo gradient
  !> given by Henkelmann&Jonsson(2000) NEB 
  SUBROUTINE PathMinimize()

    USE Material_Parameters
    USE Tetrahedral_Mesh_Data
    USE Finite_Element

    IMPLICIT NONE 


    INTEGER :: CREEPCNT, MAXCREEP, FINISHED, FTrailLength
    INTEGER :: nstart, i, ResetCnt, NEval, pos

    REAL(KIND=DP), ALLOCATABLE :: POldMag(:,:,:), POldNEBGrad(:,:,:) 

    DOUBLE PRECISION :: ALPHA, DALPHA, OldTarget, DeltaF, GSQUARE, &
      TOLERANCE, GTOL, BestF, AlphaScale, norm
    DOUBLE PRECISION, ALLOCATABLE :: FTrail(:)


    Allocate(POldMag(PathN,NNODE,3), POldNEBGrad(PathN,NNODE,3) )

    MAXCREEP=4         ! Controls the number of creep steps
    DALPHA=2.6         ! Controls the alpha acceleration/deceleration

    FTrailLength=13    ! Length of trailing action value for finishing 
    Allocate(FTrail(FTrailLength))
    TOLERANCE=1.e-8    ! Finished if consecutive actions with distance FTrailLength are < TOLERANCE
    GTOL=1.e-10        ! Finished if Average pseudo-gradient < GTOL
    nstart=1
    FTrail(:)=0.0
    BestF=0.
    NEval=0
    ResetCnt=0
    AlphaScale =0.001


10  call  CalcPathAction()
    NEval=NEval+1

    OldTarget=PTarget
    POldMag(:,:,:)=PMag(:,:,:)
    POldNEBGrad(:,:,:)=PNEBGrad(:,:,:)

    if(MODULO(NEval,10).eq.0) write(*,'(I5, 6F15.6)') NEval ,ALPHA, PLength, DeltaF/TOLERANCE, PDeltaGeodesic, PAction, PTarget  
    if(MODULO(NEval,100).eq.1) write(*,'(A5, 6A15)') 'NEval' ,'ALPHA','PLength','dS/TOL' ,'D-Geodesic', 'PAction', 'PTarget'
    if(PathLoggingQ.and.(MODULO(NEval,500).eq.0)) then
       PathOutFile=PLogFile
       call WRITETecplotPath()
    endif

    FTrail(nstart)=PTarget
    nstart=nstart+1  
    IF(nstart>FTrailLength)  nstart=1
    ALPHA=1.

    FINISHED=0
    30  CREEPCNT=0
    DO WHILE (CREEPCNT < MAXCREEP)


       Do pos=2,PathN-1  
          PMag(pos,:,:)=PMag(pos,:,:) - ALPHA*AlphaScale* PNEBGrad(pos,:,:)   
          Do i=1,NNODE
             norm=sqrt(PMag(pos,i,1)**2+PMag(pos,i,2)**2+PMag(pos,i,3)**2)
             PMag(pos,i,:)=PMag(pos,i,:)/norm
          EndDo
       EndDo

       PathChangedQ(:)=.TRUE.

       call  CalcPathAction()
       NEval=NEval+1

       if(MODULO(NEval,10).eq.0) write(*,'(I5, 6F15.6)') NEval ,ALPHA, PLength, DeltaF/TOLERANCE, PDeltaGeodesic, PAction, PTarget  
       if(MODULO(NEval,100).eq.1) write(*,'(A5, 6A15)') 'NEval' ,'ALPHA','PLength','dS/TOL' ,'D-Geodesic', 'PAction', 'PTarget'
       if(PathLoggingQ.and.(MODULO(NEval,500).eq.0)) then
          PathOutFile=PLogFile
          call WRITETecplotPath()
       endif

       FTrail(nstart)=PTarget
       nstart=nstart+1  
       IF(nstart>FTrailLength) nstart=1


       GSQUARE=0
       DO i=1,PathN
          GSQUARE = GSQUARE + MagNorm(PNEBGrad(i,:,:))**2
       ENDDO
       GSQUARE = GSQUARE/PathN

       DeltaF=Abs(FTrail(nstart)-PTarget)/FTrailLength ! Average step difference between trailing F and new F

       IF ( (NEval>FTrailLength*10) .and. (DeltaF < TOLERANCE))  THEN
          Write(*,*) 'Change in action DeltaS negligible:',DeltaF
          GOTO 100 ! FINISHED Delta F negligible
       ENDIF
       IF ( NEval.ge.MaxPathEval)  THEN
          Write(*,*) 'MAX Path Evaluations reached!! DeltaS:',DeltaF
          GOTO 100 ! FINISHED TOO MANY PATH EVALUATIONS
       ENDIF
       IF(OldTarget<PTarget) THEN
          CREEPCNT=0
          ALPHA= ALPHA/DALPHA/DALPHA
          !         Write(*,*) 'QQ HM-> ALPHA = ',ALPHA
          IF(ALPHA<1.e-3) THEN
             write(*,'(I5, 6F15.6,A10)') NEval ,ALPHA, PLength, DeltaF/TOLERANCE, PDeltaGeodesic, PAction,PTarget,' <Reset>' 
             ResetCnt=ResetCnt+1
             BestF=OldTarget
             call RefinePathTo(PathN)  ! Resets the path to equidistant structures (smoothing  kinks?)
             PathChangedQ(:)=.TRUE.
             if(ResetCnt>20) then
                write(*,*) '+++++   FAILED TO CONVERGE +++++'
                goto 100
             endif
             GOTO 10 !    !!  ALPHA TOO SMALL: RESTART From worse state
          Else
             PMag(:,:,:)=POldMag(:,:,:)
             PNEBGrad(:,:,:)=POldNEBGrad(:,:,:)
          ENDIF

       ELSE
          CREEPCNT=CREEPCNT+1
          !         Write(*,*) 'QQ HM     CREEP #',CREEPCNT
          OldTarget=PTarget
          POldMag(:,:,:)=PMag(:,:,:)
          POldNEBGrad(:,:,:)=PNEBGrad(:,:,:)
          IF ( GSQUARE < GTOL)   THEN
             Write(*,*) 'PSEUDO-GRADIENT Negligible:',GSQUARE
             GOTO 100 ! FINISHED Grad=0
          ENDIF
       ENDIF
    ENDDO
    ALPHA=DALPHA*ALPHA
    ResetCnt=0
    !     Write(*,*) 'QQ HM-> ALPHA = ',ALPHA
    GOTO 30


100 Write(*,*) 
    Write(*,*) '   MINIMIZATION FINISHED '
    Write(*,*)
    Write(*,'(A43,I5)')       '   ||                 CalcPathAction Calls:',NEval
    Write(*,'(A43,F15.6,A2)') '   ||                               Action:',PAction 
    Write(*,'(A43,F12.2)')    '   ||                           DeltaS/TOL:',DeltaF/TOLERANCE
    Write(*,'(A43,F12.2)')    '   ||                    sqrt(grad^2/GTOL):',sqrt(GSQUARE/GTOL)          

    Deallocate(POldMag , POldNEBGrad  )

    RETURN


  END SUBROUTINE PathMinimize


END MODULE Magnetization_Path



