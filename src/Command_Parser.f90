!> The Command_Parser module.
MODULE Command_Parser
  USE MERRILL

  IMPLICIT NONE

  !> A type representing a MScript function parser, which should parse
  !> a MScript function and execute the equivalent Fortran functions.
  TYPE :: CommandParser
    !> The name of the command.
    CHARACTER(len=200) :: name

    !> The parser for the command.
    PROCEDURE(CommandParserInterface), POINTER, NOPASS :: parser => NULL()
  END TYPE CommandParser


  ! Values representing the success or failure of a CommandParser parser.

  !> The return value for a successful parse.
  INTEGER, PARAMETER :: PARSE_SUCCESS = 1

  !> The return value for when no parsing is done.
  INTEGER, PARAMETER :: NO_PARSE = 0

  !> The return value for when an error occurred during parsing.
  INTEGER, PARAMETER :: PARSE_ERROR = -1



  INTERFACE
    !> The interface for a `parser` referenced by a CommandParser.
    SUBROUTINE CommandParserInterface(args, ierr)
      !> The interface for a `parser` referenced by a CommandParser.
      !> @param[in]  args The arguments to parse
      !> @param[out] ierr The result of the parse
      IMPLICIT NONE

      CHARACTER(len=*), INTENT(IN) :: args(:)
      INTEGER, INTENT(OUT) :: ierr
    END SUBROUTINE CommandParserInterface
  END INTERFACE


  !> A list of the available command parsers.
  TYPE(CommandParser), ALLOCATABLE :: command_parsers(:)


  CONTAINS


  !> Initialize command_parsers array with the default commands
  SUBROUTINE InitializeCommandParser()
    IMPLICIT NONE

    CALL DestroyCommandParser()
    ALLOCATE(command_parsers(0))

    CALL AddCommandParser("print", ParsePrint)
    CALL AddCommandParser("systemcommand", ParseSystemCommand)
    CALL AddCommandParser("keypause", ParseKeyPause)
    CALL AddCommandParser("reportenergy", ParseReportEnergy)

    CALL AddCommandParser("cubic",    ParseAnisotropy)
    CALL AddCommandParser("uniaxial", ParseAnisotropy)
    CALL AddCommandParser("uni",      ParseAnisotropy)
    CALL AddCommandParser("cubicrotation", ParseCubicRotation)
    CALL AddCommandParser("cubicaxes", ParseCubicAxes)
    CALL AddCommandParser("easyaxis", ParseEasyAxis)

    CALL AddCommandParser("magnetite", ParseMagnetite)
    CALL AddCommandParser("iron", ParseIron)
    CALL AddCommandParser("TM54", ParseTM54)

    CALL AddCommandParser("external", ParseSetField)
    CALL AddCommandParser("magnetic", ParseSetField)

    CALL AddCommandParser("readmesh", ParseReadMesh)
    CALL AddCommandParser("loadmesh", ParseLoadMesh)
    CALL AddCommandParser("savemesh", ParseSaveMesh)
    CALL AddCommandParser("remesh", ParseRemesh)

    CALL AddCommandParser("readmag",           ParseReadMag)
    CALL AddCommandParser("readmagnetization", ParseReadMag)
    CALL AddCommandParser("writemag",           ParseWriteMag)
    CALL AddCommandParser("writedemag",           ParseWriteDemag)
    CALL AddCommandParser("writemagnetization", ParseWriteMag)
    CALL AddCommandParser("writehyst", ParseWriteHyst)
    CALL AddCommandParser("writeboxdata", ParseWriteBoxData)
    CALL AddCommandParser("energylog", ParseEnergyLog)
    CALL AddCommandParser("logfile",   ParseEnergyLog)
    CALL AddCommandParser("pathlogfile", ParsePathLogFile)
    CALL AddCommandParser("endlog",       ParseCloseLogFile)
    CALL AddCommandParser("closelog",     ParseCloseLogFile)
    CALL AddCommandParser("closelogfile", ParseCloseLogFile)

    CALL AddCommandParser("conjugategradient", ParseConjugateGradient)
    CALL AddCommandParser("steepestdescent", ParseSteepestDescent)
    CALL AddCommandParser("minimize", ParseEnergyMin)
    CALL AddCommandParser("pathminimize", ParsePathMinimize)

    CALL AddCommandParser("resize", ParseResize)
    CALL AddCommandParser("gradienttest", ParseGradientTest)

    CALL AddCommandParser("uniform", ParseUniformMagnetization)
    CALL AddCommandParser("randomize", ParseRandomizeMagnetization)
    CALL AddCommandParser("invert", ParseInvertMagnetization)
    CALL AddCommandParser("vortex", ParseVortexMagnetization)

    CALL AddCommandParser("magtopath",           ParseMagnetizationToPath)
    CALL AddCommandParser("magnetizationtopath", ParseMagnetizationToPath)
    CALL AddCommandParser("pathtomag",           ParsePathToMagnetization)
    CALL AddCommandParser("pathtomagnetization", ParsePathToMagnetization)
    CALL AddCommandParser("pathrenew", ParsePathRenewDist)
    CALL AddCommandParser("renewpath", ParsePathRenewDist)
    CALL AddCommandParser("makepath",  ParsePathRenewDist)
    CALL AddCommandParser("makeinitialpath", ParseMakeInitialPath)
    CALL AddCommandParser("refinepathto", ParseRefinePathTo)
    CALL AddCommandParser("writetecplotpath", ParseWriteTecplotPath)
    CALL AddCommandParser("readtecplotpath", ParseReadTecplotPath)
    CALL AddCommandParser("readtecplotzone", ParseReadTecplotZone)
    CALL AddCommandParser("pathstructureenergies", ParsePathStructureEnergies)
    CALL AddCommandParser("writepathstructures", ParseWritePathStructures)
    CALL AddCommandParser("energy", ParseEnergy)

    CALL AddCommandParser("GenerateCubeMesh", ParseGenerateCubeMesh)
    CALL AddCommandParser("GenerateSphereMesh", ParseGenerateSphereMesh)

    CALL AddCommandParser("loadplugin", ParseLoadPlugin)
  END SUBROUTINE InitializeCommandParser


  !> Destroy the command_parsers array
  SUBROUTINE DestroyCommandParser()
    IF(ALLOCATED(command_parsers)) DEALLOCATE(command_parsers)
  END SUBROUTINE DestroyCommandParser



  !>
  !> Add a command parser 'parser' with the name 'command_name' to
  !> the command_parsers array.
  !>
  SUBROUTINE AddCommandParser(command_name, parser)
    USE strings
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: command_name
    PROCEDURE(CommandParserInterface) :: parser

    TYPE(CommandParser) :: command_parser

    TYPE(CommandParser), ALLOCATABLE :: tmp(:)
    INTEGER :: n_commands

    command_parser%name   =  TRIM(lowercase(command_name))
    command_parser%parser => parser

    ALLOCATE(tmp(SIZE(command_parsers)+1))

    n_commands = SIZE(command_parsers)
    IF(n_commands .GT. 0) tmp(1:n_commands) = command_parsers
    CALL MOVE_ALLOC(tmp, command_parsers)
    n_commands = n_commands+1

    command_parsers(n_commands) = command_parser
  END SUBROUTINE AddCommandParser


  !>
  !> Run the parser in command_parsers with the name command_name
  !>
  SUBROUTINE ParseCommand(command_name, args, ierr)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: command_name
    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    TYPE(CommandParser) command_parser
    INTEGER :: i

    ierr = NO_PARSE

    DO i=1,SIZE(command_parsers)
      IF(TRIM(command_parsers(i)%name) .EQ. TRIM(command_name)) THEN
        command_parser = command_parsers(i)

        CALL command_parser%parser(args, ierr)
        EXIT
      END IF
    END DO
  END SUBROUTINE ParseCommand


  !> Assert the array \c args has \c nargs values.
  !> If not, print an error message.
  !>
  !> @param[in] args  The array to check
  !> @param[in] nargs The number of arguments to check for
  !> @returns .TRUE. if `array` has `nargs` elements.
  FUNCTION AssertNArgs(args, nargs)
    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(IN) :: nargs
    LOGICAL :: AssertNArgs
    INTEGER :: i

    IF(SIZE(args) .EQ. nargs) THEN
      AssertNArgs = .TRUE.
    ELSE
      WRITE(*,'(A,A,A,I4,A,A,I4,A)') &
        "Command '", TRIM(args(1)), "' expects ", nargs, " arguments.", &
        " Got ", SIZE(args), " arguments."
      WRITE(*,*) "ARGS: "
      DO i=1,SIZE(args)
        WRITE(*,'(I4,A,A)') i, ": ", TRIM(args(i))
      END DO
      AssertNArgs = .FALSE.
    END IF
  END FUNCTION AssertNArgs


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! CommandParser parsers                                                      !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  !> Parse the `Print` command
  !>
  !> @param[in]  args The arguments to the `Print` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParsePrint(args, ierr)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    INTEGER :: nargs

    CHARACTER(len=200) :: line
    INTEGER :: i

    nargs = SIZE(args)

    line=''
    DO i=2,nargs
      line=line(:LEN_TRIM(line))//' '//args(i)(:LEN_TRIM(args(i)))
    END DO
    Write(*,*) line(:LEN_TRIM(line))

    ierr = PARSE_SUCCESS
  END SUBROUTINE ParsePrint


  !> Parse the `SystemCommand` command
  !>
  !> @param[in]  args The arguments to the `SystemCommand` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseSystemCommand(args, ierr)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    INTEGER :: nargs
    CHARACTER(len=200) :: sysline
    INTEGER :: sys

    INTEGER :: i

    nargs = SIZE(args)

    IF (nargs>2) THEN
      sysline=''
      DO i=2,nargs
        sysline=sysline(:LEN_TRIM(sysline))//' '//args(i)(:LEN_TRIM(args(i)))
      END DO

      CALL EXECUTE_COMMAND_LINE(sysline, exitstat=sys)

      IF(sys.EQ.0) THEN
        WRITE(*,*)          '          `-> OK'
        ierr = PARSE_SUCCESS
      ELSE
        WRITE(*,'(A20,I3)') '          `-> Error:', sys
        ierr = PARSE_ERROR
      END IF
    ELSE
      WRITE(*,*) "Error in ", TRIM(args(1)), ": No command given."
      ierr = PARSE_ERROR
    END IF
  END SUBROUTINE ParseSystemCommand


  !> Parse the `Anisotropy` command
  !> MScript example:
  !> @code
  !>    Uniaxial Anisotropy
  !>    Cubic Anisotropy
  !>    Uniaxial Anisotropy sd = n
  !> @endcode
  !>
  !> @param[in]  args The arguments to the `Anisotropy` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseAnisotropy(args, ierr)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    INTEGER :: sd

    IF(SIZE(args) .EQ. 5) THEN
      sd = ParseSubdomain(args(3:5), ierr)
      CALL setanis(args(1), ierr, sd = sd)
    ELSE
      CALL setanis(args(1), ierr)
    END IF

  END SUBROUTINE ParseAnisotropy


  !> Parse the `CubicRotation` command
  !> MScript Example:
  !> @code
  !>    CubicRotation phi theta alpha
  !>    CubicRotation phi theta alpha sd = n
  !> @endcode
  !>
  !> @param[in]  args The arguments to the `CubicRotation` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseCubicRotation(args, ierr)
    USE strings
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    REAL(KIND=DP) :: phirot, thetarot, alpharot
    REAL(KIND=DP) :: dbltmp

    INTEGER :: sd
    INTEGER :: ios

    LOGICAL :: l

    ierr = PARSE_ERROR

    ! define phi, theta, alpha of cubic axes rotation matrix
    print*,'args = ', SIZE(args)
    IF(SIZE(args) .GE. 4) THEN
      CALL value(args(2),dbltmp,ios)
      IF(ios.EQ.0) THEN
        phirot=dbltmp
      ELSE
        WRITE(*,*) "Error parsing value ", TRIM(args(2))
        ierr = PARSE_ERROR
        RETURN
      END IF

      CALL value(args(3),dbltmp,ios)
      IF(ios.EQ.0) THEN
        thetarot= dbltmp
      ELSE
        WRITE(*,*) "Error parsing value ", TRIM(args(3))
        ierr = PARSE_ERROR
        RETURN
      END IF

      CALL value(args(4),dbltmp,ios)
      if(ios.eq.0) then
        alpharot= dbltmp
      ELSE
        WRITE(*,*) "Error parsing value ", TRIM(args(4))
        ierr = PARSE_ERROR
        RETURN
      END IF

      IF(SIZE(args) .EQ. 7) THEN
        sd = ParseSubdomain(args(5:7), ierr)
        IF(ierr .NE. PARSE_ERROR) THEN
          CALL SetRotationMatrix(phirot, thetarot, alpharot, sd=sd)
          ierr = PARSE_SUCCESS
        END IF
      ELSE IF(AssertNArgs(args, 4)) THEN
        CALL SetRotationMatrix(phirot, thetarot, alpharot)
        ierr = PARSE_SUCCESS
      ELSE
        ierr = PARSE_ERROR
      END IF

    ELSE
      l = AssertNArgs(args, 4)
      ierr = PARSE_ERROR
    END IF
  END SUBROUTINE ParseCubicRotation


  !> Parse the `ConjugateGradient` command
  !>
  !> @param[in]  args The arguments to the `ConjugateGradient` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseConjugateGradient(args, ierr)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    ConGradQ=.TRUE.

    ierr = PARSE_SUCCESS
  END SUBROUTINE ParseConjugateGradient


  !> Parse the `SteepestDescent` command
  !>
  !> @param[in]  args The arguments to the `SteepestDescent` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseSteepestDescent(args, ierr)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    ConGradQ=.FALSE.

    ierr = PARSE_SUCCESS
  END SUBROUTINE ParseSteepestDescent


  !> Parse the `CubicAxes` command
  !> Users should take care axes are mutually orthogonal!
  !>
  !> MScript example:
  !> CubicAxes x1 y1 z1 x2 y2 z2 x3 y3 z3
  !> CubicAxes x1 y1 z1 x2 y2 z2 x3 y3 z3 sd = n
  !>
  !> @param[in]  args The arguments to the `CubicAxes` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseCubicAxes(args, ierr)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    ! subdomains to set
    INTEGER :: sd
    INTEGER :: i

    ! default all subdomains
    ierr = PARSE_ERROR

    ! Look for sd = #subdomain
    IF(SIZE(args) .EQ. 13) THEN
      sd = ParseSubdomain(args(11:13), ierr)

      IF(ierr .NE. PARSE_ERROR) THEN
        CALL RunParse(args(2:10), sd, sd, ierr)
      END IF
    ELSE IF(AssertNArgs(args, 10)) THEN
      CALL RunParse(args(2:10), 1, NMaterials, ierr)
    ELSE
      ierr = PARSE_ERROR
    END IF

    WRITE(*,*) "CubicAxes:"
    BLOCK
      INTEGER :: sd, i
      DO sd=1,NMaterials
        WRITE(*,*) "SD: ", sd
        DO i=1,3
          WRITE(*,*) CubicAxes(:,i,sd)
        END DO
      END DO
    END BLOCK

  CONTAINS
    SUBROUTINE RunParse(args, sd_min, sd_max, ierr)
      USE strings
      IMPLICIT NONE

      CHARACTER(LEN=*), INTENT(IN) :: args(9)
      INTEGER, INTENT(IN) :: sd_min, sd_max
      INTEGER, INTENT(OUT) :: ierr

      ! temporary flattened axes
      REAL(KIND=DP) :: axes(9)

      REAL(KIND=DP) :: norm
      INTEGER :: i, j

      ! Convert args to axes
      DO i=1,9
        CALL value(args(i), axes(i), ierr)
        IF(ierr .NE. 0) THEN
          WRITE(*,*) "Error parsing floating value: ", TRIM(args(i))
          ierr = PARSE_ERROR
          RETURN
        END IF
      END DO

      ! Set CubicAxes
      DO i=sd_min, sd_max
        CubicAxes(:,:,i) = RESHAPE(axes, (/ 3, 3 /))
      END DO

      ! Renormalize
      DO i=sd_min, sd_max
        DO j=1,3
          norm = NORM2(CubicAxes(:,j,i))
          IF(NONZERO(norm)) THEN
            CubicAxes(:,j,i) = CubicAxes(:,j,i) / norm
          END IF
        END DO
      END DO

      ierr = PARSE_SUCCESS
    END SUBROUTINE RunParse
  END SUBROUTINE ParseCubicAxes


  !> Parse the `EasyAxis` command
  !>
  !> MScript Example:
  !> @code
  !>    EasyAxis x y z
  !>    EasyAxis x y z sd = n
  !> @endcode
  !>
  !> @param[in]  args The arguments to the `EasyAxis` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseEasyAxis(args, ierr)
    USE strings
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    INTEGER :: sd
    INTEGER :: i

    ! Look for sd = #subdomain
    IF(SIZE(args) .EQ. 7) THEN
      ! Ensure format ... sd = n
      IF( &
        lowercase(TRIM(args(5))) .EQ. "sd" &
        .AND. &
        TRIM(args(6)) .EQ. "=" &
      ) THEN

        ! Set sd
        CALL value(args(7), sd, ierr)
        IF(ierr .NE. 0) THEN
          WRITE(*,*) "Error parsing subdomain id: ", TRIM(args(7))
          ierr = PARSE_ERROR
          RETURN
        END IF

        ! Change subdomain to subdomain index
        DO i=1,SIZE(SubDomainIds)
          IF(sd .EQ. SubDomainIds(i)) THEN
            sd = i
            EXIT
          END IF

          IF(i .EQ. SIZE(SubDomainIds)) THEN
            WRITE(*,*) "Error: Unknown subdomain: ", sd
            ierr = PARSE_ERROR
            RETURN
          END IF
        END DO

        CALL seteasyaxis(args(2), args(3), args(4), ierr, sd = sd)
      END IF
    ELSE IF(AssertNArgs(args, 4)) THEN
      CALL seteasyaxis(args(2), args(3), args(4), ierr)
    ELSE
      ierr = PARSE_ERROR
    END IF
  END SUBROUTINE ParseEasyAxis


  !> Parse the `Magnetite` command
  !>
  !> @param[in]  args The arguments to the `Magnetite` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseMagnetite(args, ierr)
    USE strings
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    REAL(KIND=DP) :: dbltmp
    INTEGER :: ios

    ierr = PARSE_ERROR

    !  "Magnetite  400 C"  sets material constants for magnetite at 400 deg C
    IF(AssertNArgs(args, 3)) then
      IF(args(3).eq.'C') then
        CALL value(args(2),dbltmp,ios)
        IF(ios.EQ.0) THEN
          CALL magnetite(dbltmp)

          ierr = PARSE_SUCCESS
        ENDIF
      ENDIF
    ENDIF
  END SUBROUTINE ParseMagnetite


  !> Parse the `Iron` command
  !>
  !> @param[in]  args The arguments to the `Iron` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseIron(args, ierr)
    USE strings
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    REAL(KIND=DP) :: dbltmp
    INTEGER :: ios

    ierr = PARSE_ERROR

    !  "Iron  400 C"  sets material constants for Iron at 400 deg C
    IF(AssertNArgs(args, 3)) THEN
      IF(args(3).EQ.'C') THEN
        CALL value(args(2),dbltmp,ios)
        IF(ios.EQ.0) THEN
          CALL iron(dbltmp)

          ierr = PARSE_SUCCESS
        ENDIF
      ENDIF
    ENDIF
  END SUBROUTINE ParseIron



  !> Parse the `Iron` command
  !>
  !> @param[in]  args The arguments to the `Iron` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseTM54(args, ierr)
    USE strings
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    REAL(KIND=DP) :: dbltmp
    INTEGER :: ios

    ierr = PARSE_ERROR

    !  "Iron  400 C"  sets material constants for Iron at 400 deg C
    IF(AssertNArgs(args, 3)) THEN
      IF(args(3).EQ.'C') THEN
        CALL value(args(2),dbltmp,ios)
        IF(ios.EQ.0) THEN
          CALL TM54(dbltmp)

          ierr = PARSE_SUCCESS
        ENDIF
      ENDIF
    ENDIF
  END SUBROUTINE ParseTM54



  !> Parse the `Field` command
  !>
  !> @param[in]  args The arguments to the `Field` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseSetField(args, ierr)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    CHARACTER(len=200) :: tmp(4)

    INTEGER :: nargs

    nargs = SIZE(args)

    IF( nargs .LE. 6 ) THEN
      tmp = '0'
      tmp(1:nargs-2) = args(3:nargs)
      ! 'external field direction 1 0 0 '  or 'magnetic field strength 1e-3'
      CALL setfield(tmp(1), tmp(2), tmp(3), tmp(4), ierr)
    ELSE
      ierr = PARSE_ERROR
    END IF
  END SUBROUTINE ParseSetField


  !> Parse the `ReadMesh` command
  !>
  !> @code
  !>    ReadMesh N MeshFile [filetype = PATRAN | TECPLOT]
  !> @endcode
  !>
  !> @param[in]  args The arguments to the `ReadMesh` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseReadMesh(args, ierr)
    USE Mesh_IO
    USE strings
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    INTEGER :: cnt
    INTEGER :: MeshFileType
    INTEGER, PARAMETER :: FILETYPE_PATRAN = 1, FILETYPE_TECPLOT = 2

    INTEGER :: ios
    INTEGER :: i

    LOGICAL :: l

    IF(SIZE(args) .LT. 3) THEN
      l = AssertNArgs(args, 3)
      ierr = PARSE_ERROR
      RETURN
    END IF

    !  e.g.:  ' ReadMesh 4 sphere-7.pat'
    CALL value(args(2),cnt,ios)
    IF(ios .NE. 0) THEN
      WRITE(*,*) "ERROR: Unable to parse INTEGER from ", TRIM(args(2))
      ierr = PARSE_ERROR
      RETURN
    END IF

    IF(cnt .GT. MaxMeshNumber) THEN
      WRITE(*,*) "ERROR: Index ", cnt, " larger than MaxMeshNumber ", MaxMeshNumber
      ierr = PARSE_ERROR
      RETURN
    END IF

    MeshFileType = FILETYPE_PATRAN
    IF(SIZE(args) .GT. 3) THEN
      IF(SIZE(args) .GE. 6) THEN
        IF(lowercase(TRIM(args(4))) .NE. "filetype") THEN
          WRITE(*,*) "ERROR: Unexpected parameter ", TRIM(args(4))
          WRITE(*,*) 'ERROR: Expected "filetype"'
          ierr = PARSE_ERROR
          RETURN
        END IF

        IF(lowercase(TRIM(args(5))) .NE. "=") THEN
          WRITE(*,*) "ERROR: Expected = after parameter ", TRIM(args(4))
          ierr = PARSE_ERROR
          return
        END IF

        SELECT CASE(lowercase(TRIM(args(6))))
          CASE("patran")
            MeshFileType = FILETYPE_PATRAN
          CASE("tecplot")
            MeshFileType = FILETYPE_TECPLOT
        END SELECT
      END IF

      IF(SIZE(args) .GT. 6) THEN
        WRITE(*,*) "ERROR: Too many arguments to ", TRIM(args(1))
      END IF
    END IF

    SELECT CASE(MeshFileType)
      CASE(FILETYPE_PATRAN)
        CALL ReadPatranMesh(args(3))
      CASE(FILETYPE_TECPLOT)
        CALL ReadTecplotMesh(args(3))
    END SELECT

    ! Save mesh data for later saving/loading
    CALL SaveMesh(cnt)
    CALL SaveFEM(cnt)

    ! Initialize extra energy calculators, typically loaded by
    ! LoadPlugin
    DO i=1,SIZE(ExtraEnergyCalculators)
      CALL ExtraEnergyCalculators(i)%Initialize()
    END DO

    ierr = PARSE_SUCCESS
  END SUBROUTINE ParseReadMesh


  !> Parse the `LoadMesh` command
  !>
  !> @param[in]  args The arguments to the `LoadMesh` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseLoadMesh(args, ierr)
    USE strings
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    INTEGER :: cnt
    INTEGER :: ios

    !  e.g.:  ' LoadMesh 2'
    CALL value(args(2),cnt,ios)
    IF( (ios.EQ.0) .AND. (cnt.LE.maxmeshnumber)) THEN

      ! BEM Calculator
      CALL LoadMesh(cnt)
      CALL LoadFEM(cnt)
      CALL BuildMaterialParameters()
      CALL BuildEnergyCalculator()

      ierr = PARSE_SUCCESS
    ELSE
      ierr = PARSE_ERROR
    END IF
  END SUBROUTINE ParseLoadMesh


  !> Parse the `SaveMesh` command
  !>
  !> @param[in]  args The arguments to the `SaveMesh` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseSaveMesh(args, ierr)
    USE strings
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    INTEGER :: cnt
    INTEGER :: ios

    !  e.g.:  ' SaveMesh 2'
    CALL value(args(2),cnt,ios)
    IF( (ios.EQ.0) .AND. (cnt.LE.maxmeshnumber)) THEN

      ! BEM Calculator
      CALL SaveMesh(cnt)
      CALL SaveFEM(cnt)

      ierr = PARSE_SUCCESS
    ELSE
      ierr = PARSE_ERROR
    END IF
  END SUBROUTINE ParseSaveMesh


  !> Parse the `ReadMag` command
  !>
  !> @param[in]  args The arguments to the `ReadMag` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseReadMag(args, ierr)
    USE Mesh_IO
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    !  e.g.:  ' ReadMag trapez.dat'  Make sure it fits to the current mesh!!
    call ReadMag(args(2))

    ierr = PARSE_SUCCESS
  END SUBROUTINE ParseReadMag


  !> Parse the `EnergyMin` command
  !>
  !> @param[in]  args The arguments to the `EnergyMin` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseEnergyMin(args, ierr)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    !  e.g.:  ' Minimize'
    ! Calls the minimization routine for the current mesh and magnetization
    CALL EnergyMin()

    ierr = PARSE_SUCCESS
  END SUBROUTINE ParseEnergyMin


  !> Parse the `PathMinimize` command
  !>
  !> @param[in]  args The arguments to the `PathMinimize` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParsePathMinimize(args, ierr)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    !  e.g.:  ' PathMinimize'
    ! Calls the path minimization routine for the current path
    CALL PathMinimize()

    ierr = PARSE_SUCCESS
  END SUBROUTINE ParsePathMinimize


  !> Parse the `Remesh` command
  !>
  !> @param[in]  args The arguments to the `Remesh` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseRemesh(args, ierr)
    USE strings
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    REAL(KIND=DP), ALLOCATABLE :: mremesh(:,:)

    INTEGER :: cnt
    INTEGER :: ios

    !  e.g.:  'ReMesh 3'
    ! Remeshes current magnetization to the saved mesh with index 3
    ! and loads this
    CALL value(args(2),cnt,ios)
    IF( (ios.EQ.0).AND. (cnt.LE.maxmeshnumber)) THEN
      CALL RemeshTo(cnt, mremesh)
      CALL LoadMesh(cnt)
      CALL LoadFEM(cnt)
      CALL BuildMaterialParameters()
      CALL BuildEnergyCalculator()
      m(:,:)=mremesh(:,:)

      ierr = PARSE_SUCCESS
    ELSE
      ierr = PARSE_ERROR
    END IF
  END SUBROUTINE ParseRemesh

  !> Parse the `Print` command
  !>
  !> MScript example:
  !> Resizes mesh lengths from 500 nm to 100 nm, len=len/500*100
  !> @code
  !>    Resize 500 100
  !> @endcode
  !>
  !> @param[in]  args The arguments to the `Print` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseResize(args, ierr)
    USE strings
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    REAL(KIND=DP) :: dbltmp
    INTEGER :: ios

    IF(.NOT. AssertNArgs(args, 3)) THEN
      RETURN
    END IF

    CALL value(args(2),dbltmp,ios)
    IF(ios.EQ.0) THEN
      Ls=SQRT(Ls)*dbltmp ! Note that lengthes are  Len = 1/SQRT(Ls)
    ELSE
      WRITE(*,*) "ERROR: Unable to parse REAL from ", TRIM(args(2))
      ierr = PARSE_ERROR
      RETURN
    END IF

    CALL value(args(3),dbltmp,ios)
    IF(ios.EQ.0) THEN
      Ls=(Ls/dbltmp)
    ELSE
      WRITE(*,*) "ERROR: Unable to parse REAL from ", TRIM(args(3))
      ierr = PARSE_ERROR
    END IF
      ls=ls*ls

    ierr = PARSE_SUCCESS
  END SUBROUTINE ParseResize


  !> Parse the `WriteMag` command
  !>
  !> @param[in]  args The arguments to the `WriteMag` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseWriteMag(args, ierr)
    USE Mesh_IO
    USE strings
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    INTEGER :: nargs

    nargs = SIZE(args)

    !  e.g.:  ' WriteMag trapez'  uses stem 'trapez'
    IF (nargs>1) THEN
      stem=args(2)
      CALL compact(stem)
      datafile=stem(:LEN_TRIM(stem))//'.dat'
    END IF
    !  Writes mag ->  MODULE Mesh_IO
    CALL WriteMag()
    ! zoneflag = -1.0 implies use of zone/zoneinc
    CALL WriteTecplot(zoneflag,stem,1)

    ierr = PARSE_SUCCESS
  END SUBROUTINE ParseWriteMag


  !> Parse the `WriteDemag` command
  !>
  !> @param[in]  args The arguments to the `WriteDemag` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseWriteDemag(args, ierr)
    USE Mesh_IO
    USE strings
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    INTEGER :: nargs

    nargs = SIZE(args)

    !  e.g.:  ' WriteMag trapez'  uses stem 'trapez'
    IF (nargs>1) THEN
      stem=args(2)
      CALL compact(stem)
    END IF
    !  Writes demag field ->  MODULE Mesh_IO
    CALL WriteDemag(zoneflag,stem,1)


    ierr = PARSE_SUCCESS
  END SUBROUTINE ParseWriteDemag


  !> Parse the `WriteHyst` command
  !>
  !> @param[in]  args The arguments to the `WriteHyst` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseWriteHyst(args, ierr)
    USE Mesh_IO
    USE strings
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    INTEGER :: nargs

    nargs = SIZE(args)

    ! e.g.: ' WriteHyst trapez' uses stem 'trapez' to output file 'trapez.hyst'
    IF (nargs>1) THEN
      stem=args(2)
      CALL compact(stem)
      hystfile=stem(:LEN_TRIM(stem))//'.hyst'
    END IF
    CALL WRITEhyst()

    ierr = PARSE_SUCCESS
  END SUBROUTINE ParseWriteHyst


  !> Parse the `WriteBoxData` command
  !> 
  !> @param[in]  args The arguments of the `WriteBoxData` (output file name)
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseWriteBoxData(args, ierr)
    USE Mesh_IO
    USE strings
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    INTEGER :: nargs

    nargs = SIZE(args)

    ! e.g.: ' WriteBoxData trapez' uses stem 'trapez' to output file 'trapez.vbox'
    IF (nargs>1) THEN
      stem=args(2)
      CALL compact(stem)
      vboxfile=stem(:LEN_TRIM(stem))//'.vbox'
    END IF
    CALL WRITEvbox()

    ierr = PARSE_SUCCESS
  END SUBROUTINE ParseWriteBoxData


  !> Parse the `GradientTest` command
  !>
  !> @param[in]  args The arguments to the `GradientTest` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseGradientTest(args, ierr)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    ! tests the energy gradient
    IF(AssertNArgs(args, 1)) THEN
      CALL GradientTest()

      ierr = PARSE_SUCCESS
    ELSE
      ierr = PARSE_ERROR
    END IF
  END SUBROUTINE ParseGradientTest


  !> Parse the `ReportEnergy` command
  !>
  !> @param[in]  args The arguments to the `ReportEnergy` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseReportEnergy(args, ierr)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    ! reports the micromagnetic energies, e.g. for test purposes
    IF(AssertNArgs(args, 1)) THEN
      CALL ReportEnergy()

      ierr = PARSE_SUCCESS
    ELSE
      ierr = PARSE_ERROR
    END IF
  END SUBROUTINE ParseReportEnergy


  !> Parse the `KeyPause` command
  !>
  !> @param[in]  args The arguments to the `KeyPause` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseKeyPause(args, ierr)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    CHARACTER(len=1024) :: tempstr

    !  Wait for [enter]
    WRITE(*,*) ' Pause: Press any key + ENTER to continue'
    READ (*,*) tempstr

    ierr = PARSE_SUCCESS
  END SUBROUTINE ParseKeyPause


  !> Parse the `UniformMagnetization` command
  !>
  !> @param[in]  args The arguments to the `UniformMagnetization` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseUniformMagnetization(args, ierr)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    INTEGER :: nargs,sd
    CHARACTER(len=100), PARAMETER :: zero = '0'



    nargs = SIZE(args)

    ierr = PARSE_ERROR

    !  "uniform magnetization 0 1 1"
    ! Create uniform   magnetization  with defined direction
    
        ! "randomize magnetization 20"
    ! Changes current magnetization by random angles <20 deg
    
    IF (nargs==8) THEN
      sd = ParseSubdomain(args(6:8), ierr)
        IF(ierr .NE. PARSE_ERROR) THEN
          CALL setuniform(args(3),args(4),args(5), ierr, sd=sd)
        
        endif
      ELSE IF(nargs==5) THEN
      CALL setuniform(args(3),args(4),args(5), ierr)
    else
      ierr=PARSE_ERROR
    END IF
  END SUBROUTINE ParseUniformMagnetization


  !> Parse the `RandomizeMagnetization` command
  !>
  !> @param[in]  args The arguments to the `RandomizeMagnetization` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseRandomizeMagnetization(args, ierr)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr
    
    
    INTEGER :: sd
    INTEGER :: ios

    ! "randomize magnetization 20"
    ! Changes current magnetization by random angles <20 deg
    IF(SIZE(args) .EQ. 6) then
      sd = ParseSubdomain(args(4:6), ierr)
         IF(ierr .NE. PARSE_ERROR) THEN
           CALL randomchange(args(2), args(3), sd=sd) ! ->Module_Mesh
           ierr = PARSE_SUCCESS
        END IF
      ELSE IF(AssertNArgs(args, 3)) THEN
        CALL randomchange(args(2), args(3)) ! ->Module_Mesh
        ierr = PARSE_SUCCESS
      ELSE
        ierr = PARSE_ERROR
      END IF
      
  END SUBROUTINE ParseRandomizeMagnetization


  !> Parse the `ParseInvertMagnetization` command
  !>
  !> @param[in]  args The arguments to the `ParseInvertMagnetization` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseInvertMagnetization(args, ierr)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    ! "invert magnetization 1 -1 1"
    ! Changes current magnetization mx,my,mz -> mx, -my, mz
    IF (AssertNArgs(args, 5)) THEN
      CALL InvertMag(args(3), args(4), args(5)) ! ->Module_Mesh

      ierr = PARSE_SUCCESS
    ELSE
      ierr = PARSE_ERROR
    END IF
  END SUBROUTINE ParseInvertMagnetization


  !> Parse the `Vortex Magnetization` command
  !>
  !> MScript Example:
  !> @code
  !>    Vortex Magnetization px py pz [p0x p0y p0z] [v] [RH/LH]
  !> @endcode
  !>
  !> @param[in]  args The arguments to the `VortexMagnetization` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseVortexMagnetization(args, ierr)
    USE strings
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    REAL(KIND=DP) :: p(3), p0(3), v
    CHARACTER(len=3) :: orientation

    p = 0
    p0 = 0
    v  = 0
    orientation = 'RH'
    IF(SIZE(args) .GE. 5) THEN

      ! First 3 args are p
      CALL TryParseP(args(3:5), ierr)
      IF(ierr .NE. PARSE_SUCCESS) RETURN

      ! Try find p0
      IF(SIZE(args) .GE. 8) THEN
        CALL TryParseP0(args(6:8), ierr)
        IF(ierr .NE. PARSE_SUCCESS) RETURN

        IF(SIZE(args) .GT. 8) THEN
          CALL TryParseVAndRH(args(9:), ierr)
        END IF
      ELSE IF(SIZE(args) .GT. 5) THEN
        CALL TryParseVAndRH(args(6:), ierr)
      END IF

    ELSE
      WRITE(*,*) "ERROR: Vortex Magnetization expects at least 3 arguments"
      ierr = PARSE_ERROR
    END IF

    IF(ierr .EQ. PARSE_SUCCESS) THEN
      CALL VortexMag(p, p0 = p0, v = v, orientation = orientation)
    END IF

  CONTAINS
    SUBROUTINE TryParseReal(arg, dest, varname, ierr)
      CHARACTER(len=*), INTENT(IN) :: arg
      REAL(KIND=DP), INTENT(OUT) :: dest
      CHARACTER(len=*), INTENT(IN) :: varname
      INTEGER, INTENT(OUT) :: ierr

      CALL value(arg, dest, ierr)
      IF(ierr .NE. 0) THEN
        WRITE(*,*) "ERROR: Unable to parse REAL for argument p1"
        ierr = PARSE_ERROR
      ELSE
        ierr = PARSE_SUCCESS
      END IF
    END SUBROUTINE

    SUBROUTINE TryParseP(args, ierr)
      CHARACTER(len=*), INTENT(IN) :: args(3)
      INTEGER, INTENT(OUT) :: ierr

      CALL TryParseReal(args(1), p(1), "px", ierr)
      IF(ierr .EQ. PARSE_ERROR) RETURN

      CALL TryParseReal(args(2), p(2), "py", ierr)
      IF(ierr .EQ. PARSE_ERROR) RETURN

      CALL TryParseReal(args(3), p(3), "pz", ierr)
      IF(ierr .EQ. PARSE_ERROR) RETURN
    END SUBROUTINE TryParseP

    SUBROUTINE TryParseP0(args, ierr)
      CHARACTER(len=*), INTENT(IN) :: args(3)
      INTEGER, INTENT(OUT) :: ierr

      CALL TryParseReal(args(1), p0(1), "p0x", ierr)
      IF(ierr .EQ. PARSE_ERROR) RETURN

      CALL TryParseReal(args(2), p0(2), "p0y", ierr)
      IF(ierr .EQ. PARSE_ERROR) RETURN

      CALL TryParseReal(args(3), p0(3), "p0z", ierr)
      IF(ierr .EQ. PARSE_ERROR) RETURN
    END SUBROUTINE TryParseP0

    SUBROUTINE TryParseVAndRH(args, ierr)
      CHARACTER(len=*), INTENT(IN) :: args(:)
      INTEGER, INTENT(OUT) :: ierr

      LOGICAL :: FoundV, FoundRHLH
      REAL(KIND=DP) :: tmp_v
      INTEGER :: i

      FoundV      = .FALSE.
      FoundRHLH = .FALSE.
      DO i=1,SIZE(args)
        ! Expect every iteration to parse something
        ierr = PARSE_ERROR

        ! Try parse v
        IF(.NOT. FoundV) THEN
          CALL value(args(i), tmp_v, ierr)
          IF(ierr .EQ. 0) THEN
            v = tmp_v
            FoundV = .TRUE.
            ierr = PARSE_SUCCESS
            CYCLE
          END IF
        END IF

        ! Try parse RH/LH
        IF(.NOT. FoundRHLH) THEN
          SELECT CASE(TRIM(lowercase(args(i))))
            CASE('rh')
              orientation = 'RH'
              FoundRHLH = .TRUE.
              ierr = PARSE_SUCCESS
            CASE('lh')
              orientation = 'LH'
              FoundRHLH = .TRUE.
              ierr = PARSE_SUCCESS
          END SELECT
        END IF

        IF(ierr .EQ. PARSE_ERROR) THEN
          WRITE(*,*) "ERROR: Unable to parse argument ", TRIM(args(i))
          WRITE(*,*) "ERROR: Expecting either REAL for 'v', or RH/LH for 'orientation'"
        END IF
      END DO

    END SUBROUTINE TryParseVAndRH

  END SUBROUTINE ParseVortexMagnetization


  !> Parse the `EnergyLog` command
  !>
  !> @param[in]  args The arguments to the `EnergyLog` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseEnergyLog(args, ierr)
    USE strings

    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr
    INTEGER i


    ! Start logging the energy
    IF(AssertNArgs(args, 2)) THEN
      stem=args(2)
      CALL compact(stem)
      logfile=stem(:LEN_TRIM(stem))//'.log'
      IF(EnergyLogging) CLOSE(logunit)
      EnergyLogging=.TRUE.
      OPEN(NEWUNIT=logunit, FILE=logfile, STATUS='unknown')
      WRITE(logunit, '(A)') &
        "Micromagnetic energy calculation : BEMScript 2014"
      WRITE(logunit, '(A)') "Mesh data:"
      CALL tstamp(logunit)
      WRITE(logunit, '(5A14)') &
        "Nodes","Tetrahedra", "Bndry nodes", "Bndry faces", "Volume"
      WRITE(logunit, '(4I14,E14.6)') &
        NNODE, NTRI, BNODE, BFCE, total_volume
      WRITE(logunit, '(A)') "Material data:"
      	do i = 1 , SIZE(Ms)
      	if (SIZE(Ms).gt.1)  WRITE(logunit, '(A15,I2)') "Sub Domain",i
      	WRITE(logunit, '(8A14)') &
        	"Temp (c)","Ms","K1", "A_ex", "Vol^(1/3) (m)", &
        	"QHardness", "Lam_Ex (nm)", &
        	"Kd V (J)"
      	WRITE(logunit, '(i14,7E14.6)') &
        	INT(T), Ms(i), K1(i), Aex(i), (total_volume**(1./3.))/SQRT(Ls), &
        	QHardness, LambdaEx*1.d9, &
        	Kd*total_volume/(SQRT(Ls)**3)
      enddo
      WRITE(logunit, '(A)') "External field direction (x,y,z) &
        &and strength B (T):"
      WRITE(logunit, '(4A14)') "hx","hy", "hz", "B (T)"
      WRITE(logunit, '(4E14.6)')  hz(1),hz(2),hz(3),extapp
      WRITE(logunit, '(A)') "Energies in units of Kd V:"
      WRITE(logunit, '(2A12,1A20,11A28)') &
        "Global-N-Eval", "N-Eval","E-Anis", "E-Exch1","E-Exch2",&
        "E-Exch3","E-Exch4", "E-ext", "E-Demag",&
        "E-Tot","Mx","My","Mz"

      ierr = PARSE_SUCCESS
    ELSE
      ierr = PARSE_ERROR
    END IF
  END SUBROUTINE ParseEnergyLog


  !> Parse the `PathLogFile` command
  !>
  !> @param[in]  args The arguments to the `PathLogFile` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParsePathLogFile(args, ierr)
    USE strings
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    ! Start logging the energy
    IF(AssertNArgs(args, 2)) THEN
      stem=args(2)
      CALL compact(stem)
      PLogEnFile=stem(:LEN_TRIM(stem))//'.enlog'
      PLogGradFile=stem(:LEN_TRIM(stem))//'.grlog'
      PLogDistFile=stem(:LEN_TRIM(stem))//'.dlog'
      PLogFile=stem(:LEN_TRIM(stem))//'-last.dat'
      IF(PathLoggingQ) THEN
        CLOSE(PLogEnUnit)
        CLOSE(PLogGradUnit)
        CLOSE(PLogDistUnit)
        CLOSE(PLogDistUnit)
      END IF
      PathLoggingQ=.TRUE.
      OPEN(NEWUNIT=PLogEnUnit,FILE=PLogEnFile,  STATUS='unknown')
      OPEN(NEWUNIT=PLogGradUnit,FILE=PLogGradFile,STATUS='unknown')
      OPEN(NEWUNIT=PLogDistUnit,FILE=PLogDistFile,STATUS='unknown')
      WRITE(PLogEnUnit, '(A, A)') "Micromagnetic path energies: ", &
        MERRILL_VERSION_STRING
      WRITE(PLogEnUnit, '(A)') "Mesh data:"
      CALL tstamp(PLogEnUnit)
      WRITE(PLogEnUnit, '(5A14)') "Nodes","Tetrahedra", "Bndry nodes", &
        "Bndry faces", "Volume"
      WRITE(PLogEnUnit, '(4I14,E14.6)') NNODE,NTRI,BNODE,BFCE,total_volume
      WRITE(PLogEnUnit, '(A)') "Material data:"
      WRITE(PLogEnUnit, '(7A14)') "Ms","K1", "A_ex", "V^(1/3) (m)", "QHardness", &
        "Lam_Ex (nm)", "Kd V (J)"
      WRITE(PLogEnUnit, '(7E14.6)') &
        Ms, K1, Aex, &
        (total_volume**(1./3.))/SQRT(Ls), QHardness, &
        LambdaEx*1.d9,Kd*total_volume/(SQRT(Ls)**3)
      WRITE(PLogEnUnit, '(A)') "External field direction (x,y,z) &
        &and strength B (T):"
      WRITE(PLogEnUnit, '(4A14)') "hx","hy", "hz", "B (T)"
      WRITE(PLogEnUnit, '(4E14.6)')  hz(1),hz(2),hz(3),extapp
      WRITE(PLogEnUnit, '(A13,I5)') "Path Length: ",PathN

      WRITE(PLogEnUnit, *) "Path Energies in Kd V: "
      WRITE(PLogGradUnit, '(A, A)') "Micromagnetic path energy gradient norms: ", &
        MERRILL_VERSION_STRING
      WRITE(PLogDistUnit, '(A, A)') "Micromagnetic path cumulative distances: ", &
        MERRILL_VERSION_STRING

      ierr = PARSE_SUCCESS
    ELSE
      ierr = PARSE_ERROR
    END IF
  END SUBROUTINE ParsePathLogFile


  !> Parse the `CloseLogFile` command
  !>
  !> @param[in]  args The arguments to the `CloseLogFile` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseCloseLogFile(args, ierr)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    ! Stop logging the energy
    IF(EnergyLogging) THEN
      CALL CloseIfOpen(logunit)
      EnergyLogging=.FALSE.
    END IF
    IF(PathLoggingQ) THEN
      CALL CloseIfOpen(PLogEnUnit)
      CALL CloseIfOpen(PLogGradUnit)
      CALL CloseIfOpen(PLogDistUnit)
    END IF
    PathLoggingQ=.FALSE.

    ierr = PARSE_SUCCESS
  CONTAINS
    SUBROUTINE CloseIfOpen(unit)
      INTEGER, INTENT(INOUT) :: unit
      LOGICAL :: is_open
      is_open = .FALSE.
      IF(unit .NE. 0) INQUIRE(UNIT=unit, OPENED=is_open)
      IF(is_open) CLOSE(unit)

      unit = 0
    END SUBROUTINE CloseIfOpen
  END SUBROUTINE ParseCloseLogFile


  !> Parse the `MagnetizationToPath` command
  !>
  !> @param[in]  args The arguments to the `MagnetizationToPath` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseMagnetizationToPath(args, ierr)
    USE strings
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    INTEGER :: cnt
    INTEGER :: ios

    ! Saves current magnetization to path index
    CALL value(args(2),cnt,ios)
    IF(ios.EQ.0) THEN
      IF (cnt<0) cnt=PathN+1-cnt
      IF (cnt>0 .AND. cnt<(PathN+1))  PMag(cnt,:,:)= m(:,:)

      ierr = PARSE_SUCCESS
    ELSE
      ierr = PARSE_ERROR
    END IF
  END SUBROUTINE ParseMagnetizationToPath


  !> Parse the `PathToMagnetization` command
  !>
  !> @param[in]  args The arguments to the `PathToMagnetization` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParsePathToMagnetization(args, ierr)
    USE strings
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    INTEGER :: cnt
    INTEGER :: ios

    ! Saves current magnetization to path index
    CALL value(args(2),cnt,ios)
    IF(ios.EQ.0) THEN
      IF (cnt<0) cnt=PathN+1-cnt
      IF (cnt>0 .AND. cnt<(PathN+1))  m(:,:)=PMag(cnt,:,:)

      ierr = PARSE_SUCCESS
    ELSE
      ierr = PARSE_ERROR
    END IF
  END SUBROUTINE ParsePathToMagnetization


  !> Parse the `RenewDist` command
  !>
  !> @param[in]  args The arguments to the `RenewDist` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParsePathRenewDist(args, ierr)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    ! defines the Path distances assuming all magnetizations are filled
    CALL PathRenewDist()

    ierr = PARSE_SUCCESS
  END SUBROUTINE ParsePathRenewDist


  !> Parse the `MakeInitialPath` command
  !>
  !> @param[in]  args The arguments to the `MakeInitialPath` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseMakeInitialPath(args, ierr)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    ! defines the Path variables assuming all magnetizations are filled
    CALL MakeInitialPath()

    ierr = PARSE_SUCCESS
  END SUBROUTINE ParseMakeInitialPath


  !> Parse the `RefinePathTo` command
  !>
  !> @param[in]  args The arguments to the `RefinePathTo` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseRefinePathTo(args, ierr)
    USE strings
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    INTEGER :: cnt
    INTEGER :: ios

    ! Refines current path to new number of states
    CALL value(args(2),cnt,ios)
    IF(ios.EQ.0) THEN
      IF (cnt>1 ) CALL RefinePathTo(cnt)

      ierr = PARSE_SUCCESS
    ELSE
      ierr = PARSE_ERROR
    END IF
  END SUBROUTINE ParseRefinePathTo


  !> Parse the `WriteTecplotPath` command
  !>
  !> @param[in]  args The arguments to the `WriteTecplotPath` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseWriteTecplotPath(args, ierr)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    ! Exports current PATH to TecPlot file
    IF(AssertNArgs(args, 2)) THEN
      PathOutFile=args(2)
      CALL WriteTecplotPath()

      ierr = PARSE_SUCCESS
    ELSE
      ierr = PARSE_ERROR
    END IF
  END SUBROUTINE ParseWriteTecplotPath


  !> Parse the `ReadTecplotPath` command
  !>
  !> @param[in]  args The arguments to the `ReadTecplotPath` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseReadTecplotPath(args, ierr)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    ! Imports path from a TecPlot file
    IF(AssertNArgs(args, 2)) THEN
      PathInFile=args(2)
      CALL ReadTecplotPath()

      ierr = PARSE_SUCCESS
    ELSE
      ierr = PARSE_ERROR
    END IF
  END SUBROUTINE ParseReadTecplotPath


  !> Parse the `ReadTecplotZone` command
  !>
  !> @param[in]  args The arguments to the `ReadTecplotZone` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseReadTecplotZone(args, ierr)
    USE strings
    USE Material_Parameters
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr
    INTEGER :: ios
    ! Imports a single zone Izone from a TecPlot file
    ierr = PARSE_SUCCESS

    ! Ensure usage: ReadTecplotZone width izone
    IF(AssertNArgs(args, 3)) THEN
      IF(ierr.NE.PARSE_ERROR) THEN
        CALL value(args(3), izone, ios)
        IF(ios.NE.0) ierr = PARSE_ERROR
      END IF
      IF(ierr.NE.PARSE_ERROR) THEN
        CALL value(args(3), izone, ios)
        IF(ios.NE.0) ierr = PARSE_ERROR
      END IF


      IF(ierr.NE.PARSE_ERROR) THEN
        ZoneInFile=args(2)
!      print*, 'Call read zone'
!      print*, 'izone = ', izone
!      print*, 'filename = ', PathInFile
        CALL ReadTecplotZone( )
      END IF
    ELSE
      ierr = PARSE_ERROR
    END IF


  END SUBROUTINE ParseReadTecplotZone


  !> Parse the `PathStructureEnergies` command
  !>
  !> @param[in]  args The arguments to the `PathStructureEnergies` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParsePathStructureEnergies(args, ierr)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    INTEGER :: nargs

    REAL(KIND=DP) :: dbltmp
    INTEGER :: i
    INTEGER :: pseunit

    nargs = SIZE(args)

    ! Output the energies for the path
    IF(nargs==2) THEN
      ! Write the path energies to a file.
      OPEN(NEWUNIT=pseunit, FILE=args(2), STATUS='unknown')
      DO i = 1, pathn
        dbltmp = structureEnergy(i)
        ! Normalize energy by length scale cubed to give Joule.
        dbltmp = dbltmp / SQRT(Ls)**3
        WRITE(pseunit, '(I20, E30.15E5)') i, dbltmp
      END DO
      CLOSE(pseunit)

      ierr = PARSE_SUCCESS
    ELSE IF(nargs == 1) THEN
      ! Write the path energies to standard output.
      DO i = 1, pathn
        dbltmp = structureEnergy(i)
        ! Normalize energy by length scale cubed to give Joule.
        dbltmp = dbltmp / SQRT(Ls)**3
        WRITE(*, '(I20, E30.15E5)') i, dbltmp
      END DO

      ierr = PARSE_SUCCESS
    ELSE
      WRITE(*,*) "Command '", TRIM(args(1)), "' expected 1 or 2 arguments. &
        &Got ", nargs
      ierr = PARSE_ERROR
    END IF
  END SUBROUTINE ParsePathStructureEnergies


  !> Parse the `WritePathStructures` command
  !>
  !> @param[in]  args The arguments to the `WritePathStructures` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseWritePathStructures(args, ierr)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    INTEGER :: i

    ! Writes each structure as a separate tecplot file.
    DO i = 1, pathn
       WRITE(*,*) "it's the stem: ", stem
    END DO

    ierr = PARSE_SUCCESS
  END SUBROUTINE ParseWritePathStructures


  !> Parse the `Energy` command
  !>
  !> @param[in]  args The arguments to the `Energy` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseEnergy(args, ierr)
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    INTEGER :: nargs

    REAL(KIND=DP) :: dbltmp
    INTEGER :: i

    nargs = SIZE(args)

    dbltmp = currentMagEnergy()
    dbltmp = dbltmp / SQRT(Ls)**3
    WRITE(*, '(E30.15E5)') dbltmp

    ierr = PARSE_SUCCESS
  END SUBROUTINE ParseEnergy


  !> Parse the `LoadPlugin` command
  !>
  !> @param[in]  args The arguments to the `LoadPlugin` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseLoadPlugin(args, ierr)
    USE Plugins
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    ! Load plugin
    IF(AssertNArgs(args, 2)) THEN
      CALL LoadPlugin(args(2))

      ierr = PARSE_SUCCESS
    ELSE
      ierr = PARSE_ERROR
    END IF
  END SUBROUTINE ParseLoadPlugin





  !> Parse the `GenerateCubeMesh` command
  !>
  !> @param[in]  args The arguments to the `GenerateCubeMesh` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseGenerateCubeMesh(args, ierr)
    USE Mesh_IO
    USE strings
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    REAL(KIND=DP) :: width, edge_length
    INTEGER :: ios

    ierr = PARSE_SUCCESS

    ! Ensure usage: GenerateCubeMesh width edge_length
    IF(AssertNArgs(args, 3)) THEN
      IF(ierr.NE.PARSE_ERROR) THEN
        CALL value(args(2), width, ios)
        IF(ios.NE.0) ierr = PARSE_ERROR
      END IF

      IF(ierr.NE.PARSE_ERROR) THEN
        CALL value(args(3), edge_length, ios)
        IF(ios.NE.0) ierr = PARSE_ERROR
      END IF

      IF(ierr.NE. PARSE_ERROR) THEN
        CALL GenerateCubeMesh(width, edge_length)
      END IF
    ELSE
      ierr = PARSE_ERROR
    END IF
  END SUBROUTINE ParseGenerateCubeMesh


  !> Parse the `GenerateSphereMesh` command
  !>
  !> @param[in]  args The arguments to the `GenerateSphereMesh` command.
  !> @param[out] ierr The result of the parse
  !> @todo DOCUMENT ME
  SUBROUTINE ParseGenerateSphereMesh(args, ierr)
    USE Mesh_IO
    USE strings
    IMPLICIT NONE

    CHARACTER(len=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    REAL(KIND=DP) :: width, edge_length
    INTEGER :: ios

    ierr = PARSE_SUCCESS

    ! Ensure usage: GenerateCubeMesh width edge_length
    IF(AssertNArgs(args, 3)) THEN
      IF(ierr.NE.PARSE_ERROR) THEN
        CALL value(args(2), width, ios)
        IF(ios.NE.0) ierr = PARSE_ERROR
      END IF

      IF(ierr.NE.PARSE_ERROR) THEN
        CALL value(args(3), edge_length, ios)
        IF(ios.NE.0) ierr = PARSE_ERROR
      END IF

      IF(ierr.NE. PARSE_ERROR) THEN
        CALL GenerateSphereMesh(width, edge_length)
      END IF
    ELSE
      ierr = PARSE_ERROR
    END IF
  END SUBROUTINE ParseGenerateSphereMesh


  !---------------------------------------------------------------
  !     subroutine setanis(anis)
  !---------------------------------------------------------------

  !> Set the anisotropy of subdomain \c sd based on the name of the
  !> anisotropy type in \c anis.
  !>
  !> @param[in]  anis The type of anisotropy form to set
  !> @param[out] ierr The result of parsing and setting
  !> @param[in]  sd   The subdomain to set the anisotropy for.
  !>                  If omitted, every subdomain is set to the given
  !>                  anisotropy.
  SUBROUTINE setanis(anis, ierr, sd)
    USE strings
    IMPLICIT NONE

    CHARACTER(LEN=*), INTENT(IN) :: anis
    INTEGER, INTENT(OUT) :: ierr
    INTEGER, OPTIONAL, INTENT(IN) :: sd

    SELECT CASE(lowercase(TRIM(anis)))
      CASE('cubic', 'cubicanisotropy')
        WRITE(*,*) "Anisotropy Form: Cubic"
        IF(PRESENT(sd)) THEN
          anisform(sd) = ANISFORM_CUBIC
        ELSE
          anisform(:)  = ANISFORM_CUBIC
        END IF
        ierr = PARSE_SUCCESS

      CASE('uniaxial', 'uni')
        WRITE(*,*) "Anisotropy Form: Uniaxial"
        IF(PRESENT(sd)) THEN
          anisform(sd) = ANISFORM_UNIAXIAL
        ELSE
          anisform(:)  = ANISFORM_UNIAXIAL
        END IF
        ierr = PARSE_SUCCESS

      CASE DEFAULT
        WRITE(*,*) "Unknown Anisotropy :", TRIM(anis)
        ierr = PARSE_ERROR
    END SELECT
  END SUBROUTINE setanis


  !---------------------------------------------------------------
  !     subroutine seteasyaxis(args(3),args(4),args(5))
  !---------------------------------------------------------------

  !> Set the easy axis of the material for the subdomain \c sd.
  !>
  !> @param[in]  ax   The x-component of the easy axis.
  !> @param[in]  ay   The y-component of the easy axis.
  !> @param[in]  az   The z-component of the easz axis.
  !> @param[out] ierr The result of the parsing and setting.
  !> @param[in]  sd   The subdomain to set the easy axis for. If omitted,
  !>                  the easy axis is set for all subdomains.
  SUBROUTINE seteasyaxis(ax, ay, az, ierr, sd)
    USE strings
    IMPLICIT NONE

    CHARACTER(LEN=*), INTENT(IN) :: ax, ay, az
    INTEGER, INTENT(OUT) :: ierr
    INTEGER, OPTIONAL, INTENT(IN) :: sd

    INTEGER :: ios
    REAL(KIND=DP) anix,aniy,aniz
    REAL(KIND=DP) eanorm

    INTEGER :: i

    ierr = PARSE_SUCCESS

    CALL value(ax,anix,ios)
    IF(ios/=0) THEN
      WRITE(*,*) 'ERROR setting anisotropy x: ', ax
      ierr = PARSE_ERROR
    END IF
    CALL value(ay,aniy,ios)
    IF(ios/=0) THEN
      WRITE(*,*) 'ERROR setting anisotropy y: ', ay
      ierr = PARSE_ERROR
    END IF
    CALL value(az,aniz,ios)
    IF(ios/=0) THEN
      WRITE(*,*) 'ERROR setting anisotropy z: ', az
      ierr = PARSE_ERROR
    END IF

    IF(PRESENT(sd)) THEN
      EasyAxis(1,sd) = anix
      EasyAxis(2,sd) = aniy
      EasyAxis(3,sd) = aniz
    ELSE
      EasyAxis(1,:) = anix
      EasyAxis(2,:) = aniy
      EasyAxis(3,:) = aniz
    END IF

    DO i=1,NMaterials
      eanorm = SQRT(EasyAxis(1,i)**2 + EasyAxis(2,i)**2 + EasyAxis(3,i)**2)
      IF(NONZERO(eanorm)) THEN
        EasyAxis(1,i) = EasyAxis(1,i)/eanorm
        EasyAxis(2,i) = EasyAxis(2,i)/eanorm
        EasyAxis(3,i) = EasyAxis(3,i)/eanorm
      ELSE
        WRITE(*,*) "Easy Axis is Zero!"
        ierr = PARSE_ERROR
      END IF
    END DO

  END SUBROUTINE seteasyaxis

  !---------------------------------------------------------------
  !     subroutine setfield(args(3),args(4),args(5),args(6))
  !---------------------------------------------------------------

  !> Set the external/Zeeman field.
  !>
  !> @param[in]  str The mode of this function.
  !>                 If the value is 'direction', the `shx`, `shy`, and `shz`
  !>                 parameters will be used to set the unit direction of the
  !>                 Zeeman field.
  !>                 If the value is 'strength', the `shx` parameter will be
  !>                 used as the value, and `shy` will be used as the units,
  !>                 effectively a scaling of the strength.
  !> @param[in]  shx When `str == 'direction'`, this is the x-component of the
  !>                 unit direction of the field.
  !>                 When `str == 'strength'`, this is the strength of the
  !>                 field.
  !> @param[in]  shy When `str == 'direction'`, this is the y-component of the
  !>                 unit direction of the field.
  !>                 When `str == 'strength'`, this is the units used to define
  !>                 the field strength.
  !> @param[in]  shz Only used when `str == 'direction'`. This is the
  !>                 z-component of the unit direction of the field.
  !> @param[out] ierr The result of the parse and the set.
  SUBROUTINE setfield(str, shx, shy, shz, ierr)
    USE strings
    IMPLICIT NONE

    CHARACTER(LEN=100) :: str, shx, shy, shz
    INTEGER :: ios
    REAL(KIND=DP) :: dh(3)
    REAL(KIND=DP) :: hznorm

    INTEGER :: ierr

    ierr = NO_PARSE


    SELECT CASE(lowercase(str))
      CASE('direction')
        call value(shx,dh(1),ios)
        IF(ios/=0) THEN
          WRITE(*,*)  'ERROR setting external field x: ',shx
          ierr = PARSE_ERROR
        END IF
        CALL value(shy,dh(2),ios)
        IF(ios/=0) THEN
          WRITE(*,*)  'ERROR setting external field y: ',shy
          ierr = PARSE_ERROR
        END IF
        CALL value(shz,dh(3),ios)
        IF(ios/=0) THEN
          WRITE (*,*)  'ERROR setting external field z: ',shz
          ierr = PARSE_ERROR
        END IF

        IF(ierr .NE. PARSE_ERROR) THEN
          hz(:)=dh(:)
          hznorm=SQRT(hz(1)**2 + hz(2)**2 + hz(3)**2)
          IF(NONZERO(hznorm)) THEN
            hz(1)=hz(1)/hznorm
            hz(2)=hz(2)/hznorm
            hz(3)=hz(3)/hznorm

            ierr = PARSE_SUCCESS
          ELSE
            WRITE(*,*) "Error: |H| = 0!"
            ierr = PARSE_ERROR
          END IF
        END IF

      CASE('strength')
        CALL value(shx,dh(1),ios)
        IF(ios == 0) THEN
          if (lowercase(shy).eq.'mt') dh(1)=dh(1)/1000.
          if (lowercase(shy).eq.'mut') dh(1)=dh(1)/1.e6
          extapp=dh(1)

          ierr = PARSE_SUCCESS
        ELSE
          WRITE(*,*) "Error: Invalid field strength!"
          ierr = PARSE_ERROR
        END IF

      CASE DEFAULT
        WRITE(*,*) "Error: unknown command"
        ierr = PARSE_ERROR
    END SELECT
  END SUBROUTINE setfield


  !---------------------------------------------------------------
  !     subroutine setuniform(args(3),args(4),sd,args(6))
  !---------------------------------------------------------------

  !> Set a uniform magnetization
  !>
  !> @param[in]  smx  The x-component of the magnetization.
  !> @param[in]  smy  The y-component of the magnetization.
  !> @param[in]  smz  The z-component of the magnetization.
  !> @param[in]  bstr The block to set the magnetization for. If 0, all
  !>                  blocks are set.
  !> @param[out] ierr The result of the parse and set.
  SUBROUTINE setuniform(smx, smy, smz, ierr, sd)
    USE strings
    IMPLICIT NONE

    CHARACTER(LEN=100) :: smx, smy, smz
    INTEGER, OPTIONAL, INTENT(IN) :: sd
    INTEGER :: ios, i,k, block
    REAL(KIND=DP) :: dm(3), dmnorm

    INTEGER :: ierr

    ierr = NO_PARSE

    CALL value(smx,dm(1),ios)
    IF(ios/=0) THEN
      WRITE(*,*)  'ERROR setting magnetization x: ',smx
      ierr = PARSE_ERROR
    END IF
    CALL value(smy,dm(2),ios)
    IF(ios/=0) THEN
      WRITE(*,*)  'ERROR setting magnetization y: ',smy
      ierr = PARSE_ERROR
    END IF
    CALL value(smz,dm(3),ios)
    IF(ios/=0) THEN
      WRITE(*,*)  'ERROR setting magnetization z: ',smz
      ierr = PARSE_ERROR
    END IF

    IF(PRESENT(sd)) THEN

    IF(ierr .NE. PARSE_ERROR) THEN
      dmnorm=SQRT(dm(1)**2 + dm(2)**2 + dm(3)**2)
      IF(NONZERO(dmnorm)) THEN
        dm(1)=dm(1)/dmnorm
        dm(2)=dm(2)/dmnorm
        dm(3)=dm(3)/dmnorm
       endif
        DO i=1,NTRI
          ! set magnetization if in the subdomain
          if(TetSubDomains(i).eq.sd) then
          do k=1,4 !nodes per tet
            m(Til(i,k),:) = dm(:)
          enddo
          endif
        END DO

        ierr = PARSE_SUCCESS
      ELSE
        ierr = PARSE_ERROR
      END IF
 


    ELSE


    IF(ierr .NE. PARSE_ERROR) THEN
      dmnorm=SQRT(dm(1)**2 + dm(2)**2 + dm(3)**2)
      IF(NONZERO(dmnorm)) THEN
        dm(1)=dm(1)/dmnorm
        dm(2)=dm(2)/dmnorm
        dm(3)=dm(3)/dmnorm

        DO i=1,NNODE
          ! set magnetization if in the right block
          ! block=0 means all magnetizations
         m(i,:)=dm(:)
        END DO

        ierr = PARSE_SUCCESS
      ELSE
        ierr = PARSE_ERROR
      END IF
    END IF

    ENDIF    

  END SUBROUTINE setuniform


  !> TIMESTAMP prints the current YMDHMS date as a time stamp.
  !>
  !>  Example:
  !>
  !>    31 May 2001   9:45:54.872 AM
  !>
  !>  Licensing:
  !>
  !>    This code is distributed under the GNU LGPL license.
  !>
  !>  Modified:
  !>
  !>    18 May 2013
  !>
  !>  Author:
  !>
  !>    John Burkardt
  !>
  !>  Parameters:
  !>
  !>    None
  !>
  !> @param[in] FileID The file unit to write to.
  !> @todo REPLACE ME
  subroutine tstamp (FileID )

    implicit none

    character ( len = 8 ) ampm
    integer ( kind = 4 ) d
    integer ( kind = 4 ) h
    integer ( kind = 4 ) m
    integer ( kind = 4 ) mm
    character ( len = 9 ), parameter, dimension(12) :: month = (/ &
      'January  ', 'February ', 'March    ', 'April    ', &
      'May      ', 'June     ', 'July     ', 'August   ', &
      'September', 'October  ', 'November ', 'December ' /)
    integer ( kind = 4 ) n
    integer ( kind = 4 ) s
    integer ( kind = 4 ) values(8)
    integer ( kind = 4 ) y
    integer ( kind = 4 ) FileID

    call date_and_time ( values = values )

    y = values(1)
    m = values(2)
    d = values(3)
    h = values(5)
    n = values(6)
    s = values(7)
    mm = values(8)

    if ( h < 12 ) then
      ampm = 'AM'
    else if ( h == 12 ) then
      if ( n == 0 .and. s == 0 ) then
        ampm = 'Noon'
      else
        ampm = 'PM'
      end if
    else
      h = h - 12
      if ( h < 12 ) then
        ampm = 'PM'
      else if ( h == 12 ) then
        if ( n == 0 .and. s == 0 ) then
          ampm = 'Midnight'
        else
          ampm = 'AM'
        end if
      end if
    end if

    write ( FileID, '(i2,1x,a,1x,i4,2x,i2,a1,i2.2,a1,i2.2,a1,i3.3,1x,a)' ) &
      d, trim ( month(m) ), y, h, ':', n, ':', s, '.', mm, trim ( ampm )

    return
  end subroutine tstamp


  !> Parse 'sd = XXX' from the list of `args` passed in.
  !> @param[in]  args A list of arguments.
  !> @param[out] ierr The result of the parse.
  !> @returns The value `XXX` of the parsed subdomain, if a subdomain was
  !>          found.
  INTEGER FUNCTION ParseSubdomain(args, ierr)
    USE strings
    IMPLICIT NONE

    CHARACTER(LEN=*), INTENT(IN) :: args(:)
    INTEGER, INTENT(OUT) :: ierr

    INTEGER :: i

    ParseSubdomain = HUGE(ParseSubdomain)

    IF(SIZE(args) .NE. 3) THEN
      ierr = PARSE_ERROR
      RETURN
    END IF

    ! Ensure format ... sd = n
    IF( &
      lowercase(TRIM(args(1))) .EQ. "sd" &
      .AND. &
      TRIM(args(2)) .EQ. "=" &
    ) THEN

      ! Set sd
      CALL value(args(3), ParseSubdomain, ierr)
      IF(ierr .NE. 0) THEN
        WRITE(*,*) "Error parsing subdomain id: ", TRIM(args(3))
        ierr = PARSE_ERROR
        RETURN
      END IF

      ! Change subdomain to subdomain index
      DO i=1,SIZE(SubDomainIds)
        IF(ParseSubdomain .EQ. SubDomainIds(i)) THEN
          ParseSubdomain = i
          ierr = PARSE_SUCCESS
          RETURN
        END IF

        IF(i .EQ. SIZE(SubDomainIds)) THEN
          WRITE(*,*) "Error: Unknown subdomain: ", ParseSubdomain
          ierr = PARSE_ERROR
          RETURN
        END IF
      END DO
    ELSE
      WRITE(*,*) "Expected 'sd = ...' specifier."
      ierr = PARSE_ERROR
    END IF

    ierr = PARSE_ERROR
  END FUNCTION ParseSubdomain

END MODULE Command_Parser
