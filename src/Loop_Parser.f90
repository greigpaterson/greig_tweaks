!> The Loop_parser module contains the routines used to parse an MScript
!> program.
MODULE Loop_Parser
  USE strings
  USE Utils

  IMPLICIT NONE
  SAVE

  CHARACTER(LEN=200), ALLOCATABLE :: LineIn(:,:), LineOutStack(:,:), DefVar(:)

  INTEGER, ALLOCATABLE :: NArgsIn(:),NArgsOut(:)
  REAL(KIND=DP), ALLOCATABLE :: DefVal(:)
  INTEGER :: FileLength, LoopCnt, AllLineCnt, NDefVar

CONTAINS

  SUBROUTINE InitializeLoopParser()
    CALL DestroyLoopParser()
  END SUBROUTINE InitializeLoopParser

  SUBROUTINE DestroyLoopParser()
    IF(ALLOCATED(LineIn))       DEALLOCATE(LineIn)
    IF(ALLOCATED(LineOutStack)) DEALLOCATE(LineOutStack)
    IF(ALLOCATED(DefVar))       DEALLOCATE(DefVar)
    IF(ALLOCATED(NArgsIn))      DEALLOCATE(NArgsIn)
    IF(ALLOCATED(NArgsOut))     DEALLOCATE(NArgsOut)
    IF(ALLOCATED(DefVal))       DEALLOCATE(DefVal)
  END SUBROUTINE DestroyLoopParser

  !---------------------------------------------------------------
  !   Subroutine ReadLoopFile
  !---------------------------------------------------------------

  Subroutine  ReadLoopFile(filename)
    IMPLICIT NONE

    CHARACTER (LEN=200) ::  filename , args(20),lstr,varstr
    CHARACTER (LEN=200) :: line

    INTEGER ios, nargs, i, j, k
    INTEGER OutCnt, LineNo, lp,MaxDef

    REAL(KIND=DP)  :: a,b,s, dbltmp, varval

    INTEGER, ALLOCATABLE  ::   LoopStart(:), LoopEnd(:)

    INTEGER :: scriptunit

    OPEN(NEWUNIT=scriptunit,file=filename,status='unknown')

    !      Read original scriptfile and count number of script lines ->FileLength
    LineNo=0
    ios = 1
    DO WHILE(ios.GE.0)
      call Readline(scriptunit,line,ios)
      LineNo=LineNo+1
    END DO
    FileLength=LineNo

    Rewind(scriptunit)
    !  Allocate more than enough space to parse all loops
    If(ALLOCATED(LineIn))    DEALLOCATE(LineIn)
    IF(ALLOCATED(NArgsIn))   DEALLOCATE(NArgsIn)
    IF(ALLOCATED(LoopStart)) DEALLOCATE(LoopStart)
    IF(ALLOCATED(LoopEnd))   DEALLOCATE(LoopEnd)

    ALLOCATE(LineIn(FileLength+10,20))
    ALLOCATE(NArgsIn(FileLength+10))
    ALLOCATE(LoopStart(FileLength))
    ALLOCATE(LoopEnd(FileLength))


    MaxDef=0
    LineNo=0
    LoopCnt=0
    AllLineCnt=0 ! Number of lines after unravelling all loops
    lp=0         ! lp is the number of current inner loop lines
    ios=1
    DO WHILE(ios.GE.0)
      call Readline(scriptunit,line,ios)
      if(ios.LT.0) line="end" ! ios=-1 at end of file and 'line' is garbage
      LineNo=LineNo+1
      if(lp > 0) then
        AllLineCnt=AllLineCnt+lp
      else
        AllLineCnt=AllLineCnt+1
      endif
      call Parse(line, ' ,:', args, nargs)
      NArgsIn(LineNo)=nargs   ! Number of all input arguments in LineNo
      Do i =1,nargs
        LineIn(LineNo,i)=args(i) ! LineIn =  Array of all input arguments in LineNo
      End Do
      lstr=lowercase(args(1))
      Select case(lstr)
      case('define')
        MaxDef=MaxDef+1  !contains maximal number of defined variables
      case('loop')
        if(lp>0) then
          write(*,*) 'ERROR in scriptfile :' ,filename
          write(*,*) 'Nested loop command @:' ,LineNo
          write(*,*) 'Line :' ,line
          STOP
        end if
        call value(args(3),a,ios)
        call value(args(4),b,ios)
        if(nargs>4) THEN
          call value(args(5),s,ios)
        else
          s=1.0
        endif
        lp=int(abs((b-a)/s)+1)  ! number of loop repetitions
        LoopCnt=LoopCnt+1
        LoopStart(LoopCnt)=LineNo
      case('endloop')
        if(lp.eq.0) then
          write(*,*) 'ERROR in scriptfile :' ,filename
          write(*,*) 'No loop to end  @:' ,LineNo
          write(*,*) 'Line :' ,line
          STOP
        end if
        lp=0
        LoopEnd(LoopCnt)=LineNo
      end select
    END DO

    ! Allocate more than enough space for the unravelled loops
    If(ALLOCATED(LineOutStack)) DEALLOCATE(LineOutStack)
    IF(ALLOCATED(NArgsOut))     DEALLOCATE(NArgsOut)
    ALLOCATE(LineOutStack(AllLineCnt+10,20))
    ALLOCATE(NArgsOut(AllLineCnt+10))



    LineNo=1
    OutCnt=1
    Do i=1,LoopCnt
      ! If not in a loop just copy commands from In -> Out
      Do j=LineNo,LoopStart(i)-1
        LineOutStack(OutCnt,:)=LineIn(j,:)
        NArgsOut(OutCnt)=NArgsIn(j)
        OutCnt=OutCnt+1
      End Do
      call value(LineIn(LoopStart(i), 3),a,ios)
      call value(LineIn(LoopStart(i), 4),b,ios)
      if(NArgsIn(LoopStart(i))>4) THEN
        call value(LineIn(LoopStart(i), 5),s,ios)
      else
        s=1.0
      endif
      lp=int(abs((b-a)/s)+1)    ! number of i-th loop repetitions
      varstr=LineIn(LoopStart(i), 2)  ! String with the loop variable
      Do j=1,lp   ! Unravel  loop i from In -> Out
        varval=a+(j-1)*s
        Do k= LoopStart(i)+1,LoopEnd(i)-1
          ! Replace varstr by right value
          call ReplaceVar( k,OutCnt, varstr,varval)
          ! and   LineIn(k,:)  --->  LineOutStack(OutCnt,:)
          NArgsOut(OutCnt)=NArgsIn(k)
          OutCnt=OutCnt+1
        End Do
      End DO
      LineNo=LoopEnd(i)+1
    End Do
    Do j=LineNo,FileLength
       LineOutStack(OutCnt,:)=LineIn(j,:)
       NArgsOut(OutCnt)=NArgsIn(j)
       OutCnt=OutCnt+1
    End Do
    !       Now replace defined variables    Define xx 23
    !                                        AddTo  xx 5
    !                                        Undefine xx
    !
    ! Redefine LineIn for easy second parsing
    DEALLOCATE(LineIn,NArgsIn)
    ALLOCATE(LineIn(AllLineCnt+10,20),NArgsIn(AllLineCnt+10))
    ALLOCATE(DefVar(MaxDef+10),DefVal(MaxDef+10))
    LineIn(:,:)=LineOutStack(:,:)
    NArgsIn(:)=NArgsOut(:)
    AllLineCnt=OutCnt-1
    NDefVar=0
    Do i=1,AllLineCnt
      lstr=lowercase(LineIn(i,1))
      Select case(lstr)
      case('define')
        if(NArgsIn(i)>2) then
          k=0
          Do j=1,NDefVar
            ! check if variable is already defined
            if(LineIn(i,2).eq.DefVar(j)) k=j
          EndDo
          if(k.eq.0) then
            NDefVar=NDefVar+1 ! new variable
            DefVar(NDefVar)=LineIn(i,2)
            !WRITE(*,*) i, LineIn(i,3)
            call ReplaceVar(i, i, DefVar(NDefVar-1),DefVal(NDefVar-1))
            LineIn(i,3)=LineOutStack(i,3)
            !WRITE(*,*) LineOutStack(i,:)
            call value(LineIn(i,3),DefVal(NDefVar),ios)
          else
            call ReplaceVar(i, i, DefVar(k+1),DefVal(k+1))
            LineIn(i,3)=LineOutStack(i,3)
            !WRITE(*,*) LineOutStack(i,:)
            !WRITE(*,*) DefVar(k+1)
            !WRITE(*,*) DefVar(k+1)
            call value(LineIn(i,3),DefVal(k),ios) ! reassign variable
          endif
        endif
      case('addto')
        if(NArgsIn(i)>2) then
          !call value(LineIn(i,3), dbltmp,ios)
          Do j=1,NDefVar
            call ReplaceVar(i, i, DefVar(j),DefVal(j))
            LineIn(i,3)=LineOutStack(i,3)
            call value(LineIn(i,3), dbltmp,ios)
            if(LineIn(i,2).eq.DefVar(j)) DefVal(j)=DefVal(j)+dbltmp
          EndDo
        endif
      case('multby')
        if(NArgsIn(i)>2) then
          !call value(LineIn(i,3), dbltmp,ios)
          Do j=1,NDefVar
            call ReplaceVar(i, i, DefVar(j),DefVal(j))
            LineIn(i,3)=LineOutStack(i,3)
            call value(LineIn(i,3), dbltmp,ios)
            if(LineIn(i,2).eq.DefVar(j)) DefVal(j)=DefVal(j)*dbltmp
          EndDo
        endif
      case('undefine')
        if(NArgsIn(i)>1) then
          k=0
          Do j=1,NDefVar
            if(LineIn(i,2).eq.DefVar(j)) k=j
          EndDo
          if(k>0) then
            NDefVar=NDefVar-1
            if(k<NDefVar) then
              DefVar(k)=DefVar(NDefVar)
              DefVal(k)=DefVal(NDefVar)
            endif
          endif
        endif
      end select
      Do j=1,NDefVar
        call ReplaceVar(i,i, DefVar(j),DefVal(j))
        LineIn(i,:)=LineOutStack(i,:)
        NArgsIn(i)=NArgsOut(i)
      EndDo
    EndDo

    CLOSE(scriptunit)
  END Subroutine  ReadLoopFile


  !---------------------------------------------------------------
  !   Subroutine ReplaceVar
  !
  !---------------------------------------------------------------


  Subroutine ReplaceVar(k,OutCnt, varstr,varval)
    IMPLICIT NONE

    INTEGER k,OutCnt, varint,i
    CHARACTER (LEN=200) ::  varstr
    CHARACTER (LEN=20) ::   vi,vd,vs,vsi,vsd
    REAL(KIND=DP) ::   varval
    Logical :: IntQ
    varint= int(varval)
    if((varval-varint)<MachEps) then
      intQ=.TRUE.
    else
      intQ=.FALSE.
    endif

    call writenum(varint,vsi,'I8')
    call writenum(varval,vsd,'E12.6')

    vi='#'//varstr(:len_trim(varstr))
    vd='%'//varstr(:len_trim(varstr))
    vs='$'//varstr(:len_trim(varstr))//'$'
    LineOutStack(OutCnt,:)=LineIn(k,:)

    Do i=2,  NArgsIn(k)
      call repsubstr(LineOutStack(OutCnt,i),vi,vsi)
      call repsubstr(LineOutStack(OutCnt,i),vd,vsd)
      if(intQ) then
        call repsubstr(LineOutStack(OutCnt,i),vs,vsi)
      else
        call repsubstr(LineOutStack(OutCnt,i),vd,vsd)
      end if
    End do

  End  subroutine ReplaceVar



  !**********************************************************************

  subroutine repsubstr(str,substr,repstr)
    implicit none
    ! Deletes first occurrence of substring 'substr' from string 'str' and
    ! replaces it by repstr. Trailing spaces or blanks are
    ! not considered part of 'substr'.

    character(len=*):: str,substr,repstr
    integer :: ipos, lensubstr

    lensubstr=len_trim(substr)
    ipos=index(str,substr(:lensubstr))

    Do while (ipos>0)
      if(ipos == 1) then
        str=repstr(:len_trim(repstr))//str(lensubstr+1:)
      else
        str=str(:ipos-1)//repstr(:len_trim(repstr))//str(ipos+lensubstr:)
      end if
      ipos=index(str,substr(:lensubstr))
    end do
    return

  end subroutine repsubstr

End MODULE Loop_Parser
