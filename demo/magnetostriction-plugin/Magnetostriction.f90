MODULE Magnetostriction

    USE, INTRINSIC :: ISO_C_BINDING, ONLY: &
        C_DOUBLE, C_INT, C_SIZE_T, C_PTR, C_LOC, C_F_POINTER

    USE MERRILL
    USE Command_Parser
    USE Variable_Setter
    USE Utils
    USE slatec_dsiccg
    USE slatec_dsics
    USE slatec_xermsg
    USE slatec_dsllti
    USE slatec_dsmtv
    USE slatec_dcg

    IMPLICIT NONE


    CHARACTER(LEN=*), PARAMETER :: &
        MagnetostrictionCalculatorName = "MagnetostrictionCalculator"


    INTERFACE

        !
        ! functions defined in magnetostriction.cpp
        !

        ! void magnetostriction_build_mechanical_rhs(double* m, double* rhs);
        SUBROUTINE magnetostriction_build_mechanical_rhs(m, rhs) BIND(C)
            USE, INTRINSIC :: ISO_C_BINDING, ONLY: C_DOUBLE
            REAL(C_DOUBLE), INTENT(IN) :: m(*)
            REAL(C_DOUBLE), INTENT(OUT) :: rhs(*)
        END SUBROUTINE magnetostriction_build_mechanical_rhs

        ! void magnetostriction_build_magnetic_rhs(double* m, double* rhs);
        SUBROUTINE magnetostriction_build_magnetic_rhs(m, u0, rhs) BIND(C)
            USE, INTRINSIC :: ISO_C_BINDING, ONLY: C_DOUBLE
            REAL(C_DOUBLE), INTENT(IN) :: m(*)
            REAL(C_DOUBLE), INTENT(IN) :: u0(*)
            REAL(C_DOUBLE), INTENT(OUT) :: rhs(*)
        END SUBROUTINE magnetostriction_build_magnetic_rhs

        ! void magnetostriction_build_energy(double* m, double* rhs);
        SUBROUTINE magnetostriction_build_energy(m, u0, energy) BIND(C)
            USE, INTRINSIC :: ISO_C_BINDING, ONLY: C_DOUBLE
            REAL(C_DOUBLE), INTENT(IN) :: m(*)
            REAL(C_DOUBLE), INTENT(IN) :: u0(*)
            REAL(C_DOUBLE), INTENT(OUT) :: energy
        END SUBROUTINE magnetostriction_build_energy

        !void magnetostriction_build(
        !    double ms,
        !    double c11, double c12, double c44,
        !    double b1, double b2,
        !    double scale,
        !    int nnode, double* vcl, int ntri, int* til
        !);
        SUBROUTINE magnetostriction_build( &
            ms, c11, c12, c44, b1, b2, scale, &
            nnode, vcl, ntri, til &
        ) BIND(C)
            USE, INTRINSIC :: ISO_C_BINDING, ONLY: C_DOUBLE, C_INT
            REAL(C_DOUBLE), VALUE, INTENT(IN) :: &
                Ms, c11, c12, c44, b1, b2, scale
            INTEGER(C_INT), VALUE, INTENT(IN) :: nnode
            REAL(C_DOUBLE), INTENT(IN) :: vcl(*)
            INTEGER(C_INT), VALUE, INTENT(IN) :: ntri
            INTEGER(C_INT), INTENT(IN) :: til(*)
        END SUBROUTINE magnetostriction_build


        ! The returned value "value" is effectively the force per unit area
        ! on the surface. Returned values shouldn't worry about Ls scaling,
        ! although the position input will be in the scaled mesh units.
        !
        ! void traction(double[3] value, double[3] position, double[3] normal)
        SUBROUTINE traction_interface(value, position, normal)
            USE, INTRINSIC :: ISO_C_BINDING, ONLY: C_DOUBLE

            REAL(C_DOUBLE), INTENT(OUT) :: value(3)
            REAL(C_DOUBLE), INTENT(IN)  :: position(3), normal(3)
        END SUBROUTINE traction_interface

    END INTERFACE


    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !! Parameters                                                            !!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! Elastic and Magnetic fields
    REAL(KIND=DP), ALLOCATABLE :: umstr(:,:)

    ! Elastic and magnetoelastic constants
    REAL(KIND=DP) :: c11, c12, c44, b1, b2

    ! Test if magnetostriction is being calculated
    LOGICAL :: calculating_magnetostriction

    ! Dirty bit. If set, magnetostriction matrices are rebuilt
    LOGICAL :: magnetostriction_dirty


    PROCEDURE(traction_interface), POINTER :: traction_fnptr => null()


    REAL(KIND=DP), ALLOCATABLE :: MechanicalStiffnessMatrix(:)
    INTEGER, ALLOCATABLE :: CNR_MeSM(:), RNR_MeSM(:)
    REAL(KIND=DP), ALLOCATABLE :: RWORK_MeSM(:)
    INTEGER, ALLOCATABLE :: IWORK_MeSM(:)
    REAL(KIND=DP) :: Scale_MeSM

    REAL(KIND=DP), ALLOCATABLE :: MagneticStiffnessMatrix(:)
    INTEGER, ALLOCATABLE :: CNR_MaSM(:), RNR_MaSM(:)
    REAL(KIND=DP), ALLOCATABLE :: RWORK_MaSM(:)
    INTEGER, ALLOCATABLE :: IWORK_MaSM(:)

    REAL(KIND=DP), ALLOCATABLE :: rhs(:)


CONTAINS

    FUNCTION MagnetostrictionCalculator()
        TYPE(EnergyCalculator) :: MagnetostrictionCalculator

        MagnetostrictionCalculator%Initialize &
            => InitializeMagnetostrictionCalculator
        MagnetostrictionCalculator%Build &
            => BuildMagnetostrictionCalculator
        MagnetostrictionCalculator%Destroy &
            => DestroyMagnetostrictionCalculator
        MagnetostrictionCalculator%Run &
            => RunMagnetostrictionCalculator
    END FUNCTION MagnetostrictionCalculator


    SUBROUTINE InitializeMagnetostriction()
        TYPE(EnergyCalculator) :: mc

        ! Initialize values
        c11 = 0.0
        c12 = 0.0
        c44 = 0.0
        b1  = 0.0
        b2  = 0.0

        magnetostriction_dirty = .TRUE.
        calculating_magnetostriction = .FALSE.
        traction_fnptr => zero_traction

        ! Add calculator
        mc = MagnetostrictionCalculator()
        CALL AddEnergyCalculator( &
            MagnetostrictionCalculatorName, mc &
        )

        ! Add Magnetostriction commands
        CALL AddCommandParser("magnetostriction", ParseMagnetostriction)
        CALL AddCommandParser("writedisplacement", ParseWriteDisplacement)


        ! Add Magnetostriction variables
        CALL AddRealVariableSetter("c11", c11)
        CALL AddRealVariableSetter("c12", c12)
        CALL AddRealVariableSetter("c44", c44)
        CALL AddRealVariableSetter("b1",  b1)
        CALL AddRealVariableSetter("b2",  b2)
    END SUBROUTINE InitializeMagnetostriction



    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! EnergyCalculator subroutines                                         !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    SUBROUTINE InitializeMagnetostrictionCalculator(self)
        IMPLICIT NONE

        CLASS(EnergyCalculator), INTENT(INOUT) :: self


        CALL self%Destroy()

        self%energy = 0
        magnetostriction_dirty = .TRUE.
    END SUBROUTINE InitializeMagnetostrictionCalculator

    SUBROUTINE BuildMagnetostrictionCalculator(self)
        IMPLICIT NONE

        CLASS(EnergyCalculator), INTENT(INOUT) :: self

        INTEGER, PARAMETER :: LOCRB=1, LOCIB=11
        INTEGER :: MeSM_LOCJEL, MeSM_LOCIEL, MeSM_LOCIW
        INTEGER :: MaSM_LOCJEL, MaSM_LOCIEL, MaSM_LOCIW
        INTEGER :: MeSM_LOCEL, MeSM_LOCDIN, MeSM_LOCR, MeSM_LOCZ, MeSM_LOCP, &
            MeSM_LOCDZ, MeSM_LOCW
        INTEGER :: MaSM_LOCEL, MaSM_LOCDIN, MaSM_LOCR, MaSM_LOCZ, MaSM_LOCP, &
            MaSM_LOCDZ, MaSM_LOCW
        INTEGER :: IERR
        CHARACTER(len=8) :: XERN1
        INTEGER :: nze_mesm, nze_masm

        CALL self%Destroy()

        ALLOCATE(self%h(NNODE, 3))
        ALLOCATE(umstr(NNODE, 3))
        ALLOCATE(rhs(3*NNODE))

        self%h = 0
        self%Energy = 0
        umstr = 0
        rhs = 0

        CALL magnetostriction_build( &
            Ms(1), c11, c12, c44, b1, b2, 1/SQRT(Ls), &
            NNODE, VCL, NTRI, TIL &
        )
        magnetostriction_dirty = .FALSE.



        ! DSICS doesn't work for zero matrix.
        IF(.NOT. NONZERO(NORM2(MechanicalStiffnessMatrix))) THEN
            RETURN
        END IF
        IF(.NOT. NONZERO(NORM2(MagneticStiffnessMatrix))) THEN
            RETURN
        END IF

        ! Compute offsets for RWORK and IWORK for MechanicalStiffnessMatrix and
        ! MagneticStiffnessMatrix for use with DSICS. This is lifted straight
        ! from DSICCS with values changed to suit the dummy arguments.

        nze_mesm = SIZE(MechanicalStiffnessMatrix)
        nze_masm = SIZE(MagneticStiffnessMatrix)

        MeSM_LOCJEL = LOCIB
        MaSM_LOCJEL = LOCIB
        MeSM_LOCIEL = MeSM_LOCJEL + nze_mesm
        MaSM_LOCIEL = MaSM_LOCJEL + nze_masm
        MeSM_LOCIW  = MeSM_LOCIEL + 3*NNODE + 1
        MaSM_LOCIW  = MaSM_LOCIEL + 3*NNODE + 1

        MeSM_LOCEL = LOCRB
        MaSM_LOCEL = LOCRB
        MeSM_LOCDIN = MeSM_LOCEL + nze_mesm
        MaSM_LOCDIN = MaSM_LOCEL + nze_masm
        MeSM_LOCR = MeSM_LOCDIN + 3*NNODE
        MaSM_LOCR = MaSM_LOCDIN + 3*NNODE
        MeSM_LOCZ = MeSM_LOCR + 3*NNODE
        MaSM_LOCZ = MaSM_LOCR + 3*NNODE
        MeSM_LOCP = MeSM_LOCZ + 3*NNODE
        MaSM_LOCP = MaSM_LOCZ + 3*NNODE
        MeSM_LOCDZ = MeSM_LOCP + 3*NNODE
        MaSM_LOCDZ = MaSM_LOCP + 3*NNODE
        MeSM_LOCW = MeSM_LOCDZ + 3*NNODE
        MaSM_LOCW = MaSM_LOCDZ + 3*NNODE

        ! IWORK(1) = NL
        IWORK_MeSM(1) = nze_mesm
        IWORK_MaSM(1) = nze_masm
        ! IWORK(2) = LOCJEL
        IWORK_MeSM(2) = MeSM_LOCJEL
        IWORK_MaSM(2) = MaSM_LOCJEL
        ! IWORK(3) = LOCIEL
        IWORK_MeSM(3) = MeSM_LOCIEL
        IWORK_MaSM(3) = MaSM_LOCIEL
        ! IWORK(4) = LOCEL
        IWORK_MeSM(4) = MeSM_LOCEL
        IWORK_MaSM(4) = MaSM_LOCEL
        ! IWORK(5) = LOCDIN
        IWORK_MeSM(5) = MeSM_LOCDIN
        IWORK_MaSM(5) = MaSM_LOCDIN
        ! IWORK(9) = LOCIW
        IWORK_MeSM(9) = MeSM_LOCIW
        IWORK_MaSM(9) = MaSM_LOCIW
        ! IWORK(10) = LOCW
        IWORK_MeSM(10) = MeSM_LOCW
        IWORK_MaSM(10) = MaSM_LOCW


        ! Generate RWORK_MeSM, IWORK_MeSM for MechanicalStiffnessMatrix
        CALL DSICS( &
          3*NNODE, nze_mesm, RNR_MeSM, CNR_MeSM, MechanicalStiffnessMatrix, 0, &
          (nze_mesm + 3*NNODE)/2, IWORK_MeSM(MeSM_LOCIEL), &
          IWORK_MeSM(MeSM_LOCJEL), &
          RWORK_MeSM(MeSM_LOCEL), RWORK_MeSM(MeSM_LOCDIN), &
          RWORK_MeSM(MeSM_LOCR), IERR &
        )

        IF( IERR.NE.0 ) THEN
           WRITE (XERN1, '(I8)') IERR
           CALL XERMSG ('SLATEC', 'DSICCG', &
              'IC factorization broke down on step ' // XERN1 // &
              '.  Diagonal was set to unity and factorization proceeded.', &
              1, 1)
           IERR = 7
        ENDIF

        ! Generate RWORK_MeSM, IWORK_MeSM for MechanicalStiffnessMatrix
        CALL DSICS( &
          3*NNODE, nze_mesm, RNR_MaSM, CNR_MaSM, MechanicalStiffnessMatrix, 0, &
          (nze_mesm + 3*NNODE)/2, IWORK_MaSM(MaSM_LOCIEL), &
          IWORK_MaSM(MaSM_LOCJEL), &
          RWORK_MaSM(MaSM_LOCEL), RWORK_MaSM(MaSM_LOCDIN), &
          RWORK_MaSM(MaSM_LOCR), IERR &
        )

        IF( IERR.NE.0 ) THEN
           WRITE (XERN1, '(I8)') IERR
           CALL XERMSG ('SLATEC', 'DSICCG', &
              'IC factorization broke down on step ' // XERN1 // &
              '.  Diagonal was set to unity and factorization proceeded.', &
              1, 1)
           IERR = 7
        ENDIF

        WRITE(*,*) "Building Magnetostriction"
    END SUBROUTINE BuildMagnetostrictionCalculator

    SUBROUTINE DestroyMagnetostrictionCalculator(self)
        IMPLICIT NONE

        CLASS(EnergyCalculator), INTENT(INOUT) :: self

        IF(ALLOCATED(self%h)) DEALLOCATE(self%h)
        IF(ALLOCATED(umstr))  DEALLOCATE(umstr)

        IF(ALLOCATED(MechanicalStiffnessMatrix)) THEN
            DEALLOCATE(MechanicalStiffnessMatrix)
        END IF
        IF(ALLOCATED(CNR_MeSM)) DEALLOCATE(CNR_MeSM)
        IF(ALLOCATED(RNR_MeSM)) DEALLOCATE(RNR_MeSM)
        IF(ALLOCATED(RWORK_MeSM)) DEALLOCATE(RWORK_MeSM)
        IF(ALLOCATED(IWORK_MeSM)) DEALLOCATE(IWORK_MeSM)

        IF(ALLOCATED(MagneticStiffnessMatrix)) THEN
            DEALLOCATE(MagneticStiffnessMatrix)
        END IF
        IF(ALLOCATED(CNR_MaSM)) DEALLOCATE(CNR_MaSM)
        IF(ALLOCATED(RNR_MaSM)) DEALLOCATE(RNR_MaSM)
        IF(ALLOCATED(RWORK_MaSM)) DEALLOCATE(RWORK_MaSM)
        IF(ALLOCATED(IWORK_MaSM)) DEALLOCATE(IWORK_MaSM)

        IF(ALLOCATED(rhs)) DEALLOCATE(rhs)
    END SUBROUTINE DestroyMagnetostrictionCalculator

    SUBROUTINE RunMagnetostrictionCalculator(self)
        IMPLICIT NONE

        CLASS(EnergyCalculator), INTENT(INOUT) :: self

        INTEGER :: ISYM
        INTEGER :: ITOL
        REAL(KIND=DP) :: TOL
        INTEGER :: ITMAX
        INTEGER :: ITER
        REAL(KIND=DP) :: ERR
        INTEGER :: IERR
        INTEGER :: IUNIT

        INTEGER, PARAMETER :: LOCRB=1, LOCIB=11
        INTEGER :: MeSM_LOCEL, MeSM_LOCDIN, MeSM_LOCR, MeSM_LOCZ, MeSM_LOCP, &
            MeSM_LOCDZ
        INTEGER :: MaSM_LOCEL, MaSM_LOCDIN, MaSM_LOCR, MaSM_LOCZ, MaSM_LOCP, &
            MaSM_LOCDZ

        INTEGER :: nze_mesm, nze_masm

        REAL(KIND=DP) :: Scale_rhs

        IF ( calculating_magnetostriction .EQV. .FALSE. ) THEN
            self%h = 0
            self%Energy = 0
            RETURN
        END IF

        IF(magnetostriction_dirty .EQV. .TRUE.) THEN
            CALL self%Build()
        END IF

        nze_mesm = SIZE(MechanicalStiffnessMatrix)
        nze_masm = SIZE(MagneticStiffnessMatrix)

        ! Set up IWORK and RWORK values for DCG
        MeSM_LOCEL  = LOCRB
        MaSM_LOCEL  = LOCRB
        MeSM_LOCDIN = MeSM_LOCEL  + nze_mesm
        MaSM_LOCDIN = MaSM_LOCEL  + nze_masm
        MeSM_LOCR   = MeSM_LOCDIN + 3*NNODE
        MaSM_LOCR   = MaSM_LOCDIN + 3*NNODE
        MeSM_LOCZ   = MeSM_LOCR   + 3*NNODE
        MaSM_LOCZ   = MaSM_LOCR   + 3*NNODE
        MeSM_LOCP   = MeSM_LOCZ   + 3*NNODE
        MaSM_LOCP   = MaSM_LOCZ   + 3*NNODE
        MeSM_LOCDZ  = MeSM_LOCP   + 3*NNODE
        MaSM_LOCDZ  = MaSM_LOCP   + 3*NNODE

        ISYM=0
        ITOL=1
        TOL=FEMTolerance
        ITMAX=4000
        IUNIT=0

        CALL magnetostriction_build_mechanical_rhs(m, rhs);
        Scale_rhs = MAXVAL(ABS(rhs))
        rhs = rhs / Scale_rhs
        CALL DCG( &
          3*NNODE, rhs, umstr, nze_mesm, &
          RNR_MeSM, CNR_MeSM, MechanicalStiffnessMatrix, &
          ISYM, DSMTV, DSLLTI, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
          RWORK_MeSM(MeSM_LOCR),  RWORK_MeSM(MeSM_LOCZ), RWORK_MeSM(MeSM_LOCP), &
          RWORK_MeSM(MeSM_LOCDZ), RWORK_MeSM(1), IWORK_MeSM(1) &
        )
        umstr = umstr/Scale_MeSM*Scale_rhs

        IF (IERR/=0)  WRITE(*,*) 'GMRES error in u0:',IERR


        CALL magnetostriction_build_magnetic_rhs(m, umstr, self%h);
        self%h = -Ms(1) * self%h * SQRT(Ls)**3

        CALL magnetostriction_build_energy(m, umstr, self%Energy)
        self%Energy = self%Energy * SQRT(Ls)**3
    END SUBROUTINE RunMagnetostrictionCalculator



    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Magnetostriction Commands for BEMScript                                  !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! Command: magnetostriction [on|off]
    ! Enable or disable calculation of magnetostriction
    SUBROUTINE ParseMagnetostriction(args, ierr)
        USE strings
        IMPLICIT NONE

        CHARACTER(len=*), INTENT(IN) :: args(:)
        INTEGER, INTENT(OUT) :: ierr

        INTEGER :: nargs


        nargs = SIZE(args)

        ierr = PARSE_SUCCESS

        IF ( nargs .EQ. 1 ) THEN
            calculating_magnetostriction = .TRUE.
        ELSE IF ( nargs .EQ. 2 ) THEN
            IF( lowercase(TRIM(args(2))) .EQ. "off" ) THEN
                calculating_magnetostriction = .FALSE.
            ELSE
                calculating_magnetostriction = .TRUE.
            END IF
        ELSE
            WRITE(*,*) "Error setting magnetostriction: too many arguments!"
            ierr = PARSE_ERROR
        END IF

        IF ( calculating_magnetostriction .EQV. .TRUE. ) THEN
            WRITE(*,*) "Calculating magnetostriction."
        ELSE
            WRITE(*,*) "Not calculating magnetostriction."
        END IF
    END SUBROUTINE ParseMagnetostriction

    ! Command: WriteDisplacement filebase
    ! Writes the displacement field to filebase.dat and filebase.tec
    ! representing raw pointwise data and a full tecplot file.
    SUBROUTINE ParseWriteDisplacement(args, ierr)
        IMPLICIT NONE

        CHARACTER(len=*), INTENT(IN) :: args(:)
        INTEGER, INTENT(OUT) :: ierr

        INTEGER :: nargs

        nargs = SIZE(args)

        IF(nargs .NE. 2) THEN
            WRITE(*,*) "WriteDisplacement Error: Expected 2 arguments"
            ierr = PARSE_ERROR
        ELSE
            CALL WriteDisplacement(TRIM(args(2)) // ".dat")
            CALL WriteDisplacementTecplot(TRIM(args(2)) // ".tec")

            ierr = PARSE_SUCCESS
        END IF
    END SUBROUTINE ParseWriteDisplacement


    !---------------------------------------------------------------
    ! WriteDisplacement
    !---------------------------------------------------------------

    SUBROUTINE WriteDisplacement(filename)
        USE Material_Parameters
        IMPLICIT NONE

        CHARACTER(LEN=*), INTENT(IN) :: filename
        INTEGER :: i

        OPEN(UNIT=405, FILE=filename, STATUS='REPLACE')

        DO i=1,NNODE
            WRITE(405, *) umstr(i,:)
        END DO

        CLOSE(405)

    END SUBROUTINE WriteDisplacement


    !---------------------------------------------------------------
    ! WriteDisplacementTecplot
    !---------------------------------------------------------------
    SUBROUTINE WriteDisplacementTecplot(filename)
        USE Material_Parameters
        IMPLICIT NONE

        CHARACTER(LEN=*), INTENT(IN) :: filename
        INTEGER :: i

        OPEN(UNIT=405, FILE=filename, STATUS='REPLACE')

        WRITE(405, *) 'TITLE = "', TRIM(filename), '"'
        WRITE(405, *) 'VARIABLES = "X","Y","Z","Ux","Uy","Uz"'
        WRITE(405, *) 'ZONE T="0.000"  N=', NNODE, ',  E=', NTRI, &
            ', F=FEPOINT, ET=TETRAHEDRON'

        DO i=1,NNODE
            WRITE(405, *)                          &
                VCL(i,1), VCL(i,2), VCL(i,3),      &
                umstr(i,1), umstr(i,2), umstr(i,3)
        END DO

        DO i=1,NTRI
            WRITE(405,*) TIL(i,1), TIL(i,2), TIL(i,3), TIL(i,4)
        ENDDO

        CLOSE(405)

    END SUBROUTINE WriteDisplacementTecplot



    SUBROUTINE zero_traction(value, position, normal)
        REAL(C_DOUBLE), INTENT(OUT) :: value(3)
        REAL(C_DOUBLE), INTENT(IN)  :: position(3), normal(3)

        value = 0.0
    END SUBROUTINE zero_traction


    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !! Fortran to C interfaces                                               !!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


    SUBROUTINE set_mechanical_matrix(rows, nrows, cols, data, nze) &
    BIND(C, name="set_mechanical_matrix")
        IMPLICIT NONE
        INTEGER(C_SIZE_T), VALUE, INTENT(IN) :: nze
        INTEGER(C_SIZE_T), VALUE, INTENT(IN) :: nrows
        INTEGER(C_INT), INTENT(IN) :: rows(nrows+1), cols(nze)
        REAL(C_DOUBLE), INTENT(IN) :: data(nze)

        INTEGER :: i, j, ik, jk, ci, fi, cj, fj, dpos, jstart, jend
        INTEGER :: cstart, cend, clen

        CALL EnsureSize(MechanicalStiffnessMatrix, nze)
        CALL IEnsureSize(CNR_MeSM, nrows+1)
        CALL IEnsureSize(RNR_MeSM, nze)
        CALL EnsureSize(RWORK_MeSM, nze + 5*nrows)
        CALL IEnsureSize(IWORK_MeSM, nze + nrows + 11)

        ! Set the transpose of the input matrix.
        ! The matrix should be symmetric, so this should be fine.

        CNR_MeSM = rows + 1
        CNR_MeSM(nrows+1) = nze+1
        RNR_MeSM = cols+1
        MechanicalStiffnessMatrix = data

        ! Move diagonal entries to the start of each column
        DO i=1,nrows
            ! Find start, end, diagonal
            jstart = CNR_MeSM(i)
            jend   = CNR_MeSM(i+1)-1
            dpos   = 0
            DO j=jstart,jend
                IF(i .EQ. RNR_MeSM(j)) THEN
                    dpos = j
                    EXIT
                END IF
            END DO

            ! Cycle diagonal entry to the start
            RNR_MeSM(jstart:dpos) = CSHIFT(RNR_MeSM(jstart:dpos), -1)
            MechanicalStiffnessMatrix(jstart:dpos) = &
                CSHIFT(MechanicalStiffnessMatrix(jstart:dpos), -1)
        END DO

        Scale_MeSM = MAXVAL(ABS(MechanicalStiffnessMatrix))
        IF(.NOT. NONZERO(Scale_MeSM)) Scale_MeSM = 1
        Scale_MeSM = 1
        MechanicalStiffnessMatrix = MechanicalStiffnessMatrix / Scale_MeSM
    CONTAINS
        SUBROUTINE EnsureSize(target, len)
            REAL(KIND=DP), ALLOCATABLE, INTENT(INOUT) :: target(:)
            INTEGER(C_SIZE_T), INTENT(IN) :: len

            IF(ALLOCATED(target)) THEN
                IF(SIZE(target) .NE. len) THEN
                    DEALLOCATE(target)
                    ALLOCATE(target(len))
                END IF
            ELSE
                ALLOCATE(target(len))
            END IF
        END SUBROUTINE
        SUBROUTINE IEnsureSize(target, len)
            INTEGER, ALLOCATABLE, INTENT(INOUT) :: target(:)
            INTEGER(C_SIZE_T), INTENT(IN) :: len

            IF(ALLOCATED(target)) THEN
                IF(SIZE(target) .NE. len) THEN
                    DEALLOCATE(target)
                    ALLOCATE(target(len))
                END IF
            ELSE
                ALLOCATE(target(len))
            END IF
        END SUBROUTINE
    END SUBROUTINE set_mechanical_matrix


    SUBROUTINE set_magnetic_matrix(rows, nrows, cols, data, nze) &
    BIND(C, name="set_magnetic_matrix")
        IMPLICIT NONE
        INTEGER(C_SIZE_T), VALUE, INTENT(IN) :: nze
        INTEGER(C_SIZE_T), VALUE, INTENT(IN) :: nrows
        INTEGER(C_INT), INTENT(IN) :: rows(nrows+1), cols(nze)
        REAL(C_DOUBLE), INTENT(IN) :: data(nze)

        INTEGER :: i, j, dpos, jstart, jend

        CALL EnsureSize(MagneticStiffnessMatrix, nze)
        CALL IEnsureSize(CNR_MaSM, nrows+1)
        CALL IEnsureSize(RNR_MaSM, nze)
        CALL EnsureSize(RWORK_MaSM, nze + 5*nrows)
        CALL IEnsureSize(IWORK_MaSM, nze + nrows + 11)

        ! Set the transpose of the input matrix.
        ! The matrix should be symmetric, so this should be fine.

        ! Copy data, cols and rows, but put diagonal entries at start
        CNR_MaSM = rows + 1
        CNR_MaSM(nrows+1) = nze+1
        RNR_MaSM = cols+1
        MagneticStiffnessMatrix = data
        DO i=1,nrows
            ! Find start, end, diagonal
            jstart = CNR_MaSM(i)
            jend   = CNR_MaSM(i+1)-1
            DO j=jstart,jend
                IF(i .EQ. RNR_MaSM(j)) THEN
                    dpos = j
                    EXIT
                END IF
            END DO

            ! Cycle diagonal entry to the start
            RNR_MaSM(jstart:dpos) = CSHIFT(RNR_MaSM(jstart:dpos), -1)
            MagneticStiffnessMatrix(jstart:dpos) = &
                CSHIFT(MagneticStiffnessMatrix(jstart:dpos), -1)
        END DO
    CONTAINS
        SUBROUTINE EnsureSize(target, len)
            REAL(KIND=DP), ALLOCATABLE, INTENT(INOUT) :: target(:)
            INTEGER(C_SIZE_T), INTENT(IN) :: len

            IF(ALLOCATED(target)) THEN
                IF(SIZE(target) .NE. len) THEN
                    DEALLOCATE(target)
                    ALLOCATE(target(len))
                END IF
            ELSE
                ALLOCATE(target(len))
            END IF
        END SUBROUTINE
        SUBROUTINE IEnsureSize(target, len)
            INTEGER, ALLOCATABLE, INTENT(INOUT) :: target(:)
            INTEGER(C_SIZE_T), INTENT(IN) :: len

            IF(ALLOCATED(target)) THEN
                IF(SIZE(target) .NE. len) THEN
                    DEALLOCATE(target)
                    ALLOCATE(target(len))
                END IF
            ELSE
                ALLOCATE(target(len))
            END IF
        END SUBROUTINE
    END SUBROUTINE set_magnetic_matrix


    ! Wrapper for calling traction_fnptr in C

    ! void traction(double[3] value, double[3] position, double[3] normal)
    SUBROUTINE traction(value, position, normal) BIND(C, name="traction")
        REAL(C_DOUBLE), INTENT(OUT) :: value(3)
        REAL(C_DOUBLE), INTENT(IN)  :: position(3), normal(3)

        call traction_fnptr(value, position, normal)
    END SUBROUTINE traction

END MODULE Magnetostriction
