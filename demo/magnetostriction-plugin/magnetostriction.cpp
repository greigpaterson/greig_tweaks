#include <iostream>
#include <cstring>
#include <functional>

#include "magnetostriction.h"
#include <dolfin.h>

extern "C" {

// Definitions in Magnetostriction.f90

void set_mechanical_matrix(
    const int* rows, std::size_t nrows,
    const int* cols, const double* data, std::size_t nze
);
void set_magnetic_matrix(
    const int* rows, std::size_t nrows,
    const int* cols, const double* data, std::size_t nze
);

void traction(double values[3], double position[3], double normal[3]);


// C to Fortran interfaces
void magnetostriction_build(
    double ms,
    double c11, double c12, double c44,
    double b1, double b2,
    double scale,
    int nnode, const double* vcl, int ntri, const int* til
);
void magnetostriction_build_mechanical_rhs(const double* m, double* rhs);
void magnetostriction_build_magnetic_rhs(
    const double* m, const double* u0, double* rhs
);
void magnetostriction_build_energy(
    const double* m, const double* u0, double* energy
);

} // extern "C"



namespace merrill {



//
// Dolfin, ie MPI and PETSc, initialization manager.
//
// Make sure this is called before any static dolfin types are declared
// to make sure dolfin's MPI/PETSc manager finalizes these after the static
// variables are destructed.
//
class DolfinManager {
public:
    typedef std::shared_ptr<void> Guard;

    static Guard guard() {
        initialize();

        static Guard guard(NULL, DolfinManager::guard_finalize);

        return guard;
    }

    static void initialize() {
        static bool first_entry = true;
        if(!first_entry) return;
        first_entry = false;
    }

    static void finalize() {
        static bool first_entry = true;
        if(!first_entry) return;
        first_entry = false;

        dolfin::SubSystemsManager::finalize();
    }

    // For compatibility with shared_ptr deleter signature
    static void guard_finalize(void*) {
        finalize();
    }


private:
    DolfinManager() {}
    ~DolfinManager() {}
};



//
// Generate a dolfin mesh from the merrill mesh
//
std::shared_ptr<const dolfin::Mesh> merrill_mesh_to_dolfin(
    int nnode, const double* vcl, int ntri, const int* til
) {

    dolfin::parameters["reorder_dofs_serial"] = false;
    std::shared_ptr<dolfin::Mesh> mesh = std::make_shared<dolfin::Mesh>();

    //
    // Get mesh points and connectivities from MERRILL and
    // build a dolfin mesh with them.
    //
    dolfin::MeshEditor mesh_editor;
    mesh_editor.open(*mesh, dolfin::CellType::tetrahedron, 3, 3);

    //int nnode = 0;
    //int ntri = 0;

    //get_nnode(&nnode);
    //get_ntri(&ntri);

    mesh_editor.init_vertices(nnode);
    mesh_editor.init_cells(ntri);

    // Vertices
    for(int i=0; i<nnode; i++) {
        mesh_editor.add_vertex(
            i, vcl[0*nnode+i], vcl[1*nnode+i], vcl[2*nnode+i]
        );
    }

    // Connectivities
    for(int i=0; i<ntri; i++) {
        mesh_editor.add_cell(
            i,
            til[0*ntri+i]-1, til[1*ntri+i]-1, til[2*ntri+i]-1, til[3*ntri+i]-1
        );
    }
    mesh_editor.close();


    //
    // Test function value ordering is the same as the vertex ordering
    // between MERRILL and Dolfin functions.
    //
    class XsExpr: public dolfin::Expression {
    public:
        XsExpr(): Expression(3) {}

        void eval(
            dolfin::Array<double>& vs, const dolfin::Array<double>& xs
        ) const {
            vs[0] = xs[0];
            vs[1] = xs[1];
            vs[2] = xs[2];
        }
    };

    magnetostriction::FunctionSpace V(
        std::const_pointer_cast<const dolfin::Mesh>(mesh)
    );
    dolfin::Function m(V);
    m.interpolate(XsExpr());

    std::vector<dolfin::la_index> vtd_map = dolfin::vertex_to_dof_map(V);

    for(std::size_t i=0; i<vtd_map.size()/3; i++) {
        if(
               vtd_map[3*i+0] != 0*nnode + i
            || vtd_map[3*i+1] != 1*nnode + i
            || vtd_map[3*i+2] != 2*nnode + i
        ) {
            throw std::runtime_error(
                "Dolfin indices reordered!"
            );
        }
    }

    for(int i=0; i<mesh->num_vertices(); i++) {
        double p[3] = {
            vcl[0*mesh->num_vertices()+i],
            vcl[1*mesh->num_vertices()+i],
            vcl[2*mesh->num_vertices()+i]
        };
        double mn[3];

        // Get Dolfin fuction values
        //std::vector<std::size_t> dofs(3*mesh->num_vertices());
        //dolfin::la_index ind[3] = {3*i+0, 3*i+1, 3*i+2};
        dolfin::la_index ind[3] = {
            vtd_map[3*i+0], vtd_map[3*i+1], vtd_map[3*i+2]
        };
        m.vector()->get_local(mn, 3, ind);

        double d2 =
            std::pow(p[0] - mn[0], 2)
            + std::pow(p[1] - mn[1], 2)
            + std::pow(p[2] - mn[2], 2);

        if(d2 > std::pow(1e-6,2)) {
            throw std::runtime_error(
                "Difference between Merrill and Dolfin representations"
                " is large!!"
            );
        }
    }

    return mesh;
}



class TractionWrap: public dolfin::Expression {
public:
    typedef dolfin::Array<double> Array;

    std::shared_ptr<const dolfin::Mesh> mesh;

    TractionWrap(std::shared_ptr<const dolfin::Mesh> mesh):
        dolfin::Expression(3),
        mesh(mesh)
    {}

    void eval(Array& values, const Array& x, const ufc::cell& ufc_cell) const {
        dolfin::Cell dolfin_cell(*mesh, ufc_cell.index);
        dolfin::Point normal = dolfin_cell.normal(ufc_cell.local_facet);

        double d_value[3];
        double d_position[3] = {x[0], x[1], x[2]};
        double d_normal[3]   = {normal[0], normal[1], normal[2]};

        // Call Fortran Magnetostriction::traction
        traction(d_value, d_position, d_normal);

        values[0] = d_value[0];
        values[1] = d_value[1];
        values[2] = d_value[2];
    }
};


// Stuff that needs to be set before Magnetostriction is even initialized
class Magnetostriction_Helper {
    DolfinManager::Guard dolfin_guard;
public:
    Magnetostriction_Helper() {
        dolfin::parameters["linear_algebra_backend"] = "Eigen";
    }
};


class Magnetostriction: private Magnetostriction_Helper {
public:

    typedef dolfin::EigenVector DolfinVector;
    typedef dolfin::EigenMatrix DolfinMatrix;
    typedef dolfin::EigenKrylovSolver KrylovSolver;

    std::shared_ptr<const dolfin::Mesh> mesh;
    std::shared_ptr<magnetostriction::FunctionSpace> V;

    std::shared_ptr<dolfin::Function> m;
    std::shared_ptr<dolfin::Function> u0;
    std::shared_ptr<dolfin::Function> h;

    std::shared_ptr<magnetostriction::Form_mechanical_L> form_mechanical_L;
    std::shared_ptr<DolfinVector> vector_mechanical_L;
    std::shared_ptr<DolfinMatrix> matrix_mechanical_a;

    std::shared_ptr<magnetostriction::Form_magnetic_L> form_magnetic_L;
    std::shared_ptr<DolfinVector> vector_magnetic_L;
    std::shared_ptr<DolfinMatrix> matrix_magnetic_a;

    std::shared_ptr<magnetostriction::Form_energy> form_energy;
    std::shared_ptr<dolfin::Scalar> scalar_energy;

    std::vector<dolfin::la_index> vtd_map;



    //
    // Build magnetostriction matrices
    //
    void build(
        double ms,
        double c11, double c12, double c44,
        double b1, double b2,
        double scale,
        int nnode, const double* vcl, int ntri, const int* til
    ) {
        namespace mstr = magnetostriction;

        std::cout
            << std::endl
            << " Initializing Magnetostriction module" << std::endl
            << " ------------------------------------" << std::endl;


        //
        // Set up the mesh
        //

        mesh = merrill_mesh_to_dolfin(nnode, vcl, ntri, til);


        //
        // Define Variables and Constants
        //

        V = std::make_shared<magnetostriction::FunctionSpace>(mesh);

        m  = std::make_shared<dolfin::Function>(V);
        u0 = std::make_shared<dolfin::Function>(V);
        h  = std::make_shared<dolfin::Function>(V);



        std::cout
            << std::endl
            << " - Magnetostriction parameters:"
            << std::endl
            << "     Ms:  " << ms << std::endl
            << "     c11: " << c11 << std::endl
            << "     c12: " << c12 << std::endl
            << "     c44: " << c44 << std::endl
            << "     b1:  " << b1  << std::endl
            << "     b2:  " << b2  << std::endl
            << "     scale: " << scale
            << std::endl
            << std::endl;

        auto c11_c = std::make_shared<dolfin::Constant>(c11);
        auto c12_c = std::make_shared<dolfin::Constant>(c12);
        auto c44_c = std::make_shared<dolfin::Constant>(c44);

        auto scale_c = std::make_shared<dolfin::Constant>(scale);

        auto b_c = std::make_shared<dolfin::Constant>(b1, b2);

        auto e1_c = std::make_shared<dolfin::Constant>(1.0, 0.0, 0.0);
        auto e2_c = std::make_shared<dolfin::Constant>(0.0, 1.0, 0.0);
        auto e3_c = std::make_shared<dolfin::Constant>(0.0, 0.0, 1.0);

        auto Ms_c = std::make_shared<dolfin::Constant>(ms);

        //auto t = std::make_shared<Pressure>(mesh, pressure_d);
        auto t = std::make_shared<TractionWrap>(mesh);


        //
        // Set up forms
        //

        mstr::Form_mechanical_a form_mechanical_a(V,V);

        form_mechanical_a.set_coefficient("scale", scale_c);
        form_mechanical_a.set_coefficient("c11", c11_c);
        form_mechanical_a.set_coefficient("c12", c12_c);
        form_mechanical_a.set_coefficient("c44", c44_c);

        mstr::Form_mechanical_a_perturb form_mechanical_a_perturb(V,V);


        form_mechanical_L = std::make_shared<mstr::Form_mechanical_L>(V);

        form_mechanical_L->set_coefficient("scale", scale_c);
        form_mechanical_L->set_coefficient("m",  m);
        form_mechanical_L->set_coefficient("b",  b_c);
        form_mechanical_L->set_coefficient("e1", e1_c);
        form_mechanical_L->set_coefficient("e2", e2_c);
        form_mechanical_L->set_coefficient("e3", e3_c);
        form_mechanical_L->set_coefficient("t", t);


        mstr::Form_magnetic_a form_magnetic_a(V,V);

        form_magnetic_a.set_coefficient("scale", scale_c);


        form_magnetic_L = std::make_shared<mstr::Form_magnetic_L>(V);

        form_magnetic_L->set_coefficient("scale", scale_c);
        form_magnetic_L->set_coefficient("m",  m);
        form_magnetic_L->set_coefficient("u0", u0);
        form_magnetic_L->set_coefficient("b",  b_c);
        form_magnetic_L->set_coefficient("Ms", Ms_c);
        form_magnetic_L->set_coefficient("e1", e1_c);
        form_magnetic_L->set_coefficient("e2", e2_c);
        form_magnetic_L->set_coefficient("e3", e3_c);


        form_energy = std::make_shared<mstr::Form_energy>(mesh);

        form_energy->set_coefficient("scale", scale_c);
        form_energy->set_coefficient("m",  m);
        form_energy->set_coefficient("u0", u0);
        form_energy->set_coefficient("b",  b_c);
        form_energy->set_coefficient("c11", c11_c);
        form_energy->set_coefficient("c12", c12_c);
        form_energy->set_coefficient("c44", c44_c);
        form_energy->set_coefficient("e1", e1_c);
        form_energy->set_coefficient("e2", e2_c);
        form_energy->set_coefficient("e3", e3_c);


        //
        // Assemble the matrices
        //
        matrix_mechanical_a = std::make_shared<DolfinMatrix>();
        auto matrix_mechanical_a_perturb = std::make_shared<DolfinMatrix>();
        vector_mechanical_L = std::make_shared<DolfinVector>();
        matrix_magnetic_a = std::make_shared<DolfinMatrix>();
        vector_magnetic_L = std::make_shared<DolfinVector>();
        scalar_energy = std::make_shared<dolfin::Scalar>();

        dolfin::assemble(*matrix_mechanical_a, form_mechanical_a);
        dolfin::assemble(
            *matrix_mechanical_a_perturb, form_mechanical_a_perturb
        );
        dolfin::assemble(*matrix_magnetic_a,   form_magnetic_a);

        matrix_mechanical_a->init_vector(*vector_mechanical_L, 0);
        matrix_magnetic_a->init_vector(*vector_magnetic_L, 0);

        std::cout
            << " - FEM Matrices:" << std::endl
            << "     Mechanical Equilibrium:     [ "
                << matrix_mechanical_a->size(0)
                << " X "
                << matrix_mechanical_a->size(1)
            << " ]"
            << std::endl
            << "     Effective Field Projection: [ "
                << matrix_magnetic_a->size(0)
                << " X "
                << matrix_magnetic_a->size(1)
            << " ]"
            << std::endl
            << std::endl;

        double a_max = matrix_mechanical_a->norm("frobenius");
        double a_perturb_max = matrix_mechanical_a_perturb->norm("frobenius");
        double perturb_scale = a_max/a_perturb_max * 1e-10;
        matrix_mechanical_a->axpy(
            perturb_scale, *matrix_mechanical_a_perturb, true
        );


        // Generate the vertex to dof and dof to vertex maps
        vtd_map = dolfin::vertex_to_dof_map(*V);


        // Pass matrices back to Fortran
        typedef
            std::tuple<const int*, const int*, const double*, std::size_t>
            Data;
        Data data;
        
        data = matrix_mechanical_a->data();
        ::set_mechanical_matrix(
            std::get<0>(data), matrix_mechanical_a->size(0),
            std::get<1>(data), std::get<2>(data), std::get<3>(data)
        );

        data = matrix_magnetic_a->data();
        ::set_magnetic_matrix(
            std::get<0>(data), matrix_magnetic_a->size(0),
            std::get<1>(data), std::get<2>(data), std::get<3>(data)
        );
    }

    void build_mechanical_rhs(const double* m, double* rhs) {
        for(int i=0; i<this->m->vector()->size(); i++) {
            this->m->vector()->set_local(&m[i], 1, &i);
        }
        this->m->vector()->apply("insert");

        dolfin::assemble(*vector_mechanical_L, *form_mechanical_L);

        for(int i=0; i<vector_mechanical_L->size(); i++) {
            vector_mechanical_L->get_local(&rhs[i], 1, &i);
        }
    }

    void build_magnetic_rhs(const double* m, const double* u0, double* rhs) {
        for(int i=0; i<this->m->vector()->size(); i++) {
            this->m->vector()->set_local(&m[i], 1, &i);
            this->u0->vector()->set_local(&u0[i], 1, &i);
        }

        dolfin::assemble(*vector_magnetic_L, *form_magnetic_L);

        for(int i=0; i<vector_magnetic_L->size(); i++) {
            vector_magnetic_L->get_local(&rhs[i], 1, &i);
        }
    }

    void build_energy(const double* m, const double* u0, double* energy) {
        for(int i=0; i<this->m->vector()->size(); i++) {
            this->m->vector()->set_local(&m[i], 1, &i);
            this->u0->vector()->set_local(&u0[i], 1, &i);
        }

        *energy = dolfin::assemble(*form_energy);
    }
};

Magnetostriction magnetostriction;

} // namespace merrill


void magnetostriction_build(
    double ms,
    double c11, double c12, double c44,
    double b1, double b2,
    double scale,
    int nnode, const double* vcl, int ntri, const int* til
) {
    merrill::magnetostriction.build(
        ms, c11, c12, c44, b1, b2, scale,
        nnode, vcl, ntri, til
    );
}

void magnetostriction_build_mechanical_rhs(const double* m, double* rhs) {
    merrill::magnetostriction.build_mechanical_rhs(m, rhs);
}

void magnetostriction_build_magnetic_rhs(
    const double* m, const double* u0, double* rhs
) {
    merrill::magnetostriction.build_magnetic_rhs(m, u0, rhs);
}

void magnetostriction_build_energy(
    const double* m, const double* u0, double* energy
) {
    merrill::magnetostriction.build_energy(m, u0, energy);
}
