include(ExternalProject)

find_package(PythonInterp REQUIRED)


set(
    UFL_URL "https://bitbucket.org/fenics-project/ufl/downloads/ufl-1.6.0.tar.gz" CACHE STRING
    "The URL to get the ufl tarball."
)
set(
    UFL_MD5 "c40c5f04eaa847377ab2323122284016" CACHE STRING
    "MD5 hash of the ufl tarball"
)


set(UFL_PREFIX       "${CMAKE_BINARY_DIR}/ufl"   CACHE PATH "")
set(UFL_DOWNLOAD_DIR "${UFL_PREFIX}/src"         CACHE PATH "")
set(UFL_BUILD_DIR    "${UFL_PREFIX}/build/build" CACHE PATH "")
set(UFL_SOURCE_DIR   "${UFL_PREFIX}/build/src"   CACHE PATH "")
set(UFL_TMP_DIR      "${UFL_PREFIX}/build/tmp"   CACHE PATH "")
set(UFL_INSTALL_DIR  "${UFL_PREFIX}"             CACHE PATH "")

set(UFL_PYTHON_INSTALL_DIR "${UFL_INSTALL_DIR}/lib/python" CACHE PATH "")


ExternalProject_Add(
    ufl

    DEPENDS six

    PREFIX ${UFL_PREFIX}

    SOURCE_DIR ${UFL_SOURCE_DIR}
    BINARY_DIR ${UFL_BUILD_DIR}
    INSTALL_DIR ${UFL_INSTALL_DIR}

    STAMP_DIR ${UFL_BUILD_DIR}
    TMP_DIR   ${UFL_TMP_DIR}

    URL ${UFL_URL}
    URL_MD5 ${UFL_MD5}
    DOWNLOAD_DIR ${UFL_DOWNLOAD_DIR}

    CONFIGURE_COMMAND
        ${CMAKE_COMMAND} -E make_directory ${UFL_BUILD_DIR}
    COMMAND
        ${CMAKE_COMMAND} -E copy_directory ${UFL_SOURCE_DIR} ${UFL_BUILD_DIR}

    BUILD_COMMAND
        ${CMAKE_COMMAND} -E chdir ${UFL_BUILD_DIR}
            ${PYTHON_EXECUTABLE} setup.py build
    INSTALL_COMMAND
        ${CMAKE_COMMAND} -E chdir ${UFL_BUILD_DIR}
            ${PYTHON_EXECUTABLE} setup.py install
                "--prefix=${UFL_INSTALL_DIR}"
                "--install-lib=${UFL_PYTHON_INSTALL_DIR}"
)
